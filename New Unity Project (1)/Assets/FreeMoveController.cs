﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreeMoveController : MonoBehaviour
{

    public float speed = 5.0f; // Contols the speed of the character.  and because public can be edited in unity


    private Vector3 input; // Tracks the pressed inputs as numbers
    private Vector3 motion; // Tracks the movement step per frame.
    private CharacterController controller;

    // awake called before start
    private void Awake()
    {
        //Make refference to required character controller component.

        controller = GetComponent<CharacterController>();  //because no object specified it will use the one the script is attached to.
    }

    // Update is called once per frame
    void Update()
    {
        motion = Vector3.zero;
        input = new Vector3(Input.GetAxis("Horizontal"),Input.GetAxis("Depth"),Input.GetAxis("Vertical"));
        motion += transform.forward.normalized * input.z;
        motion += transform.right.normalized * input.x;
        motion += Vector3.up.normalized * input.y;
        controller.Move(motion * speed * Time.deltaTime);


    }
}
