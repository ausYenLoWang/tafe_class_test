using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Health : MonoBehaviour
{
    public delegate void DamageDelegate(); //definition of a delegate Event type

    public float maxHealth = 100f;
    public Image healthBar;
    public event DamageDelegate damageDelegate = delegate { }; //Variable instance of delegate type

    public float CurrentHealth { get; private set; }



    // Start is called before the first frame update
    void Start()
    {
        CurrentHealth = maxHealth;
        healthBar.fillAmount = CurrentHealth / maxHealth;
        damageDelegate = new DamageDelegate(OnDeath);  //assign first method to delegate
        damageDelegate += SomeOtherMethod;  //MultiCast the delegate 


    }

    public bool OnDamage(float amount)
    {
        if(CurrentHealth > 0)
        {
            CurrentHealth -= amount;
            if(CurrentHealth <= 0)
            {
                CurrentHealth = 0;
                OnDeath();
            }
            damageDelegate.Invoke();
            healthBar.fillAmount = CurrentHealth / maxHealth;
            return true;
        }
        return false;
    }

    private void SomeOtherMethod()
    {
        Debug.Log("Some other method has been called");
    }

    private void OnDeath()
    {
        Debug.Log("Player has died");
            
    }


 
}
