using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLook : MonoBehaviour
{
    public float sensitivity = 2.5f;
    public float drag = 1.5f;
    public bool lookEnabled = true;

    private Transform character;
    private Vector2 mouseDir;
    private Vector2 smoothing;
    private Vector2 result;


    public bool CursorToggle
    {

        set
        {
            if (value == true)
           
            {
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;

            }

            else
            {
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Locked;

            }
        }
    }


    private void Awake()
    {
        character = transform.parent;
        CursorToggle = false;
    
    }



    // Update is called once per frame
    private void Update()
    {
        if (lookEnabled == true)
        {
            mouseDir = new Vector2(Input.GetAxisRaw("Mouse X") * sensitivity, Input.GetAxisRaw("Mouse Y") * sensitivity);
            smoothing = Vector2.Lerp(smoothing, mouseDir, 1 / drag);
            result += smoothing;
            result.y = Mathf.Clamp(result.y, -80, 80);

            transform.localRotation = Quaternion.AngleAxis(-result.y, Vector3.right);
            character.rotation = Quaternion.AngleAxis(result.x, character.transform.up);
        }

        if (Input.GetKeyDown(KeyCode.Comma) == true)
        {
            CursorToggle = true;
        }
        else if (Input.GetKeyDown(KeyCode.Period) == true)
        {
            CursorToggle = false;
        }
    }

   
}
