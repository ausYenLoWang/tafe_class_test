message = input("input your termperature here: ")    #gets the user to input their temperature
temp = float(message)    #converts that temperature to a float, most temps are not in WHOLE numbers but also have decimal values so an int is no good
if(temp < 37):
    print(message + " degrees Celcius is too cold you need to warm up!")  #these 2 lines are to set a minimum value would also give information ADDITIONAL to client brief and from here everything ABOVE 37 deg would trigger the further responses.
elif(temp >= 37 and temp < 38 ):
    print("Normal Body Temperature")    #temps between 37 and 38 are the normal body temp, each consecutive line adds a new temp bracket and prints what level of fever it would be related to
elif(temp >=38 and temp < 39):
    print(message + " degrees Celcius Is a Fever")
elif(temp >= 39 and temp < 40):
    print(message + " degrees Celcius Is a High Fever")
elif(temp >=40 and temp < 41):
    print(message + " degrees Celcius Is A Very High Fever")
elif(temp >= 41):
    print(message + " degrees Celcius Is A Serious Emergency")