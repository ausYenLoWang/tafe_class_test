#the below is the text that will be added to the text file
text = "On August 18th, 2005, artist Erwin Beekveld submitted a video titled Theyre Taking The Hobbits To Isengard to the flash website Albino Blacksheep[1] (shown below). The video featured scenes from the second Lord of the Rings film accompanied by a music track made from the line Theyre taking the hobbits to Isengard! uttered by the character Legolas (played by Orlando Bloom)."
file = open("news.txt", "w")  #this line will create or open the text file with the intention of writing to it
file.write(text)  #this line here commands it to write the text to the file
file.close()  #this line is to close the text file
