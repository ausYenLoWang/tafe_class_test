def search(fruitlist, fruit):  #this is the function
    for line in fruitlist:  #for loop that access the list 
        if(fruit == line):  #compares the user input fruit to the list
            index = fruitlist.index(fruit)
            return index 
    return -1 


fruit = input("please input your choice of fruit here: ")  #asking the user to input a fruit type to be checked against the list
fruitlist = ["mango", "watermelon", "apple", "orange", "grape", "banana"]  #the list of fruit that needs to be checked against
returnmessage = search(fruitlist, fruit)  #calls the function and changes the return variable to return message
if returnmessage != -1:  #Elif used to  decide if the fruit was found in the list or not based on if it has an index value of 0 or greater and prints the result
    print(returnmessage)
elif returnmessage == -1:
    print(fruit + " not found")

