


output_string = "Python has been an important part of Google since the beginning, and remains so as the system grows and evolves. " + "\"Today dozens of Google engineers use Python, and we're looking for more people with skills in this language.\" said Peter Norvig, director of search quality at Google, Inc."
file = open("Python.txt", "w")  #this line will create or open the text file with the intention of writing to it
file.write(output_string)  #this line here commands it to write the text to the file
file.close()  #this line is to close the text file



file = open("Python.txt", "r")  #this line is using the fole open command and directs it to open the Python.txt file, as well as commands it to Read it.
for line in file:  #the for loop creating the variable of "line" 
    print(line)  #prints the variable from the loop 
file.close()  #closes the text file after printing it.
