def search(os_type, operating):  #this is the function
    for line in os_type:  #for loop that access the list 
        if(operating == line):  #compares the user input of Operating System to the Tuple library
            index = os_type.index(operating)
            return index #tells you the index position of where the OS is in the tuple
    return -1 #if the OS isnt found in the tuple it will return a result of -1

operating = input("please input your operating system here: ")  #asking the user to input their operating system to be checked against the tuple
os_type = ("Windows 10", "Linux Mint", "Mac OS 11", "Android Oreo", "Android Pie", "Android 11", "iOS 14")  #the tuple of operating systems that needs to be checked against
return_message = search(os_type, operating)  #calls the function and changes the return variable to return message
if return_message != -1:  #Elif used to  decide if the Operating System was found in the list or not based on if it has an index value of 0 or greater and prints the result
    message = str(return_message)
    print(operating + " was found in position " + message) #prints the inputed OS's position in the Tuple telling the user what they inputed and where it was found
    print(type(os_type))  #checks the type of library that the data is stored in and prints it._telif return_message == -1:
    print(operating + " not found")
