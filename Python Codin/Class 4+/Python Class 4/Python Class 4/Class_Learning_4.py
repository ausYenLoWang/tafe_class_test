#Example of writing text to a text file
text = "Line 1\nLine 2\nLine 3\nLine 4\nLine 5"
file = open("test.txt", "w")
file.write(text)
file.close()

#example of Reading from a text file.
file = open("test.txt", "r")
for line in file:
    print(line)
file.close()
#Method for writing content to a text file
def write_to_file(name, content):
    file = open(name + ".txt", "w")
    file.write(content)
    file.close()
    #method for reading a text filr
def read_file(name):
    file = open(name + ".txt", "r")
    for line in file:
        print(line)
    file.close()
#Method for searching the file for target content
def linear_file_search(name, target):
    file = open(name + ".txt", "r")
    for line in file:
        if(line.find(str(target)) is not -1):
            return target + " has been found."
    file.close()
    return target + " was not found in the file."

write_to_file("test", "Line 1\nLine 2\nLine 3\nLine 4\nLine 5")
read_file("test")
write_to_file("new file", "some other boring stuff. \nWOOOOOOO")
read_file("new file")
result = linear_file_search("test", "Line 1")
print(result)

