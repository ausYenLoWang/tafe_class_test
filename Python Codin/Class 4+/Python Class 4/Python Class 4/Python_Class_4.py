
import random  #imports a randomnumber module


def check_range(num, min, max):  #check range function
    try:
        if(int(num)):
            if(num >= min and num <= max):
                return True
            print("Integer is out of defined range: ")
            return False
    except:
        print("Cannot convert string to integer: ")
        return False


print("0. Rock")
print("1. Paper")
print("2. Scissors")


user_selection = input("Enter a number from 0 to 2: ")
while(not user_selection.isnumeric()):
    user_selection = input("Enter a number from 0 to 2: ")
user_selection = int(user_selection)
while(check_range(user_selection, 0, 2) == False):
    user_selection = input("Enter a number from 0 to 2: ")

if(user_selection == 0):
    print("Player selected 'Rock'")
elif(user_selection == 1):
    print("Player selected 'Paper'")
elif(user_selection == 2):
    print("Player selected 'Scissors'")

computer_selection = random.randrange(0, 3)
print(computer_selection)
if(computer_selection == 0):
    print("Player selected 'Rock'")
elif(computer_selection == 1):
    print("Player selected 'Paper'")
elif(computer_selection == 2):
    print("Player selected 'Scissors'")