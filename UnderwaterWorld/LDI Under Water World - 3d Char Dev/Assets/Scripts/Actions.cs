using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Actions : MonoBehaviour
{
    public float boostMultiplier = 5;
    public float boostTime = 3;
    public float sonarTime = 3; 
    public float sonarRadius = 15;
    public int iconLimit = 5;
    public LayerMask interactionLayer;
    public GameObject sonarIconPrefab;

    public static Actions Instance { get; private set; }

    private float sonarTimer = -1;
    private float boostTimer = -1;
    private List<GameObject> spawnedSonarIcons = new List<GameObject>();

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            enabled = false;
        }
    }


    // Start is called before the first frame update
    void Start()
    {
        for(int i = 0; i < iconLimit; i++)
        {
            spawnedSonarIcons.Add(Instantiate(sonarIconPrefab, ActionMenu.Instance.transform.root, false));
            spawnedSonarIcons[i].transform.position = Vector3.zero;
            spawnedSonarIcons[i].transform.SetAsFirstSibling();
            spawnedSonarIcons[i].SetActive(false);
        }
        Map(false);
    }

    // Update is called once per frame
    void Update()
    {
        //if the world map is not active
        if (WorldMap.Instance.legend.activeSelf == false) 
        {
            if (ActionMenu.Instance.gameObject.activeSelf == false)
            {
                if (DmInteractionMenu.Instance.gameObject.activeSelf == false)
                {
                    if (Input.GetButtonDown("Action") == true)
                    {
                        ActionMenu.Instance.Activate();
                    }
                }
            }
            else
            {
                if (Input.GetButtonUp("Action") == true)
                {
                    int selection = ActionMenu.Instance.Deactivate();
                    switch (selection)
                    {
                        case 0:
                            Boost(true);
                            break;
                        case 1:
                            Sonar(true);
                            break;
                        case 2:
                            Map(true);
                            break;
                    }
                }
            }
        }
        else if(Input.GetButtonDown("Action") == true)
        {
            Map(false);
        }
        //else if get button down "action"
            //Turn off map

        //Boost Timer
        if(boostTimer > -1)
        {
            boostTimer += Time.deltaTime;
            if(boostTimer > boostTime)
            {
                Boost(false); // deactivate boost
            }
        }

        if(sonarTimer > -1)  //sonar timer
        {
            Collider[] closeInteractions = Physics.OverlapSphere(transform.position, sonarRadius, interactionLayer);
            int interactionCount = closeInteractions.Length;
            for(int i = 0; i < iconLimit; i++)
            {
                if(i < interactionCount)
                {
                    Vector3 screenPos = Camera.main.WorldToScreenPoint(closeInteractions[i].transform.position); //Z value becomes Depth
                    if(screenPos.z > 0) //if icon is visible on screen (negative Z means object is not visible on screen)
                    {
                        spawnedSonarIcons[i].transform.position = screenPos;
                        if(spawnedSonarIcons[i].activeSelf == false)
                        {
                            spawnedSonarIcons[i].SetActive(true);
                        }
                    }
                    else if(spawnedSonarIcons[i].activeSelf == true)  //if icon is active and not on screen
                    {
                        spawnedSonarIcons[i].SetActive(false);
                    }
                }
                else if (spawnedSonarIcons[i].activeSelf == true) //if icon is active but no corresponding interaction
                {
                    spawnedSonarIcons[i].SetActive(false);  //turns off icon
                }
                {

                }
            }
            sonarTimer += Time.deltaTime;
            if(sonarTimer > sonarTime)
            {
                Sonar(false);
            }
        }
    }

    void Boost(bool toggle) //boost functionality
    {
        if(toggle == true)  //turn on functionality
        {
            if(boostTimer == -1)
            {
                boostTimer = 0;
                PlayerController.Instance.speed *= boostMultiplier;
            }
        }
        else  //turn off functionality
        {
            if (boostTimer == -1)
            {
                boostTimer = -1;
                PlayerController.Instance.speed /= boostMultiplier;
            }
               
        }
    }

    void Sonar(bool toggle)  //sonar functionality
    {
        if(toggle == true) //activate
        {
            if(sonarTimer == -1)
            {
                sonarTimer = 0;
            }
        }
        else //deactivate
        {
            sonarTimer = -1;
            if(spawnedSonarIcons.Count > 0)
            {
                foreach (GameObject go in spawnedSonarIcons)
                {
                    go.SetActive(false);
                }
            }

        }
    }

    void Map(bool toggle)  //map functionality
    { 
        if(toggle == true)
        {
            PlayerController.Instance.enabled = false;  //turn off player movement
            MouseLook.Instance.enabled = false;  //turn off mouselook
            Interaction.Instance.enabled = false;  //turn off interaction
            Sonar(false);  //deactivate sonar
            Boost(false);  //deactivate boost
            WorldMap.Instance.Activate();  //activate map
        }
        else
        {
            PlayerController.Instance.enabled = true;  //turn on player movement
            MouseLook.Instance.enabled = true;  //turn on mouselook
            Interaction.Instance.enabled = true;  //turn on interaction
            WorldMap.Instance.Deactivate();  //deactivate map
        }

    }
}
