using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DmFlockAgent : Fish
{
    public float viewAngle;
    public float smoothDamp;
    public LayerMask obstacleMask;
    public Vector3[] collisionRays;

    private float speed;
    private Vector3 velocity;
    private Vector3 currentAvoidance;
    private DmFlockManager flockInstance;
    private List<DmFlockAgent> cohesionNeighbours = new List<DmFlockAgent>();
    private List<DmFlockAgent> seperationNeighbours = new List<DmFlockAgent>();
    private List<DmFlockAgent> alignmentNeighbours = new List<DmFlockAgent>();

    /// <summary>
    /// Initializes the agent with the passed flock and speed
    /// </summary>
    /// <param name="manager"></param>
    /// <param name="initSpeed"></param>
    public void Initialize(DmFlockManager manager, float initSpeed)
    {
        flockInstance = manager;
        speed = initSpeed;
    }

    /// <summary>
    /// This method calculates and applies movement for the agent on a frame by frame basis
    /// </summary>
    public void Move()
    {
        FindNeighbors();
        CalculateSpeed();

        Vector3 cohesion = Cohese() * flockInstance.cohesion;
        Vector3 separation = Separate() * flockInstance.separation;
        Vector3 alignment = Align() * flockInstance.alignment;
        Vector3 bounds = Bounds() * flockInstance.bounds;
        Vector3 avoidance = ObstacleAvoidance() * flockInstance.avoidance;



        Vector3 motion = cohesion + separation + alignment + bounds + avoidance;
        motion = Vector3.SmoothDamp(transform.forward, motion, ref velocity, smoothDamp);
        motion = motion.normalized * speed;
        if(motion == Vector3.zero)
        {
            motion = transform.forward;
        }
        transform.forward = motion; // redirect orientation
        transform.position += motion * Time.deltaTime; // move position
    }

    /// <summary>
    /// Gets the Neighbors for cohesion, allignement and separation
    /// </summary>
    private void FindNeighbors()
    {
        cohesionNeighbours.Clear();
        seperationNeighbours.Clear();
        alignmentNeighbours.Clear();
        for(int i = 0; i < flockInstance.SpawnedAgents.Length; i++)
        {
            DmFlockAgent agent = flockInstance.SpawnedAgents[i];
            if(agent != this)
            {
                float distanceSqr = Vector3.SqrMagnitude(agent.transform.position - transform.position);
                if(distanceSqr <= flockInstance.cohesionDistance * flockInstance.cohesionDistance)
                {
                    cohesionNeighbours.Add(agent);
                }
                if (distanceSqr <= flockInstance.cohesionDistance * flockInstance.cohesionDistance)
                {
                    seperationNeighbours.Add(agent);
                }
                if (distanceSqr <= flockInstance.cohesionDistance * flockInstance.cohesionDistance)
                {
                    alignmentNeighbours.Add(agent);
                }
            }
        }
    }

    /// <summary>
    /// Calculates the agents average speed based on neighbors
    /// </summary>
    private void CalculateSpeed()
    {
        if(cohesionNeighbours.Count == 0)
        {
            return;
        }
        speed = 0;
        for(int i = 0; i < cohesionNeighbours.Count; i++)
        {
            speed += cohesionNeighbours[i].speed;
        }
        speed /= cohesionNeighbours.Count;
        speed = Mathf.Clamp(speed, flockInstance.speed.x, flockInstance.speed.y);

    }

    /// <summary>
    /// generates the cohesion vector on a frame by frame basis
    /// </summary>
    /// <returns></returns>
    private Vector3 Cohese()
    {
        Vector3 vector = Vector3.zero;
        int neighboursInView = 0;
        if(cohesionNeighbours.Count == 0)
        {
            return vector;
        }
        for(int i = 0; i < cohesionNeighbours.Count; i++)
        {
            //if neighbour is in view
            if(VectorInView(cohesionNeighbours[i].transform.position) == true)
            {
                neighboursInView++;
                vector += cohesionNeighbours[i].transform.position;

            }
        }
        if(neighboursInView == 0)
        {
            return vector;
        }
        vector /= neighboursInView;
        vector -= transform.position;
        return vector.normalized;
    }


  /// <summary>
  /// Generates the separation vector on a frame by frame basis.
  /// </summary>
  /// <returns></returns>
    private Vector3 Separate()
    {
        Vector3 vector = transform.forward;
        if(seperationNeighbours.Count == 0)
        {
            return vector;
        }
        int neighboursinview = 0;
        for(int i = 0; i < seperationNeighbours.Count; i++)
        {
            if(VectorInView(seperationNeighbours[i].transform.position) == true)
            {
                neighboursinview ++;
                vector += transform.position - seperationNeighbours[i].transform.position;
            }
        }
        vector /= neighboursinview;
        return vector.normalized;
    }

    /// <summary>
    /// Generates the allignments vector on a frame by frame basis
    /// </summary>
    /// <returns></returns>
    private Vector3 Align()
    {
        Vector3 vector = transform.forward;
        if(alignmentNeighbours.Count == 0)
        {
            return vector;
        }
        int neighboursinview = 0;
        for(int i = 0; i < alignmentNeighbours.Count; i++)
        {
            if(VectorInView(alignmentNeighbours[i].transform.position) == true)
            {
                neighboursinview ++;
                vector += alignmentNeighbours[i].transform.position;
            }
        }
        vector /= neighboursinview;
        return vector.normalized;
    }

/// <summary>
/// Generates a vector that keeps the agent close to the flock object
/// </summary>
/// <returns></returns>
    private Vector3 Bounds()
    {
        Vector3 centerOffset = flockInstance.transform.position - transform.position;
        bool withinBounds = (centerOffset.magnitude >= flockInstance.boundsDistance * 0.9);
        if(withinBounds == true)
        {
            return centerOffset.normalized;
        }
        return Vector3.zero;
    }

    /// <summary>
    /// generates an obstacle avoidance vector on a frame by frame basis
    /// </summary>
    /// <returns></returns>
    private Vector3 ObstacleAvoidance()
    {
        Vector3 vector = Vector3.zero;
        if(Physics.Raycast(transform.position, transform.forward, out RaycastHit hit, flockInstance.obstacleDistance, obstacleMask) == true)
        {
            vector = AvoidObstacle();

        }
        else
        {
            currentAvoidance = Vector3.zero;
        }
        return vector;
    }
    
    /// <summary>
    /// Checks all collision rays for collisions.
    /// returns the most suitable direction to move in
    ///
    /// </summary>
    /// <returns></returns>
    private Vector3 AvoidObstacle()
    {
        if(currentAvoidance != Vector3.zero)
        {
            if(Physics.Raycast(transform.position, transform.forward, out RaycastHit hit, flockInstance.obstacleDistance, obstacleMask) == false)
            {
                return currentAvoidance;
            }
        }
        float maxDist = int.MinValue;
        Vector3 dir = Vector3.zero;
        for(int i = 0; i < collisionRays.Length; i++)
        {
            Vector3 currentDir = transform.TransformDirection(collisionRays[i].normalized);
            if(Physics.Raycast(transform.position, currentDir, out RaycastHit hit, flockInstance.obstacleDistance, obstacleMask) == true)
            {
                float sqrDist = (hit.point - transform.position).sqrMagnitude;
                if(sqrDist > maxDist)
                {
                    maxDist = sqrDist;
                    dir = currentDir;
                }
            }
            else
            {
                dir = currentDir;
                currentAvoidance = currentDir.normalized;
                return dir.normalized;
                 
            }
        }
        return dir.normalized;
    }

    /// <summary>
    /// Returns true if the given position is within agents view radius
    /// </summary>
    /// <param name="position"></param>
    /// <returns></returns>
    private bool VectorInView(Vector3 position)
    {
        return Vector3.Angle(transform.forward, position - transform.position) <= viewAngle;
    }


}
