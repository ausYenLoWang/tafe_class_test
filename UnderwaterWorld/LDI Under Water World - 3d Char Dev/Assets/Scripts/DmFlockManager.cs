using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DmFlockManager : MonoBehaviour


{
    [Header("Flock Agent Setup")]
    public DmFlockAgent agentPrefab;
    public int size;
    public Vector3 spawnBounds;
    public Vector2 speed;

    [Header("Distance Settings")]
    [Range(0, 10)] public float cohesionDistance;
    [Range(0, 10)] public float separationDistance;
    [Range(0, 10)] public float alignmentDistance;
    [Range(0, 10)] public float boundsDistance;
    [Range(0, 10)] public float obstacleDistance;

    [Header("Weights")]
    [Range(0, 10)] public float cohesion;
    [Range(0, 10)] public float separation;
    [Range(0, 10)] public float alignment;
    [Range(0, 10)] public float bounds;
    [Range(0, 100)] public float avoidance;

    public DmFlockAgent[] SpawnedAgents { get; private set; }


    /// <summary>
    /// Spawns the flock agents on the first frame of the game.
    /// </summary>
    void Start()
    {
        SpawnAgents(); 
    }

   /// <summary>
   /// updates each flock agent and applies thier movement.
   /// </summary>
    void Update()
    {
        //move each agent
        for (int i = 0; i < SpawnedAgents.Length; i++)
        {
            SpawnedAgents[i].Move();
        }


    }
    /// <summary>
    /// This method is used to spawn the flock agents into the world
    /// </summary>
    void SpawnAgents()
    {
        SpawnedAgents = new DmFlockAgent[size];
        for (int i = 0; i < size; i++)
        {
            Vector3 random = Random.insideUnitSphere;
            random = new Vector3(random.x * spawnBounds.x, random.y * spawnBounds.y, random.z * spawnBounds.z);
            Vector3 spawnPos = transform.position + random;
            Quaternion rot = Quaternion.Euler(0, Random.Range(0, 360), 0);
            SpawnedAgents[i] = Instantiate(agentPrefab, spawnPos, rot);
            //initialize agents
            SpawnedAgents[i].Initialize(this, Random.Range(speed.x, speed.y));
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(transform.position, spawnBounds);
    }

}
