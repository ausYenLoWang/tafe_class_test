using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Fish : MonoBehaviour, IFauna
{
    [TextArea]
    public string tooltipText;


    public string Inspect()
    {
        return tooltipText;
    }

    public void Photograph(string folderPath = null)  //takes the picture and sets the format for the file name.
    {
        if(folderPath != null) 
        {
            string date = System.DateTime.Now.Day.ToString() + System.DateTime.Now.Month.ToString() + System.DateTime.Now.TimeOfDay.ToString().Replace(':', '-');
            ScreenCapture.CaptureScreenshot(folderPath + $"/UWW_{date}.png", 2);
        }

        
        if(GameManager.Instance.TargetFound == false && (Object)GameManager.Target == this) // if this is target fish, and target fish has notbeen found
        {
            GameManager.Instance.TargetFound = true; // target has been found = true
        }
            

    }

}
