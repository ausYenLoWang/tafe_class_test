using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Key : MonoBehaviour, IKey
{
    public enum KeyType { a, b}

    public KeyType type;
    [TextArea]  // increases text area
    public string tooltipText;


    
    void Start()
    {
        WorldMap.Instance.AddInteraction(gameObject);
    }

    public void Pickup()
    {
        // if the key is succsessfully picked up
        if(Interaction.Instance.Addkey(type) == true)
        {
            gameObject.SetActive(false);
        }
    }

    public string Inspect()
    {
        return tooltipText;
    }
}
