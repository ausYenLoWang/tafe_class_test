using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public GameObject galleryPanel;
    public GameObject infoPanel;

    private Dropdown galleryDropdown;
    private RawImage galleryDisplay;
    private List<Texture2D> screenshots = new List<Texture2D>();

    private void Awake()
    {
        galleryDropdown = galleryPanel.GetComponentInChildren<Dropdown>();
        galleryDisplay = galleryPanel.GetComponentInChildren<RawImage>();
        galleryDropdown.ClearOptions();
        #region Confirm Gallery Folder Exists
        //check for gallery folder, create if it does not exist
        string galleryFolder = Application.dataPath + "/Resources/Photogallery";
        if (Directory.Exists(galleryFolder) == false)
        {
            Directory.CreateDirectory(galleryFolder);
        }
        #endregion

        DirectoryInfo dir = new DirectoryInfo(galleryFolder);
        FileInfo[] ScreenshotInfos = dir.GetFiles("UWW_?*.png");
        if(ScreenshotInfos.Length > 0)
        {
            foreach (FileInfo info in ScreenshotInfos)
            {
                //add a drop down option for this file
                galleryDropdown.options.Add(new Dropdown.OptionData(info.Name));
                //create a new texture for the screenshot
                Texture2D texture = new Texture2D(960, 540);
                //load hte image from the file as a texture
                texture.LoadImage(File.ReadAllBytes(info.FullName));
                //add the texture to our list of screenshots
                screenshots.Add(texture);
            }
            galleryDisplay.texture = screenshots[0];

        }
        else
        {
            galleryDisplay.gameObject.SetActive(false);
        }
    }

    void Start()
    {
        galleryPanel.SetActive(false);
        infoPanel.SetActive(false);
    }

    IEnumerator LoadGame()
    {
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene(1);
    }

    public void StartGame()
    {
        StartCoroutine(LoadGame());
    }

    public void Info() 
    {
        if(galleryPanel.activeSelf == true)
        {
            galleryPanel.SetActive(false);
        }
        infoPanel.SetActive(!infoPanel.activeSelf);
    }

    public void Gallery() 
    {
        if (infoPanel.activeSelf == true)
        {
            infoPanel.SetActive(false);
        }
        galleryPanel.SetActive(!galleryPanel.activeSelf);
    }

    public void SelectGalleryImage() 
    {
        if(screenshots.Count > 0)
        {
            if (galleryDisplay.gameObject.activeSelf == false)
            {
                galleryDisplay.gameObject.SetActive(true);
            }
            galleryDisplay.texture = screenshots[galleryDropdown.value];
        }
    }

    public void QuitToDesktop() 
    {
        Application.Quit();
    }

}
