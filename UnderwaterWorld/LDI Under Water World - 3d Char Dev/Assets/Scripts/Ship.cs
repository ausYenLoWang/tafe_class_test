using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.AI;
using FiniteStateMachine;

public class Ship : MonoBehaviour, IShip
{
    [TextArea]
    public string tooltipText;
    public Vector3 playerSpawnOffset;
    public Bounds boundBox;
    public ShipWait waitState;
    public ShipMove moveState;

    public StateMachine StateMachine { get; private set; }

    public NavMeshAgent Agent { get; private set; }

    private void Awake()
    {
        Agent = GetComponent<NavMeshAgent>();
        waitState = new ShipWait(this) { waitTime = waitState.waitTime};
        moveState = new ShipMove(this);
        StateMachine = new StateMachine(waitState);
    }

    // Start is called before the first frame update
    void Start()
    {
        PlayerController.Instance.MoveToPosition(transform.position + playerSpawnOffset);
        WorldMap.Instance.AddInteraction(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        StateMachine.OnUpdate();
    }


    public string Inspect()
    {
        return tooltipText;
    }

    public void Leave()
    {
        
            //go to main menu
        //else
            //Tooltip 'you must find the target fish'
        if(GameManager.Instance.TargetFound == true)  //if target fish found
        {
            MouseLook.Instance.CursorToggle = true;
            SceneManager.LoadScene(0);
        }
        else
        {
            string message = $"You need to find the target fish'{GameManager.Instance.targetText.text}' before you can leave";
            ToolTip.Instance.Toggle(message);
        }
    }
    public void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(boundBox.center, boundBox.size);
        if(StateMachine != null && StateMachine.CurrentState != null)
        {
            StateMachine.GetCurrentStateAsType<ShipState>().DrawGizmos();
        }
    }
}

public abstract class ShipState : IState
{

    public Ship Instance { get; private set; }

    public ShipState(Ship instance)
    {
        Instance = instance;
    }


    public virtual void OnEnter()
    {
        
    }

    public virtual void OnExit()
    {
        
    }

    public virtual void OnUpdate()
    {
        
    }

    public virtual void DrawGizmos() { }
}
[System.Serializable]

public class ShipWait : ShipState
{
    public Vector2 waitTime = new Vector2(1, 3);

    private float time = 0;
    private float timer = -1;
    public ShipWait(Ship instance) : base(instance)
    {

    }
    public override void OnEnter()
    {
        time = Random.Range(waitTime.x, waitTime.y);
        timer = 0;
    }

    public override void OnUpdate()
    {
        if(timer > -1)
        {
            timer += Time.deltaTime;
            if(timer > time)
            {
                Instance.StateMachine.SetState(Instance.moveState);
            }
        }
    }

    public override void OnExit()
    {
        timer = -1;
    }

}
[System.Serializable]

public class ShipMove : ShipState
{
    private Vector3 targetPos;
    public ShipMove(Ship instance) : base(instance)
    {
    }

    public override void OnEnter()
    {

        targetPos = GetRandomPosInBounds();
        Instance.Agent.SetDestination(targetPos);
    }

    public override void OnUpdate()
    {
        if(Vector3.Distance(Instance.transform.position, targetPos) < Instance.Agent.stoppingDistance)
        {
            Instance.StateMachine.SetState(Instance.waitState);
        }
    }

    public override void DrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(targetPos, 1);

    }

    public Vector3 GetRandomPosInBounds()
    {
        return new Vector3(Random.Range(-Instance.boundBox.extents.x, Instance.boundBox.extents.x), Instance.transform.position.y, Random.Range(-Instance.boundBox.extents.z, Instance.boundBox.extents.z));
    }
}
