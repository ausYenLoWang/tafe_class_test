using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WorldMap : MonoBehaviour
{
    public static WorldMap Instance { get; private set; }
    public GameObject legend;
    public Camera mapCam;
    public Image[] legendOptions;
    public GameObject keyIconPrefab;
    public GameObject gateIconPrefab;
    public GameObject shipIconPrefab;

    private float timer = -1;
    private int legendIndex = 0;
    private GameObject ship;
    private List<GameObject> activeInteractions = new List<GameObject>();
    private List<GameObject> gateInteractions = new List<GameObject>();
    private List<GameObject> keyInteractions = new List<GameObject>();
    private List<GameObject> mapIcons = new List<GameObject>();

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            enabled = false;
        }
    }

    void Start()
    {
        
    }


    void Update()
    {
        if(legend.activeSelf == true && timer == -1)
        {
            //detect inputs
            float axis = Input.GetAxis("Menu Selection");
            if(axis != 0)
            {
                timer = 0;
                legendIndex += Mathf.FloorToInt(axis);
                if(legendIndex < 0)
                {
                    legendIndex = legendOptions.Length - 1;
                }
                else if(legendIndex > legendOptions.Length - 1)
                {
                    legendIndex = 0;
                }
                UpdateLegendSelection();
            }

            for(int i = 0; i < activeInteractions.Count; i++)  //actiates map icons and sets them to relevant screenposition
            {
                Vector3 screenPos = mapCam.WorldToScreenPoint(activeInteractions[i].transform.position);
                screenPos.z = 0;
                mapIcons[i].transform.position = screenPos;
            }
        }

        if(timer > -1) //legend selection timer
        {
            timer += Time.deltaTime;
            if(timer > 0.2f)
            {
                timer = -1;
            }
        }

    }

    void UpdateLegendSelection()
    { 
        for(int i = 0; i < legendOptions.Length; i++)
        {
            if(i == legendIndex)
            {
                legendOptions[i].color = Color.white;
                switch (i)
                {
                    case 0:
                        activeInteractions = new List<GameObject>(keyInteractions);
                        break;
                    case 1:
                        activeInteractions = new List<GameObject>(gateInteractions);
                        break;
                    case 2:
                        if (ship != null)
                        {
                            activeInteractions = new List<GameObject>() { ship };
                        }
                        break;
                }
                continue;
            }
            else
            {
                legendOptions[i].color = Color.grey;
            }
        }
        for(int i = 0; i < mapIcons.Count; i++)
        {
            if (i < activeInteractions.Count)
            {
                mapIcons[i].SetActive(true);
            }
            else
            {
                mapIcons[i].SetActive(false);
            }
        }

    }

    public bool AddInteraction(GameObject interaction)
    {
        if (interaction.TryGetComponent(out IInteraction i) == true)
        {
            GameObject icon = null;
            if (i is IKey)  //type check object against key interaction interface
            {
                keyInteractions.Add(interaction);
                icon = Instantiate(keyIconPrefab, legend.transform, false);
            }
            else if (i is IGate) //type check object against gate interaction interface
            {
                gateInteractions.Add(interaction);
                icon = Instantiate(gateIconPrefab, legend.transform, false);
            }
            else if (i is IShip)  //type check object against ship interaction interface
            {
                ship = interaction;
                icon = Instantiate(shipIconPrefab, legend.transform, false);
            }
            mapIcons.Add(icon);
            icon.transform.position = Vector3.zero;
            return true;
        }
        return false;
    }

    public void Activate() 
    {
        legendIndex = 0;
        legend.SetActive(true);
        mapCam.transform.position = new Vector3(PlayerController.Instance.transform.position.x, mapCam.transform.position.y, PlayerController.Instance.transform.position.z);
        mapCam.gameObject.SetActive(true);
        UpdateLegendSelection();
    
    }

    public void Deactivate() 
    {
        legend.SetActive(false);
        mapCam.gameObject.SetActive(false);
    }

}
