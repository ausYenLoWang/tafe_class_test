﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct InterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// System.Action`1<UnityEngine.Playables.PlayableDirector>
struct Action_1_tEFFD33105460921F68C4261E79791D397C76CB1B;
// System.Collections.Generic.IEnumerable`1<UnityEngine.GameObject>
struct IEnumerable_1_t59A95566E6FBF6B195B841B400D0A6B7264A738B;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t52B1AC8D9E5E1ED28DF6C46A37C9A1B00B394F9D;
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t34AA4AF4E7352129CA58045901530E41445AC16D;
// System.Collections.Generic.List`1<FlockAgent>
struct List_1_t50CCFC552038CBD55842D81AC8F7EE04848C7255;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5;
// System.Collections.Generic.List`1<UnityEngine.UI.Image>
struct List_1_t815A476B0A21E183042059E705F9E505478CD8AE;
// System.Collections.Generic.List`1<System.Int32Enum>
struct List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5;
// System.Collections.Generic.List`1<UnityEngine.Texture2D>
struct List_1_t67CA4414F3746D817D6D1A1D16FD9E7C85CED2D7;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0;
// System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem>
struct List_1_t4CFF6A6E1A912AE4990A34B2AA4A1FE2C9FB0033;
// System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>
struct List_1_tAF6577A540702C9F6C407DE69A8FAFB502339DC4;
// System.Collections.Generic.List`1<Key/KeyType>
struct List_1_tD99207A373A130469C76CB8634AADFB31FF317AE;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>
struct TweenRunner_1_t428873023FD8831B6DCE3CBD53ADD7D37AC8222D;
// System.Byte[]
struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// UnityEngine.Collider[]
struct ColliderU5BU5D_t5124940293343DB3D31DA41C593709905906E486;
// System.IO.FileInfo[]
struct FileInfoU5BU5D_tB6A5BC6AD9BEC388BF3236B4E8FBB7760F96080B;
// FlockAgent[]
struct FlockAgentU5BU5D_t012E3C58876A70268E6BFEDBF651E5B8D23AAF84;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642;
// UnityEngine.UI.Image[]
struct ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224;
// System.Int32[]
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32;
// System.IntPtr[]
struct IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6;
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
// UnityEngine.UI.Selectable[]
struct SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971;
// UnityEngine.Texture2D[]
struct Texture2DU5BU5D_t0CBDCEA1648F6CBEA47C64E1E48F22B9692B3316;
// UnityEngine.Transform[]
struct TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4;
// UnityEngine.UI.Dropdown/OptionData[]
struct OptionDataU5BU5D_t76E953160486FF629DE132F60738702D374E11A5;
// Key/KeyType[]
struct KeyTypeU5BU5D_tF7D6A39E689B206AEE8B7AE7158B77FB2EF39259;
// ActionMenu
struct ActionMenu_tE3C589CB5B0B532FF5504CF0D558D489281BED01;
// Actions
struct Actions_tF16D3C582B8D8DF5DD96B758A4D691F304BE44BB;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11;
// UnityEngine.Behaviour
struct Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9;
// UnityEngine.Camera
struct Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C;
// UnityEngine.Canvas
struct Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E;
// UnityEngine.CharacterController
struct CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E;
// UnityEngine.Collider
struct Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02;
// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684;
// UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7;
// System.IO.DirectoryInfo
struct DirectoryInfo_t4EF3610F45F0D234800D01ADA8F3F476AE0CF5CD;
// UnityEngine.UI.Dropdown
struct Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96;
// System.IO.FileInfo
struct FileInfo_t6C8B2EAA1E23F9E6D7C287C58E4EEEB2049ABAB9;
// Fish
struct Fish_t45F7EC0A7EE404E12CC412CF93723EA8799CA46C;
// FlockAgent
struct FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04;
// FlockManager
struct FlockManager_t241CFB1B2E6593247EFA52A9E2CB2A111400F1F8;
// UnityEngine.UI.FontData
struct FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738;
// GameManager
struct GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1;
// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319;
// Gate
struct Gate_tAA47F9BD94C2B1C12EADC5ECCAF1CAD07A035F45;
// UnityEngine.UI.Graphic
struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24;
// System.Collections.IDictionary
struct IDictionary_t99871C56B8EC2452AC5C4CF3831695E617B89D3A;
// System.Collections.IEnumerator
struct IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105;
// IFauna
struct IFauna_tECBAA4EFAA9D07BFB7CB55CEE86755A301872FC5;
// IGate
struct IGate_tC633C584B29457590CF59B3448AEE38AC9FB33C6;
// IInteraction
struct IInteraction_t1C1C3E6C29AFB203F5021CA7300B6FEA0EFE91FE;
// IKey
struct IKey_t524F44506E887E37C88431701D58645C41BD5F3C;
// IShip
struct IShip_tC6FB125A646115EE3BCEDE1326BF546935215C99;
// FiniteStateMachine.IState
struct IState_t64095858CEB21D62A4A03489C4738FA56F5243CD;
// UnityEngine.UI.Image
struct Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C;
// Interaction
struct Interaction_t41E997DF6BD70C3EAB8669FCC65D603292EF42A1;
// InteractionMenu
struct InteractionMenu_t7C2715221BAC6E0BB83B07EF82F68C6AA34129CB;
// Key
struct Key_tB313BF742406084E17C47FE2B934490E0261AC8E;
// MainMenu
struct MainMenu_tEB11F5A993C42E93B585FBB65C9E92EC91C5707C;
// UnityEngine.Material
struct Material_t8927C00353A72755313F046D0CE85178AE8218EE;
// UnityEngine.Mesh
struct Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A;
// MouseLook
struct MouseLook_t1E1EE79B13022184F3EF9AC8FE8C6771053890DA;
// UnityEngine.AI.NavMeshAgent
struct NavMeshAgent_tB9746B6C38013341DB63973CA7ED657494EFB41B;
// NewBehaviourScript
struct NewBehaviourScript_t5ED8D4BD4668DBECFEFD841BD2A8CC1D8FCF38CB;
// System.NotImplementedException
struct NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6;
// System.NotSupportedException
struct NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339;
// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A;
// UnityEngine.Playables.PlayableAsset
struct PlayableAsset_t5AD1606B76C9753A7F4C6B1061193F581023F137;
// UnityEngine.Playables.PlayableDirector
struct PlayableDirector_t63912C98A4EC9D4701E88B633E27E0D97209BA38;
// PlayerController
struct PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9;
// UnityEngine.UI.RawImage
struct RawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A;
// UnityEngine.UI.RectMask2D
struct RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15;
// UnityEngine.RectTransform
struct RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F;
// UnityEngine.UI.Selectable
struct Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD;
// Ship
struct Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E;
// ShipMove
struct ShipMove_t761409814A6DA69DE131EA554AEC499CB81D3953;
// ShipState
struct ShipState_tF56B93F7FFD3E22AB4F4ECDF9A73D50A3FE508A5;
// ShipWait
struct ShipWait_tB05589ED6695523DD921B075C449CE03E0B836FE;
// SoloFish
struct SoloFish_t248C1128425014659AF96F9294BAC7D1799179C2;
// UnityEngine.Sprite
struct Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9;
// FiniteStateMachine.StateMachine
struct StateMachine_t8E24C2687A18E15B27A6A8FEED2DFE22D910119F;
// System.String
struct String_t;
// TempInteraction
struct TempInteraction_t379C5510CC31B27F05D6803C86D8ACBAAA4D0587;
// TestFIsh
struct TestFIsh_t2AC494E6BA9A5DD9FB776A1C4D31A78DC0AD98B2;
// UnityEngine.UI.Text
struct Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1;
// UnityEngine.TextGenerator
struct TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70;
// UnityEngine.Texture
struct Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE;
// UnityEngine.Texture2D
struct Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF;
// ToolTip
struct ToolTip_t044257731DC5931927D3AB0671C17515BD8E251E;
// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1;
// UnityEngine.Events.UnityAction
struct UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099;
// UnityEngine.UI.VertexHelper
struct VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// UnityEngine.WaitForEndOfFrame
struct WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013;
// WorldMap
struct WorldMap_t01A07DDC9EA386C88D5814CF18E0F37CAF05270C;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D;
// UnityEngine.UI.Dropdown/DropdownEvent
struct DropdownEvent_tEB2C75C3DBC789936B31D9A979FD62E047846CFB;
// UnityEngine.UI.Dropdown/OptionData
struct OptionData_t5F665DC13C1E4307727D66ECC1100B3A77E3E857;
// UnityEngine.UI.Dropdown/OptionDataList
struct OptionDataList_t524EBDB7A2B178269FD5B4740108D0EC6404B4B6;
// GameManager/<PlayCutScene>d__46
struct U3CPlayCutSceneU3Ed__46_tC2D2594FAEFE8718E713CAC6338A1701F09CADAA;
// MainMenu/<LoadGame>d__7
struct U3CLoadGameU3Ed__7_tC6D4D2F6FFFF4C3DA7A96BE826554FAE56FB9D74;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4;

IL2CPP_EXTERN_C RuntimeClass* ActionMenu_tE3C589CB5B0B532FF5504CF0D558D489281BED01_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Actions_tF16D3C582B8D8DF5DD96B758A4D691F304BE44BB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* DirectoryInfo_t4EF3610F45F0D234800D01ADA8F3F476AE0CF5CD_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* FlockAgentU5BU5D_t012E3C58876A70268E6BFEDBF651E5B8D23AAF84_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IFauna_tECBAA4EFAA9D07BFB7CB55CEE86755A301872FC5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IGate_tC633C584B29457590CF59B3448AEE38AC9FB33C6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IInteraction_t1C1C3E6C29AFB203F5021CA7300B6FEA0EFE91FE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IKey_t524F44506E887E37C88431701D58645C41BD5F3C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IShip_tC6FB125A646115EE3BCEDE1326BF546935215C99_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IState_t64095858CEB21D62A4A03489C4738FA56F5243CD_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* InteractionMenu_t7C2715221BAC6E0BB83B07EF82F68C6AA34129CB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Interaction_t41E997DF6BD70C3EAB8669FCC65D603292EF42A1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* KeyType_t9EEB0AD19791311F275DCC1CE12BD7AAD39A3C13_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t50CCFC552038CBD55842D81AC8F7EE04848C7255_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t67CA4414F3746D817D6D1A1D16FD9E7C85CED2D7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_tD99207A373A130469C76CB8634AADFB31FF317AE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MouseLook_t1E1EE79B13022184F3EF9AC8FE8C6771053890DA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* OptionData_t5F665DC13C1E4307727D66ECC1100B3A77E3E857_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ShipMove_t761409814A6DA69DE131EA554AEC499CB81D3953_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ShipWait_tB05589ED6695523DD921B075C449CE03E0B836FE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StateMachine_t8E24C2687A18E15B27A6A8FEED2DFE22D910119F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ToolTip_t044257731DC5931927D3AB0671C17515BD8E251E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CLoadGameU3Ed__7_tC6D4D2F6FFFF4C3DA7A96BE826554FAE56FB9D74_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CPlayCutSceneU3Ed__46_tC2D2594FAEFE8718E713CAC6338A1701F09CADAA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WorldMap_t01A07DDC9EA386C88D5814CF18E0F37CAF05270C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral11E7C4352B11858AEFB14F47D96849AC6696EC2B;
IL2CPP_EXTERN_C String_t* _stringLiteral16DD21BE77B115D392226EB71A2D3A9FDC29E3F0;
IL2CPP_EXTERN_C String_t* _stringLiteral265E15F1F86F1C766555899D5771CF29055DE75A;
IL2CPP_EXTERN_C String_t* _stringLiteral3E006858D3E74664A4AB5E9754647D20A4D98A94;
IL2CPP_EXTERN_C String_t* _stringLiteral4D2CACA605B9C43AD760EA775DD96667ED22EDA4;
IL2CPP_EXTERN_C String_t* _stringLiteral4D43BD5775F2AB357D4F679F6BE3377EF52F5BAD;
IL2CPP_EXTERN_C String_t* _stringLiteral521B13C1C2102F7779BF79109F54CF81F4D7ECE7;
IL2CPP_EXTERN_C String_t* _stringLiteral7F8C014BD4810CC276D0F9F81A1E759C7B098B1E;
IL2CPP_EXTERN_C String_t* _stringLiteral87657B66CDDAD40B19EA8B71A88ED298D2AF6F2F;
IL2CPP_EXTERN_C String_t* _stringLiteral88BEE283254D7094E258B3A88730F4CC4F1E4AC7;
IL2CPP_EXTERN_C String_t* _stringLiteralA15C898F015A9B0BC3268E8883CD03008A56DE26;
IL2CPP_EXTERN_C String_t* _stringLiteralA887E0FF7814A894C42824B8F164C9E3F0509345;
IL2CPP_EXTERN_C String_t* _stringLiteralA91C7F3122FF007BB627E35A579A9AC513F8A14A;
IL2CPP_EXTERN_C String_t* _stringLiteralAA8142B38C59D66391C7A7D71BCD21A8EE61FC8B;
IL2CPP_EXTERN_C String_t* _stringLiteralAF8E649A3EF1AB74A43C9F88015FA68AC719215A;
IL2CPP_EXTERN_C String_t* _stringLiteralC09F0D4A7B660E1924D2300CB0765C6FFCB6FDE4;
IL2CPP_EXTERN_C String_t* _stringLiteralC9E357C460CFAAA857877CCC0114469CFFEF676B;
IL2CPP_EXTERN_C String_t* _stringLiteralCC112E57619A877173F6FAE64FE37E7A6BBF8CBA;
IL2CPP_EXTERN_C String_t* _stringLiteralCCDD2BB80FDD5E8E6A309332CA959EBF10BABCEC;
IL2CPP_EXTERN_C String_t* _stringLiteralCEB37FDDA4CCFF50204045296E416398C098BA6A;
IL2CPP_EXTERN_C String_t* _stringLiteralD8126D7FB6A15D1DF1ED742A3550A992581F56F8;
IL2CPP_EXTERN_C String_t* _stringLiteralDCB6AFB2D0F0FAD8B76294039022E7626E5D71BC;
IL2CPP_EXTERN_C String_t* _stringLiteralDD86881DBA7E6B755DFE2849A5B0579CC3D828CD;
IL2CPP_EXTERN_C String_t* _stringLiteralE27F31821C3D549C27ECCCC1928221B8D933E5BF;
IL2CPP_EXTERN_C String_t* _stringLiteralE5368AE200A37FDFED8AA9B3CB8C7E26E81CA911;
IL2CPP_EXTERN_C String_t* _stringLiteralE8D08BFECF9F5FBAAB31AB2F1B32784ADAB8F601;
IL2CPP_EXTERN_C String_t* _stringLiteralF19A0A1611C3B19235821CD4D354C47A2B95313F;
IL2CPP_EXTERN_C String_t* _stringLiteralF4164D1DF7A1099D188B845751FF90B8C55A023D;
IL2CPP_EXTERN_C String_t* _stringLiteralF5260CA4F761D522A3E8A6463DF1FAC59EBA2525;
IL2CPP_EXTERN_C String_t* _stringLiteralF7725728A0F11C8DECA5441C8D7FF63F5803B2C0;
IL2CPP_EXTERN_C String_t* _stringLiteralF88D019D310EFCC979975957D01E97BE7C0390BC;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mFB5C182E24F496A866F116D3F75CBD8616A46AB3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisCharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E_m3DB1DD5819F96D7C7F6F19C12138AC48D21DBBF2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisNavMeshAgent_tB9746B6C38013341DB63973CA7ED657494EFB41B_m15077FF184192C84FCF6B2A1FC8ECF53C9220F2F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_TryGetComponent_TisIInteraction_t1C1C3E6C29AFB203F5021CA7300B6FEA0EFE91FE_mEC43AA2983C9283A4104774AD183EECD789EFD70_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_Dispose_m4B68F0A4E0441A036D7E39BC7E639536164196D9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_MoveNext_mF39107B3A55F66C83EBCA798CBC93AC4C990DBD7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_get_Current_mB38DBEFCD264B4682A190F8592464C0658F702B7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponentInChildren_TisDropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96_m8B8487C221C1219A04D3F96C76A0E7CFFA9B4CD2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponentInChildren_TisRawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A_mBE7B553FDA545118EB6051F0473AAEC7F78D8CAB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisIFauna_tECBAA4EFAA9D07BFB7CB55CEE86755A301872FC5_m3DDC9275C8F6A20642A992058DD3465A7B43896A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_TryGetComponent_TisIInteraction_t1C1C3E6C29AFB203F5021CA7300B6FEA0EFE91FE_m619A9E6EA0CC7690AEDF0BA79C3534C945F5906B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_TryGetComponent_TisSoloFish_t248C1128425014659AF96F9294BAC7D1799179C2_m77EB52771C7FC7A5EF16539BFE0797509D5DA199_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* InteractionMenu_GetInteractionAsType_TisIFauna_tECBAA4EFAA9D07BFB7CB55CEE86755A301872FC5_m4E764DDABB046069A791E256957F04B862FD2182_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* InteractionMenu_GetInteractionAsType_TisIGate_tC633C584B29457590CF59B3448AEE38AC9FB33C6_mDD834F8EB0D1D7DCE6A48654C2AD746D46BB8279_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* InteractionMenu_GetInteractionAsType_TisIKey_t524F44506E887E37C88431701D58645C41BD5F3C_m244F7034AC80DC277E1467E43379BC7EFEFD5F00_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* InteractionMenu_GetInteractionAsType_TisIShip_tC6FB125A646115EE3BCEDE1326BF546935215C99_m22472493C6CA081B6709F1EAE1AD3E87BDAFA312_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m3DD76DE838FA83DF972E0486A296345EB3A7DDF3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m6470E90BACC0611067C594D4D8259D1BC40DBB5A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m9AF452292436C834FC154E6457CDD42B18FB310F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_mC2611000DA97145D1DFC867ECA604FBB3E8628EC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_mF1329DCD6488AD8E4E570B756253384836CC2E82_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Clear_m2BD50CE116D1680EF02FA4859D6802F72B45324F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Contains_m69468F32EA1D0A3E5BC817F0CE7F0560332383E6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_GetEnumerator_m3616D04A85546C8251A6C376656CDB5358D893F6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Remove_m4FBB736F89C431F0D19B655E23269EAB321B863A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Remove_mD36BF07C31C1DF947856EFECE89BAF4D6A24DEB7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m612E7D379DA89488D1FC2AB1FB9E2BD2AAA9AABA_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m6C09283D126DAC46B64EE124C67FADA66797752F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m859B0EE8491FDDEB1A3F7115D334B863E025BBC8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m98321E90C6288A40B856F52CFF529E59A738E641_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_mD04CAD03BDE2013E799D002230D9D16EFD84FEFC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_m6206A8F48C7A98B9CFC23A13548CB215717573BD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_m82AF14687C6FA2B1572D859A551E3ADBCBADC3C0_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_mB64A4B531A3EB080CDD6EBBD4D31F9BCB6730F76_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_mC0601A2601297DDBF0B461A2A8B8EF9528741B88_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m69B62B1F3FDC9F7B1D0906604855E457795130AB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m8036333D84A83C0B1348EFFA8C2CA6EBCB99E363_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m8B222F262DF0C4B49E12B4E87AB2162202744499_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Object_Instantiate_TisFlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04_mB3A05C03EA7F057D8762864B17A537999578B839_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m81B599A0051F8F4543E5C73A11585E96E940943B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_mC197A913A8DF8E125DEFD1C13605BC1DFD23C2F7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* StateMachine_GetCurrentStateAsType_TisShipState_tF56B93F7FFD3E22AB4F4ECDF9A73D50A3FE508A5_m955D4460B9487D2450FF1C450DEB3B39C73F6255_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TempInteraction_Leave_mB34E7DA19EA52B8D6922633E0BA000ABABF47032_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CLoadGameU3Ed__7_System_Collections_IEnumerator_Reset_mEAC358DD13F122444F8E3E2908F91C48BFE5DC9A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CPlayCutSceneU3Ed__46_System_Collections_IEnumerator_Reset_m328A12CA0CF591B501C192E4A0942571129FBCC6_RuntimeMethod_var;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726;
struct ColliderU5BU5D_t5124940293343DB3D31DA41C593709905906E486;
struct FileInfoU5BU5D_tB6A5BC6AD9BEC388BF3236B4E8FBB7760F96080B;
struct FlockAgentU5BU5D_t012E3C58876A70268E6BFEDBF651E5B8D23AAF84;
struct ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224;
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
struct Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_tFDCAFCBB4B3431CFF2DC4D3E03FBFDF54EFF7E9A 
{
public:

public:
};


// System.Object


// System.Collections.Generic.List`1<FlockAgent>
struct List_1_t50CCFC552038CBD55842D81AC8F7EE04848C7255  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	FlockAgentU5BU5D_t012E3C58876A70268E6BFEDBF651E5B8D23AAF84* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t50CCFC552038CBD55842D81AC8F7EE04848C7255, ____items_1)); }
	inline FlockAgentU5BU5D_t012E3C58876A70268E6BFEDBF651E5B8D23AAF84* get__items_1() const { return ____items_1; }
	inline FlockAgentU5BU5D_t012E3C58876A70268E6BFEDBF651E5B8D23AAF84** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(FlockAgentU5BU5D_t012E3C58876A70268E6BFEDBF651E5B8D23AAF84* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t50CCFC552038CBD55842D81AC8F7EE04848C7255, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t50CCFC552038CBD55842D81AC8F7EE04848C7255, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t50CCFC552038CBD55842D81AC8F7EE04848C7255, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t50CCFC552038CBD55842D81AC8F7EE04848C7255_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	FlockAgentU5BU5D_t012E3C58876A70268E6BFEDBF651E5B8D23AAF84* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t50CCFC552038CBD55842D81AC8F7EE04848C7255_StaticFields, ____emptyArray_5)); }
	inline FlockAgentU5BU5D_t012E3C58876A70268E6BFEDBF651E5B8D23AAF84* get__emptyArray_5() const { return ____emptyArray_5; }
	inline FlockAgentU5BU5D_t012E3C58876A70268E6BFEDBF651E5B8D23AAF84** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(FlockAgentU5BU5D_t012E3C58876A70268E6BFEDBF651E5B8D23AAF84* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5, ____items_1)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get__items_1() const { return ____items_1; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5_StaticFields, ____emptyArray_5)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get__emptyArray_5() const { return ____emptyArray_5; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Object>
struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____items_1)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get__items_1() const { return ____items_1; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5_StaticFields, ____emptyArray_5)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.Texture2D>
struct List_1_t67CA4414F3746D817D6D1A1D16FD9E7C85CED2D7  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Texture2DU5BU5D_t0CBDCEA1648F6CBEA47C64E1E48F22B9692B3316* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t67CA4414F3746D817D6D1A1D16FD9E7C85CED2D7, ____items_1)); }
	inline Texture2DU5BU5D_t0CBDCEA1648F6CBEA47C64E1E48F22B9692B3316* get__items_1() const { return ____items_1; }
	inline Texture2DU5BU5D_t0CBDCEA1648F6CBEA47C64E1E48F22B9692B3316** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Texture2DU5BU5D_t0CBDCEA1648F6CBEA47C64E1E48F22B9692B3316* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t67CA4414F3746D817D6D1A1D16FD9E7C85CED2D7, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t67CA4414F3746D817D6D1A1D16FD9E7C85CED2D7, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t67CA4414F3746D817D6D1A1D16FD9E7C85CED2D7, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t67CA4414F3746D817D6D1A1D16FD9E7C85CED2D7_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	Texture2DU5BU5D_t0CBDCEA1648F6CBEA47C64E1E48F22B9692B3316* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t67CA4414F3746D817D6D1A1D16FD9E7C85CED2D7_StaticFields, ____emptyArray_5)); }
	inline Texture2DU5BU5D_t0CBDCEA1648F6CBEA47C64E1E48F22B9692B3316* get__emptyArray_5() const { return ____emptyArray_5; }
	inline Texture2DU5BU5D_t0CBDCEA1648F6CBEA47C64E1E48F22B9692B3316** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(Texture2DU5BU5D_t0CBDCEA1648F6CBEA47C64E1E48F22B9692B3316* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0, ____items_1)); }
	inline TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* get__items_1() const { return ____items_1; }
	inline TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0_StaticFields, ____emptyArray_5)); }
	inline TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* get__emptyArray_5() const { return ____emptyArray_5; }
	inline TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>
struct List_1_tAF6577A540702C9F6C407DE69A8FAFB502339DC4  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	OptionDataU5BU5D_t76E953160486FF629DE132F60738702D374E11A5* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tAF6577A540702C9F6C407DE69A8FAFB502339DC4, ____items_1)); }
	inline OptionDataU5BU5D_t76E953160486FF629DE132F60738702D374E11A5* get__items_1() const { return ____items_1; }
	inline OptionDataU5BU5D_t76E953160486FF629DE132F60738702D374E11A5** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(OptionDataU5BU5D_t76E953160486FF629DE132F60738702D374E11A5* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tAF6577A540702C9F6C407DE69A8FAFB502339DC4, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tAF6577A540702C9F6C407DE69A8FAFB502339DC4, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tAF6577A540702C9F6C407DE69A8FAFB502339DC4, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tAF6577A540702C9F6C407DE69A8FAFB502339DC4_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	OptionDataU5BU5D_t76E953160486FF629DE132F60738702D374E11A5* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tAF6577A540702C9F6C407DE69A8FAFB502339DC4_StaticFields, ____emptyArray_5)); }
	inline OptionDataU5BU5D_t76E953160486FF629DE132F60738702D374E11A5* get__emptyArray_5() const { return ____emptyArray_5; }
	inline OptionDataU5BU5D_t76E953160486FF629DE132F60738702D374E11A5** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(OptionDataU5BU5D_t76E953160486FF629DE132F60738702D374E11A5* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<Key/KeyType>
struct List_1_tD99207A373A130469C76CB8634AADFB31FF317AE  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	KeyTypeU5BU5D_tF7D6A39E689B206AEE8B7AE7158B77FB2EF39259* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tD99207A373A130469C76CB8634AADFB31FF317AE, ____items_1)); }
	inline KeyTypeU5BU5D_tF7D6A39E689B206AEE8B7AE7158B77FB2EF39259* get__items_1() const { return ____items_1; }
	inline KeyTypeU5BU5D_tF7D6A39E689B206AEE8B7AE7158B77FB2EF39259** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(KeyTypeU5BU5D_tF7D6A39E689B206AEE8B7AE7158B77FB2EF39259* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tD99207A373A130469C76CB8634AADFB31FF317AE, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tD99207A373A130469C76CB8634AADFB31FF317AE, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tD99207A373A130469C76CB8634AADFB31FF317AE, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tD99207A373A130469C76CB8634AADFB31FF317AE_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	KeyTypeU5BU5D_tF7D6A39E689B206AEE8B7AE7158B77FB2EF39259* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tD99207A373A130469C76CB8634AADFB31FF317AE_StaticFields, ____emptyArray_5)); }
	inline KeyTypeU5BU5D_tF7D6A39E689B206AEE8B7AE7158B77FB2EF39259* get__emptyArray_5() const { return ____emptyArray_5; }
	inline KeyTypeU5BU5D_tF7D6A39E689B206AEE8B7AE7158B77FB2EF39259** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(KeyTypeU5BU5D_tF7D6A39E689B206AEE8B7AE7158B77FB2EF39259* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};

struct Il2CppArrayBounds;

// System.Array


// System.MarshalByRefObject
struct MarshalByRefObject_tD4DF91B488B284F899417EC468D8E50E933306A8  : public RuntimeObject
{
public:
	// System.Object System.MarshalByRefObject::_identity
	RuntimeObject * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_tD4DF91B488B284F899417EC468D8E50E933306A8, ____identity_0)); }
	inline RuntimeObject * get__identity_0() const { return ____identity_0; }
	inline RuntimeObject ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(RuntimeObject * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____identity_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tD4DF91B488B284F899417EC468D8E50E933306A8_marshaled_pinvoke
{
	Il2CppIUnknown* ____identity_0;
};
// Native definition for COM marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tD4DF91B488B284F899417EC468D8E50E933306A8_marshaled_com
{
	Il2CppIUnknown* ____identity_0;
};

// ShipState
struct ShipState_tF56B93F7FFD3E22AB4F4ECDF9A73D50A3FE508A5  : public RuntimeObject
{
public:
	// Ship ShipState::<Instance>k__BackingField
	Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * ___U3CInstanceU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ShipState_tF56B93F7FFD3E22AB4F4ECDF9A73D50A3FE508A5, ___U3CInstanceU3Ek__BackingField_0)); }
	inline Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * get_U3CInstanceU3Ek__BackingField_0() const { return ___U3CInstanceU3Ek__BackingField_0; }
	inline Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E ** get_address_of_U3CInstanceU3Ek__BackingField_0() { return &___U3CInstanceU3Ek__BackingField_0; }
	inline void set_U3CInstanceU3Ek__BackingField_0(Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * value)
	{
		___U3CInstanceU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CInstanceU3Ek__BackingField_0), (void*)value);
	}
};


// FiniteStateMachine.StateMachine
struct StateMachine_t8E24C2687A18E15B27A6A8FEED2DFE22D910119F  : public RuntimeObject
{
public:
	// FiniteStateMachine.IState FiniteStateMachine.StateMachine::<CurrentState>k__BackingField
	RuntimeObject* ___U3CCurrentStateU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CCurrentStateU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachine_t8E24C2687A18E15B27A6A8FEED2DFE22D910119F, ___U3CCurrentStateU3Ek__BackingField_0)); }
	inline RuntimeObject* get_U3CCurrentStateU3Ek__BackingField_0() const { return ___U3CCurrentStateU3Ek__BackingField_0; }
	inline RuntimeObject** get_address_of_U3CCurrentStateU3Ek__BackingField_0() { return &___U3CCurrentStateU3Ek__BackingField_0; }
	inline void set_U3CCurrentStateU3Ek__BackingField_0(RuntimeObject* value)
	{
		___U3CCurrentStateU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CCurrentStateU3Ek__BackingField_0), (void*)value);
	}
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// UnityEngine.YieldInstruction
struct YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_com
{
};

// UnityEngine.UI.Dropdown/OptionData
struct OptionData_t5F665DC13C1E4307727D66ECC1100B3A77E3E857  : public RuntimeObject
{
public:
	// System.String UnityEngine.UI.Dropdown/OptionData::m_Text
	String_t* ___m_Text_0;
	// UnityEngine.Sprite UnityEngine.UI.Dropdown/OptionData::m_Image
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_Image_1;

public:
	inline static int32_t get_offset_of_m_Text_0() { return static_cast<int32_t>(offsetof(OptionData_t5F665DC13C1E4307727D66ECC1100B3A77E3E857, ___m_Text_0)); }
	inline String_t* get_m_Text_0() const { return ___m_Text_0; }
	inline String_t** get_address_of_m_Text_0() { return &___m_Text_0; }
	inline void set_m_Text_0(String_t* value)
	{
		___m_Text_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Text_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Image_1() { return static_cast<int32_t>(offsetof(OptionData_t5F665DC13C1E4307727D66ECC1100B3A77E3E857, ___m_Image_1)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_Image_1() const { return ___m_Image_1; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_Image_1() { return &___m_Image_1; }
	inline void set_m_Image_1(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_Image_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Image_1), (void*)value);
	}
};


// GameManager/<PlayCutScene>d__46
struct U3CPlayCutSceneU3Ed__46_tC2D2594FAEFE8718E713CAC6338A1701F09CADAA  : public RuntimeObject
{
public:
	// System.Int32 GameManager/<PlayCutScene>d__46::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object GameManager/<PlayCutScene>d__46::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// GameManager GameManager/<PlayCutScene>d__46::<>4__this
	GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * ___U3CU3E4__this_2;
	// UnityEngine.Playables.PlayableAsset GameManager/<PlayCutScene>d__46::cutScene
	PlayableAsset_t5AD1606B76C9753A7F4C6B1061193F581023F137 * ___cutScene_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CPlayCutSceneU3Ed__46_tC2D2594FAEFE8718E713CAC6338A1701F09CADAA, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CPlayCutSceneU3Ed__46_tC2D2594FAEFE8718E713CAC6338A1701F09CADAA, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CPlayCutSceneU3Ed__46_tC2D2594FAEFE8718E713CAC6338A1701F09CADAA, ___U3CU3E4__this_2)); }
	inline GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_cutScene_3() { return static_cast<int32_t>(offsetof(U3CPlayCutSceneU3Ed__46_tC2D2594FAEFE8718E713CAC6338A1701F09CADAA, ___cutScene_3)); }
	inline PlayableAsset_t5AD1606B76C9753A7F4C6B1061193F581023F137 * get_cutScene_3() const { return ___cutScene_3; }
	inline PlayableAsset_t5AD1606B76C9753A7F4C6B1061193F581023F137 ** get_address_of_cutScene_3() { return &___cutScene_3; }
	inline void set_cutScene_3(PlayableAsset_t5AD1606B76C9753A7F4C6B1061193F581023F137 * value)
	{
		___cutScene_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cutScene_3), (void*)value);
	}
};


// MainMenu/<LoadGame>d__7
struct U3CLoadGameU3Ed__7_tC6D4D2F6FFFF4C3DA7A96BE826554FAE56FB9D74  : public RuntimeObject
{
public:
	// System.Int32 MainMenu/<LoadGame>d__7::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object MainMenu/<LoadGame>d__7::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadGameU3Ed__7_tC6D4D2F6FFFF4C3DA7A96BE826554FAE56FB9D74, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadGameU3Ed__7_tC6D4D2F6FFFF4C3DA7A96BE826554FAE56FB9D74, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}
};


// System.Collections.Generic.List`1/Enumerator<UnityEngine.GameObject>
struct Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14, ___list_0)); }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * get_list_0() const { return ___list_0; }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14, ___current_3)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_current_3() const { return ___current_3; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// System.Collections.Generic.List`1/Enumerator<System.Object>
struct Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___list_0)); }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * get_list_0() const { return ___list_0; }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Byte
struct Byte_t0111FAB8B8685667EDDAF77683F0D8F86B659056 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Byte_t0111FAB8B8685667EDDAF77683F0D8F86B659056, ___m_value_0)); }
	inline uint8_t get_m_value_0() const { return ___m_value_0; }
	inline uint8_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint8_t value)
	{
		___m_value_0 = value;
	}
};


// System.Char
struct Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14 
{
public:
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14, ___m_value_0)); }
	inline Il2CppChar get_m_value_0() const { return ___m_value_0; }
	inline Il2CppChar* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(Il2CppChar value)
	{
		___m_value_0 = value;
	}
};

struct Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_StaticFields
{
public:
	// System.Byte[] System.Char::categoryForLatin1
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___categoryForLatin1_3;

public:
	inline static int32_t get_offset_of_categoryForLatin1_3() { return static_cast<int32_t>(offsetof(Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_StaticFields, ___categoryForLatin1_3)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_categoryForLatin1_3() const { return ___categoryForLatin1_3; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_categoryForLatin1_3() { return &___categoryForLatin1_3; }
	inline void set_categoryForLatin1_3(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___categoryForLatin1_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___categoryForLatin1_3), (void*)value);
	}
};


// UnityEngine.Color
struct Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// System.DateTime
struct DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DaysToMonth365_29), (void*)value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DaysToMonth366_30), (void*)value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_StaticFields, ___MinValue_31)); }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_StaticFields, ___MaxValue_32)); }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  value)
	{
		___MaxValue_32 = value;
	}
};


// System.Double
struct Double_t42821932CB52DE2057E685D0E1AF3DE5033D2181 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Double_t42821932CB52DE2057E685D0E1AF3DE5033D2181, ___m_value_0)); }
	inline double get_m_value_0() const { return ___m_value_0; }
	inline double* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(double value)
	{
		___m_value_0 = value;
	}
};

struct Double_t42821932CB52DE2057E685D0E1AF3DE5033D2181_StaticFields
{
public:
	// System.Double System.Double::NegativeZero
	double ___NegativeZero_7;

public:
	inline static int32_t get_offset_of_NegativeZero_7() { return static_cast<int32_t>(offsetof(Double_t42821932CB52DE2057E685D0E1AF3DE5033D2181_StaticFields, ___NegativeZero_7)); }
	inline double get_NegativeZero_7() const { return ___NegativeZero_7; }
	inline double* get_address_of_NegativeZero_7() { return &___NegativeZero_7; }
	inline void set_NegativeZero_7(double value)
	{
		___NegativeZero_7 = value;
	}
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.LayerMask
struct LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};


// UnityEngine.Quaternion
struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___identityQuaternion_4 = value;
	}
};


// UnityEngine.Rect
struct Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};


// System.Single
struct Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// UnityEngine.UI.SpriteState
struct SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E 
{
public:
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_SelectedSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_SelectedSprite_2;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_DisabledSprite_3;

public:
	inline static int32_t get_offset_of_m_HighlightedSprite_0() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_HighlightedSprite_0)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_HighlightedSprite_0() const { return ___m_HighlightedSprite_0; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_HighlightedSprite_0() { return &___m_HighlightedSprite_0; }
	inline void set_m_HighlightedSprite_0(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_HighlightedSprite_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_HighlightedSprite_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_1() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_PressedSprite_1)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_PressedSprite_1() const { return ___m_PressedSprite_1; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_PressedSprite_1() { return &___m_PressedSprite_1; }
	inline void set_m_PressedSprite_1(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_PressedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PressedSprite_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectedSprite_2() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_SelectedSprite_2)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_SelectedSprite_2() const { return ___m_SelectedSprite_2; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_SelectedSprite_2() { return &___m_SelectedSprite_2; }
	inline void set_m_SelectedSprite_2(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_SelectedSprite_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectedSprite_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_DisabledSprite_3() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_DisabledSprite_3)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_DisabledSprite_3() const { return ___m_DisabledSprite_3; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_DisabledSprite_3() { return &___m_DisabledSprite_3; }
	inline void set_m_DisabledSprite_3(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_DisabledSprite_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DisabledSprite_3), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E_marshaled_pinvoke
{
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_HighlightedSprite_0;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_PressedSprite_1;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_SelectedSprite_2;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_DisabledSprite_3;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E_marshaled_com
{
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_HighlightedSprite_0;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_PressedSprite_1;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_SelectedSprite_2;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_DisabledSprite_3;
};

// UnityEngine.Vector2
struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___zeroVector_2)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___oneVector_3)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___upVector_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___downVector_5)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___leftVector_6)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___rightVector_7)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// UnityEngine.Vector3
struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___zeroVector_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___oneVector_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___upVector_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___downVector_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___leftVector_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___rightVector_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___forwardVector_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___backVector_12)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// UnityEngine.Vector4
struct Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___zeroVector_5)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___oneVector_6)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___negativeInfinityVector_8 = value;
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// UnityEngine.WaitForEndOfFrame
struct WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4  : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF
{
public:

public:
};


// UnityEngine.WaitForSeconds
struct WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013  : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF
{
public:
	// System.Single UnityEngine.WaitForSeconds::m_Seconds
	float ___m_Seconds_0;

public:
	inline static int32_t get_offset_of_m_Seconds_0() { return static_cast<int32_t>(offsetof(WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013, ___m_Seconds_0)); }
	inline float get_m_Seconds_0() const { return ___m_Seconds_0; }
	inline float* get_address_of_m_Seconds_0() { return &___m_Seconds_0; }
	inline void set_m_Seconds_0(float value)
	{
		___m_Seconds_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_marshaled_pinvoke : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_pinvoke
{
	float ___m_Seconds_0;
};
// Native definition for COM marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_marshaled_com : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_com
{
	float ___m_Seconds_0;
};

// UnityEngine.Bounds
struct Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37, ___m_Center_0)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37, ___m_Extents_1)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_Extents_1 = value;
	}
};


// UnityEngine.CollisionFlags
struct CollisionFlags_t435530D092E80B20FFD0DA008B4F298BF224B903 
{
public:
	// System.Int32 UnityEngine.CollisionFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CollisionFlags_t435530D092E80B20FFD0DA008B4F298BF224B903, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.ColorBlock
struct ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955 
{
public:
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_SelectedColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_SelectedColor_3;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_DisabledColor_4;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_5;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_6;

public:
	inline static int32_t get_offset_of_m_NormalColor_0() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_NormalColor_0)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_NormalColor_0() const { return ___m_NormalColor_0; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_NormalColor_0() { return &___m_NormalColor_0; }
	inline void set_m_NormalColor_0(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_NormalColor_0 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_1() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_HighlightedColor_1)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_HighlightedColor_1() const { return ___m_HighlightedColor_1; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_HighlightedColor_1() { return &___m_HighlightedColor_1; }
	inline void set_m_HighlightedColor_1(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_HighlightedColor_1 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_2() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_PressedColor_2)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_PressedColor_2() const { return ___m_PressedColor_2; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_PressedColor_2() { return &___m_PressedColor_2; }
	inline void set_m_PressedColor_2(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_PressedColor_2 = value;
	}

	inline static int32_t get_offset_of_m_SelectedColor_3() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_SelectedColor_3)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_SelectedColor_3() const { return ___m_SelectedColor_3; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_SelectedColor_3() { return &___m_SelectedColor_3; }
	inline void set_m_SelectedColor_3(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_SelectedColor_3 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_4() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_DisabledColor_4)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_DisabledColor_4() const { return ___m_DisabledColor_4; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_DisabledColor_4() { return &___m_DisabledColor_4; }
	inline void set_m_DisabledColor_4(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_DisabledColor_4 = value;
	}

	inline static int32_t get_offset_of_m_ColorMultiplier_5() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_ColorMultiplier_5)); }
	inline float get_m_ColorMultiplier_5() const { return ___m_ColorMultiplier_5; }
	inline float* get_address_of_m_ColorMultiplier_5() { return &___m_ColorMultiplier_5; }
	inline void set_m_ColorMultiplier_5(float value)
	{
		___m_ColorMultiplier_5 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_6() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_FadeDuration_6)); }
	inline float get_m_FadeDuration_6() const { return ___m_FadeDuration_6; }
	inline float* get_address_of_m_FadeDuration_6() { return &___m_FadeDuration_6; }
	inline void set_m_FadeDuration_6(float value)
	{
		___m_FadeDuration_6 = value;
	}
};

struct ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955_StaticFields
{
public:
	// UnityEngine.UI.ColorBlock UnityEngine.UI.ColorBlock::defaultColorBlock
	ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  ___defaultColorBlock_7;

public:
	inline static int32_t get_offset_of_defaultColorBlock_7() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955_StaticFields, ___defaultColorBlock_7)); }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  get_defaultColorBlock_7() const { return ___defaultColorBlock_7; }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955 * get_address_of_defaultColorBlock_7() { return &___defaultColorBlock_7; }
	inline void set_defaultColorBlock_7(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  value)
	{
		___defaultColorBlock_7 = value;
	}
};


// UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7  : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF
{
public:
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7_marshaled_pinvoke : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7_marshaled_com : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// UnityEngine.CursorLockMode
struct CursorLockMode_t247B41EE9632E4AD759EDADDB351AE0075162D04 
{
public:
	// System.Int32 UnityEngine.CursorLockMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CursorLockMode_t247B41EE9632E4AD759EDADDB351AE0075162D04, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Exception
struct Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};

// System.IO.FileAttributes
struct FileAttributes_t47DBB9A73CF80C7CA21C9AAB8D5336C92D32C1AE 
{
public:
	// System.Int32 System.IO.FileAttributes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FileAttributes_t47DBB9A73CF80C7CA21C9AAB8D5336C92D32C1AE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Int32Enum
struct Int32Enum_t9B63F771913F2B6D586F1173B44A41FBE26F6B5C 
{
public:
	// System.Int32 System.Int32Enum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Int32Enum_t9B63F771913F2B6D586F1173B44A41FBE26F6B5C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.RaycastHit
struct RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_Normal_1;
	// System.UInt32 UnityEngine.RaycastHit::m_FaceID
	uint32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_UV_4;
	// System.Int32 UnityEngine.RaycastHit::m_Collider
	int32_t ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89, ___m_Point_0)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89, ___m_Normal_1)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89, ___m_FaceID_2)); }
	inline uint32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline uint32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(uint32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89, ___m_UV_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89, ___m_Collider_5)); }
	inline int32_t get_m_Collider_5() const { return ___m_Collider_5; }
	inline int32_t* get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(int32_t value)
	{
		___m_Collider_5 = value;
	}
};


// ShipMove
struct ShipMove_t761409814A6DA69DE131EA554AEC499CB81D3953  : public ShipState_tF56B93F7FFD3E22AB4F4ECDF9A73D50A3FE508A5
{
public:
	// UnityEngine.Vector3 ShipMove::targetPos
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___targetPos_1;

public:
	inline static int32_t get_offset_of_targetPos_1() { return static_cast<int32_t>(offsetof(ShipMove_t761409814A6DA69DE131EA554AEC499CB81D3953, ___targetPos_1)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_targetPos_1() const { return ___targetPos_1; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_targetPos_1() { return &___targetPos_1; }
	inline void set_targetPos_1(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___targetPos_1 = value;
	}
};


// ShipWait
struct ShipWait_tB05589ED6695523DD921B075C449CE03E0B836FE  : public ShipState_tF56B93F7FFD3E22AB4F4ECDF9A73D50A3FE508A5
{
public:
	// UnityEngine.Vector2 ShipWait::waitTime
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___waitTime_1;
	// System.Single ShipWait::time
	float ___time_2;
	// System.Single ShipWait::timer
	float ___timer_3;

public:
	inline static int32_t get_offset_of_waitTime_1() { return static_cast<int32_t>(offsetof(ShipWait_tB05589ED6695523DD921B075C449CE03E0B836FE, ___waitTime_1)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_waitTime_1() const { return ___waitTime_1; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_waitTime_1() { return &___waitTime_1; }
	inline void set_waitTime_1(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___waitTime_1 = value;
	}

	inline static int32_t get_offset_of_time_2() { return static_cast<int32_t>(offsetof(ShipWait_tB05589ED6695523DD921B075C449CE03E0B836FE, ___time_2)); }
	inline float get_time_2() const { return ___time_2; }
	inline float* get_address_of_time_2() { return &___time_2; }
	inline void set_time_2(float value)
	{
		___time_2 = value;
	}

	inline static int32_t get_offset_of_timer_3() { return static_cast<int32_t>(offsetof(ShipWait_tB05589ED6695523DD921B075C449CE03E0B836FE, ___timer_3)); }
	inline float get_timer_3() const { return ___timer_3; }
	inline float* get_address_of_timer_3() { return &___timer_3; }
	inline void set_timer_3(float value)
	{
		___timer_3 = value;
	}
};


// System.TimeSpan
struct TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_3;

public:
	inline static int32_t get_offset_of__ticks_3() { return static_cast<int32_t>(offsetof(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203, ____ticks_3)); }
	inline int64_t get__ticks_3() const { return ____ticks_3; }
	inline int64_t* get_address_of__ticks_3() { return &____ticks_3; }
	inline void set__ticks_3(int64_t value)
	{
		____ticks_3 = value;
	}
};

struct TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  ___Zero_0;
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  ___MaxValue_1;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  ___MinValue_2;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyConfigChecked
	bool ____legacyConfigChecked_4;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyMode
	bool ____legacyMode_5;

public:
	inline static int32_t get_offset_of_Zero_0() { return static_cast<int32_t>(offsetof(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203_StaticFields, ___Zero_0)); }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  get_Zero_0() const { return ___Zero_0; }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203 * get_address_of_Zero_0() { return &___Zero_0; }
	inline void set_Zero_0(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  value)
	{
		___Zero_0 = value;
	}

	inline static int32_t get_offset_of_MaxValue_1() { return static_cast<int32_t>(offsetof(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203_StaticFields, ___MaxValue_1)); }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  get_MaxValue_1() const { return ___MaxValue_1; }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203 * get_address_of_MaxValue_1() { return &___MaxValue_1; }
	inline void set_MaxValue_1(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  value)
	{
		___MaxValue_1 = value;
	}

	inline static int32_t get_offset_of_MinValue_2() { return static_cast<int32_t>(offsetof(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203_StaticFields, ___MinValue_2)); }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  get_MinValue_2() const { return ___MinValue_2; }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203 * get_address_of_MinValue_2() { return &___MinValue_2; }
	inline void set_MinValue_2(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  value)
	{
		___MinValue_2 = value;
	}

	inline static int32_t get_offset_of__legacyConfigChecked_4() { return static_cast<int32_t>(offsetof(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203_StaticFields, ____legacyConfigChecked_4)); }
	inline bool get__legacyConfigChecked_4() const { return ____legacyConfigChecked_4; }
	inline bool* get_address_of__legacyConfigChecked_4() { return &____legacyConfigChecked_4; }
	inline void set__legacyConfigChecked_4(bool value)
	{
		____legacyConfigChecked_4 = value;
	}

	inline static int32_t get_offset_of__legacyMode_5() { return static_cast<int32_t>(offsetof(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203_StaticFields, ____legacyMode_5)); }
	inline bool get__legacyMode_5() const { return ____legacyMode_5; }
	inline bool* get_address_of__legacyMode_5() { return &____legacyMode_5; }
	inline void set__legacyMode_5(bool value)
	{
		____legacyMode_5 = value;
	}
};


// UnityEngine.UI.Image/FillMethod
struct FillMethod_tC37E5898D113A8FBF25A6AB6FBA451CC51E211E2 
{
public:
	// System.Int32 UnityEngine.UI.Image/FillMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FillMethod_tC37E5898D113A8FBF25A6AB6FBA451CC51E211E2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Image/Type
struct Type_tDCB08AB7425CAB70C1E46CC341F877423B5A5E12 
{
public:
	// System.Int32 UnityEngine.UI.Image/Type::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Type_tDCB08AB7425CAB70C1E46CC341F877423B5A5E12, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Key/KeyType
struct KeyType_t9EEB0AD19791311F275DCC1CE12BD7AAD39A3C13 
{
public:
	// System.Int32 Key/KeyType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(KeyType_t9EEB0AD19791311F275DCC1CE12BD7AAD39A3C13, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Navigation/Mode
struct Mode_t3113FDF05158BBA1DFC78D7F69E4C1D25135CB0F 
{
public:
	// System.Int32 UnityEngine.UI.Navigation/Mode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Mode_t3113FDF05158BBA1DFC78D7F69E4C1D25135CB0F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Selectable/Transition
struct Transition_t1FC449676815A798E758D32E8BE6DC0A2511DF14 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/Transition::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Transition_t1FC449676815A798E758D32E8BE6DC0A2511DF14, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// System.IO.MonoIOStat
struct MonoIOStat_t24C11A45B0B5F84242B31BA1EF48458595FF5F71 
{
public:
	// System.IO.FileAttributes System.IO.MonoIOStat::fileAttributes
	int32_t ___fileAttributes_0;
	// System.Int64 System.IO.MonoIOStat::Length
	int64_t ___Length_1;
	// System.Int64 System.IO.MonoIOStat::CreationTime
	int64_t ___CreationTime_2;
	// System.Int64 System.IO.MonoIOStat::LastAccessTime
	int64_t ___LastAccessTime_3;
	// System.Int64 System.IO.MonoIOStat::LastWriteTime
	int64_t ___LastWriteTime_4;

public:
	inline static int32_t get_offset_of_fileAttributes_0() { return static_cast<int32_t>(offsetof(MonoIOStat_t24C11A45B0B5F84242B31BA1EF48458595FF5F71, ___fileAttributes_0)); }
	inline int32_t get_fileAttributes_0() const { return ___fileAttributes_0; }
	inline int32_t* get_address_of_fileAttributes_0() { return &___fileAttributes_0; }
	inline void set_fileAttributes_0(int32_t value)
	{
		___fileAttributes_0 = value;
	}

	inline static int32_t get_offset_of_Length_1() { return static_cast<int32_t>(offsetof(MonoIOStat_t24C11A45B0B5F84242B31BA1EF48458595FF5F71, ___Length_1)); }
	inline int64_t get_Length_1() const { return ___Length_1; }
	inline int64_t* get_address_of_Length_1() { return &___Length_1; }
	inline void set_Length_1(int64_t value)
	{
		___Length_1 = value;
	}

	inline static int32_t get_offset_of_CreationTime_2() { return static_cast<int32_t>(offsetof(MonoIOStat_t24C11A45B0B5F84242B31BA1EF48458595FF5F71, ___CreationTime_2)); }
	inline int64_t get_CreationTime_2() const { return ___CreationTime_2; }
	inline int64_t* get_address_of_CreationTime_2() { return &___CreationTime_2; }
	inline void set_CreationTime_2(int64_t value)
	{
		___CreationTime_2 = value;
	}

	inline static int32_t get_offset_of_LastAccessTime_3() { return static_cast<int32_t>(offsetof(MonoIOStat_t24C11A45B0B5F84242B31BA1EF48458595FF5F71, ___LastAccessTime_3)); }
	inline int64_t get_LastAccessTime_3() const { return ___LastAccessTime_3; }
	inline int64_t* get_address_of_LastAccessTime_3() { return &___LastAccessTime_3; }
	inline void set_LastAccessTime_3(int64_t value)
	{
		___LastAccessTime_3 = value;
	}

	inline static int32_t get_offset_of_LastWriteTime_4() { return static_cast<int32_t>(offsetof(MonoIOStat_t24C11A45B0B5F84242B31BA1EF48458595FF5F71, ___LastWriteTime_4)); }
	inline int64_t get_LastWriteTime_4() const { return ___LastWriteTime_4; }
	inline int64_t* get_address_of_LastWriteTime_4() { return &___LastWriteTime_4; }
	inline void set_LastWriteTime_4(int64_t value)
	{
		___LastWriteTime_4 = value;
	}
};


// UnityEngine.UI.Navigation
struct Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A 
{
public:
	// UnityEngine.UI.Navigation/Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// System.Boolean UnityEngine.UI.Navigation::m_WrapAround
	bool ___m_WrapAround_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnUp_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnDown_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnLeft_4;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnRight_5;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_WrapAround_1() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_WrapAround_1)); }
	inline bool get_m_WrapAround_1() const { return ___m_WrapAround_1; }
	inline bool* get_address_of_m_WrapAround_1() { return &___m_WrapAround_1; }
	inline void set_m_WrapAround_1(bool value)
	{
		___m_WrapAround_1 = value;
	}

	inline static int32_t get_offset_of_m_SelectOnUp_2() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnUp_2)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnUp_2() const { return ___m_SelectOnUp_2; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnUp_2() { return &___m_SelectOnUp_2; }
	inline void set_m_SelectOnUp_2(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnUp_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnUp_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnDown_3() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnDown_3)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnDown_3() const { return ___m_SelectOnDown_3; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnDown_3() { return &___m_SelectOnDown_3; }
	inline void set_m_SelectOnDown_3(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnDown_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnDown_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnLeft_4() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnLeft_4)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnLeft_4() const { return ___m_SelectOnLeft_4; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnLeft_4() { return &___m_SelectOnLeft_4; }
	inline void set_m_SelectOnLeft_4(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnLeft_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnLeft_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnRight_5() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnRight_5)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnRight_5() const { return ___m_SelectOnRight_5; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnRight_5() { return &___m_SelectOnRight_5; }
	inline void set_m_SelectOnRight_5(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnRight_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnRight_5), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	int32_t ___m_WrapAround_1;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnUp_2;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnDown_3;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnLeft_4;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnRight_5;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A_marshaled_com
{
	int32_t ___m_Mode_0;
	int32_t ___m_WrapAround_1;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnUp_2;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnDown_3;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnLeft_4;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnRight_5;
};

// UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A_marshaled_pinvoke : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A_marshaled_com : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
};

// System.SystemException
struct SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62  : public Exception_t
{
public:

public:
};


// UnityEngine.Texture
struct Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};

struct Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE_StaticFields
{
public:
	// System.Int32 UnityEngine.Texture::GenerateAllMips
	int32_t ___GenerateAllMips_4;

public:
	inline static int32_t get_offset_of_GenerateAllMips_4() { return static_cast<int32_t>(offsetof(Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE_StaticFields, ___GenerateAllMips_4)); }
	inline int32_t get_GenerateAllMips_4() const { return ___GenerateAllMips_4; }
	inline int32_t* get_address_of_GenerateAllMips_4() { return &___GenerateAllMips_4; }
	inline void set_GenerateAllMips_4(int32_t value)
	{
		___GenerateAllMips_4 = value;
	}
};


// UnityEngine.Behaviour
struct Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Collider
struct Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// System.IO.FileSystemInfo
struct FileSystemInfo_t4479D65BB34DEAFCDA2A98F8B797D7C19EFDA246  : public MarshalByRefObject_tD4DF91B488B284F899417EC468D8E50E933306A8
{
public:
	// System.IO.MonoIOStat System.IO.FileSystemInfo::_data
	MonoIOStat_t24C11A45B0B5F84242B31BA1EF48458595FF5F71  ____data_1;
	// System.Int32 System.IO.FileSystemInfo::_dataInitialised
	int32_t ____dataInitialised_2;
	// System.String System.IO.FileSystemInfo::FullPath
	String_t* ___FullPath_3;
	// System.String System.IO.FileSystemInfo::OriginalPath
	String_t* ___OriginalPath_4;
	// System.String System.IO.FileSystemInfo::_displayPath
	String_t* ____displayPath_5;

public:
	inline static int32_t get_offset_of__data_1() { return static_cast<int32_t>(offsetof(FileSystemInfo_t4479D65BB34DEAFCDA2A98F8B797D7C19EFDA246, ____data_1)); }
	inline MonoIOStat_t24C11A45B0B5F84242B31BA1EF48458595FF5F71  get__data_1() const { return ____data_1; }
	inline MonoIOStat_t24C11A45B0B5F84242B31BA1EF48458595FF5F71 * get_address_of__data_1() { return &____data_1; }
	inline void set__data_1(MonoIOStat_t24C11A45B0B5F84242B31BA1EF48458595FF5F71  value)
	{
		____data_1 = value;
	}

	inline static int32_t get_offset_of__dataInitialised_2() { return static_cast<int32_t>(offsetof(FileSystemInfo_t4479D65BB34DEAFCDA2A98F8B797D7C19EFDA246, ____dataInitialised_2)); }
	inline int32_t get__dataInitialised_2() const { return ____dataInitialised_2; }
	inline int32_t* get_address_of__dataInitialised_2() { return &____dataInitialised_2; }
	inline void set__dataInitialised_2(int32_t value)
	{
		____dataInitialised_2 = value;
	}

	inline static int32_t get_offset_of_FullPath_3() { return static_cast<int32_t>(offsetof(FileSystemInfo_t4479D65BB34DEAFCDA2A98F8B797D7C19EFDA246, ___FullPath_3)); }
	inline String_t* get_FullPath_3() const { return ___FullPath_3; }
	inline String_t** get_address_of_FullPath_3() { return &___FullPath_3; }
	inline void set_FullPath_3(String_t* value)
	{
		___FullPath_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FullPath_3), (void*)value);
	}

	inline static int32_t get_offset_of_OriginalPath_4() { return static_cast<int32_t>(offsetof(FileSystemInfo_t4479D65BB34DEAFCDA2A98F8B797D7C19EFDA246, ___OriginalPath_4)); }
	inline String_t* get_OriginalPath_4() const { return ___OriginalPath_4; }
	inline String_t** get_address_of_OriginalPath_4() { return &___OriginalPath_4; }
	inline void set_OriginalPath_4(String_t* value)
	{
		___OriginalPath_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OriginalPath_4), (void*)value);
	}

	inline static int32_t get_offset_of__displayPath_5() { return static_cast<int32_t>(offsetof(FileSystemInfo_t4479D65BB34DEAFCDA2A98F8B797D7C19EFDA246, ____displayPath_5)); }
	inline String_t* get__displayPath_5() const { return ____displayPath_5; }
	inline String_t** get_address_of__displayPath_5() { return &____displayPath_5; }
	inline void set__displayPath_5(String_t* value)
	{
		____displayPath_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____displayPath_5), (void*)value);
	}
};


// System.NotImplementedException
struct NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6  : public SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62
{
public:

public:
};


// System.NotSupportedException
struct NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339  : public SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62
{
public:

public:
};


// UnityEngine.Playables.PlayableAsset
struct PlayableAsset_t5AD1606B76C9753A7F4C6B1061193F581023F137  : public ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A
{
public:

public:
};


// UnityEngine.Texture2D
struct Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF  : public Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE
{
public:

public:
};


// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Camera
struct Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};

struct Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields
{
public:
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * ___onPreCull_4;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * ___onPreRender_5;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * ___onPostRender_6;

public:
	inline static int32_t get_offset_of_onPreCull_4() { return static_cast<int32_t>(offsetof(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields, ___onPreCull_4)); }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * get_onPreCull_4() const { return ___onPreCull_4; }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D ** get_address_of_onPreCull_4() { return &___onPreCull_4; }
	inline void set_onPreCull_4(CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * value)
	{
		___onPreCull_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPreCull_4), (void*)value);
	}

	inline static int32_t get_offset_of_onPreRender_5() { return static_cast<int32_t>(offsetof(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields, ___onPreRender_5)); }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * get_onPreRender_5() const { return ___onPreRender_5; }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D ** get_address_of_onPreRender_5() { return &___onPreRender_5; }
	inline void set_onPreRender_5(CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * value)
	{
		___onPreRender_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPreRender_5), (void*)value);
	}

	inline static int32_t get_offset_of_onPostRender_6() { return static_cast<int32_t>(offsetof(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields, ___onPostRender_6)); }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * get_onPostRender_6() const { return ___onPostRender_6; }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D ** get_address_of_onPostRender_6() { return &___onPostRender_6; }
	inline void set_onPostRender_6(CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * value)
	{
		___onPostRender_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPostRender_6), (void*)value);
	}
};


// UnityEngine.CharacterController
struct CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E  : public Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02
{
public:

public:
};


// System.IO.DirectoryInfo
struct DirectoryInfo_t4EF3610F45F0D234800D01ADA8F3F476AE0CF5CD  : public FileSystemInfo_t4479D65BB34DEAFCDA2A98F8B797D7C19EFDA246
{
public:
	// System.String System.IO.DirectoryInfo::current
	String_t* ___current_6;
	// System.String System.IO.DirectoryInfo::parent
	String_t* ___parent_7;

public:
	inline static int32_t get_offset_of_current_6() { return static_cast<int32_t>(offsetof(DirectoryInfo_t4EF3610F45F0D234800D01ADA8F3F476AE0CF5CD, ___current_6)); }
	inline String_t* get_current_6() const { return ___current_6; }
	inline String_t** get_address_of_current_6() { return &___current_6; }
	inline void set_current_6(String_t* value)
	{
		___current_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_6), (void*)value);
	}

	inline static int32_t get_offset_of_parent_7() { return static_cast<int32_t>(offsetof(DirectoryInfo_t4EF3610F45F0D234800D01ADA8F3F476AE0CF5CD, ___parent_7)); }
	inline String_t* get_parent_7() const { return ___parent_7; }
	inline String_t** get_address_of_parent_7() { return &___parent_7; }
	inline void set_parent_7(String_t* value)
	{
		___parent_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___parent_7), (void*)value);
	}
};


// System.IO.FileInfo
struct FileInfo_t6C8B2EAA1E23F9E6D7C287C58E4EEEB2049ABAB9  : public FileSystemInfo_t4479D65BB34DEAFCDA2A98F8B797D7C19EFDA246
{
public:
	// System.String System.IO.FileInfo::_name
	String_t* ____name_6;

public:
	inline static int32_t get_offset_of__name_6() { return static_cast<int32_t>(offsetof(FileInfo_t6C8B2EAA1E23F9E6D7C287C58E4EEEB2049ABAB9, ____name_6)); }
	inline String_t* get__name_6() const { return ____name_6; }
	inline String_t** get_address_of__name_6() { return &____name_6; }
	inline void set__name_6(String_t* value)
	{
		____name_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____name_6), (void*)value);
	}
};


// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// UnityEngine.AI.NavMeshAgent
struct NavMeshAgent_tB9746B6C38013341DB63973CA7ED657494EFB41B  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// UnityEngine.Playables.PlayableDirector
struct PlayableDirector_t63912C98A4EC9D4701E88B633E27E0D97209BA38  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:
	// System.Action`1<UnityEngine.Playables.PlayableDirector> UnityEngine.Playables.PlayableDirector::played
	Action_1_tEFFD33105460921F68C4261E79791D397C76CB1B * ___played_4;
	// System.Action`1<UnityEngine.Playables.PlayableDirector> UnityEngine.Playables.PlayableDirector::paused
	Action_1_tEFFD33105460921F68C4261E79791D397C76CB1B * ___paused_5;
	// System.Action`1<UnityEngine.Playables.PlayableDirector> UnityEngine.Playables.PlayableDirector::stopped
	Action_1_tEFFD33105460921F68C4261E79791D397C76CB1B * ___stopped_6;

public:
	inline static int32_t get_offset_of_played_4() { return static_cast<int32_t>(offsetof(PlayableDirector_t63912C98A4EC9D4701E88B633E27E0D97209BA38, ___played_4)); }
	inline Action_1_tEFFD33105460921F68C4261E79791D397C76CB1B * get_played_4() const { return ___played_4; }
	inline Action_1_tEFFD33105460921F68C4261E79791D397C76CB1B ** get_address_of_played_4() { return &___played_4; }
	inline void set_played_4(Action_1_tEFFD33105460921F68C4261E79791D397C76CB1B * value)
	{
		___played_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___played_4), (void*)value);
	}

	inline static int32_t get_offset_of_paused_5() { return static_cast<int32_t>(offsetof(PlayableDirector_t63912C98A4EC9D4701E88B633E27E0D97209BA38, ___paused_5)); }
	inline Action_1_tEFFD33105460921F68C4261E79791D397C76CB1B * get_paused_5() const { return ___paused_5; }
	inline Action_1_tEFFD33105460921F68C4261E79791D397C76CB1B ** get_address_of_paused_5() { return &___paused_5; }
	inline void set_paused_5(Action_1_tEFFD33105460921F68C4261E79791D397C76CB1B * value)
	{
		___paused_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___paused_5), (void*)value);
	}

	inline static int32_t get_offset_of_stopped_6() { return static_cast<int32_t>(offsetof(PlayableDirector_t63912C98A4EC9D4701E88B633E27E0D97209BA38, ___stopped_6)); }
	inline Action_1_tEFFD33105460921F68C4261E79791D397C76CB1B * get_stopped_6() const { return ___stopped_6; }
	inline Action_1_tEFFD33105460921F68C4261E79791D397C76CB1B ** get_address_of_stopped_6() { return &___stopped_6; }
	inline void set_stopped_6(Action_1_tEFFD33105460921F68C4261E79791D397C76CB1B * value)
	{
		___stopped_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stopped_6), (void*)value);
	}
};


// ActionMenu
struct ActionMenu_tE3C589CB5B0B532FF5504CF0D558D489281BED01  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.UI.Image[] ActionMenu::menuOptions
	ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224* ___menuOptions_4;
	// System.Int32 ActionMenu::selection
	int32_t ___selection_6;
	// System.Single ActionMenu::timer
	float ___timer_7;

public:
	inline static int32_t get_offset_of_menuOptions_4() { return static_cast<int32_t>(offsetof(ActionMenu_tE3C589CB5B0B532FF5504CF0D558D489281BED01, ___menuOptions_4)); }
	inline ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224* get_menuOptions_4() const { return ___menuOptions_4; }
	inline ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224** get_address_of_menuOptions_4() { return &___menuOptions_4; }
	inline void set_menuOptions_4(ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224* value)
	{
		___menuOptions_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___menuOptions_4), (void*)value);
	}

	inline static int32_t get_offset_of_selection_6() { return static_cast<int32_t>(offsetof(ActionMenu_tE3C589CB5B0B532FF5504CF0D558D489281BED01, ___selection_6)); }
	inline int32_t get_selection_6() const { return ___selection_6; }
	inline int32_t* get_address_of_selection_6() { return &___selection_6; }
	inline void set_selection_6(int32_t value)
	{
		___selection_6 = value;
	}

	inline static int32_t get_offset_of_timer_7() { return static_cast<int32_t>(offsetof(ActionMenu_tE3C589CB5B0B532FF5504CF0D558D489281BED01, ___timer_7)); }
	inline float get_timer_7() const { return ___timer_7; }
	inline float* get_address_of_timer_7() { return &___timer_7; }
	inline void set_timer_7(float value)
	{
		___timer_7 = value;
	}
};

struct ActionMenu_tE3C589CB5B0B532FF5504CF0D558D489281BED01_StaticFields
{
public:
	// ActionMenu ActionMenu::<Instance>k__BackingField
	ActionMenu_tE3C589CB5B0B532FF5504CF0D558D489281BED01 * ___U3CInstanceU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(ActionMenu_tE3C589CB5B0B532FF5504CF0D558D489281BED01_StaticFields, ___U3CInstanceU3Ek__BackingField_5)); }
	inline ActionMenu_tE3C589CB5B0B532FF5504CF0D558D489281BED01 * get_U3CInstanceU3Ek__BackingField_5() const { return ___U3CInstanceU3Ek__BackingField_5; }
	inline ActionMenu_tE3C589CB5B0B532FF5504CF0D558D489281BED01 ** get_address_of_U3CInstanceU3Ek__BackingField_5() { return &___U3CInstanceU3Ek__BackingField_5; }
	inline void set_U3CInstanceU3Ek__BackingField_5(ActionMenu_tE3C589CB5B0B532FF5504CF0D558D489281BED01 * value)
	{
		___U3CInstanceU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CInstanceU3Ek__BackingField_5), (void*)value);
	}
};


// Actions
struct Actions_tF16D3C582B8D8DF5DD96B758A4D691F304BE44BB  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single Actions::boostMultiplier
	float ___boostMultiplier_4;
	// System.Single Actions::boostTime
	float ___boostTime_5;
	// System.Single Actions::sonarTime
	float ___sonarTime_6;
	// System.Single Actions::sonarRadius
	float ___sonarRadius_7;
	// System.Int32 Actions::iconLimit
	int32_t ___iconLimit_8;
	// UnityEngine.LayerMask Actions::interactionLayer
	LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  ___interactionLayer_9;
	// UnityEngine.GameObject Actions::sonarIconPrefab
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___sonarIconPrefab_10;
	// System.Single Actions::sonarTimer
	float ___sonarTimer_12;
	// System.Single Actions::boostTimer
	float ___boostTimer_13;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> Actions::spawnedSonarIcons
	List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * ___spawnedSonarIcons_14;

public:
	inline static int32_t get_offset_of_boostMultiplier_4() { return static_cast<int32_t>(offsetof(Actions_tF16D3C582B8D8DF5DD96B758A4D691F304BE44BB, ___boostMultiplier_4)); }
	inline float get_boostMultiplier_4() const { return ___boostMultiplier_4; }
	inline float* get_address_of_boostMultiplier_4() { return &___boostMultiplier_4; }
	inline void set_boostMultiplier_4(float value)
	{
		___boostMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_boostTime_5() { return static_cast<int32_t>(offsetof(Actions_tF16D3C582B8D8DF5DD96B758A4D691F304BE44BB, ___boostTime_5)); }
	inline float get_boostTime_5() const { return ___boostTime_5; }
	inline float* get_address_of_boostTime_5() { return &___boostTime_5; }
	inline void set_boostTime_5(float value)
	{
		___boostTime_5 = value;
	}

	inline static int32_t get_offset_of_sonarTime_6() { return static_cast<int32_t>(offsetof(Actions_tF16D3C582B8D8DF5DD96B758A4D691F304BE44BB, ___sonarTime_6)); }
	inline float get_sonarTime_6() const { return ___sonarTime_6; }
	inline float* get_address_of_sonarTime_6() { return &___sonarTime_6; }
	inline void set_sonarTime_6(float value)
	{
		___sonarTime_6 = value;
	}

	inline static int32_t get_offset_of_sonarRadius_7() { return static_cast<int32_t>(offsetof(Actions_tF16D3C582B8D8DF5DD96B758A4D691F304BE44BB, ___sonarRadius_7)); }
	inline float get_sonarRadius_7() const { return ___sonarRadius_7; }
	inline float* get_address_of_sonarRadius_7() { return &___sonarRadius_7; }
	inline void set_sonarRadius_7(float value)
	{
		___sonarRadius_7 = value;
	}

	inline static int32_t get_offset_of_iconLimit_8() { return static_cast<int32_t>(offsetof(Actions_tF16D3C582B8D8DF5DD96B758A4D691F304BE44BB, ___iconLimit_8)); }
	inline int32_t get_iconLimit_8() const { return ___iconLimit_8; }
	inline int32_t* get_address_of_iconLimit_8() { return &___iconLimit_8; }
	inline void set_iconLimit_8(int32_t value)
	{
		___iconLimit_8 = value;
	}

	inline static int32_t get_offset_of_interactionLayer_9() { return static_cast<int32_t>(offsetof(Actions_tF16D3C582B8D8DF5DD96B758A4D691F304BE44BB, ___interactionLayer_9)); }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  get_interactionLayer_9() const { return ___interactionLayer_9; }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 * get_address_of_interactionLayer_9() { return &___interactionLayer_9; }
	inline void set_interactionLayer_9(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  value)
	{
		___interactionLayer_9 = value;
	}

	inline static int32_t get_offset_of_sonarIconPrefab_10() { return static_cast<int32_t>(offsetof(Actions_tF16D3C582B8D8DF5DD96B758A4D691F304BE44BB, ___sonarIconPrefab_10)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_sonarIconPrefab_10() const { return ___sonarIconPrefab_10; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_sonarIconPrefab_10() { return &___sonarIconPrefab_10; }
	inline void set_sonarIconPrefab_10(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___sonarIconPrefab_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sonarIconPrefab_10), (void*)value);
	}

	inline static int32_t get_offset_of_sonarTimer_12() { return static_cast<int32_t>(offsetof(Actions_tF16D3C582B8D8DF5DD96B758A4D691F304BE44BB, ___sonarTimer_12)); }
	inline float get_sonarTimer_12() const { return ___sonarTimer_12; }
	inline float* get_address_of_sonarTimer_12() { return &___sonarTimer_12; }
	inline void set_sonarTimer_12(float value)
	{
		___sonarTimer_12 = value;
	}

	inline static int32_t get_offset_of_boostTimer_13() { return static_cast<int32_t>(offsetof(Actions_tF16D3C582B8D8DF5DD96B758A4D691F304BE44BB, ___boostTimer_13)); }
	inline float get_boostTimer_13() const { return ___boostTimer_13; }
	inline float* get_address_of_boostTimer_13() { return &___boostTimer_13; }
	inline void set_boostTimer_13(float value)
	{
		___boostTimer_13 = value;
	}

	inline static int32_t get_offset_of_spawnedSonarIcons_14() { return static_cast<int32_t>(offsetof(Actions_tF16D3C582B8D8DF5DD96B758A4D691F304BE44BB, ___spawnedSonarIcons_14)); }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * get_spawnedSonarIcons_14() const { return ___spawnedSonarIcons_14; }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 ** get_address_of_spawnedSonarIcons_14() { return &___spawnedSonarIcons_14; }
	inline void set_spawnedSonarIcons_14(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * value)
	{
		___spawnedSonarIcons_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___spawnedSonarIcons_14), (void*)value);
	}
};

struct Actions_tF16D3C582B8D8DF5DD96B758A4D691F304BE44BB_StaticFields
{
public:
	// Actions Actions::<Instance>k__BackingField
	Actions_tF16D3C582B8D8DF5DD96B758A4D691F304BE44BB * ___U3CInstanceU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(Actions_tF16D3C582B8D8DF5DD96B758A4D691F304BE44BB_StaticFields, ___U3CInstanceU3Ek__BackingField_11)); }
	inline Actions_tF16D3C582B8D8DF5DD96B758A4D691F304BE44BB * get_U3CInstanceU3Ek__BackingField_11() const { return ___U3CInstanceU3Ek__BackingField_11; }
	inline Actions_tF16D3C582B8D8DF5DD96B758A4D691F304BE44BB ** get_address_of_U3CInstanceU3Ek__BackingField_11() { return &___U3CInstanceU3Ek__BackingField_11; }
	inline void set_U3CInstanceU3Ek__BackingField_11(Actions_tF16D3C582B8D8DF5DD96B758A4D691F304BE44BB * value)
	{
		___U3CInstanceU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CInstanceU3Ek__BackingField_11), (void*)value);
	}
};


// Fish
struct Fish_t45F7EC0A7EE404E12CC412CF93723EA8799CA46C  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.String Fish::tooltipText
	String_t* ___tooltipText_4;

public:
	inline static int32_t get_offset_of_tooltipText_4() { return static_cast<int32_t>(offsetof(Fish_t45F7EC0A7EE404E12CC412CF93723EA8799CA46C, ___tooltipText_4)); }
	inline String_t* get_tooltipText_4() const { return ___tooltipText_4; }
	inline String_t** get_address_of_tooltipText_4() { return &___tooltipText_4; }
	inline void set_tooltipText_4(String_t* value)
	{
		___tooltipText_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tooltipText_4), (void*)value);
	}
};


// FlockManager
struct FlockManager_t241CFB1B2E6593247EFA52A9E2CB2A111400F1F8  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// FlockAgent FlockManager::agentPrefab
	FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04 * ___agentPrefab_4;
	// System.Int32 FlockManager::size
	int32_t ___size_5;
	// UnityEngine.Vector3 FlockManager::spawnBounds
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___spawnBounds_6;
	// UnityEngine.Vector2 FlockManager::speed
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___speed_7;
	// System.Single FlockManager::cohesionDistance
	float ___cohesionDistance_8;
	// System.Single FlockManager::separationDistance
	float ___separationDistance_9;
	// System.Single FlockManager::alignmentDistance
	float ___alignmentDistance_10;
	// System.Single FlockManager::boundsDistance
	float ___boundsDistance_11;
	// System.Single FlockManager::obstacleDistance
	float ___obstacleDistance_12;
	// System.Single FlockManager::cohesion
	float ___cohesion_13;
	// System.Single FlockManager::separation
	float ___separation_14;
	// System.Single FlockManager::alignment
	float ___alignment_15;
	// System.Single FlockManager::bounds
	float ___bounds_16;
	// System.Single FlockManager::avoidance
	float ___avoidance_17;
	// FlockAgent[] FlockManager::<SpawnedAgents>k__BackingField
	FlockAgentU5BU5D_t012E3C58876A70268E6BFEDBF651E5B8D23AAF84* ___U3CSpawnedAgentsU3Ek__BackingField_18;

public:
	inline static int32_t get_offset_of_agentPrefab_4() { return static_cast<int32_t>(offsetof(FlockManager_t241CFB1B2E6593247EFA52A9E2CB2A111400F1F8, ___agentPrefab_4)); }
	inline FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04 * get_agentPrefab_4() const { return ___agentPrefab_4; }
	inline FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04 ** get_address_of_agentPrefab_4() { return &___agentPrefab_4; }
	inline void set_agentPrefab_4(FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04 * value)
	{
		___agentPrefab_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___agentPrefab_4), (void*)value);
	}

	inline static int32_t get_offset_of_size_5() { return static_cast<int32_t>(offsetof(FlockManager_t241CFB1B2E6593247EFA52A9E2CB2A111400F1F8, ___size_5)); }
	inline int32_t get_size_5() const { return ___size_5; }
	inline int32_t* get_address_of_size_5() { return &___size_5; }
	inline void set_size_5(int32_t value)
	{
		___size_5 = value;
	}

	inline static int32_t get_offset_of_spawnBounds_6() { return static_cast<int32_t>(offsetof(FlockManager_t241CFB1B2E6593247EFA52A9E2CB2A111400F1F8, ___spawnBounds_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_spawnBounds_6() const { return ___spawnBounds_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_spawnBounds_6() { return &___spawnBounds_6; }
	inline void set_spawnBounds_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___spawnBounds_6 = value;
	}

	inline static int32_t get_offset_of_speed_7() { return static_cast<int32_t>(offsetof(FlockManager_t241CFB1B2E6593247EFA52A9E2CB2A111400F1F8, ___speed_7)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_speed_7() const { return ___speed_7; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_speed_7() { return &___speed_7; }
	inline void set_speed_7(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___speed_7 = value;
	}

	inline static int32_t get_offset_of_cohesionDistance_8() { return static_cast<int32_t>(offsetof(FlockManager_t241CFB1B2E6593247EFA52A9E2CB2A111400F1F8, ___cohesionDistance_8)); }
	inline float get_cohesionDistance_8() const { return ___cohesionDistance_8; }
	inline float* get_address_of_cohesionDistance_8() { return &___cohesionDistance_8; }
	inline void set_cohesionDistance_8(float value)
	{
		___cohesionDistance_8 = value;
	}

	inline static int32_t get_offset_of_separationDistance_9() { return static_cast<int32_t>(offsetof(FlockManager_t241CFB1B2E6593247EFA52A9E2CB2A111400F1F8, ___separationDistance_9)); }
	inline float get_separationDistance_9() const { return ___separationDistance_9; }
	inline float* get_address_of_separationDistance_9() { return &___separationDistance_9; }
	inline void set_separationDistance_9(float value)
	{
		___separationDistance_9 = value;
	}

	inline static int32_t get_offset_of_alignmentDistance_10() { return static_cast<int32_t>(offsetof(FlockManager_t241CFB1B2E6593247EFA52A9E2CB2A111400F1F8, ___alignmentDistance_10)); }
	inline float get_alignmentDistance_10() const { return ___alignmentDistance_10; }
	inline float* get_address_of_alignmentDistance_10() { return &___alignmentDistance_10; }
	inline void set_alignmentDistance_10(float value)
	{
		___alignmentDistance_10 = value;
	}

	inline static int32_t get_offset_of_boundsDistance_11() { return static_cast<int32_t>(offsetof(FlockManager_t241CFB1B2E6593247EFA52A9E2CB2A111400F1F8, ___boundsDistance_11)); }
	inline float get_boundsDistance_11() const { return ___boundsDistance_11; }
	inline float* get_address_of_boundsDistance_11() { return &___boundsDistance_11; }
	inline void set_boundsDistance_11(float value)
	{
		___boundsDistance_11 = value;
	}

	inline static int32_t get_offset_of_obstacleDistance_12() { return static_cast<int32_t>(offsetof(FlockManager_t241CFB1B2E6593247EFA52A9E2CB2A111400F1F8, ___obstacleDistance_12)); }
	inline float get_obstacleDistance_12() const { return ___obstacleDistance_12; }
	inline float* get_address_of_obstacleDistance_12() { return &___obstacleDistance_12; }
	inline void set_obstacleDistance_12(float value)
	{
		___obstacleDistance_12 = value;
	}

	inline static int32_t get_offset_of_cohesion_13() { return static_cast<int32_t>(offsetof(FlockManager_t241CFB1B2E6593247EFA52A9E2CB2A111400F1F8, ___cohesion_13)); }
	inline float get_cohesion_13() const { return ___cohesion_13; }
	inline float* get_address_of_cohesion_13() { return &___cohesion_13; }
	inline void set_cohesion_13(float value)
	{
		___cohesion_13 = value;
	}

	inline static int32_t get_offset_of_separation_14() { return static_cast<int32_t>(offsetof(FlockManager_t241CFB1B2E6593247EFA52A9E2CB2A111400F1F8, ___separation_14)); }
	inline float get_separation_14() const { return ___separation_14; }
	inline float* get_address_of_separation_14() { return &___separation_14; }
	inline void set_separation_14(float value)
	{
		___separation_14 = value;
	}

	inline static int32_t get_offset_of_alignment_15() { return static_cast<int32_t>(offsetof(FlockManager_t241CFB1B2E6593247EFA52A9E2CB2A111400F1F8, ___alignment_15)); }
	inline float get_alignment_15() const { return ___alignment_15; }
	inline float* get_address_of_alignment_15() { return &___alignment_15; }
	inline void set_alignment_15(float value)
	{
		___alignment_15 = value;
	}

	inline static int32_t get_offset_of_bounds_16() { return static_cast<int32_t>(offsetof(FlockManager_t241CFB1B2E6593247EFA52A9E2CB2A111400F1F8, ___bounds_16)); }
	inline float get_bounds_16() const { return ___bounds_16; }
	inline float* get_address_of_bounds_16() { return &___bounds_16; }
	inline void set_bounds_16(float value)
	{
		___bounds_16 = value;
	}

	inline static int32_t get_offset_of_avoidance_17() { return static_cast<int32_t>(offsetof(FlockManager_t241CFB1B2E6593247EFA52A9E2CB2A111400F1F8, ___avoidance_17)); }
	inline float get_avoidance_17() const { return ___avoidance_17; }
	inline float* get_address_of_avoidance_17() { return &___avoidance_17; }
	inline void set_avoidance_17(float value)
	{
		___avoidance_17 = value;
	}

	inline static int32_t get_offset_of_U3CSpawnedAgentsU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(FlockManager_t241CFB1B2E6593247EFA52A9E2CB2A111400F1F8, ___U3CSpawnedAgentsU3Ek__BackingField_18)); }
	inline FlockAgentU5BU5D_t012E3C58876A70268E6BFEDBF651E5B8D23AAF84* get_U3CSpawnedAgentsU3Ek__BackingField_18() const { return ___U3CSpawnedAgentsU3Ek__BackingField_18; }
	inline FlockAgentU5BU5D_t012E3C58876A70268E6BFEDBF651E5B8D23AAF84** get_address_of_U3CSpawnedAgentsU3Ek__BackingField_18() { return &___U3CSpawnedAgentsU3Ek__BackingField_18; }
	inline void set_U3CSpawnedAgentsU3Ek__BackingField_18(FlockAgentU5BU5D_t012E3C58876A70268E6BFEDBF651E5B8D23AAF84* value)
	{
		___U3CSpawnedAgentsU3Ek__BackingField_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CSpawnedAgentsU3Ek__BackingField_18), (void*)value);
	}
};


// GameManager
struct GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Collections.Generic.List`1<UnityEngine.GameObject> GameManager::fishPrefabs
	List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * ___fishPrefabs_4;
	// System.Collections.Generic.List`1<UnityEngine.Transform> GameManager::ZoneASpawns
	List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * ___ZoneASpawns_5;
	// System.Collections.Generic.List`1<UnityEngine.Transform> GameManager::ZoneBSpawns
	List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * ___ZoneBSpawns_6;
	// System.Collections.Generic.List`1<UnityEngine.Transform> GameManager::ZoneCSpawns
	List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * ___ZoneCSpawns_7;
	// UnityEngine.UI.Text GameManager::targetText
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___targetText_8;
	// UnityEngine.Camera GameManager::cutSceneCamera
	Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ___cutSceneCamera_9;
	// UnityEngine.Vector3 GameManager::cameraOffset
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___cameraOffset_10;
	// UnityEngine.Playables.PlayableDirector GameManager::cutsceneDirector
	PlayableDirector_t63912C98A4EC9D4701E88B633E27E0D97209BA38 * ___cutsceneDirector_11;
	// UnityEngine.Playables.PlayableAsset GameManager::introCutScene
	PlayableAsset_t5AD1606B76C9753A7F4C6B1061193F581023F137 * ___introCutScene_12;
	// UnityEngine.Playables.PlayableAsset GameManager::gateACutScene
	PlayableAsset_t5AD1606B76C9753A7F4C6B1061193F581023F137 * ___gateACutScene_13;
	// UnityEngine.Playables.PlayableAsset GameManager::gateBCutScene
	PlayableAsset_t5AD1606B76C9753A7F4C6B1061193F581023F137 * ___gateBCutScene_14;
	// UnityEngine.Playables.PlayableAsset GameManager::targetFishCutScene
	PlayableAsset_t5AD1606B76C9753A7F4C6B1061193F581023F137 * ___targetFishCutScene_15;
	// System.Boolean GameManager::<TargetFound>k__BackingField
	bool ___U3CTargetFoundU3Ek__BackingField_18;
	// System.Boolean GameManager::<OrbitCameraTarget>k__BackingField
	bool ___U3COrbitCameraTargetU3Ek__BackingField_19;
	// System.Boolean GameManager::<OrbitCameraTarget2>k__BackingField
	bool ___U3COrbitCameraTarget2U3Ek__BackingField_20;
	// System.Boolean GameManager::<PanToCameraTarget>k__BackingField
	bool ___U3CPanToCameraTargetU3Ek__BackingField_21;
	// UnityEngine.Transform GameManager::<CameraTarget>k__BackingField
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___U3CCameraTargetU3Ek__BackingField_22;

public:
	inline static int32_t get_offset_of_fishPrefabs_4() { return static_cast<int32_t>(offsetof(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1, ___fishPrefabs_4)); }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * get_fishPrefabs_4() const { return ___fishPrefabs_4; }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 ** get_address_of_fishPrefabs_4() { return &___fishPrefabs_4; }
	inline void set_fishPrefabs_4(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * value)
	{
		___fishPrefabs_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fishPrefabs_4), (void*)value);
	}

	inline static int32_t get_offset_of_ZoneASpawns_5() { return static_cast<int32_t>(offsetof(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1, ___ZoneASpawns_5)); }
	inline List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * get_ZoneASpawns_5() const { return ___ZoneASpawns_5; }
	inline List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 ** get_address_of_ZoneASpawns_5() { return &___ZoneASpawns_5; }
	inline void set_ZoneASpawns_5(List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * value)
	{
		___ZoneASpawns_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ZoneASpawns_5), (void*)value);
	}

	inline static int32_t get_offset_of_ZoneBSpawns_6() { return static_cast<int32_t>(offsetof(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1, ___ZoneBSpawns_6)); }
	inline List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * get_ZoneBSpawns_6() const { return ___ZoneBSpawns_6; }
	inline List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 ** get_address_of_ZoneBSpawns_6() { return &___ZoneBSpawns_6; }
	inline void set_ZoneBSpawns_6(List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * value)
	{
		___ZoneBSpawns_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ZoneBSpawns_6), (void*)value);
	}

	inline static int32_t get_offset_of_ZoneCSpawns_7() { return static_cast<int32_t>(offsetof(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1, ___ZoneCSpawns_7)); }
	inline List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * get_ZoneCSpawns_7() const { return ___ZoneCSpawns_7; }
	inline List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 ** get_address_of_ZoneCSpawns_7() { return &___ZoneCSpawns_7; }
	inline void set_ZoneCSpawns_7(List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * value)
	{
		___ZoneCSpawns_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ZoneCSpawns_7), (void*)value);
	}

	inline static int32_t get_offset_of_targetText_8() { return static_cast<int32_t>(offsetof(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1, ___targetText_8)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_targetText_8() const { return ___targetText_8; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_targetText_8() { return &___targetText_8; }
	inline void set_targetText_8(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___targetText_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___targetText_8), (void*)value);
	}

	inline static int32_t get_offset_of_cutSceneCamera_9() { return static_cast<int32_t>(offsetof(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1, ___cutSceneCamera_9)); }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * get_cutSceneCamera_9() const { return ___cutSceneCamera_9; }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C ** get_address_of_cutSceneCamera_9() { return &___cutSceneCamera_9; }
	inline void set_cutSceneCamera_9(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * value)
	{
		___cutSceneCamera_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cutSceneCamera_9), (void*)value);
	}

	inline static int32_t get_offset_of_cameraOffset_10() { return static_cast<int32_t>(offsetof(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1, ___cameraOffset_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_cameraOffset_10() const { return ___cameraOffset_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_cameraOffset_10() { return &___cameraOffset_10; }
	inline void set_cameraOffset_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___cameraOffset_10 = value;
	}

	inline static int32_t get_offset_of_cutsceneDirector_11() { return static_cast<int32_t>(offsetof(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1, ___cutsceneDirector_11)); }
	inline PlayableDirector_t63912C98A4EC9D4701E88B633E27E0D97209BA38 * get_cutsceneDirector_11() const { return ___cutsceneDirector_11; }
	inline PlayableDirector_t63912C98A4EC9D4701E88B633E27E0D97209BA38 ** get_address_of_cutsceneDirector_11() { return &___cutsceneDirector_11; }
	inline void set_cutsceneDirector_11(PlayableDirector_t63912C98A4EC9D4701E88B633E27E0D97209BA38 * value)
	{
		___cutsceneDirector_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cutsceneDirector_11), (void*)value);
	}

	inline static int32_t get_offset_of_introCutScene_12() { return static_cast<int32_t>(offsetof(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1, ___introCutScene_12)); }
	inline PlayableAsset_t5AD1606B76C9753A7F4C6B1061193F581023F137 * get_introCutScene_12() const { return ___introCutScene_12; }
	inline PlayableAsset_t5AD1606B76C9753A7F4C6B1061193F581023F137 ** get_address_of_introCutScene_12() { return &___introCutScene_12; }
	inline void set_introCutScene_12(PlayableAsset_t5AD1606B76C9753A7F4C6B1061193F581023F137 * value)
	{
		___introCutScene_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___introCutScene_12), (void*)value);
	}

	inline static int32_t get_offset_of_gateACutScene_13() { return static_cast<int32_t>(offsetof(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1, ___gateACutScene_13)); }
	inline PlayableAsset_t5AD1606B76C9753A7F4C6B1061193F581023F137 * get_gateACutScene_13() const { return ___gateACutScene_13; }
	inline PlayableAsset_t5AD1606B76C9753A7F4C6B1061193F581023F137 ** get_address_of_gateACutScene_13() { return &___gateACutScene_13; }
	inline void set_gateACutScene_13(PlayableAsset_t5AD1606B76C9753A7F4C6B1061193F581023F137 * value)
	{
		___gateACutScene_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gateACutScene_13), (void*)value);
	}

	inline static int32_t get_offset_of_gateBCutScene_14() { return static_cast<int32_t>(offsetof(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1, ___gateBCutScene_14)); }
	inline PlayableAsset_t5AD1606B76C9753A7F4C6B1061193F581023F137 * get_gateBCutScene_14() const { return ___gateBCutScene_14; }
	inline PlayableAsset_t5AD1606B76C9753A7F4C6B1061193F581023F137 ** get_address_of_gateBCutScene_14() { return &___gateBCutScene_14; }
	inline void set_gateBCutScene_14(PlayableAsset_t5AD1606B76C9753A7F4C6B1061193F581023F137 * value)
	{
		___gateBCutScene_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gateBCutScene_14), (void*)value);
	}

	inline static int32_t get_offset_of_targetFishCutScene_15() { return static_cast<int32_t>(offsetof(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1, ___targetFishCutScene_15)); }
	inline PlayableAsset_t5AD1606B76C9753A7F4C6B1061193F581023F137 * get_targetFishCutScene_15() const { return ___targetFishCutScene_15; }
	inline PlayableAsset_t5AD1606B76C9753A7F4C6B1061193F581023F137 ** get_address_of_targetFishCutScene_15() { return &___targetFishCutScene_15; }
	inline void set_targetFishCutScene_15(PlayableAsset_t5AD1606B76C9753A7F4C6B1061193F581023F137 * value)
	{
		___targetFishCutScene_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___targetFishCutScene_15), (void*)value);
	}

	inline static int32_t get_offset_of_U3CTargetFoundU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1, ___U3CTargetFoundU3Ek__BackingField_18)); }
	inline bool get_U3CTargetFoundU3Ek__BackingField_18() const { return ___U3CTargetFoundU3Ek__BackingField_18; }
	inline bool* get_address_of_U3CTargetFoundU3Ek__BackingField_18() { return &___U3CTargetFoundU3Ek__BackingField_18; }
	inline void set_U3CTargetFoundU3Ek__BackingField_18(bool value)
	{
		___U3CTargetFoundU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_U3COrbitCameraTargetU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1, ___U3COrbitCameraTargetU3Ek__BackingField_19)); }
	inline bool get_U3COrbitCameraTargetU3Ek__BackingField_19() const { return ___U3COrbitCameraTargetU3Ek__BackingField_19; }
	inline bool* get_address_of_U3COrbitCameraTargetU3Ek__BackingField_19() { return &___U3COrbitCameraTargetU3Ek__BackingField_19; }
	inline void set_U3COrbitCameraTargetU3Ek__BackingField_19(bool value)
	{
		___U3COrbitCameraTargetU3Ek__BackingField_19 = value;
	}

	inline static int32_t get_offset_of_U3COrbitCameraTarget2U3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1, ___U3COrbitCameraTarget2U3Ek__BackingField_20)); }
	inline bool get_U3COrbitCameraTarget2U3Ek__BackingField_20() const { return ___U3COrbitCameraTarget2U3Ek__BackingField_20; }
	inline bool* get_address_of_U3COrbitCameraTarget2U3Ek__BackingField_20() { return &___U3COrbitCameraTarget2U3Ek__BackingField_20; }
	inline void set_U3COrbitCameraTarget2U3Ek__BackingField_20(bool value)
	{
		___U3COrbitCameraTarget2U3Ek__BackingField_20 = value;
	}

	inline static int32_t get_offset_of_U3CPanToCameraTargetU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1, ___U3CPanToCameraTargetU3Ek__BackingField_21)); }
	inline bool get_U3CPanToCameraTargetU3Ek__BackingField_21() const { return ___U3CPanToCameraTargetU3Ek__BackingField_21; }
	inline bool* get_address_of_U3CPanToCameraTargetU3Ek__BackingField_21() { return &___U3CPanToCameraTargetU3Ek__BackingField_21; }
	inline void set_U3CPanToCameraTargetU3Ek__BackingField_21(bool value)
	{
		___U3CPanToCameraTargetU3Ek__BackingField_21 = value;
	}

	inline static int32_t get_offset_of_U3CCameraTargetU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1, ___U3CCameraTargetU3Ek__BackingField_22)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_U3CCameraTargetU3Ek__BackingField_22() const { return ___U3CCameraTargetU3Ek__BackingField_22; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_U3CCameraTargetU3Ek__BackingField_22() { return &___U3CCameraTargetU3Ek__BackingField_22; }
	inline void set_U3CCameraTargetU3Ek__BackingField_22(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___U3CCameraTargetU3Ek__BackingField_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CCameraTargetU3Ek__BackingField_22), (void*)value);
	}
};

struct GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_StaticFields
{
public:
	// GameManager GameManager::<Instance>k__BackingField
	GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * ___U3CInstanceU3Ek__BackingField_16;
	// IFauna GameManager::<Target>k__BackingField
	RuntimeObject* ___U3CTargetU3Ek__BackingField_17;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_StaticFields, ___U3CInstanceU3Ek__BackingField_16)); }
	inline GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * get_U3CInstanceU3Ek__BackingField_16() const { return ___U3CInstanceU3Ek__BackingField_16; }
	inline GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 ** get_address_of_U3CInstanceU3Ek__BackingField_16() { return &___U3CInstanceU3Ek__BackingField_16; }
	inline void set_U3CInstanceU3Ek__BackingField_16(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * value)
	{
		___U3CInstanceU3Ek__BackingField_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CInstanceU3Ek__BackingField_16), (void*)value);
	}

	inline static int32_t get_offset_of_U3CTargetU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_StaticFields, ___U3CTargetU3Ek__BackingField_17)); }
	inline RuntimeObject* get_U3CTargetU3Ek__BackingField_17() const { return ___U3CTargetU3Ek__BackingField_17; }
	inline RuntimeObject** get_address_of_U3CTargetU3Ek__BackingField_17() { return &___U3CTargetU3Ek__BackingField_17; }
	inline void set_U3CTargetU3Ek__BackingField_17(RuntimeObject* value)
	{
		___U3CTargetU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CTargetU3Ek__BackingField_17), (void*)value);
	}
};


// Gate
struct Gate_tAA47F9BD94C2B1C12EADC5ECCAF1CAD07A035F45  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.String Gate::tooltipText
	String_t* ___tooltipText_4;
	// Key/KeyType Gate::type
	int32_t ___type_5;
	// System.Boolean Gate::<Locked>k__BackingField
	bool ___U3CLockedU3Ek__BackingField_6;
	// GameManager Gate::manager
	GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * ___manager_7;
	// UnityEngine.GameObject Gate::gate
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___gate_8;
	// UnityEngine.Vector3 Gate::gateOpen
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___gateOpen_9;
	// System.Single Gate::openTime
	float ___openTime_10;

public:
	inline static int32_t get_offset_of_tooltipText_4() { return static_cast<int32_t>(offsetof(Gate_tAA47F9BD94C2B1C12EADC5ECCAF1CAD07A035F45, ___tooltipText_4)); }
	inline String_t* get_tooltipText_4() const { return ___tooltipText_4; }
	inline String_t** get_address_of_tooltipText_4() { return &___tooltipText_4; }
	inline void set_tooltipText_4(String_t* value)
	{
		___tooltipText_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tooltipText_4), (void*)value);
	}

	inline static int32_t get_offset_of_type_5() { return static_cast<int32_t>(offsetof(Gate_tAA47F9BD94C2B1C12EADC5ECCAF1CAD07A035F45, ___type_5)); }
	inline int32_t get_type_5() const { return ___type_5; }
	inline int32_t* get_address_of_type_5() { return &___type_5; }
	inline void set_type_5(int32_t value)
	{
		___type_5 = value;
	}

	inline static int32_t get_offset_of_U3CLockedU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Gate_tAA47F9BD94C2B1C12EADC5ECCAF1CAD07A035F45, ___U3CLockedU3Ek__BackingField_6)); }
	inline bool get_U3CLockedU3Ek__BackingField_6() const { return ___U3CLockedU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CLockedU3Ek__BackingField_6() { return &___U3CLockedU3Ek__BackingField_6; }
	inline void set_U3CLockedU3Ek__BackingField_6(bool value)
	{
		___U3CLockedU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_manager_7() { return static_cast<int32_t>(offsetof(Gate_tAA47F9BD94C2B1C12EADC5ECCAF1CAD07A035F45, ___manager_7)); }
	inline GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * get_manager_7() const { return ___manager_7; }
	inline GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 ** get_address_of_manager_7() { return &___manager_7; }
	inline void set_manager_7(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * value)
	{
		___manager_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___manager_7), (void*)value);
	}

	inline static int32_t get_offset_of_gate_8() { return static_cast<int32_t>(offsetof(Gate_tAA47F9BD94C2B1C12EADC5ECCAF1CAD07A035F45, ___gate_8)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_gate_8() const { return ___gate_8; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_gate_8() { return &___gate_8; }
	inline void set_gate_8(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___gate_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gate_8), (void*)value);
	}

	inline static int32_t get_offset_of_gateOpen_9() { return static_cast<int32_t>(offsetof(Gate_tAA47F9BD94C2B1C12EADC5ECCAF1CAD07A035F45, ___gateOpen_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_gateOpen_9() const { return ___gateOpen_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_gateOpen_9() { return &___gateOpen_9; }
	inline void set_gateOpen_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___gateOpen_9 = value;
	}

	inline static int32_t get_offset_of_openTime_10() { return static_cast<int32_t>(offsetof(Gate_tAA47F9BD94C2B1C12EADC5ECCAF1CAD07A035F45, ___openTime_10)); }
	inline float get_openTime_10() const { return ___openTime_10; }
	inline float* get_address_of_openTime_10() { return &___openTime_10; }
	inline void set_openTime_10(float value)
	{
		___openTime_10 = value;
	}
};


// Interaction
struct Interaction_t41E997DF6BD70C3EAB8669FCC65D603292EF42A1  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single Interaction::distance
	float ___distance_4;
	// System.Collections.Generic.List`1<Key/KeyType> Interaction::keyRing
	List_1_tD99207A373A130469C76CB8634AADFB31FF317AE * ___keyRing_6;

public:
	inline static int32_t get_offset_of_distance_4() { return static_cast<int32_t>(offsetof(Interaction_t41E997DF6BD70C3EAB8669FCC65D603292EF42A1, ___distance_4)); }
	inline float get_distance_4() const { return ___distance_4; }
	inline float* get_address_of_distance_4() { return &___distance_4; }
	inline void set_distance_4(float value)
	{
		___distance_4 = value;
	}

	inline static int32_t get_offset_of_keyRing_6() { return static_cast<int32_t>(offsetof(Interaction_t41E997DF6BD70C3EAB8669FCC65D603292EF42A1, ___keyRing_6)); }
	inline List_1_tD99207A373A130469C76CB8634AADFB31FF317AE * get_keyRing_6() const { return ___keyRing_6; }
	inline List_1_tD99207A373A130469C76CB8634AADFB31FF317AE ** get_address_of_keyRing_6() { return &___keyRing_6; }
	inline void set_keyRing_6(List_1_tD99207A373A130469C76CB8634AADFB31FF317AE * value)
	{
		___keyRing_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___keyRing_6), (void*)value);
	}
};

struct Interaction_t41E997DF6BD70C3EAB8669FCC65D603292EF42A1_StaticFields
{
public:
	// Interaction Interaction::<Instance>k__BackingField
	Interaction_t41E997DF6BD70C3EAB8669FCC65D603292EF42A1 * ___U3CInstanceU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Interaction_t41E997DF6BD70C3EAB8669FCC65D603292EF42A1_StaticFields, ___U3CInstanceU3Ek__BackingField_5)); }
	inline Interaction_t41E997DF6BD70C3EAB8669FCC65D603292EF42A1 * get_U3CInstanceU3Ek__BackingField_5() const { return ___U3CInstanceU3Ek__BackingField_5; }
	inline Interaction_t41E997DF6BD70C3EAB8669FCC65D603292EF42A1 ** get_address_of_U3CInstanceU3Ek__BackingField_5() { return &___U3CInstanceU3Ek__BackingField_5; }
	inline void set_U3CInstanceU3Ek__BackingField_5(Interaction_t41E997DF6BD70C3EAB8669FCC65D603292EF42A1 * value)
	{
		___U3CInstanceU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CInstanceU3Ek__BackingField_5), (void*)value);
	}
};


// InteractionMenu
struct InteractionMenu_t7C2715221BAC6E0BB83B07EF82F68C6AA34129CB  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.UI.Image InteractionMenu::optionA
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___optionA_4;
	// UnityEngine.UI.Image InteractionMenu::optionB
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___optionB_5;
	// UnityEngine.UI.Text InteractionMenu::optionBText
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___optionBText_6;
	// IInteraction InteractionMenu::currentInteraction
	RuntimeObject* ___currentInteraction_7;
	// System.Int32 InteractionMenu::optionIndex
	int32_t ___optionIndex_8;
	// System.Single InteractionMenu::timer
	float ___timer_9;

public:
	inline static int32_t get_offset_of_optionA_4() { return static_cast<int32_t>(offsetof(InteractionMenu_t7C2715221BAC6E0BB83B07EF82F68C6AA34129CB, ___optionA_4)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_optionA_4() const { return ___optionA_4; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_optionA_4() { return &___optionA_4; }
	inline void set_optionA_4(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___optionA_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___optionA_4), (void*)value);
	}

	inline static int32_t get_offset_of_optionB_5() { return static_cast<int32_t>(offsetof(InteractionMenu_t7C2715221BAC6E0BB83B07EF82F68C6AA34129CB, ___optionB_5)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_optionB_5() const { return ___optionB_5; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_optionB_5() { return &___optionB_5; }
	inline void set_optionB_5(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___optionB_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___optionB_5), (void*)value);
	}

	inline static int32_t get_offset_of_optionBText_6() { return static_cast<int32_t>(offsetof(InteractionMenu_t7C2715221BAC6E0BB83B07EF82F68C6AA34129CB, ___optionBText_6)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_optionBText_6() const { return ___optionBText_6; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_optionBText_6() { return &___optionBText_6; }
	inline void set_optionBText_6(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___optionBText_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___optionBText_6), (void*)value);
	}

	inline static int32_t get_offset_of_currentInteraction_7() { return static_cast<int32_t>(offsetof(InteractionMenu_t7C2715221BAC6E0BB83B07EF82F68C6AA34129CB, ___currentInteraction_7)); }
	inline RuntimeObject* get_currentInteraction_7() const { return ___currentInteraction_7; }
	inline RuntimeObject** get_address_of_currentInteraction_7() { return &___currentInteraction_7; }
	inline void set_currentInteraction_7(RuntimeObject* value)
	{
		___currentInteraction_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___currentInteraction_7), (void*)value);
	}

	inline static int32_t get_offset_of_optionIndex_8() { return static_cast<int32_t>(offsetof(InteractionMenu_t7C2715221BAC6E0BB83B07EF82F68C6AA34129CB, ___optionIndex_8)); }
	inline int32_t get_optionIndex_8() const { return ___optionIndex_8; }
	inline int32_t* get_address_of_optionIndex_8() { return &___optionIndex_8; }
	inline void set_optionIndex_8(int32_t value)
	{
		___optionIndex_8 = value;
	}

	inline static int32_t get_offset_of_timer_9() { return static_cast<int32_t>(offsetof(InteractionMenu_t7C2715221BAC6E0BB83B07EF82F68C6AA34129CB, ___timer_9)); }
	inline float get_timer_9() const { return ___timer_9; }
	inline float* get_address_of_timer_9() { return &___timer_9; }
	inline void set_timer_9(float value)
	{
		___timer_9 = value;
	}
};

struct InteractionMenu_t7C2715221BAC6E0BB83B07EF82F68C6AA34129CB_StaticFields
{
public:
	// InteractionMenu InteractionMenu::<Instance>k__BackingField
	InteractionMenu_t7C2715221BAC6E0BB83B07EF82F68C6AA34129CB * ___U3CInstanceU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(InteractionMenu_t7C2715221BAC6E0BB83B07EF82F68C6AA34129CB_StaticFields, ___U3CInstanceU3Ek__BackingField_10)); }
	inline InteractionMenu_t7C2715221BAC6E0BB83B07EF82F68C6AA34129CB * get_U3CInstanceU3Ek__BackingField_10() const { return ___U3CInstanceU3Ek__BackingField_10; }
	inline InteractionMenu_t7C2715221BAC6E0BB83B07EF82F68C6AA34129CB ** get_address_of_U3CInstanceU3Ek__BackingField_10() { return &___U3CInstanceU3Ek__BackingField_10; }
	inline void set_U3CInstanceU3Ek__BackingField_10(InteractionMenu_t7C2715221BAC6E0BB83B07EF82F68C6AA34129CB * value)
	{
		___U3CInstanceU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CInstanceU3Ek__BackingField_10), (void*)value);
	}
};


// Key
struct Key_tB313BF742406084E17C47FE2B934490E0261AC8E  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// Key/KeyType Key::type
	int32_t ___type_4;
	// System.String Key::tooltipText
	String_t* ___tooltipText_5;

public:
	inline static int32_t get_offset_of_type_4() { return static_cast<int32_t>(offsetof(Key_tB313BF742406084E17C47FE2B934490E0261AC8E, ___type_4)); }
	inline int32_t get_type_4() const { return ___type_4; }
	inline int32_t* get_address_of_type_4() { return &___type_4; }
	inline void set_type_4(int32_t value)
	{
		___type_4 = value;
	}

	inline static int32_t get_offset_of_tooltipText_5() { return static_cast<int32_t>(offsetof(Key_tB313BF742406084E17C47FE2B934490E0261AC8E, ___tooltipText_5)); }
	inline String_t* get_tooltipText_5() const { return ___tooltipText_5; }
	inline String_t** get_address_of_tooltipText_5() { return &___tooltipText_5; }
	inline void set_tooltipText_5(String_t* value)
	{
		___tooltipText_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tooltipText_5), (void*)value);
	}
};


// MainMenu
struct MainMenu_tEB11F5A993C42E93B585FBB65C9E92EC91C5707C  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject MainMenu::galleryPanel
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___galleryPanel_4;
	// UnityEngine.GameObject MainMenu::infoPanel
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___infoPanel_5;
	// UnityEngine.UI.Dropdown MainMenu::galleryDropdown
	Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96 * ___galleryDropdown_6;
	// UnityEngine.UI.RawImage MainMenu::galleryDisplay
	RawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A * ___galleryDisplay_7;
	// System.Collections.Generic.List`1<UnityEngine.Texture2D> MainMenu::screenshots
	List_1_t67CA4414F3746D817D6D1A1D16FD9E7C85CED2D7 * ___screenshots_8;

public:
	inline static int32_t get_offset_of_galleryPanel_4() { return static_cast<int32_t>(offsetof(MainMenu_tEB11F5A993C42E93B585FBB65C9E92EC91C5707C, ___galleryPanel_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_galleryPanel_4() const { return ___galleryPanel_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_galleryPanel_4() { return &___galleryPanel_4; }
	inline void set_galleryPanel_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___galleryPanel_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___galleryPanel_4), (void*)value);
	}

	inline static int32_t get_offset_of_infoPanel_5() { return static_cast<int32_t>(offsetof(MainMenu_tEB11F5A993C42E93B585FBB65C9E92EC91C5707C, ___infoPanel_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_infoPanel_5() const { return ___infoPanel_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_infoPanel_5() { return &___infoPanel_5; }
	inline void set_infoPanel_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___infoPanel_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___infoPanel_5), (void*)value);
	}

	inline static int32_t get_offset_of_galleryDropdown_6() { return static_cast<int32_t>(offsetof(MainMenu_tEB11F5A993C42E93B585FBB65C9E92EC91C5707C, ___galleryDropdown_6)); }
	inline Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96 * get_galleryDropdown_6() const { return ___galleryDropdown_6; }
	inline Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96 ** get_address_of_galleryDropdown_6() { return &___galleryDropdown_6; }
	inline void set_galleryDropdown_6(Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96 * value)
	{
		___galleryDropdown_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___galleryDropdown_6), (void*)value);
	}

	inline static int32_t get_offset_of_galleryDisplay_7() { return static_cast<int32_t>(offsetof(MainMenu_tEB11F5A993C42E93B585FBB65C9E92EC91C5707C, ___galleryDisplay_7)); }
	inline RawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A * get_galleryDisplay_7() const { return ___galleryDisplay_7; }
	inline RawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A ** get_address_of_galleryDisplay_7() { return &___galleryDisplay_7; }
	inline void set_galleryDisplay_7(RawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A * value)
	{
		___galleryDisplay_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___galleryDisplay_7), (void*)value);
	}

	inline static int32_t get_offset_of_screenshots_8() { return static_cast<int32_t>(offsetof(MainMenu_tEB11F5A993C42E93B585FBB65C9E92EC91C5707C, ___screenshots_8)); }
	inline List_1_t67CA4414F3746D817D6D1A1D16FD9E7C85CED2D7 * get_screenshots_8() const { return ___screenshots_8; }
	inline List_1_t67CA4414F3746D817D6D1A1D16FD9E7C85CED2D7 ** get_address_of_screenshots_8() { return &___screenshots_8; }
	inline void set_screenshots_8(List_1_t67CA4414F3746D817D6D1A1D16FD9E7C85CED2D7 * value)
	{
		___screenshots_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___screenshots_8), (void*)value);
	}
};


// MouseLook
struct MouseLook_t1E1EE79B13022184F3EF9AC8FE8C6771053890DA  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single MouseLook::sensitivity
	float ___sensitivity_4;
	// System.Single MouseLook::drag
	float ___drag_5;
	// UnityEngine.Transform MouseLook::character
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___character_6;
	// UnityEngine.Vector2 MouseLook::mouseDir
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___mouseDir_7;
	// UnityEngine.Vector2 MouseLook::smoothing
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___smoothing_8;
	// UnityEngine.Vector2 MouseLook::result
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___result_9;
	// System.Boolean MouseLook::<LookEnabled>k__BackingField
	bool ___U3CLookEnabledU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_sensitivity_4() { return static_cast<int32_t>(offsetof(MouseLook_t1E1EE79B13022184F3EF9AC8FE8C6771053890DA, ___sensitivity_4)); }
	inline float get_sensitivity_4() const { return ___sensitivity_4; }
	inline float* get_address_of_sensitivity_4() { return &___sensitivity_4; }
	inline void set_sensitivity_4(float value)
	{
		___sensitivity_4 = value;
	}

	inline static int32_t get_offset_of_drag_5() { return static_cast<int32_t>(offsetof(MouseLook_t1E1EE79B13022184F3EF9AC8FE8C6771053890DA, ___drag_5)); }
	inline float get_drag_5() const { return ___drag_5; }
	inline float* get_address_of_drag_5() { return &___drag_5; }
	inline void set_drag_5(float value)
	{
		___drag_5 = value;
	}

	inline static int32_t get_offset_of_character_6() { return static_cast<int32_t>(offsetof(MouseLook_t1E1EE79B13022184F3EF9AC8FE8C6771053890DA, ___character_6)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_character_6() const { return ___character_6; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_character_6() { return &___character_6; }
	inline void set_character_6(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___character_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___character_6), (void*)value);
	}

	inline static int32_t get_offset_of_mouseDir_7() { return static_cast<int32_t>(offsetof(MouseLook_t1E1EE79B13022184F3EF9AC8FE8C6771053890DA, ___mouseDir_7)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_mouseDir_7() const { return ___mouseDir_7; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_mouseDir_7() { return &___mouseDir_7; }
	inline void set_mouseDir_7(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___mouseDir_7 = value;
	}

	inline static int32_t get_offset_of_smoothing_8() { return static_cast<int32_t>(offsetof(MouseLook_t1E1EE79B13022184F3EF9AC8FE8C6771053890DA, ___smoothing_8)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_smoothing_8() const { return ___smoothing_8; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_smoothing_8() { return &___smoothing_8; }
	inline void set_smoothing_8(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___smoothing_8 = value;
	}

	inline static int32_t get_offset_of_result_9() { return static_cast<int32_t>(offsetof(MouseLook_t1E1EE79B13022184F3EF9AC8FE8C6771053890DA, ___result_9)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_result_9() const { return ___result_9; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_result_9() { return &___result_9; }
	inline void set_result_9(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___result_9 = value;
	}

	inline static int32_t get_offset_of_U3CLookEnabledU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(MouseLook_t1E1EE79B13022184F3EF9AC8FE8C6771053890DA, ___U3CLookEnabledU3Ek__BackingField_11)); }
	inline bool get_U3CLookEnabledU3Ek__BackingField_11() const { return ___U3CLookEnabledU3Ek__BackingField_11; }
	inline bool* get_address_of_U3CLookEnabledU3Ek__BackingField_11() { return &___U3CLookEnabledU3Ek__BackingField_11; }
	inline void set_U3CLookEnabledU3Ek__BackingField_11(bool value)
	{
		___U3CLookEnabledU3Ek__BackingField_11 = value;
	}
};

struct MouseLook_t1E1EE79B13022184F3EF9AC8FE8C6771053890DA_StaticFields
{
public:
	// MouseLook MouseLook::<Instance>k__BackingField
	MouseLook_t1E1EE79B13022184F3EF9AC8FE8C6771053890DA * ___U3CInstanceU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(MouseLook_t1E1EE79B13022184F3EF9AC8FE8C6771053890DA_StaticFields, ___U3CInstanceU3Ek__BackingField_10)); }
	inline MouseLook_t1E1EE79B13022184F3EF9AC8FE8C6771053890DA * get_U3CInstanceU3Ek__BackingField_10() const { return ___U3CInstanceU3Ek__BackingField_10; }
	inline MouseLook_t1E1EE79B13022184F3EF9AC8FE8C6771053890DA ** get_address_of_U3CInstanceU3Ek__BackingField_10() { return &___U3CInstanceU3Ek__BackingField_10; }
	inline void set_U3CInstanceU3Ek__BackingField_10(MouseLook_t1E1EE79B13022184F3EF9AC8FE8C6771053890DA * value)
	{
		___U3CInstanceU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CInstanceU3Ek__BackingField_10), (void*)value);
	}
};


// NewBehaviourScript
struct NewBehaviourScript_t5ED8D4BD4668DBECFEFD841BD2A8CC1D8FCF38CB  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// PlayerController
struct PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single PlayerController::speed
	float ___speed_4;
	// UnityEngine.Vector3 PlayerController::input
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___input_5;
	// UnityEngine.Vector3 PlayerController::motion
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___motion_6;
	// UnityEngine.CharacterController PlayerController::controller
	CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E * ___controller_7;

public:
	inline static int32_t get_offset_of_speed_4() { return static_cast<int32_t>(offsetof(PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9, ___speed_4)); }
	inline float get_speed_4() const { return ___speed_4; }
	inline float* get_address_of_speed_4() { return &___speed_4; }
	inline void set_speed_4(float value)
	{
		___speed_4 = value;
	}

	inline static int32_t get_offset_of_input_5() { return static_cast<int32_t>(offsetof(PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9, ___input_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_input_5() const { return ___input_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_input_5() { return &___input_5; }
	inline void set_input_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___input_5 = value;
	}

	inline static int32_t get_offset_of_motion_6() { return static_cast<int32_t>(offsetof(PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9, ___motion_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_motion_6() const { return ___motion_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_motion_6() { return &___motion_6; }
	inline void set_motion_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___motion_6 = value;
	}

	inline static int32_t get_offset_of_controller_7() { return static_cast<int32_t>(offsetof(PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9, ___controller_7)); }
	inline CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E * get_controller_7() const { return ___controller_7; }
	inline CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E ** get_address_of_controller_7() { return &___controller_7; }
	inline void set_controller_7(CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E * value)
	{
		___controller_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___controller_7), (void*)value);
	}
};

struct PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_StaticFields
{
public:
	// PlayerController PlayerController::<Instance>k__BackingField
	PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9 * ___U3CInstanceU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_StaticFields, ___U3CInstanceU3Ek__BackingField_8)); }
	inline PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9 * get_U3CInstanceU3Ek__BackingField_8() const { return ___U3CInstanceU3Ek__BackingField_8; }
	inline PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9 ** get_address_of_U3CInstanceU3Ek__BackingField_8() { return &___U3CInstanceU3Ek__BackingField_8; }
	inline void set_U3CInstanceU3Ek__BackingField_8(PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9 * value)
	{
		___U3CInstanceU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CInstanceU3Ek__BackingField_8), (void*)value);
	}
};


// Ship
struct Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.String Ship::tooltipText
	String_t* ___tooltipText_4;
	// UnityEngine.Vector3 Ship::playerSpawnOffset
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___playerSpawnOffset_5;
	// UnityEngine.Bounds Ship::boundBox
	Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  ___boundBox_6;
	// ShipWait Ship::waitState
	ShipWait_tB05589ED6695523DD921B075C449CE03E0B836FE * ___waitState_7;
	// ShipMove Ship::moveState
	ShipMove_t761409814A6DA69DE131EA554AEC499CB81D3953 * ___moveState_8;
	// FiniteStateMachine.StateMachine Ship::<StateMachine>k__BackingField
	StateMachine_t8E24C2687A18E15B27A6A8FEED2DFE22D910119F * ___U3CStateMachineU3Ek__BackingField_9;
	// UnityEngine.AI.NavMeshAgent Ship::<Agent>k__BackingField
	NavMeshAgent_tB9746B6C38013341DB63973CA7ED657494EFB41B * ___U3CAgentU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_tooltipText_4() { return static_cast<int32_t>(offsetof(Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E, ___tooltipText_4)); }
	inline String_t* get_tooltipText_4() const { return ___tooltipText_4; }
	inline String_t** get_address_of_tooltipText_4() { return &___tooltipText_4; }
	inline void set_tooltipText_4(String_t* value)
	{
		___tooltipText_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tooltipText_4), (void*)value);
	}

	inline static int32_t get_offset_of_playerSpawnOffset_5() { return static_cast<int32_t>(offsetof(Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E, ___playerSpawnOffset_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_playerSpawnOffset_5() const { return ___playerSpawnOffset_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_playerSpawnOffset_5() { return &___playerSpawnOffset_5; }
	inline void set_playerSpawnOffset_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___playerSpawnOffset_5 = value;
	}

	inline static int32_t get_offset_of_boundBox_6() { return static_cast<int32_t>(offsetof(Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E, ___boundBox_6)); }
	inline Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  get_boundBox_6() const { return ___boundBox_6; }
	inline Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 * get_address_of_boundBox_6() { return &___boundBox_6; }
	inline void set_boundBox_6(Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  value)
	{
		___boundBox_6 = value;
	}

	inline static int32_t get_offset_of_waitState_7() { return static_cast<int32_t>(offsetof(Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E, ___waitState_7)); }
	inline ShipWait_tB05589ED6695523DD921B075C449CE03E0B836FE * get_waitState_7() const { return ___waitState_7; }
	inline ShipWait_tB05589ED6695523DD921B075C449CE03E0B836FE ** get_address_of_waitState_7() { return &___waitState_7; }
	inline void set_waitState_7(ShipWait_tB05589ED6695523DD921B075C449CE03E0B836FE * value)
	{
		___waitState_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___waitState_7), (void*)value);
	}

	inline static int32_t get_offset_of_moveState_8() { return static_cast<int32_t>(offsetof(Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E, ___moveState_8)); }
	inline ShipMove_t761409814A6DA69DE131EA554AEC499CB81D3953 * get_moveState_8() const { return ___moveState_8; }
	inline ShipMove_t761409814A6DA69DE131EA554AEC499CB81D3953 ** get_address_of_moveState_8() { return &___moveState_8; }
	inline void set_moveState_8(ShipMove_t761409814A6DA69DE131EA554AEC499CB81D3953 * value)
	{
		___moveState_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___moveState_8), (void*)value);
	}

	inline static int32_t get_offset_of_U3CStateMachineU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E, ___U3CStateMachineU3Ek__BackingField_9)); }
	inline StateMachine_t8E24C2687A18E15B27A6A8FEED2DFE22D910119F * get_U3CStateMachineU3Ek__BackingField_9() const { return ___U3CStateMachineU3Ek__BackingField_9; }
	inline StateMachine_t8E24C2687A18E15B27A6A8FEED2DFE22D910119F ** get_address_of_U3CStateMachineU3Ek__BackingField_9() { return &___U3CStateMachineU3Ek__BackingField_9; }
	inline void set_U3CStateMachineU3Ek__BackingField_9(StateMachine_t8E24C2687A18E15B27A6A8FEED2DFE22D910119F * value)
	{
		___U3CStateMachineU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineU3Ek__BackingField_9), (void*)value);
	}

	inline static int32_t get_offset_of_U3CAgentU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E, ___U3CAgentU3Ek__BackingField_10)); }
	inline NavMeshAgent_tB9746B6C38013341DB63973CA7ED657494EFB41B * get_U3CAgentU3Ek__BackingField_10() const { return ___U3CAgentU3Ek__BackingField_10; }
	inline NavMeshAgent_tB9746B6C38013341DB63973CA7ED657494EFB41B ** get_address_of_U3CAgentU3Ek__BackingField_10() { return &___U3CAgentU3Ek__BackingField_10; }
	inline void set_U3CAgentU3Ek__BackingField_10(NavMeshAgent_tB9746B6C38013341DB63973CA7ED657494EFB41B * value)
	{
		___U3CAgentU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CAgentU3Ek__BackingField_10), (void*)value);
	}
};


// TempInteraction
struct TempInteraction_t379C5510CC31B27F05D6803C86D8ACBAAA4D0587  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// ToolTip
struct ToolTip_t044257731DC5931927D3AB0671C17515BD8E251E  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single ToolTip::tooltipTime
	float ___tooltipTime_4;
	// UnityEngine.UI.Text ToolTip::tooltipText
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___tooltipText_5;
	// System.Single ToolTip::timer
	float ___timer_6;

public:
	inline static int32_t get_offset_of_tooltipTime_4() { return static_cast<int32_t>(offsetof(ToolTip_t044257731DC5931927D3AB0671C17515BD8E251E, ___tooltipTime_4)); }
	inline float get_tooltipTime_4() const { return ___tooltipTime_4; }
	inline float* get_address_of_tooltipTime_4() { return &___tooltipTime_4; }
	inline void set_tooltipTime_4(float value)
	{
		___tooltipTime_4 = value;
	}

	inline static int32_t get_offset_of_tooltipText_5() { return static_cast<int32_t>(offsetof(ToolTip_t044257731DC5931927D3AB0671C17515BD8E251E, ___tooltipText_5)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_tooltipText_5() const { return ___tooltipText_5; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_tooltipText_5() { return &___tooltipText_5; }
	inline void set_tooltipText_5(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___tooltipText_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tooltipText_5), (void*)value);
	}

	inline static int32_t get_offset_of_timer_6() { return static_cast<int32_t>(offsetof(ToolTip_t044257731DC5931927D3AB0671C17515BD8E251E, ___timer_6)); }
	inline float get_timer_6() const { return ___timer_6; }
	inline float* get_address_of_timer_6() { return &___timer_6; }
	inline void set_timer_6(float value)
	{
		___timer_6 = value;
	}
};

struct ToolTip_t044257731DC5931927D3AB0671C17515BD8E251E_StaticFields
{
public:
	// ToolTip ToolTip::<Instance>k__BackingField
	ToolTip_t044257731DC5931927D3AB0671C17515BD8E251E * ___U3CInstanceU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(ToolTip_t044257731DC5931927D3AB0671C17515BD8E251E_StaticFields, ___U3CInstanceU3Ek__BackingField_7)); }
	inline ToolTip_t044257731DC5931927D3AB0671C17515BD8E251E * get_U3CInstanceU3Ek__BackingField_7() const { return ___U3CInstanceU3Ek__BackingField_7; }
	inline ToolTip_t044257731DC5931927D3AB0671C17515BD8E251E ** get_address_of_U3CInstanceU3Ek__BackingField_7() { return &___U3CInstanceU3Ek__BackingField_7; }
	inline void set_U3CInstanceU3Ek__BackingField_7(ToolTip_t044257731DC5931927D3AB0671C17515BD8E251E * value)
	{
		___U3CInstanceU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CInstanceU3Ek__BackingField_7), (void*)value);
	}
};


// UnityEngine.EventSystems.UIBehaviour
struct UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// WorldMap
struct WorldMap_t01A07DDC9EA386C88D5814CF18E0F37CAF05270C  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject WorldMap::legend
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___legend_5;
	// UnityEngine.Camera WorldMap::mapCam
	Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ___mapCam_6;
	// UnityEngine.UI.Image[] WorldMap::legendOptions
	ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224* ___legendOptions_7;
	// UnityEngine.GameObject WorldMap::keyIconPrefab
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___keyIconPrefab_8;
	// UnityEngine.GameObject WorldMap::gateIconPrefab
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___gateIconPrefab_9;
	// UnityEngine.GameObject WorldMap::shipIconPrefab
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___shipIconPrefab_10;
	// System.Single WorldMap::timer
	float ___timer_11;
	// System.Int32 WorldMap::legendIndex
	int32_t ___legendIndex_12;
	// UnityEngine.GameObject WorldMap::ship
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___ship_13;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> WorldMap::activeInteractions
	List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * ___activeInteractions_14;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> WorldMap::gateInteractions
	List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * ___gateInteractions_15;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> WorldMap::keyInteractions
	List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * ___keyInteractions_16;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> WorldMap::mapIcons
	List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * ___mapIcons_17;

public:
	inline static int32_t get_offset_of_legend_5() { return static_cast<int32_t>(offsetof(WorldMap_t01A07DDC9EA386C88D5814CF18E0F37CAF05270C, ___legend_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_legend_5() const { return ___legend_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_legend_5() { return &___legend_5; }
	inline void set_legend_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___legend_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___legend_5), (void*)value);
	}

	inline static int32_t get_offset_of_mapCam_6() { return static_cast<int32_t>(offsetof(WorldMap_t01A07DDC9EA386C88D5814CF18E0F37CAF05270C, ___mapCam_6)); }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * get_mapCam_6() const { return ___mapCam_6; }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C ** get_address_of_mapCam_6() { return &___mapCam_6; }
	inline void set_mapCam_6(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * value)
	{
		___mapCam_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mapCam_6), (void*)value);
	}

	inline static int32_t get_offset_of_legendOptions_7() { return static_cast<int32_t>(offsetof(WorldMap_t01A07DDC9EA386C88D5814CF18E0F37CAF05270C, ___legendOptions_7)); }
	inline ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224* get_legendOptions_7() const { return ___legendOptions_7; }
	inline ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224** get_address_of_legendOptions_7() { return &___legendOptions_7; }
	inline void set_legendOptions_7(ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224* value)
	{
		___legendOptions_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___legendOptions_7), (void*)value);
	}

	inline static int32_t get_offset_of_keyIconPrefab_8() { return static_cast<int32_t>(offsetof(WorldMap_t01A07DDC9EA386C88D5814CF18E0F37CAF05270C, ___keyIconPrefab_8)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_keyIconPrefab_8() const { return ___keyIconPrefab_8; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_keyIconPrefab_8() { return &___keyIconPrefab_8; }
	inline void set_keyIconPrefab_8(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___keyIconPrefab_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___keyIconPrefab_8), (void*)value);
	}

	inline static int32_t get_offset_of_gateIconPrefab_9() { return static_cast<int32_t>(offsetof(WorldMap_t01A07DDC9EA386C88D5814CF18E0F37CAF05270C, ___gateIconPrefab_9)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_gateIconPrefab_9() const { return ___gateIconPrefab_9; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_gateIconPrefab_9() { return &___gateIconPrefab_9; }
	inline void set_gateIconPrefab_9(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___gateIconPrefab_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gateIconPrefab_9), (void*)value);
	}

	inline static int32_t get_offset_of_shipIconPrefab_10() { return static_cast<int32_t>(offsetof(WorldMap_t01A07DDC9EA386C88D5814CF18E0F37CAF05270C, ___shipIconPrefab_10)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_shipIconPrefab_10() const { return ___shipIconPrefab_10; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_shipIconPrefab_10() { return &___shipIconPrefab_10; }
	inline void set_shipIconPrefab_10(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___shipIconPrefab_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___shipIconPrefab_10), (void*)value);
	}

	inline static int32_t get_offset_of_timer_11() { return static_cast<int32_t>(offsetof(WorldMap_t01A07DDC9EA386C88D5814CF18E0F37CAF05270C, ___timer_11)); }
	inline float get_timer_11() const { return ___timer_11; }
	inline float* get_address_of_timer_11() { return &___timer_11; }
	inline void set_timer_11(float value)
	{
		___timer_11 = value;
	}

	inline static int32_t get_offset_of_legendIndex_12() { return static_cast<int32_t>(offsetof(WorldMap_t01A07DDC9EA386C88D5814CF18E0F37CAF05270C, ___legendIndex_12)); }
	inline int32_t get_legendIndex_12() const { return ___legendIndex_12; }
	inline int32_t* get_address_of_legendIndex_12() { return &___legendIndex_12; }
	inline void set_legendIndex_12(int32_t value)
	{
		___legendIndex_12 = value;
	}

	inline static int32_t get_offset_of_ship_13() { return static_cast<int32_t>(offsetof(WorldMap_t01A07DDC9EA386C88D5814CF18E0F37CAF05270C, ___ship_13)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_ship_13() const { return ___ship_13; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_ship_13() { return &___ship_13; }
	inline void set_ship_13(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___ship_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ship_13), (void*)value);
	}

	inline static int32_t get_offset_of_activeInteractions_14() { return static_cast<int32_t>(offsetof(WorldMap_t01A07DDC9EA386C88D5814CF18E0F37CAF05270C, ___activeInteractions_14)); }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * get_activeInteractions_14() const { return ___activeInteractions_14; }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 ** get_address_of_activeInteractions_14() { return &___activeInteractions_14; }
	inline void set_activeInteractions_14(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * value)
	{
		___activeInteractions_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___activeInteractions_14), (void*)value);
	}

	inline static int32_t get_offset_of_gateInteractions_15() { return static_cast<int32_t>(offsetof(WorldMap_t01A07DDC9EA386C88D5814CF18E0F37CAF05270C, ___gateInteractions_15)); }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * get_gateInteractions_15() const { return ___gateInteractions_15; }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 ** get_address_of_gateInteractions_15() { return &___gateInteractions_15; }
	inline void set_gateInteractions_15(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * value)
	{
		___gateInteractions_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gateInteractions_15), (void*)value);
	}

	inline static int32_t get_offset_of_keyInteractions_16() { return static_cast<int32_t>(offsetof(WorldMap_t01A07DDC9EA386C88D5814CF18E0F37CAF05270C, ___keyInteractions_16)); }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * get_keyInteractions_16() const { return ___keyInteractions_16; }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 ** get_address_of_keyInteractions_16() { return &___keyInteractions_16; }
	inline void set_keyInteractions_16(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * value)
	{
		___keyInteractions_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___keyInteractions_16), (void*)value);
	}

	inline static int32_t get_offset_of_mapIcons_17() { return static_cast<int32_t>(offsetof(WorldMap_t01A07DDC9EA386C88D5814CF18E0F37CAF05270C, ___mapIcons_17)); }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * get_mapIcons_17() const { return ___mapIcons_17; }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 ** get_address_of_mapIcons_17() { return &___mapIcons_17; }
	inline void set_mapIcons_17(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * value)
	{
		___mapIcons_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mapIcons_17), (void*)value);
	}
};

struct WorldMap_t01A07DDC9EA386C88D5814CF18E0F37CAF05270C_StaticFields
{
public:
	// WorldMap WorldMap::<Instance>k__BackingField
	WorldMap_t01A07DDC9EA386C88D5814CF18E0F37CAF05270C * ___U3CInstanceU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(WorldMap_t01A07DDC9EA386C88D5814CF18E0F37CAF05270C_StaticFields, ___U3CInstanceU3Ek__BackingField_4)); }
	inline WorldMap_t01A07DDC9EA386C88D5814CF18E0F37CAF05270C * get_U3CInstanceU3Ek__BackingField_4() const { return ___U3CInstanceU3Ek__BackingField_4; }
	inline WorldMap_t01A07DDC9EA386C88D5814CF18E0F37CAF05270C ** get_address_of_U3CInstanceU3Ek__BackingField_4() { return &___U3CInstanceU3Ek__BackingField_4; }
	inline void set_U3CInstanceU3Ek__BackingField_4(WorldMap_t01A07DDC9EA386C88D5814CF18E0F37CAF05270C * value)
	{
		___U3CInstanceU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CInstanceU3Ek__BackingField_4), (void*)value);
	}
};


// FlockAgent
struct FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04  : public Fish_t45F7EC0A7EE404E12CC412CF93723EA8799CA46C
{
public:
	// System.Single FlockAgent::viewAngle
	float ___viewAngle_5;
	// System.Single FlockAgent::smoothDamp
	float ___smoothDamp_6;
	// UnityEngine.LayerMask FlockAgent::obstacleMask
	LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  ___obstacleMask_7;
	// UnityEngine.Vector3[] FlockAgent::collisionRays
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___collisionRays_8;
	// System.Single FlockAgent::speed
	float ___speed_9;
	// UnityEngine.Vector3 FlockAgent::velocity
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___velocity_10;
	// UnityEngine.Vector3 FlockAgent::currentAvoidance
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___currentAvoidance_11;
	// FlockManager FlockAgent::flockInstance
	FlockManager_t241CFB1B2E6593247EFA52A9E2CB2A111400F1F8 * ___flockInstance_12;
	// System.Collections.Generic.List`1<FlockAgent> FlockAgent::cohesionNeighbours
	List_1_t50CCFC552038CBD55842D81AC8F7EE04848C7255 * ___cohesionNeighbours_13;
	// System.Collections.Generic.List`1<FlockAgent> FlockAgent::seperationNeighbours
	List_1_t50CCFC552038CBD55842D81AC8F7EE04848C7255 * ___seperationNeighbours_14;
	// System.Collections.Generic.List`1<FlockAgent> FlockAgent::alignmentNeighbours
	List_1_t50CCFC552038CBD55842D81AC8F7EE04848C7255 * ___alignmentNeighbours_15;

public:
	inline static int32_t get_offset_of_viewAngle_5() { return static_cast<int32_t>(offsetof(FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04, ___viewAngle_5)); }
	inline float get_viewAngle_5() const { return ___viewAngle_5; }
	inline float* get_address_of_viewAngle_5() { return &___viewAngle_5; }
	inline void set_viewAngle_5(float value)
	{
		___viewAngle_5 = value;
	}

	inline static int32_t get_offset_of_smoothDamp_6() { return static_cast<int32_t>(offsetof(FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04, ___smoothDamp_6)); }
	inline float get_smoothDamp_6() const { return ___smoothDamp_6; }
	inline float* get_address_of_smoothDamp_6() { return &___smoothDamp_6; }
	inline void set_smoothDamp_6(float value)
	{
		___smoothDamp_6 = value;
	}

	inline static int32_t get_offset_of_obstacleMask_7() { return static_cast<int32_t>(offsetof(FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04, ___obstacleMask_7)); }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  get_obstacleMask_7() const { return ___obstacleMask_7; }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 * get_address_of_obstacleMask_7() { return &___obstacleMask_7; }
	inline void set_obstacleMask_7(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  value)
	{
		___obstacleMask_7 = value;
	}

	inline static int32_t get_offset_of_collisionRays_8() { return static_cast<int32_t>(offsetof(FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04, ___collisionRays_8)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_collisionRays_8() const { return ___collisionRays_8; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_collisionRays_8() { return &___collisionRays_8; }
	inline void set_collisionRays_8(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___collisionRays_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___collisionRays_8), (void*)value);
	}

	inline static int32_t get_offset_of_speed_9() { return static_cast<int32_t>(offsetof(FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04, ___speed_9)); }
	inline float get_speed_9() const { return ___speed_9; }
	inline float* get_address_of_speed_9() { return &___speed_9; }
	inline void set_speed_9(float value)
	{
		___speed_9 = value;
	}

	inline static int32_t get_offset_of_velocity_10() { return static_cast<int32_t>(offsetof(FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04, ___velocity_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_velocity_10() const { return ___velocity_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_velocity_10() { return &___velocity_10; }
	inline void set_velocity_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___velocity_10 = value;
	}

	inline static int32_t get_offset_of_currentAvoidance_11() { return static_cast<int32_t>(offsetof(FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04, ___currentAvoidance_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_currentAvoidance_11() const { return ___currentAvoidance_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_currentAvoidance_11() { return &___currentAvoidance_11; }
	inline void set_currentAvoidance_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___currentAvoidance_11 = value;
	}

	inline static int32_t get_offset_of_flockInstance_12() { return static_cast<int32_t>(offsetof(FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04, ___flockInstance_12)); }
	inline FlockManager_t241CFB1B2E6593247EFA52A9E2CB2A111400F1F8 * get_flockInstance_12() const { return ___flockInstance_12; }
	inline FlockManager_t241CFB1B2E6593247EFA52A9E2CB2A111400F1F8 ** get_address_of_flockInstance_12() { return &___flockInstance_12; }
	inline void set_flockInstance_12(FlockManager_t241CFB1B2E6593247EFA52A9E2CB2A111400F1F8 * value)
	{
		___flockInstance_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___flockInstance_12), (void*)value);
	}

	inline static int32_t get_offset_of_cohesionNeighbours_13() { return static_cast<int32_t>(offsetof(FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04, ___cohesionNeighbours_13)); }
	inline List_1_t50CCFC552038CBD55842D81AC8F7EE04848C7255 * get_cohesionNeighbours_13() const { return ___cohesionNeighbours_13; }
	inline List_1_t50CCFC552038CBD55842D81AC8F7EE04848C7255 ** get_address_of_cohesionNeighbours_13() { return &___cohesionNeighbours_13; }
	inline void set_cohesionNeighbours_13(List_1_t50CCFC552038CBD55842D81AC8F7EE04848C7255 * value)
	{
		___cohesionNeighbours_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cohesionNeighbours_13), (void*)value);
	}

	inline static int32_t get_offset_of_seperationNeighbours_14() { return static_cast<int32_t>(offsetof(FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04, ___seperationNeighbours_14)); }
	inline List_1_t50CCFC552038CBD55842D81AC8F7EE04848C7255 * get_seperationNeighbours_14() const { return ___seperationNeighbours_14; }
	inline List_1_t50CCFC552038CBD55842D81AC8F7EE04848C7255 ** get_address_of_seperationNeighbours_14() { return &___seperationNeighbours_14; }
	inline void set_seperationNeighbours_14(List_1_t50CCFC552038CBD55842D81AC8F7EE04848C7255 * value)
	{
		___seperationNeighbours_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___seperationNeighbours_14), (void*)value);
	}

	inline static int32_t get_offset_of_alignmentNeighbours_15() { return static_cast<int32_t>(offsetof(FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04, ___alignmentNeighbours_15)); }
	inline List_1_t50CCFC552038CBD55842D81AC8F7EE04848C7255 * get_alignmentNeighbours_15() const { return ___alignmentNeighbours_15; }
	inline List_1_t50CCFC552038CBD55842D81AC8F7EE04848C7255 ** get_address_of_alignmentNeighbours_15() { return &___alignmentNeighbours_15; }
	inline void set_alignmentNeighbours_15(List_1_t50CCFC552038CBD55842D81AC8F7EE04848C7255 * value)
	{
		___alignmentNeighbours_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___alignmentNeighbours_15), (void*)value);
	}
};


// UnityEngine.UI.Graphic
struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24  : public UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipLayoutUpdate
	bool ___m_SkipLayoutUpdate_8;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipMaterialUpdate
	bool ___m_SkipMaterialUpdate_9;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_10;
	// UnityEngine.Vector4 UnityEngine.UI.Graphic::m_RaycastPadding
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___m_RaycastPadding_11;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_RectTransform_12;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * ___m_CanvasRenderer_13;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * ___m_Canvas_14;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_15;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_16;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyLayoutCallback_17;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyVertsCallback_18;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyMaterialCallback_19;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::m_CachedMesh
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___m_CachedMesh_22;
	// UnityEngine.Vector2[] UnityEngine.UI.Graphic::m_CachedUvs
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___m_CachedUvs_23;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * ___m_ColorTweenRunner_24;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25;

public:
	inline static int32_t get_offset_of_m_Material_6() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Material_6)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_Material_6() const { return ___m_Material_6; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_Material_6() { return &___m_Material_6; }
	inline void set_m_Material_6(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_Material_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Material_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_Color_7() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Color_7)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_Color_7() const { return ___m_Color_7; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_Color_7() { return &___m_Color_7; }
	inline void set_m_Color_7(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_Color_7 = value;
	}

	inline static int32_t get_offset_of_m_SkipLayoutUpdate_8() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_SkipLayoutUpdate_8)); }
	inline bool get_m_SkipLayoutUpdate_8() const { return ___m_SkipLayoutUpdate_8; }
	inline bool* get_address_of_m_SkipLayoutUpdate_8() { return &___m_SkipLayoutUpdate_8; }
	inline void set_m_SkipLayoutUpdate_8(bool value)
	{
		___m_SkipLayoutUpdate_8 = value;
	}

	inline static int32_t get_offset_of_m_SkipMaterialUpdate_9() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_SkipMaterialUpdate_9)); }
	inline bool get_m_SkipMaterialUpdate_9() const { return ___m_SkipMaterialUpdate_9; }
	inline bool* get_address_of_m_SkipMaterialUpdate_9() { return &___m_SkipMaterialUpdate_9; }
	inline void set_m_SkipMaterialUpdate_9(bool value)
	{
		___m_SkipMaterialUpdate_9 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_10() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RaycastTarget_10)); }
	inline bool get_m_RaycastTarget_10() const { return ___m_RaycastTarget_10; }
	inline bool* get_address_of_m_RaycastTarget_10() { return &___m_RaycastTarget_10; }
	inline void set_m_RaycastTarget_10(bool value)
	{
		___m_RaycastTarget_10 = value;
	}

	inline static int32_t get_offset_of_m_RaycastPadding_11() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RaycastPadding_11)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_m_RaycastPadding_11() const { return ___m_RaycastPadding_11; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_m_RaycastPadding_11() { return &___m_RaycastPadding_11; }
	inline void set_m_RaycastPadding_11(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___m_RaycastPadding_11 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_12() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RectTransform_12)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_RectTransform_12() const { return ___m_RectTransform_12; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_RectTransform_12() { return &___m_RectTransform_12; }
	inline void set_m_RectTransform_12(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_RectTransform_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RectTransform_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_13() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CanvasRenderer_13)); }
	inline CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * get_m_CanvasRenderer_13() const { return ___m_CanvasRenderer_13; }
	inline CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E ** get_address_of_m_CanvasRenderer_13() { return &___m_CanvasRenderer_13; }
	inline void set_m_CanvasRenderer_13(CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * value)
	{
		___m_CanvasRenderer_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CanvasRenderer_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_Canvas_14() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Canvas_14)); }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * get_m_Canvas_14() const { return ___m_Canvas_14; }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA ** get_address_of_m_Canvas_14() { return &___m_Canvas_14; }
	inline void set_m_Canvas_14(Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * value)
	{
		___m_Canvas_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Canvas_14), (void*)value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_15() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_VertsDirty_15)); }
	inline bool get_m_VertsDirty_15() const { return ___m_VertsDirty_15; }
	inline bool* get_address_of_m_VertsDirty_15() { return &___m_VertsDirty_15; }
	inline void set_m_VertsDirty_15(bool value)
	{
		___m_VertsDirty_15 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_16() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_MaterialDirty_16)); }
	inline bool get_m_MaterialDirty_16() const { return ___m_MaterialDirty_16; }
	inline bool* get_address_of_m_MaterialDirty_16() { return &___m_MaterialDirty_16; }
	inline void set_m_MaterialDirty_16(bool value)
	{
		___m_MaterialDirty_16 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_17() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyLayoutCallback_17)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyLayoutCallback_17() const { return ___m_OnDirtyLayoutCallback_17; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyLayoutCallback_17() { return &___m_OnDirtyLayoutCallback_17; }
	inline void set_m_OnDirtyLayoutCallback_17(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyLayoutCallback_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyLayoutCallback_17), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_18() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyVertsCallback_18)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyVertsCallback_18() const { return ___m_OnDirtyVertsCallback_18; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyVertsCallback_18() { return &___m_OnDirtyVertsCallback_18; }
	inline void set_m_OnDirtyVertsCallback_18(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyVertsCallback_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyVertsCallback_18), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_19() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyMaterialCallback_19)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyMaterialCallback_19() const { return ___m_OnDirtyMaterialCallback_19; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyMaterialCallback_19() { return &___m_OnDirtyMaterialCallback_19; }
	inline void set_m_OnDirtyMaterialCallback_19(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyMaterialCallback_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyMaterialCallback_19), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedMesh_22() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CachedMesh_22)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_m_CachedMesh_22() const { return ___m_CachedMesh_22; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_m_CachedMesh_22() { return &___m_CachedMesh_22; }
	inline void set_m_CachedMesh_22(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___m_CachedMesh_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedMesh_22), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedUvs_23() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CachedUvs_23)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get_m_CachedUvs_23() const { return ___m_CachedUvs_23; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of_m_CachedUvs_23() { return &___m_CachedUvs_23; }
	inline void set_m_CachedUvs_23(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		___m_CachedUvs_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedUvs_23), (void*)value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_24() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_ColorTweenRunner_24)); }
	inline TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * get_m_ColorTweenRunner_24() const { return ___m_ColorTweenRunner_24; }
	inline TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 ** get_address_of_m_ColorTweenRunner_24() { return &___m_ColorTweenRunner_24; }
	inline void set_m_ColorTweenRunner_24(TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * value)
	{
		___m_ColorTweenRunner_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ColorTweenRunner_24), (void*)value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_25; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_25(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_25 = value;
	}
};

struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___s_Mesh_20;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * ___s_VertexHelper_21;

public:
	inline static int32_t get_offset_of_s_DefaultUI_4() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_DefaultUI_4)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_s_DefaultUI_4() const { return ___s_DefaultUI_4; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_s_DefaultUI_4() { return &___s_DefaultUI_4; }
	inline void set_s_DefaultUI_4(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___s_DefaultUI_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultUI_4), (void*)value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_5() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_WhiteTexture_5)); }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * get_s_WhiteTexture_5() const { return ___s_WhiteTexture_5; }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF ** get_address_of_s_WhiteTexture_5() { return &___s_WhiteTexture_5; }
	inline void set_s_WhiteTexture_5(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * value)
	{
		___s_WhiteTexture_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_WhiteTexture_5), (void*)value);
	}

	inline static int32_t get_offset_of_s_Mesh_20() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_Mesh_20)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_s_Mesh_20() const { return ___s_Mesh_20; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_s_Mesh_20() { return &___s_Mesh_20; }
	inline void set_s_Mesh_20(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___s_Mesh_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Mesh_20), (void*)value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_21() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_VertexHelper_21)); }
	inline VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * get_s_VertexHelper_21() const { return ___s_VertexHelper_21; }
	inline VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 ** get_address_of_s_VertexHelper_21() { return &___s_VertexHelper_21; }
	inline void set_s_VertexHelper_21(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * value)
	{
		___s_VertexHelper_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_VertexHelper_21), (void*)value);
	}
};


// UnityEngine.UI.Selectable
struct Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD  : public UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E
{
public:
	// System.Boolean UnityEngine.UI.Selectable::m_EnableCalled
	bool ___m_EnableCalled_6;
	// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::m_Navigation
	Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A  ___m_Navigation_7;
	// UnityEngine.UI.Selectable/Transition UnityEngine.UI.Selectable::m_Transition
	int32_t ___m_Transition_8;
	// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::m_Colors
	ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  ___m_Colors_9;
	// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::m_SpriteState
	SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E  ___m_SpriteState_10;
	// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::m_AnimationTriggers
	AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 * ___m_AnimationTriggers_11;
	// System.Boolean UnityEngine.UI.Selectable::m_Interactable
	bool ___m_Interactable_12;
	// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::m_TargetGraphic
	Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * ___m_TargetGraphic_13;
	// System.Boolean UnityEngine.UI.Selectable::m_GroupsAllowInteraction
	bool ___m_GroupsAllowInteraction_14;
	// System.Int32 UnityEngine.UI.Selectable::m_CurrentIndex
	int32_t ___m_CurrentIndex_15;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerInside>k__BackingField
	bool ___U3CisPointerInsideU3Ek__BackingField_16;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerDown>k__BackingField
	bool ___U3CisPointerDownU3Ek__BackingField_17;
	// System.Boolean UnityEngine.UI.Selectable::<hasSelection>k__BackingField
	bool ___U3ChasSelectionU3Ek__BackingField_18;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> UnityEngine.UI.Selectable::m_CanvasGroupCache
	List_1_t34AA4AF4E7352129CA58045901530E41445AC16D * ___m_CanvasGroupCache_19;

public:
	inline static int32_t get_offset_of_m_EnableCalled_6() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_EnableCalled_6)); }
	inline bool get_m_EnableCalled_6() const { return ___m_EnableCalled_6; }
	inline bool* get_address_of_m_EnableCalled_6() { return &___m_EnableCalled_6; }
	inline void set_m_EnableCalled_6(bool value)
	{
		___m_EnableCalled_6 = value;
	}

	inline static int32_t get_offset_of_m_Navigation_7() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Navigation_7)); }
	inline Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A  get_m_Navigation_7() const { return ___m_Navigation_7; }
	inline Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A * get_address_of_m_Navigation_7() { return &___m_Navigation_7; }
	inline void set_m_Navigation_7(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A  value)
	{
		___m_Navigation_7 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnUp_2), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnDown_3), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnLeft_4), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnRight_5), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_Transition_8() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Transition_8)); }
	inline int32_t get_m_Transition_8() const { return ___m_Transition_8; }
	inline int32_t* get_address_of_m_Transition_8() { return &___m_Transition_8; }
	inline void set_m_Transition_8(int32_t value)
	{
		___m_Transition_8 = value;
	}

	inline static int32_t get_offset_of_m_Colors_9() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Colors_9)); }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  get_m_Colors_9() const { return ___m_Colors_9; }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955 * get_address_of_m_Colors_9() { return &___m_Colors_9; }
	inline void set_m_Colors_9(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  value)
	{
		___m_Colors_9 = value;
	}

	inline static int32_t get_offset_of_m_SpriteState_10() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_SpriteState_10)); }
	inline SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E  get_m_SpriteState_10() const { return ___m_SpriteState_10; }
	inline SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E * get_address_of_m_SpriteState_10() { return &___m_SpriteState_10; }
	inline void set_m_SpriteState_10(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E  value)
	{
		___m_SpriteState_10 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_HighlightedSprite_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_PressedSprite_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_SelectedSprite_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_DisabledSprite_3), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_AnimationTriggers_11() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_AnimationTriggers_11)); }
	inline AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 * get_m_AnimationTriggers_11() const { return ___m_AnimationTriggers_11; }
	inline AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 ** get_address_of_m_AnimationTriggers_11() { return &___m_AnimationTriggers_11; }
	inline void set_m_AnimationTriggers_11(AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 * value)
	{
		___m_AnimationTriggers_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AnimationTriggers_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_Interactable_12() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Interactable_12)); }
	inline bool get_m_Interactable_12() const { return ___m_Interactable_12; }
	inline bool* get_address_of_m_Interactable_12() { return &___m_Interactable_12; }
	inline void set_m_Interactable_12(bool value)
	{
		___m_Interactable_12 = value;
	}

	inline static int32_t get_offset_of_m_TargetGraphic_13() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_TargetGraphic_13)); }
	inline Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * get_m_TargetGraphic_13() const { return ___m_TargetGraphic_13; }
	inline Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 ** get_address_of_m_TargetGraphic_13() { return &___m_TargetGraphic_13; }
	inline void set_m_TargetGraphic_13(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * value)
	{
		___m_TargetGraphic_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TargetGraphic_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_GroupsAllowInteraction_14() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_GroupsAllowInteraction_14)); }
	inline bool get_m_GroupsAllowInteraction_14() const { return ___m_GroupsAllowInteraction_14; }
	inline bool* get_address_of_m_GroupsAllowInteraction_14() { return &___m_GroupsAllowInteraction_14; }
	inline void set_m_GroupsAllowInteraction_14(bool value)
	{
		___m_GroupsAllowInteraction_14 = value;
	}

	inline static int32_t get_offset_of_m_CurrentIndex_15() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_CurrentIndex_15)); }
	inline int32_t get_m_CurrentIndex_15() const { return ___m_CurrentIndex_15; }
	inline int32_t* get_address_of_m_CurrentIndex_15() { return &___m_CurrentIndex_15; }
	inline void set_m_CurrentIndex_15(int32_t value)
	{
		___m_CurrentIndex_15 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerInsideU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___U3CisPointerInsideU3Ek__BackingField_16)); }
	inline bool get_U3CisPointerInsideU3Ek__BackingField_16() const { return ___U3CisPointerInsideU3Ek__BackingField_16; }
	inline bool* get_address_of_U3CisPointerInsideU3Ek__BackingField_16() { return &___U3CisPointerInsideU3Ek__BackingField_16; }
	inline void set_U3CisPointerInsideU3Ek__BackingField_16(bool value)
	{
		___U3CisPointerInsideU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerDownU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___U3CisPointerDownU3Ek__BackingField_17)); }
	inline bool get_U3CisPointerDownU3Ek__BackingField_17() const { return ___U3CisPointerDownU3Ek__BackingField_17; }
	inline bool* get_address_of_U3CisPointerDownU3Ek__BackingField_17() { return &___U3CisPointerDownU3Ek__BackingField_17; }
	inline void set_U3CisPointerDownU3Ek__BackingField_17(bool value)
	{
		___U3CisPointerDownU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3ChasSelectionU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___U3ChasSelectionU3Ek__BackingField_18)); }
	inline bool get_U3ChasSelectionU3Ek__BackingField_18() const { return ___U3ChasSelectionU3Ek__BackingField_18; }
	inline bool* get_address_of_U3ChasSelectionU3Ek__BackingField_18() { return &___U3ChasSelectionU3Ek__BackingField_18; }
	inline void set_U3ChasSelectionU3Ek__BackingField_18(bool value)
	{
		___U3ChasSelectionU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_m_CanvasGroupCache_19() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_CanvasGroupCache_19)); }
	inline List_1_t34AA4AF4E7352129CA58045901530E41445AC16D * get_m_CanvasGroupCache_19() const { return ___m_CanvasGroupCache_19; }
	inline List_1_t34AA4AF4E7352129CA58045901530E41445AC16D ** get_address_of_m_CanvasGroupCache_19() { return &___m_CanvasGroupCache_19; }
	inline void set_m_CanvasGroupCache_19(List_1_t34AA4AF4E7352129CA58045901530E41445AC16D * value)
	{
		___m_CanvasGroupCache_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CanvasGroupCache_19), (void*)value);
	}
};

struct Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD_StaticFields
{
public:
	// UnityEngine.UI.Selectable[] UnityEngine.UI.Selectable::s_Selectables
	SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535* ___s_Selectables_4;
	// System.Int32 UnityEngine.UI.Selectable::s_SelectableCount
	int32_t ___s_SelectableCount_5;

public:
	inline static int32_t get_offset_of_s_Selectables_4() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD_StaticFields, ___s_Selectables_4)); }
	inline SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535* get_s_Selectables_4() const { return ___s_Selectables_4; }
	inline SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535** get_address_of_s_Selectables_4() { return &___s_Selectables_4; }
	inline void set_s_Selectables_4(SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535* value)
	{
		___s_Selectables_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Selectables_4), (void*)value);
	}

	inline static int32_t get_offset_of_s_SelectableCount_5() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD_StaticFields, ___s_SelectableCount_5)); }
	inline int32_t get_s_SelectableCount_5() const { return ___s_SelectableCount_5; }
	inline int32_t* get_address_of_s_SelectableCount_5() { return &___s_SelectableCount_5; }
	inline void set_s_SelectableCount_5(int32_t value)
	{
		___s_SelectableCount_5 = value;
	}
};


// SoloFish
struct SoloFish_t248C1128425014659AF96F9294BAC7D1799179C2  : public Fish_t45F7EC0A7EE404E12CC412CF93723EA8799CA46C
{
public:
	// UnityEngine.Bounds SoloFish::boundBox
	Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  ___boundBox_5;
	// System.Single SoloFish::smoothDamp
	float ___smoothDamp_6;
	// UnityEngine.Vector3[] SoloFish::collisionRays
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___collisionRays_7;
	// System.Single SoloFish::boundsDistance
	float ___boundsDistance_8;
	// System.Single SoloFish::obstacleDistance
	float ___obstacleDistance_9;
	// System.Single SoloFish::speed
	float ___speed_10;
	// UnityEngine.LayerMask SoloFish::obstacleMask
	LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  ___obstacleMask_11;
	// System.Single SoloFish::boundsWeight
	float ___boundsWeight_12;
	// System.Single SoloFish::obstacleWeight
	float ___obstacleWeight_13;
	// System.Single SoloFish::targetWeight
	float ___targetWeight_14;
	// UnityEngine.Vector3 SoloFish::currentVelocity
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___currentVelocity_15;
	// UnityEngine.Vector3 SoloFish::currentAvoidance
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___currentAvoidance_16;
	// UnityEngine.Vector3 SoloFish::targetPosition
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___targetPosition_17;

public:
	inline static int32_t get_offset_of_boundBox_5() { return static_cast<int32_t>(offsetof(SoloFish_t248C1128425014659AF96F9294BAC7D1799179C2, ___boundBox_5)); }
	inline Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  get_boundBox_5() const { return ___boundBox_5; }
	inline Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 * get_address_of_boundBox_5() { return &___boundBox_5; }
	inline void set_boundBox_5(Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  value)
	{
		___boundBox_5 = value;
	}

	inline static int32_t get_offset_of_smoothDamp_6() { return static_cast<int32_t>(offsetof(SoloFish_t248C1128425014659AF96F9294BAC7D1799179C2, ___smoothDamp_6)); }
	inline float get_smoothDamp_6() const { return ___smoothDamp_6; }
	inline float* get_address_of_smoothDamp_6() { return &___smoothDamp_6; }
	inline void set_smoothDamp_6(float value)
	{
		___smoothDamp_6 = value;
	}

	inline static int32_t get_offset_of_collisionRays_7() { return static_cast<int32_t>(offsetof(SoloFish_t248C1128425014659AF96F9294BAC7D1799179C2, ___collisionRays_7)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_collisionRays_7() const { return ___collisionRays_7; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_collisionRays_7() { return &___collisionRays_7; }
	inline void set_collisionRays_7(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___collisionRays_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___collisionRays_7), (void*)value);
	}

	inline static int32_t get_offset_of_boundsDistance_8() { return static_cast<int32_t>(offsetof(SoloFish_t248C1128425014659AF96F9294BAC7D1799179C2, ___boundsDistance_8)); }
	inline float get_boundsDistance_8() const { return ___boundsDistance_8; }
	inline float* get_address_of_boundsDistance_8() { return &___boundsDistance_8; }
	inline void set_boundsDistance_8(float value)
	{
		___boundsDistance_8 = value;
	}

	inline static int32_t get_offset_of_obstacleDistance_9() { return static_cast<int32_t>(offsetof(SoloFish_t248C1128425014659AF96F9294BAC7D1799179C2, ___obstacleDistance_9)); }
	inline float get_obstacleDistance_9() const { return ___obstacleDistance_9; }
	inline float* get_address_of_obstacleDistance_9() { return &___obstacleDistance_9; }
	inline void set_obstacleDistance_9(float value)
	{
		___obstacleDistance_9 = value;
	}

	inline static int32_t get_offset_of_speed_10() { return static_cast<int32_t>(offsetof(SoloFish_t248C1128425014659AF96F9294BAC7D1799179C2, ___speed_10)); }
	inline float get_speed_10() const { return ___speed_10; }
	inline float* get_address_of_speed_10() { return &___speed_10; }
	inline void set_speed_10(float value)
	{
		___speed_10 = value;
	}

	inline static int32_t get_offset_of_obstacleMask_11() { return static_cast<int32_t>(offsetof(SoloFish_t248C1128425014659AF96F9294BAC7D1799179C2, ___obstacleMask_11)); }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  get_obstacleMask_11() const { return ___obstacleMask_11; }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 * get_address_of_obstacleMask_11() { return &___obstacleMask_11; }
	inline void set_obstacleMask_11(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  value)
	{
		___obstacleMask_11 = value;
	}

	inline static int32_t get_offset_of_boundsWeight_12() { return static_cast<int32_t>(offsetof(SoloFish_t248C1128425014659AF96F9294BAC7D1799179C2, ___boundsWeight_12)); }
	inline float get_boundsWeight_12() const { return ___boundsWeight_12; }
	inline float* get_address_of_boundsWeight_12() { return &___boundsWeight_12; }
	inline void set_boundsWeight_12(float value)
	{
		___boundsWeight_12 = value;
	}

	inline static int32_t get_offset_of_obstacleWeight_13() { return static_cast<int32_t>(offsetof(SoloFish_t248C1128425014659AF96F9294BAC7D1799179C2, ___obstacleWeight_13)); }
	inline float get_obstacleWeight_13() const { return ___obstacleWeight_13; }
	inline float* get_address_of_obstacleWeight_13() { return &___obstacleWeight_13; }
	inline void set_obstacleWeight_13(float value)
	{
		___obstacleWeight_13 = value;
	}

	inline static int32_t get_offset_of_targetWeight_14() { return static_cast<int32_t>(offsetof(SoloFish_t248C1128425014659AF96F9294BAC7D1799179C2, ___targetWeight_14)); }
	inline float get_targetWeight_14() const { return ___targetWeight_14; }
	inline float* get_address_of_targetWeight_14() { return &___targetWeight_14; }
	inline void set_targetWeight_14(float value)
	{
		___targetWeight_14 = value;
	}

	inline static int32_t get_offset_of_currentVelocity_15() { return static_cast<int32_t>(offsetof(SoloFish_t248C1128425014659AF96F9294BAC7D1799179C2, ___currentVelocity_15)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_currentVelocity_15() const { return ___currentVelocity_15; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_currentVelocity_15() { return &___currentVelocity_15; }
	inline void set_currentVelocity_15(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___currentVelocity_15 = value;
	}

	inline static int32_t get_offset_of_currentAvoidance_16() { return static_cast<int32_t>(offsetof(SoloFish_t248C1128425014659AF96F9294BAC7D1799179C2, ___currentAvoidance_16)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_currentAvoidance_16() const { return ___currentAvoidance_16; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_currentAvoidance_16() { return &___currentAvoidance_16; }
	inline void set_currentAvoidance_16(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___currentAvoidance_16 = value;
	}

	inline static int32_t get_offset_of_targetPosition_17() { return static_cast<int32_t>(offsetof(SoloFish_t248C1128425014659AF96F9294BAC7D1799179C2, ___targetPosition_17)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_targetPosition_17() const { return ___targetPosition_17; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_targetPosition_17() { return &___targetPosition_17; }
	inline void set_targetPosition_17(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___targetPosition_17 = value;
	}
};


// TestFIsh
struct TestFIsh_t2AC494E6BA9A5DD9FB776A1C4D31A78DC0AD98B2  : public Fish_t45F7EC0A7EE404E12CC412CF93723EA8799CA46C
{
public:

public:
};


// UnityEngine.UI.Dropdown
struct Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96  : public Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.Dropdown::m_Template
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_Template_20;
	// UnityEngine.UI.Text UnityEngine.UI.Dropdown::m_CaptionText
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___m_CaptionText_21;
	// UnityEngine.UI.Image UnityEngine.UI.Dropdown::m_CaptionImage
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___m_CaptionImage_22;
	// UnityEngine.UI.Text UnityEngine.UI.Dropdown::m_ItemText
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___m_ItemText_23;
	// UnityEngine.UI.Image UnityEngine.UI.Dropdown::m_ItemImage
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___m_ItemImage_24;
	// System.Int32 UnityEngine.UI.Dropdown::m_Value
	int32_t ___m_Value_25;
	// UnityEngine.UI.Dropdown/OptionDataList UnityEngine.UI.Dropdown::m_Options
	OptionDataList_t524EBDB7A2B178269FD5B4740108D0EC6404B4B6 * ___m_Options_26;
	// UnityEngine.UI.Dropdown/DropdownEvent UnityEngine.UI.Dropdown::m_OnValueChanged
	DropdownEvent_tEB2C75C3DBC789936B31D9A979FD62E047846CFB * ___m_OnValueChanged_27;
	// System.Single UnityEngine.UI.Dropdown::m_AlphaFadeSpeed
	float ___m_AlphaFadeSpeed_28;
	// UnityEngine.GameObject UnityEngine.UI.Dropdown::m_Dropdown
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___m_Dropdown_29;
	// UnityEngine.GameObject UnityEngine.UI.Dropdown::m_Blocker
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___m_Blocker_30;
	// System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem> UnityEngine.UI.Dropdown::m_Items
	List_1_t4CFF6A6E1A912AE4990A34B2AA4A1FE2C9FB0033 * ___m_Items_31;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween> UnityEngine.UI.Dropdown::m_AlphaTweenRunner
	TweenRunner_1_t428873023FD8831B6DCE3CBD53ADD7D37AC8222D * ___m_AlphaTweenRunner_32;
	// System.Boolean UnityEngine.UI.Dropdown::validTemplate
	bool ___validTemplate_33;

public:
	inline static int32_t get_offset_of_m_Template_20() { return static_cast<int32_t>(offsetof(Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96, ___m_Template_20)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_Template_20() const { return ___m_Template_20; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_Template_20() { return &___m_Template_20; }
	inline void set_m_Template_20(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_Template_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Template_20), (void*)value);
	}

	inline static int32_t get_offset_of_m_CaptionText_21() { return static_cast<int32_t>(offsetof(Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96, ___m_CaptionText_21)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_m_CaptionText_21() const { return ___m_CaptionText_21; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_m_CaptionText_21() { return &___m_CaptionText_21; }
	inline void set_m_CaptionText_21(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___m_CaptionText_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CaptionText_21), (void*)value);
	}

	inline static int32_t get_offset_of_m_CaptionImage_22() { return static_cast<int32_t>(offsetof(Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96, ___m_CaptionImage_22)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_m_CaptionImage_22() const { return ___m_CaptionImage_22; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_m_CaptionImage_22() { return &___m_CaptionImage_22; }
	inline void set_m_CaptionImage_22(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___m_CaptionImage_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CaptionImage_22), (void*)value);
	}

	inline static int32_t get_offset_of_m_ItemText_23() { return static_cast<int32_t>(offsetof(Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96, ___m_ItemText_23)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_m_ItemText_23() const { return ___m_ItemText_23; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_m_ItemText_23() { return &___m_ItemText_23; }
	inline void set_m_ItemText_23(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___m_ItemText_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ItemText_23), (void*)value);
	}

	inline static int32_t get_offset_of_m_ItemImage_24() { return static_cast<int32_t>(offsetof(Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96, ___m_ItemImage_24)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_m_ItemImage_24() const { return ___m_ItemImage_24; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_m_ItemImage_24() { return &___m_ItemImage_24; }
	inline void set_m_ItemImage_24(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___m_ItemImage_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ItemImage_24), (void*)value);
	}

	inline static int32_t get_offset_of_m_Value_25() { return static_cast<int32_t>(offsetof(Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96, ___m_Value_25)); }
	inline int32_t get_m_Value_25() const { return ___m_Value_25; }
	inline int32_t* get_address_of_m_Value_25() { return &___m_Value_25; }
	inline void set_m_Value_25(int32_t value)
	{
		___m_Value_25 = value;
	}

	inline static int32_t get_offset_of_m_Options_26() { return static_cast<int32_t>(offsetof(Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96, ___m_Options_26)); }
	inline OptionDataList_t524EBDB7A2B178269FD5B4740108D0EC6404B4B6 * get_m_Options_26() const { return ___m_Options_26; }
	inline OptionDataList_t524EBDB7A2B178269FD5B4740108D0EC6404B4B6 ** get_address_of_m_Options_26() { return &___m_Options_26; }
	inline void set_m_Options_26(OptionDataList_t524EBDB7A2B178269FD5B4740108D0EC6404B4B6 * value)
	{
		___m_Options_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Options_26), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnValueChanged_27() { return static_cast<int32_t>(offsetof(Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96, ___m_OnValueChanged_27)); }
	inline DropdownEvent_tEB2C75C3DBC789936B31D9A979FD62E047846CFB * get_m_OnValueChanged_27() const { return ___m_OnValueChanged_27; }
	inline DropdownEvent_tEB2C75C3DBC789936B31D9A979FD62E047846CFB ** get_address_of_m_OnValueChanged_27() { return &___m_OnValueChanged_27; }
	inline void set_m_OnValueChanged_27(DropdownEvent_tEB2C75C3DBC789936B31D9A979FD62E047846CFB * value)
	{
		___m_OnValueChanged_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnValueChanged_27), (void*)value);
	}

	inline static int32_t get_offset_of_m_AlphaFadeSpeed_28() { return static_cast<int32_t>(offsetof(Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96, ___m_AlphaFadeSpeed_28)); }
	inline float get_m_AlphaFadeSpeed_28() const { return ___m_AlphaFadeSpeed_28; }
	inline float* get_address_of_m_AlphaFadeSpeed_28() { return &___m_AlphaFadeSpeed_28; }
	inline void set_m_AlphaFadeSpeed_28(float value)
	{
		___m_AlphaFadeSpeed_28 = value;
	}

	inline static int32_t get_offset_of_m_Dropdown_29() { return static_cast<int32_t>(offsetof(Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96, ___m_Dropdown_29)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_m_Dropdown_29() const { return ___m_Dropdown_29; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_m_Dropdown_29() { return &___m_Dropdown_29; }
	inline void set_m_Dropdown_29(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___m_Dropdown_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Dropdown_29), (void*)value);
	}

	inline static int32_t get_offset_of_m_Blocker_30() { return static_cast<int32_t>(offsetof(Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96, ___m_Blocker_30)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_m_Blocker_30() const { return ___m_Blocker_30; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_m_Blocker_30() { return &___m_Blocker_30; }
	inline void set_m_Blocker_30(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___m_Blocker_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Blocker_30), (void*)value);
	}

	inline static int32_t get_offset_of_m_Items_31() { return static_cast<int32_t>(offsetof(Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96, ___m_Items_31)); }
	inline List_1_t4CFF6A6E1A912AE4990A34B2AA4A1FE2C9FB0033 * get_m_Items_31() const { return ___m_Items_31; }
	inline List_1_t4CFF6A6E1A912AE4990A34B2AA4A1FE2C9FB0033 ** get_address_of_m_Items_31() { return &___m_Items_31; }
	inline void set_m_Items_31(List_1_t4CFF6A6E1A912AE4990A34B2AA4A1FE2C9FB0033 * value)
	{
		___m_Items_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Items_31), (void*)value);
	}

	inline static int32_t get_offset_of_m_AlphaTweenRunner_32() { return static_cast<int32_t>(offsetof(Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96, ___m_AlphaTweenRunner_32)); }
	inline TweenRunner_1_t428873023FD8831B6DCE3CBD53ADD7D37AC8222D * get_m_AlphaTweenRunner_32() const { return ___m_AlphaTweenRunner_32; }
	inline TweenRunner_1_t428873023FD8831B6DCE3CBD53ADD7D37AC8222D ** get_address_of_m_AlphaTweenRunner_32() { return &___m_AlphaTweenRunner_32; }
	inline void set_m_AlphaTweenRunner_32(TweenRunner_1_t428873023FD8831B6DCE3CBD53ADD7D37AC8222D * value)
	{
		___m_AlphaTweenRunner_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AlphaTweenRunner_32), (void*)value);
	}

	inline static int32_t get_offset_of_validTemplate_33() { return static_cast<int32_t>(offsetof(Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96, ___validTemplate_33)); }
	inline bool get_validTemplate_33() const { return ___validTemplate_33; }
	inline bool* get_address_of_validTemplate_33() { return &___validTemplate_33; }
	inline void set_validTemplate_33(bool value)
	{
		___validTemplate_33 = value;
	}
};

struct Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96_StaticFields
{
public:
	// UnityEngine.UI.Dropdown/OptionData UnityEngine.UI.Dropdown::s_NoOptionData
	OptionData_t5F665DC13C1E4307727D66ECC1100B3A77E3E857 * ___s_NoOptionData_34;

public:
	inline static int32_t get_offset_of_s_NoOptionData_34() { return static_cast<int32_t>(offsetof(Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96_StaticFields, ___s_NoOptionData_34)); }
	inline OptionData_t5F665DC13C1E4307727D66ECC1100B3A77E3E857 * get_s_NoOptionData_34() const { return ___s_NoOptionData_34; }
	inline OptionData_t5F665DC13C1E4307727D66ECC1100B3A77E3E857 ** get_address_of_s_NoOptionData_34() { return &___s_NoOptionData_34; }
	inline void set_s_NoOptionData_34(OptionData_t5F665DC13C1E4307727D66ECC1100B3A77E3E857 * value)
	{
		___s_NoOptionData_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_NoOptionData_34), (void*)value);
	}
};


// UnityEngine.UI.MaskableGraphic
struct MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE  : public Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_26;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_MaskMaterial_27;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * ___m_ParentMask_28;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_29;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IsMaskingGraphic
	bool ___m_IsMaskingGraphic_30;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_31;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * ___m_OnCullStateChanged_32;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_33;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_34;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___m_Corners_35;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ShouldRecalculateStencil_26)); }
	inline bool get_m_ShouldRecalculateStencil_26() const { return ___m_ShouldRecalculateStencil_26; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_26() { return &___m_ShouldRecalculateStencil_26; }
	inline void set_m_ShouldRecalculateStencil_26(bool value)
	{
		___m_ShouldRecalculateStencil_26 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_MaskMaterial_27)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_MaskMaterial_27() const { return ___m_MaskMaterial_27; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_MaskMaterial_27() { return &___m_MaskMaterial_27; }
	inline void set_m_MaskMaterial_27(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_MaskMaterial_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_MaskMaterial_27), (void*)value);
	}

	inline static int32_t get_offset_of_m_ParentMask_28() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ParentMask_28)); }
	inline RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * get_m_ParentMask_28() const { return ___m_ParentMask_28; }
	inline RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 ** get_address_of_m_ParentMask_28() { return &___m_ParentMask_28; }
	inline void set_m_ParentMask_28(RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * value)
	{
		___m_ParentMask_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParentMask_28), (void*)value);
	}

	inline static int32_t get_offset_of_m_Maskable_29() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_Maskable_29)); }
	inline bool get_m_Maskable_29() const { return ___m_Maskable_29; }
	inline bool* get_address_of_m_Maskable_29() { return &___m_Maskable_29; }
	inline void set_m_Maskable_29(bool value)
	{
		___m_Maskable_29 = value;
	}

	inline static int32_t get_offset_of_m_IsMaskingGraphic_30() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_IsMaskingGraphic_30)); }
	inline bool get_m_IsMaskingGraphic_30() const { return ___m_IsMaskingGraphic_30; }
	inline bool* get_address_of_m_IsMaskingGraphic_30() { return &___m_IsMaskingGraphic_30; }
	inline void set_m_IsMaskingGraphic_30(bool value)
	{
		___m_IsMaskingGraphic_30 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_31() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_IncludeForMasking_31)); }
	inline bool get_m_IncludeForMasking_31() const { return ___m_IncludeForMasking_31; }
	inline bool* get_address_of_m_IncludeForMasking_31() { return &___m_IncludeForMasking_31; }
	inline void set_m_IncludeForMasking_31(bool value)
	{
		___m_IncludeForMasking_31 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_32() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_OnCullStateChanged_32)); }
	inline CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * get_m_OnCullStateChanged_32() const { return ___m_OnCullStateChanged_32; }
	inline CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 ** get_address_of_m_OnCullStateChanged_32() { return &___m_OnCullStateChanged_32; }
	inline void set_m_OnCullStateChanged_32(CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * value)
	{
		___m_OnCullStateChanged_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnCullStateChanged_32), (void*)value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_33() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ShouldRecalculate_33)); }
	inline bool get_m_ShouldRecalculate_33() const { return ___m_ShouldRecalculate_33; }
	inline bool* get_address_of_m_ShouldRecalculate_33() { return &___m_ShouldRecalculate_33; }
	inline void set_m_ShouldRecalculate_33(bool value)
	{
		___m_ShouldRecalculate_33 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_34() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_StencilValue_34)); }
	inline int32_t get_m_StencilValue_34() const { return ___m_StencilValue_34; }
	inline int32_t* get_address_of_m_StencilValue_34() { return &___m_StencilValue_34; }
	inline void set_m_StencilValue_34(int32_t value)
	{
		___m_StencilValue_34 = value;
	}

	inline static int32_t get_offset_of_m_Corners_35() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_Corners_35)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_m_Corners_35() const { return ___m_Corners_35; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_m_Corners_35() { return &___m_Corners_35; }
	inline void set_m_Corners_35(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___m_Corners_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Corners_35), (void*)value);
	}
};


// UnityEngine.UI.Image
struct Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C  : public MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE
{
public:
	// UnityEngine.Sprite UnityEngine.UI.Image::m_Sprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_Sprite_37;
	// UnityEngine.Sprite UnityEngine.UI.Image::m_OverrideSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_OverrideSprite_38;
	// UnityEngine.UI.Image/Type UnityEngine.UI.Image::m_Type
	int32_t ___m_Type_39;
	// System.Boolean UnityEngine.UI.Image::m_PreserveAspect
	bool ___m_PreserveAspect_40;
	// System.Boolean UnityEngine.UI.Image::m_FillCenter
	bool ___m_FillCenter_41;
	// UnityEngine.UI.Image/FillMethod UnityEngine.UI.Image::m_FillMethod
	int32_t ___m_FillMethod_42;
	// System.Single UnityEngine.UI.Image::m_FillAmount
	float ___m_FillAmount_43;
	// System.Boolean UnityEngine.UI.Image::m_FillClockwise
	bool ___m_FillClockwise_44;
	// System.Int32 UnityEngine.UI.Image::m_FillOrigin
	int32_t ___m_FillOrigin_45;
	// System.Single UnityEngine.UI.Image::m_AlphaHitTestMinimumThreshold
	float ___m_AlphaHitTestMinimumThreshold_46;
	// System.Boolean UnityEngine.UI.Image::m_Tracked
	bool ___m_Tracked_47;
	// System.Boolean UnityEngine.UI.Image::m_UseSpriteMesh
	bool ___m_UseSpriteMesh_48;
	// System.Single UnityEngine.UI.Image::m_PixelsPerUnitMultiplier
	float ___m_PixelsPerUnitMultiplier_49;
	// System.Single UnityEngine.UI.Image::m_CachedReferencePixelsPerUnit
	float ___m_CachedReferencePixelsPerUnit_50;

public:
	inline static int32_t get_offset_of_m_Sprite_37() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_Sprite_37)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_Sprite_37() const { return ___m_Sprite_37; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_Sprite_37() { return &___m_Sprite_37; }
	inline void set_m_Sprite_37(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_Sprite_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Sprite_37), (void*)value);
	}

	inline static int32_t get_offset_of_m_OverrideSprite_38() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_OverrideSprite_38)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_OverrideSprite_38() const { return ___m_OverrideSprite_38; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_OverrideSprite_38() { return &___m_OverrideSprite_38; }
	inline void set_m_OverrideSprite_38(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_OverrideSprite_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OverrideSprite_38), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type_39() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_Type_39)); }
	inline int32_t get_m_Type_39() const { return ___m_Type_39; }
	inline int32_t* get_address_of_m_Type_39() { return &___m_Type_39; }
	inline void set_m_Type_39(int32_t value)
	{
		___m_Type_39 = value;
	}

	inline static int32_t get_offset_of_m_PreserveAspect_40() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_PreserveAspect_40)); }
	inline bool get_m_PreserveAspect_40() const { return ___m_PreserveAspect_40; }
	inline bool* get_address_of_m_PreserveAspect_40() { return &___m_PreserveAspect_40; }
	inline void set_m_PreserveAspect_40(bool value)
	{
		___m_PreserveAspect_40 = value;
	}

	inline static int32_t get_offset_of_m_FillCenter_41() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillCenter_41)); }
	inline bool get_m_FillCenter_41() const { return ___m_FillCenter_41; }
	inline bool* get_address_of_m_FillCenter_41() { return &___m_FillCenter_41; }
	inline void set_m_FillCenter_41(bool value)
	{
		___m_FillCenter_41 = value;
	}

	inline static int32_t get_offset_of_m_FillMethod_42() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillMethod_42)); }
	inline int32_t get_m_FillMethod_42() const { return ___m_FillMethod_42; }
	inline int32_t* get_address_of_m_FillMethod_42() { return &___m_FillMethod_42; }
	inline void set_m_FillMethod_42(int32_t value)
	{
		___m_FillMethod_42 = value;
	}

	inline static int32_t get_offset_of_m_FillAmount_43() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillAmount_43)); }
	inline float get_m_FillAmount_43() const { return ___m_FillAmount_43; }
	inline float* get_address_of_m_FillAmount_43() { return &___m_FillAmount_43; }
	inline void set_m_FillAmount_43(float value)
	{
		___m_FillAmount_43 = value;
	}

	inline static int32_t get_offset_of_m_FillClockwise_44() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillClockwise_44)); }
	inline bool get_m_FillClockwise_44() const { return ___m_FillClockwise_44; }
	inline bool* get_address_of_m_FillClockwise_44() { return &___m_FillClockwise_44; }
	inline void set_m_FillClockwise_44(bool value)
	{
		___m_FillClockwise_44 = value;
	}

	inline static int32_t get_offset_of_m_FillOrigin_45() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillOrigin_45)); }
	inline int32_t get_m_FillOrigin_45() const { return ___m_FillOrigin_45; }
	inline int32_t* get_address_of_m_FillOrigin_45() { return &___m_FillOrigin_45; }
	inline void set_m_FillOrigin_45(int32_t value)
	{
		___m_FillOrigin_45 = value;
	}

	inline static int32_t get_offset_of_m_AlphaHitTestMinimumThreshold_46() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_AlphaHitTestMinimumThreshold_46)); }
	inline float get_m_AlphaHitTestMinimumThreshold_46() const { return ___m_AlphaHitTestMinimumThreshold_46; }
	inline float* get_address_of_m_AlphaHitTestMinimumThreshold_46() { return &___m_AlphaHitTestMinimumThreshold_46; }
	inline void set_m_AlphaHitTestMinimumThreshold_46(float value)
	{
		___m_AlphaHitTestMinimumThreshold_46 = value;
	}

	inline static int32_t get_offset_of_m_Tracked_47() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_Tracked_47)); }
	inline bool get_m_Tracked_47() const { return ___m_Tracked_47; }
	inline bool* get_address_of_m_Tracked_47() { return &___m_Tracked_47; }
	inline void set_m_Tracked_47(bool value)
	{
		___m_Tracked_47 = value;
	}

	inline static int32_t get_offset_of_m_UseSpriteMesh_48() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_UseSpriteMesh_48)); }
	inline bool get_m_UseSpriteMesh_48() const { return ___m_UseSpriteMesh_48; }
	inline bool* get_address_of_m_UseSpriteMesh_48() { return &___m_UseSpriteMesh_48; }
	inline void set_m_UseSpriteMesh_48(bool value)
	{
		___m_UseSpriteMesh_48 = value;
	}

	inline static int32_t get_offset_of_m_PixelsPerUnitMultiplier_49() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_PixelsPerUnitMultiplier_49)); }
	inline float get_m_PixelsPerUnitMultiplier_49() const { return ___m_PixelsPerUnitMultiplier_49; }
	inline float* get_address_of_m_PixelsPerUnitMultiplier_49() { return &___m_PixelsPerUnitMultiplier_49; }
	inline void set_m_PixelsPerUnitMultiplier_49(float value)
	{
		___m_PixelsPerUnitMultiplier_49 = value;
	}

	inline static int32_t get_offset_of_m_CachedReferencePixelsPerUnit_50() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_CachedReferencePixelsPerUnit_50)); }
	inline float get_m_CachedReferencePixelsPerUnit_50() const { return ___m_CachedReferencePixelsPerUnit_50; }
	inline float* get_address_of_m_CachedReferencePixelsPerUnit_50() { return &___m_CachedReferencePixelsPerUnit_50; }
	inline void set_m_CachedReferencePixelsPerUnit_50(float value)
	{
		___m_CachedReferencePixelsPerUnit_50 = value;
	}
};

struct Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Image::s_ETC1DefaultUI
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___s_ETC1DefaultUI_36;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_VertScratch
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___s_VertScratch_51;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_UVScratch
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___s_UVScratch_52;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Xy
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___s_Xy_53;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Uv
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___s_Uv_54;
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> UnityEngine.UI.Image::m_TrackedTexturelessImages
	List_1_t815A476B0A21E183042059E705F9E505478CD8AE * ___m_TrackedTexturelessImages_55;
	// System.Boolean UnityEngine.UI.Image::s_Initialized
	bool ___s_Initialized_56;

public:
	inline static int32_t get_offset_of_s_ETC1DefaultUI_36() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_ETC1DefaultUI_36)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_s_ETC1DefaultUI_36() const { return ___s_ETC1DefaultUI_36; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_s_ETC1DefaultUI_36() { return &___s_ETC1DefaultUI_36; }
	inline void set_s_ETC1DefaultUI_36(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___s_ETC1DefaultUI_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ETC1DefaultUI_36), (void*)value);
	}

	inline static int32_t get_offset_of_s_VertScratch_51() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_VertScratch_51)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get_s_VertScratch_51() const { return ___s_VertScratch_51; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of_s_VertScratch_51() { return &___s_VertScratch_51; }
	inline void set_s_VertScratch_51(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		___s_VertScratch_51 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_VertScratch_51), (void*)value);
	}

	inline static int32_t get_offset_of_s_UVScratch_52() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_UVScratch_52)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get_s_UVScratch_52() const { return ___s_UVScratch_52; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of_s_UVScratch_52() { return &___s_UVScratch_52; }
	inline void set_s_UVScratch_52(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		___s_UVScratch_52 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_UVScratch_52), (void*)value);
	}

	inline static int32_t get_offset_of_s_Xy_53() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_Xy_53)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_s_Xy_53() const { return ___s_Xy_53; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_s_Xy_53() { return &___s_Xy_53; }
	inline void set_s_Xy_53(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___s_Xy_53 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Xy_53), (void*)value);
	}

	inline static int32_t get_offset_of_s_Uv_54() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_Uv_54)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_s_Uv_54() const { return ___s_Uv_54; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_s_Uv_54() { return &___s_Uv_54; }
	inline void set_s_Uv_54(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___s_Uv_54 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Uv_54), (void*)value);
	}

	inline static int32_t get_offset_of_m_TrackedTexturelessImages_55() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___m_TrackedTexturelessImages_55)); }
	inline List_1_t815A476B0A21E183042059E705F9E505478CD8AE * get_m_TrackedTexturelessImages_55() const { return ___m_TrackedTexturelessImages_55; }
	inline List_1_t815A476B0A21E183042059E705F9E505478CD8AE ** get_address_of_m_TrackedTexturelessImages_55() { return &___m_TrackedTexturelessImages_55; }
	inline void set_m_TrackedTexturelessImages_55(List_1_t815A476B0A21E183042059E705F9E505478CD8AE * value)
	{
		___m_TrackedTexturelessImages_55 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TrackedTexturelessImages_55), (void*)value);
	}

	inline static int32_t get_offset_of_s_Initialized_56() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_Initialized_56)); }
	inline bool get_s_Initialized_56() const { return ___s_Initialized_56; }
	inline bool* get_address_of_s_Initialized_56() { return &___s_Initialized_56; }
	inline void set_s_Initialized_56(bool value)
	{
		___s_Initialized_56 = value;
	}
};


// UnityEngine.UI.RawImage
struct RawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A  : public MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE
{
public:
	// UnityEngine.Texture UnityEngine.UI.RawImage::m_Texture
	Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * ___m_Texture_36;
	// UnityEngine.Rect UnityEngine.UI.RawImage::m_UVRect
	Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  ___m_UVRect_37;

public:
	inline static int32_t get_offset_of_m_Texture_36() { return static_cast<int32_t>(offsetof(RawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A, ___m_Texture_36)); }
	inline Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * get_m_Texture_36() const { return ___m_Texture_36; }
	inline Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE ** get_address_of_m_Texture_36() { return &___m_Texture_36; }
	inline void set_m_Texture_36(Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * value)
	{
		___m_Texture_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Texture_36), (void*)value);
	}

	inline static int32_t get_offset_of_m_UVRect_37() { return static_cast<int32_t>(offsetof(RawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A, ___m_UVRect_37)); }
	inline Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  get_m_UVRect_37() const { return ___m_UVRect_37; }
	inline Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 * get_address_of_m_UVRect_37() { return &___m_UVRect_37; }
	inline void set_m_UVRect_37(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  value)
	{
		___m_UVRect_37 = value;
	}
};


// UnityEngine.UI.Text
struct Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1  : public MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE
{
public:
	// UnityEngine.UI.FontData UnityEngine.UI.Text::m_FontData
	FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 * ___m_FontData_36;
	// System.String UnityEngine.UI.Text::m_Text
	String_t* ___m_Text_37;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCache
	TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * ___m_TextCache_38;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCacheForLayout
	TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * ___m_TextCacheForLayout_39;
	// System.Boolean UnityEngine.UI.Text::m_DisableFontTextureRebuiltCallback
	bool ___m_DisableFontTextureRebuiltCallback_41;
	// UnityEngine.UIVertex[] UnityEngine.UI.Text::m_TempVerts
	UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* ___m_TempVerts_42;

public:
	inline static int32_t get_offset_of_m_FontData_36() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_FontData_36)); }
	inline FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 * get_m_FontData_36() const { return ___m_FontData_36; }
	inline FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 ** get_address_of_m_FontData_36() { return &___m_FontData_36; }
	inline void set_m_FontData_36(FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 * value)
	{
		___m_FontData_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_FontData_36), (void*)value);
	}

	inline static int32_t get_offset_of_m_Text_37() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_Text_37)); }
	inline String_t* get_m_Text_37() const { return ___m_Text_37; }
	inline String_t** get_address_of_m_Text_37() { return &___m_Text_37; }
	inline void set_m_Text_37(String_t* value)
	{
		___m_Text_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Text_37), (void*)value);
	}

	inline static int32_t get_offset_of_m_TextCache_38() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_TextCache_38)); }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * get_m_TextCache_38() const { return ___m_TextCache_38; }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 ** get_address_of_m_TextCache_38() { return &___m_TextCache_38; }
	inline void set_m_TextCache_38(TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * value)
	{
		___m_TextCache_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextCache_38), (void*)value);
	}

	inline static int32_t get_offset_of_m_TextCacheForLayout_39() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_TextCacheForLayout_39)); }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * get_m_TextCacheForLayout_39() const { return ___m_TextCacheForLayout_39; }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 ** get_address_of_m_TextCacheForLayout_39() { return &___m_TextCacheForLayout_39; }
	inline void set_m_TextCacheForLayout_39(TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * value)
	{
		___m_TextCacheForLayout_39 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextCacheForLayout_39), (void*)value);
	}

	inline static int32_t get_offset_of_m_DisableFontTextureRebuiltCallback_41() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_DisableFontTextureRebuiltCallback_41)); }
	inline bool get_m_DisableFontTextureRebuiltCallback_41() const { return ___m_DisableFontTextureRebuiltCallback_41; }
	inline bool* get_address_of_m_DisableFontTextureRebuiltCallback_41() { return &___m_DisableFontTextureRebuiltCallback_41; }
	inline void set_m_DisableFontTextureRebuiltCallback_41(bool value)
	{
		___m_DisableFontTextureRebuiltCallback_41 = value;
	}

	inline static int32_t get_offset_of_m_TempVerts_42() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_TempVerts_42)); }
	inline UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* get_m_TempVerts_42() const { return ___m_TempVerts_42; }
	inline UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A** get_address_of_m_TempVerts_42() { return &___m_TempVerts_42; }
	inline void set_m_TempVerts_42(UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* value)
	{
		___m_TempVerts_42 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TempVerts_42), (void*)value);
	}
};

struct Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Text::s_DefaultText
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___s_DefaultText_40;

public:
	inline static int32_t get_offset_of_s_DefaultText_40() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_StaticFields, ___s_DefaultText_40)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_s_DefaultText_40() const { return ___s_DefaultText_40; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_s_DefaultText_40() { return &___s_DefaultText_40; }
	inline void set_s_DefaultText_40(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___s_DefaultText_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultText_40), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// UnityEngine.UI.Image[]
struct ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * m_Items[1];

public:
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.Collider[]
struct ColliderU5BU5D_t5124940293343DB3D31DA41C593709905906E486  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * m_Items[1];

public:
	inline Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// FlockAgent[]
struct FlockAgentU5BU5D_t012E3C58876A70268E6BFEDBF651E5B8D23AAF84  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04 * m_Items[1];

public:
	inline FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  m_Items[1];

public:
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		m_Items[index] = value;
	}
};
// System.IO.FileInfo[]
struct FileInfoU5BU5D_tB6A5BC6AD9BEC388BF3236B4E8FBB7760F96080B  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) FileInfo_t6C8B2EAA1E23F9E6D7C287C58E4EEEB2049ABAB9 * m_Items[1];

public:
	inline FileInfo_t6C8B2EAA1E23F9E6D7C287C58E4EEEB2049ABAB9 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline FileInfo_t6C8B2EAA1E23F9E6D7C287C58E4EEEB2049ABAB9 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, FileInfo_t6C8B2EAA1E23F9E6D7C287C58E4EEEB2049ABAB9 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline FileInfo_t6C8B2EAA1E23F9E6D7C287C58E4EEEB2049ABAB9 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline FileInfo_t6C8B2EAA1E23F9E6D7C287C58E4EEEB2049ABAB9 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, FileInfo_t6C8B2EAA1E23F9E6D7C287C58E4EEEB2049ABAB9 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Byte[]
struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0,UnityEngine.Transform,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Object_Instantiate_TisRuntimeObject_mD346AB5980B67F04B2C7E12D88693520FFBAD37D_gshared (RuntimeObject * ___original0, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___parent1, bool ___worldPositionStays2, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Add_mE5B3CBB3A625606D9BC4337FEAAF1D66BCB6F96E_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, RuntimeObject * ___item0, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  List_1_GetEnumerator_m1739A5E25DF502A6984F9B98CFCAC2D3FABCF233_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_gshared_inline (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0_gshared (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enumerator_Dispose_mCFB225D9E5E597A1CC8F958E53BEA1367D8AC7B8_gshared (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Clear_m5FB5A9C59D8625FDFB06876C4D8848F0F07ABFD0_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Object_Instantiate_TisRuntimeObject_mB05DEC51C29EF5BB8BD17D055E80217F11E571AA_gshared (RuntimeObject * ___original0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position1, Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___rotation2, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<System.Object>::Remove(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool List_1_Remove_m753F7B4281CC4D02C07AE90726F51EF34B588DF7_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, RuntimeObject * ___item0, const RuntimeMethod* method);
// System.Boolean UnityEngine.GameObject::TryGetComponent<System.Object>(!!0&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GameObject_TryGetComponent_TisRuntimeObject_mC7639C3E3E2A65695113BCF25718826CC392D6CB_gshared (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, RuntimeObject ** ___component0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Component::TryGetComponent<System.Object>(!!0&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Component_TryGetComponent_TisRuntimeObject_m409D64ADC25ED175ADC7ED4C3F7ABF5439AD63AF_gshared (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, RuntimeObject ** ___component0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<System.Int32Enum>::Contains(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool List_1_Contains_m793EB605F2702C16DE665C690BFA9B9675529D94_gshared (List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A * __this, int32_t ___item0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Int32Enum>::Add(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Add_mBA0FDF41792A78B3EB9E395D711706E268313F0F_gshared (List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A * __this, int32_t ___item0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Int32Enum>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_mF1D0377D81949B2ADB6104D9994F7CEE1A1E5040_gshared (List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A * __this, const RuntimeMethod* method);
// System.Boolean InteractionMenu::GetInteractionAsType<System.Object>(InteractionType&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool InteractionMenu_GetInteractionAsType_TisRuntimeObject_mA693C5C52F4B137C964546489620C7088B3F44EE_gshared (InteractionMenu_t7C2715221BAC6E0BB83B07EF82F68C6AA34129CB * __this, RuntimeObject ** ___target0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponentInChildren<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * GameObject_GetComponentInChildren_TisRuntimeObject_mC8FC6687C66150FA89090C2A7733B2EE2E1315FD_gshared (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// StateType FiniteStateMachine.StateMachine::GetCurrentStateAsType<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * StateMachine_GetCurrentStateAsType_TisRuntimeObject_m461BA580D5A4CC0A0D536C14160EDF25F93699E3_gshared (StateMachine_t8E24C2687A18E15B27A6A8FEED2DFE22D910119F * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponentInChildren<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponentInChildren_TisRuntimeObject_mB377B32275A969E0D1A738DBC693DE8EB3593642_gshared (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m1D864B65CCD0498EC4BFFBDA8F8D04AE5333195A_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, RuntimeObject* ___collection0, const RuntimeMethod* method);

// ActionMenu ActionMenu::get_Instance()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ActionMenu_tE3C589CB5B0B532FF5504CF0D558D489281BED01 * ActionMenu_get_Instance_m0A358E0ED4326BDF16E867E22F1E5BF5B9AD2DC0_inline (const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// System.Void ActionMenu::set_Instance(ActionMenu)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void ActionMenu_set_Instance_m4D0D66C264341B224B923BEA333A7257A7B8E82F_inline (ActionMenu_tE3C589CB5B0B532FF5504CF0D558D489281BED01 * ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Behaviour::set_enabled(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Behaviour_set_enabled_mDE415591B28853D1CD764C53CB499A2142247F32 (Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9 * __this, bool ___value0, const RuntimeMethod* method);
// System.Int32 ActionMenu::Deactivate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ActionMenu_Deactivate_mFA0E81D3F5C0B0FC0267242D68B3B0BA21F36D85 (ActionMenu_tE3C589CB5B0B532FF5504CF0D558D489281BED01 * __this, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.GameObject::get_activeSelf()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GameObject_get_activeSelf_m4865097C24FB29F3C31F5C30619AF242297F23EE (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Input::GetAxis(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Input_GetAxis_m939297DEB2ECF8D8D09AD66EB69979AAD2B62326 (String_t* ___axisName0, const RuntimeMethod* method);
// System.Int32 UnityEngine.Mathf::FloorToInt(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Mathf_FloorToInt_m9164D538D17B8C3C8A6C4E4FA95032F757D9091E (float ___f0, const RuntimeMethod* method);
// System.Void ActionMenu::UpdateUISelection()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ActionMenu_UpdateUISelection_mA81DEC42B1B5D91EF6D7FDC89FC3C5BDBD2FC85F (ActionMenu_tE3C589CB5B0B532FF5504CF0D558D489281BED01 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_deltaTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290 (const RuntimeMethod* method);
// System.Boolean UnityEngine.ColorUtility::TryParseHtmlString(System.String,UnityEngine.Color&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ColorUtility_TryParseHtmlString_m69BEFAF655920930399471B79CF668FC3BAD4069 (String_t* ___htmlString0, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * ___color1, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, const RuntimeMethod* method);
// Actions Actions::get_Instance()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Actions_tF16D3C582B8D8DF5DD96B758A4D691F304BE44BB * Actions_get_Instance_mFCF21C897955B911E7D3B36BE438793642556B1C_inline (const RuntimeMethod* method);
// System.Void Actions::set_Instance(Actions)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Actions_set_Instance_m1F264080CD39D9EF3210325A20AEC5721E281845_inline (Actions_tF16D3C582B8D8DF5DD96B758A4D691F304BE44BB * ___value0, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Component::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Transform::get_root()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * Transform_get_root_mDEB1F3B4DB26B32CEED6DFFF734F85C79C4DDA91 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0,UnityEngine.Transform,System.Boolean)
inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_mC197A913A8DF8E125DEFD1C13605BC1DFD23C2F7 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___original0, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___parent1, bool ___worldPositionStays2, const RuntimeMethod* method)
{
	return ((  GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 *, bool, const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_mD346AB5980B67F04B2C7E12D88693520FFBAD37D_gshared)(___original0, ___parent1, ___worldPositionStays2, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject>::Add(!0)
inline void List_1_Add_m3DD76DE838FA83DF972E0486A296345EB3A7DDF3 (List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * __this, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 *, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))List_1_Add_mE5B3CBB3A625606D9BC4337FEAAF1D66BCB6F96E_gshared)(__this, ___item0, method);
}
// !0 System.Collections.Generic.List`1<UnityEngine.GameObject>::get_Item(System.Int32)
inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_inline (List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * (*) (List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 *, int32_t, const RuntimeMethod*))List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline)(__this, ___index0, method);
}
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6 (const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::SetAsFirstSibling()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_SetAsFirstSibling_mD5C02831BA6C7C3408CD491191EAF760ECB7E754 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// System.Void Actions::Map(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Actions_Map_m76DFCD66B1AA4F860F1956778B07ADE092E771D9 (Actions_tF16D3C582B8D8DF5DD96B758A4D691F304BE44BB * __this, bool ___toggle0, const RuntimeMethod* method);
// WorldMap WorldMap::get_Instance()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR WorldMap_t01A07DDC9EA386C88D5814CF18E0F37CAF05270C * WorldMap_get_Instance_m119402AD39383BC71435F9F36C3219A2281289CA_inline (const RuntimeMethod* method);
// InteractionMenu InteractionMenu::get_Instance()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR InteractionMenu_t7C2715221BAC6E0BB83B07EF82F68C6AA34129CB * InteractionMenu_get_Instance_m60C03978FEDDC7D708EB1C5C30EBD081344385FF_inline (const RuntimeMethod* method);
// System.Boolean UnityEngine.Input::GetButtonDown(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Input_GetButtonDown_m2001112EBCA3D5C7B0344EF62C896667F7E67DDF (String_t* ___buttonName0, const RuntimeMethod* method);
// System.Void ActionMenu::Activate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ActionMenu_Activate_mD3CE0F110A26F537889E78930F7F78B0969FCC4D (ActionMenu_tE3C589CB5B0B532FF5504CF0D558D489281BED01 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Input::GetButtonUp(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Input_GetButtonUp_m15AA6B42BD0DDCC7802346E49F30653D750260DD (String_t* ___buttonName0, const RuntimeMethod* method);
// System.Void Actions::Boost(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Actions_Boost_m5CB5C10BC32C2DE52E94274EAEF5F486716E49E4 (Actions_tF16D3C582B8D8DF5DD96B758A4D691F304BE44BB * __this, bool ___toggle0, const RuntimeMethod* method);
// System.Void Actions::Sonar(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Actions_Sonar_m9974B6F2FE0C34C6FADC1792DD56F5013D247130 (Actions_tF16D3C582B8D8DF5DD96B758A4D691F304BE44BB * __this, bool ___toggle0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.LayerMask::op_Implicit(UnityEngine.LayerMask)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t LayerMask_op_Implicit_mD89E9970822613D8D19B2EBCA36C79391C287BE0 (LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  ___mask0, const RuntimeMethod* method);
// UnityEngine.Collider[] UnityEngine.Physics::OverlapSphere(UnityEngine.Vector3,System.Single,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ColliderU5BU5D_t5124940293343DB3D31DA41C593709905906E486* Physics_OverlapSphere_mE1FC40C646B1468905057516601DB49DD41E0223 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position0, float ___radius1, int32_t ___layerMask2, const RuntimeMethod* method);
// UnityEngine.Camera UnityEngine.Camera::get_main()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * Camera_get_main_mC337C621B91591CEF89504C97EF64D717C12871C (const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Camera::WorldToScreenPoint(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Camera_WorldToScreenPoint_m44710195E7736CE9DE5A9B05E32059A9A950F95C (Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position0, const RuntimeMethod* method);
// PlayerController PlayerController::get_Instance()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9 * PlayerController_get_Instance_m8C373614456176CE37B49E24BA9B7E019C5A0440_inline (const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<UnityEngine.GameObject>::get_Count()
inline int32_t List_1_get_Count_m6206A8F48C7A98B9CFC23A13548CB215717573BD_inline (List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 *, const RuntimeMethod*))List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline)(__this, method);
}
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<UnityEngine.GameObject>::GetEnumerator()
inline Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14  List_1_GetEnumerator_m3616D04A85546C8251A6C376656CDB5358D893F6 (List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14  (*) (List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 *, const RuntimeMethod*))List_1_GetEnumerator_m1739A5E25DF502A6984F9B98CFCAC2D3FABCF233_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.GameObject>::get_Current()
inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * Enumerator_get_Current_mB38DBEFCD264B4682A190F8592464C0658F702B7_inline (Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14 * __this, const RuntimeMethod* method)
{
	return ((  GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * (*) (Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14 *, const RuntimeMethod*))Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_gshared_inline)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.GameObject>::MoveNext()
inline bool Enumerator_MoveNext_mF39107B3A55F66C83EBCA798CBC93AC4C990DBD7 (Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14 *, const RuntimeMethod*))Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.GameObject>::Dispose()
inline void Enumerator_Dispose_m4B68F0A4E0441A036D7E39BC7E639536164196D9 (Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14 * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14 *, const RuntimeMethod*))Enumerator_Dispose_mCFB225D9E5E597A1CC8F958E53BEA1367D8AC7B8_gshared)(__this, method);
}
// MouseLook MouseLook::get_Instance()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR MouseLook_t1E1EE79B13022184F3EF9AC8FE8C6771053890DA * MouseLook_get_Instance_m4F889A9C207EAE72011AF1BA9DA70046C5404386_inline (const RuntimeMethod* method);
// Interaction Interaction::get_Instance()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Interaction_t41E997DF6BD70C3EAB8669FCC65D603292EF42A1 * Interaction_get_Instance_m49A31B95D41F5321AA7036861F8330D6BB09A940_inline (const RuntimeMethod* method);
// System.Void WorldMap::Activate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WorldMap_Activate_m270FF00B4D2A645DB02BFEE99F12E78D75054808 (WorldMap_t01A07DDC9EA386C88D5814CF18E0F37CAF05270C * __this, const RuntimeMethod* method);
// System.Void WorldMap::Deactivate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WorldMap_Deactivate_m45F3C427C11E7AA6FC6C11B1346F94BA221F8641 (WorldMap_t01A07DDC9EA386C88D5814CF18E0F37CAF05270C * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject>::.ctor()
inline void List_1__ctor_m859B0EE8491FDDEB1A3F7115D334B863E025BBC8 (List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 *, const RuntimeMethod*))List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared)(__this, method);
}
// System.DateTime System.DateTime::get_Now()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  DateTime_get_Now_mCAC695993D6E2C57B900C83BEF3F8B18BC4EBC2C (const RuntimeMethod* method);
// System.Int32 System.DateTime::get_Day()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t DateTime_get_Day_m9D698CA2A7D1FBEE7BEC0A982A119239F513CBA8 (DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405 * __this, const RuntimeMethod* method);
// System.String System.Int32::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411 (int32_t* __this, const RuntimeMethod* method);
// System.Int32 System.DateTime::get_Month()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t DateTime_get_Month_m46CC2AED3F24A91104919B1F6B5103DD1F8BBAE8 (DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405 * __this, const RuntimeMethod* method);
// System.TimeSpan System.DateTime::get_TimeOfDay()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  DateTime_get_TimeOfDay_mE6A177963C8D8AA8AA2830239F1C7B3D11AFC645 (DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405 * __this, const RuntimeMethod* method);
// System.String System.TimeSpan::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* TimeSpan_ToString_mB89DE4C354B8A29F627C22FA7EA31E94B1DA884B (TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203 * __this, const RuntimeMethod* method);
// System.String System.String::Replace(System.Char,System.Char)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Replace_mD912844A1141FE8771F55255C4A8D29C80C47618 (String_t* __this, Il2CppChar ___oldChar0, Il2CppChar ___newChar1, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m89EAB4C6A96B0E5C3F87300D6BE78D386B9EFC44 (String_t* ___str00, String_t* ___str11, String_t* ___str22, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m37A5BF26F8F8F1892D60D727303B23FB604FEE78 (String_t* ___str00, String_t* ___str11, String_t* ___str22, String_t* ___str33, const RuntimeMethod* method);
// System.Void UnityEngine.ScreenCapture::CaptureScreenshot(System.String,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ScreenCapture_CaptureScreenshot_mB0DE010388F478603FB22C2DE37E2E01C5F49A28 (String_t* ___filename0, int32_t ___superSize1, const RuntimeMethod* method);
// GameManager GameManager::get_Instance()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * GameManager_get_Instance_m758C0D98BBDEF0C2DCA163A5D60B22DBB5B07232_inline (const RuntimeMethod* method);
// System.Boolean GameManager::get_TargetFound()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool GameManager_get_TargetFound_mD646125D22C8183B642D2ECD48C64FE59D1D12AE_inline (GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * __this, const RuntimeMethod* method);
// IFauna GameManager::get_Target()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject* GameManager_get_Target_m3ACB72AC913A603DF141C5D74A3F5C3B42D25B8D_inline (const RuntimeMethod* method);
// System.Void GameManager::set_TargetFound(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void GameManager_set_TargetFound_mA5DAB51223A80F7D092FF2967C48AC6EB5464CB2_inline (GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void FlockAgent::FindNeighbors()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FlockAgent_FindNeighbors_m084145F3ABC4617EA0A00724C318BEBD459B1BBE (FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04 * __this, const RuntimeMethod* method);
// System.Void FlockAgent::CalculateSpeed()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FlockAgent_CalculateSpeed_m0CA1C3F5ACDD0DA9A4A121596281AA5AEC6646B1 (FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 FlockAgent::Cohese()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  FlockAgent_Cohese_mCBA014DB1CABC0C892246D2B8C48DC8B8CCC7D48 (FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, float ___d1, const RuntimeMethod* method);
// UnityEngine.Vector3 FlockAgent::Separate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  FlockAgent_Separate_mD2D03FDAC8EB6894C85FED42F19C5BD69AA1DBC2 (FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 FlockAgent::Align()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  FlockAgent_Align_m7890991F9B43B074A1420EB79A42EC79A7659EBB (FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 FlockAgent::Bounds()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  FlockAgent_Bounds_m4300D54C6A0AF7BF4FB2B650EB878EA72A996CD1 (FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 FlockAgent::ObstacleAvoidance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  FlockAgent_ObstacleAvoidance_m630F88E9A64AE156AA32C14C11538797BCB8BDDD (FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_forward()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_forward_mD850B9ECF892009E3485408DC0D375165B7BF053 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::SmoothDamp(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3&,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_SmoothDamp_m4655944DBD5B44125F8F4B5A15E31DE94CB0F627 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___current0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___target1, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * ___currentVelocity2, float ___smoothTime3, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_normalized()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_get_normalized_m2FA6DF38F97BDA4CCBDAE12B9FE913A241DAC8D5 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Vector3::op_Equality(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Vector3_op_Equality_m8A98C7F38641110A2F90445EF8E98ECE14B08296 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___lhs0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rhs1, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_forward(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_forward_mAE46B156F55F2F90AB495B17F7C20BF59A5D7D4D (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<FlockAgent>::Clear()
inline void List_1_Clear_m2BD50CE116D1680EF02FA4859D6802F72B45324F (List_1_t50CCFC552038CBD55842D81AC8F7EE04848C7255 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t50CCFC552038CBD55842D81AC8F7EE04848C7255 *, const RuntimeMethod*))List_1_Clear_m5FB5A9C59D8625FDFB06876C4D8848F0F07ABFD0_gshared)(__this, method);
}
// FlockAgent[] FlockManager::get_SpawnedAgents()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR FlockAgentU5BU5D_t012E3C58876A70268E6BFEDBF651E5B8D23AAF84* FlockManager_get_SpawnedAgents_m050E5346A4490CAAD7EB7B9F8A7E6399405B4C7A_inline (FlockManager_t241CFB1B2E6593247EFA52A9E2CB2A111400F1F8 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method);
// System.Single UnityEngine.Vector3::SqrMagnitude(UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Vector3_SqrMagnitude_m286141DAE9BF2AD40B0FA58C1D97B5773327A5B9_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___vector0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<FlockAgent>::Add(!0)
inline void List_1_Add_mF1329DCD6488AD8E4E570B756253384836CC2E82 (List_1_t50CCFC552038CBD55842D81AC8F7EE04848C7255 * __this, FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04 * ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t50CCFC552038CBD55842D81AC8F7EE04848C7255 *, FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04 *, const RuntimeMethod*))List_1_Add_mE5B3CBB3A625606D9BC4337FEAAF1D66BCB6F96E_gshared)(__this, ___item0, method);
}
// System.Int32 System.Collections.Generic.List`1<FlockAgent>::get_Count()
inline int32_t List_1_get_Count_mB64A4B531A3EB080CDD6EBBD4D31F9BCB6730F76_inline (List_1_t50CCFC552038CBD55842D81AC8F7EE04848C7255 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t50CCFC552038CBD55842D81AC8F7EE04848C7255 *, const RuntimeMethod*))List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline)(__this, method);
}
// !0 System.Collections.Generic.List`1<FlockAgent>::get_Item(System.Int32)
inline FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04 * List_1_get_Item_m8036333D84A83C0B1348EFFA8C2CA6EBCB99E363_inline (List_1_t50CCFC552038CBD55842D81AC8F7EE04848C7255 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04 * (*) (List_1_t50CCFC552038CBD55842D81AC8F7EE04848C7255 *, int32_t, const RuntimeMethod*))List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline)(__this, ___index0, method);
}
// System.Single UnityEngine.Mathf::Clamp(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87 (float ___value0, float ___min1, float ___max2, const RuntimeMethod* method);
// System.Boolean FlockAgent::VectorInView(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool FlockAgent_VectorInView_m40DF4A6C365ADD655A97BB18413186B022867CE5 (FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Division(UnityEngine.Vector3,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Division_mE5ACBFB168FED529587457A83BA98B7DB32E2A05_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, float ___d1, const RuntimeMethod* method);
// System.Single UnityEngine.Vector3::get_magnitude()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Vector3_get_magnitude_mDDD40612220D8104E77E993E18A101A69A944991 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Physics_Raycast_mCBD5F7D498C246713EDDBB446E97205DA206C49C (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___origin0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___direction1, RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * ___hitInfo2, float ___maxDistance3, int32_t ___layerMask4, const RuntimeMethod* method);
// UnityEngine.Vector3 FlockAgent::AvoidObstacle()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  FlockAgent_AvoidObstacle_m9274B9DABEC71E0C1CFCABC33E63019C2BE14B6B (FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Vector3::op_Inequality(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Vector3_op_Inequality_m15190A795B416EB699E69E6190DE6F1C1F208710 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___lhs0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rhs1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::TransformDirection(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_TransformDirection_m6B5E3F0A7C6323159DEC6D9BC035FB53ADD96E91 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___direction0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.RaycastHit::get_point()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  RaycastHit_get_point_m32F7282CBB2E13393A33BAD046BDA218E48DD21E (RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Vector3::get_sqrMagnitude()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Vector3_get_sqrMagnitude_mC567EE6DF411501A8FE1F23A0038862630B88249 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Vector3::Angle(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Vector3_Angle_m3715AB03A36C59D8CF08F8D71E2F46454EB884C1 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___from0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___to1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<FlockAgent>::.ctor()
inline void List_1__ctor_m98321E90C6288A40B856F52CFF529E59A738E641 (List_1_t50CCFC552038CBD55842D81AC8F7EE04848C7255 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t50CCFC552038CBD55842D81AC8F7EE04848C7255 *, const RuntimeMethod*))List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared)(__this, method);
}
// System.Void Fish::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Fish__ctor_mEDE54A2E8EAB7C21444968366A1E9E12AC8D4A48 (Fish_t45F7EC0A7EE404E12CC412CF93723EA8799CA46C * __this, const RuntimeMethod* method);
// System.Void FlockManager::SpawnAgents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FlockManager_SpawnAgents_m075D462688CF50D13A0F934C0989C1CFCE54B3B3 (FlockManager_t241CFB1B2E6593247EFA52A9E2CB2A111400F1F8 * __this, const RuntimeMethod* method);
// System.Void FlockAgent::Move()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FlockAgent_Move_mE006DB28962C8459F5677FCE8F3B9DA4302F4C99 (FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04 * __this, const RuntimeMethod* method);
// System.Void FlockManager::set_SpawnedAgents(FlockAgent[])
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void FlockManager_set_SpawnedAgents_m86395D548FB2FC61178CBDF82A5C5629A8716BB6_inline (FlockManager_t241CFB1B2E6593247EFA52A9E2CB2A111400F1F8 * __this, FlockAgentU5BU5D_t012E3C58876A70268E6BFEDBF651E5B8D23AAF84* ___value0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Random::get_insideUnitSphere()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Random_get_insideUnitSphere_m43E5AE1F6A6CFA892BAE6E3ED71BEBFCE308CE90 (const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// System.Int32 UnityEngine.Random::Range(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Random_Range_m4B3A0037ACA057F33C94508F908546B9317D996A (int32_t ___minInclusive0, int32_t ___maxExclusive1, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  Quaternion_Euler_m37BF99FFFA09F4B3F83DC066641B82C59B19A9C3 (float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<FlockAgent>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
inline FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04 * Object_Instantiate_TisFlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04_mB3A05C03EA7F057D8762864B17A537999578B839 (FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04 * ___original0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position1, Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___rotation2, const RuntimeMethod* method)
{
	return ((  FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04 * (*) (FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04 *, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E , Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 , const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_mB05DEC51C29EF5BB8BD17D055E80217F11E571AA_gshared)(___original0, ___position1, ___rotation2, method);
}
// System.Single UnityEngine.Random::Range(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Random_Range_mC15372D42A9ABDCAC3DE82E114D60A40C9C311D2 (float ___minInclusive0, float ___maxInclusive1, const RuntimeMethod* method);
// System.Void FlockAgent::Initialize(FlockManager,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FlockAgent_Initialize_m8D1C04B1942FF5534A44DAC4526F9A8EF16CA1A6 (FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04 * __this, FlockManager_t241CFB1B2E6593247EFA52A9E2CB2A111400F1F8 * ___manager0, float ___initSpeed1, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::get_red()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  Color_get_red_m9BD55EBF7A74A515330FA5F7AC7A67C8A8913DD8 (const RuntimeMethod* method);
// System.Void UnityEngine.Gizmos::set_color(UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Gizmos_set_color_m937ACC6288C81BAFFC3449FAA03BB4F680F4E74F (Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Gizmos::DrawWireCube(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Gizmos_DrawWireCube_mC526244E50C6E5793D4066C9C99023D5FF8424BF (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___center0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___size1, const RuntimeMethod* method);
// System.Void GameManager::set_Instance(GameManager)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void GameManager_set_Instance_mAEF75ABE6135BDD61B9A3AED1606C0449787C5C7_inline (GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * ___value0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<IFauna>()
inline RuntimeObject* GameObject_GetComponent_TisIFauna_tECBAA4EFAA9D07BFB7CB55CEE86755A301872FC5_m3DDC9275C8F6A20642A992058DD3465A7B43896A (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1<UnityEngine.GameObject>::Remove(!0)
inline bool List_1_Remove_mD36BF07C31C1DF947856EFECE89BAF4D6A24DEB7 (List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * __this, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___item0, const RuntimeMethod* method)
{
	return ((  bool (*) (List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 *, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))List_1_Remove_m753F7B4281CC4D02C07AE90726F51EF34B588DF7_gshared)(__this, ___item0, method);
}
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Transform>::get_Count()
inline int32_t List_1_get_Count_m82AF14687C6FA2B1572D859A551E3ADBCBADC3C0_inline (List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 *, const RuntimeMethod*))List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline)(__this, method);
}
// !0 System.Collections.Generic.List`1<UnityEngine.Transform>::get_Item(System.Int32)
inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * List_1_get_Item_m8B222F262DF0C4B49E12B4E87AB2162202744499_inline (List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * (*) (List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 *, int32_t, const RuntimeMethod*))List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline)(__this, ___index0, method);
}
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  Transform_get_rotation_m4AA3858C00DF4C9614B80352558C4C37D08D2200 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m81B599A0051F8F4543E5C73A11585E96E940943B (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___original0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position1, Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___rotation2, const RuntimeMethod* method)
{
	return ((  GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E , Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 , const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_mB05DEC51C29EF5BB8BD17D055E80217F11E571AA_gshared)(___original0, ___position1, ___rotation2, method);
}
// System.Boolean UnityEngine.GameObject::TryGetComponent<SoloFish>(!!0&)
inline bool GameObject_TryGetComponent_TisSoloFish_t248C1128425014659AF96F9294BAC7D1799179C2_m77EB52771C7FC7A5EF16539BFE0797509D5DA199 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, SoloFish_t248C1128425014659AF96F9294BAC7D1799179C2 ** ___component0, const RuntimeMethod* method)
{
	return ((  bool (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, SoloFish_t248C1128425014659AF96F9294BAC7D1799179C2 **, const RuntimeMethod*))GameObject_TryGetComponent_TisRuntimeObject_mC7639C3E3E2A65695113BCF25718826CC392D6CB_gshared)(__this, ___component0, method);
}
// System.Void UnityEngine.Bounds::set_center(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Bounds_set_center_mAC54A53224BBEFE37A4387DCBD0EF3774751221E (Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// System.String UnityEngine.Object::get_name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Object::set_name(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_set_name_m87C4006618ADB325ABE5439DF159E10DD8DD0781 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void GameManager::set_Target(IFauna)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void GameManager_set_Target_mB22AB1128EBC08C3818777166428FF6DEEE341E6_inline (RuntimeObject* ___value0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Transform>::Remove(!0)
inline bool List_1_Remove_m4FBB736F89C431F0D19B655E23269EAB321B863A (List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * __this, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___item0, const RuntimeMethod* method)
{
	return ((  bool (*) (List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 *, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 *, const RuntimeMethod*))List_1_Remove_m753F7B4281CC4D02C07AE90726F51EF34B588DF7_gshared)(__this, ___item0, method);
}
// System.Void GameManager::SpawnZone(System.Collections.Generic.List`1<UnityEngine.Transform>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameManager_SpawnZone_m095B9AA853D168B937A20973F44CE3D7D5B3B8CE (GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * __this, List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * ___spawnList0, const RuntimeMethod* method);
// System.Collections.IEnumerator GameManager::PlayCutScene(UnityEngine.Playables.PlayableAsset)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* GameManager_PlayCutScene_mEE157DD535C601E8C248C3286884E2622B9E5851 (GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * __this, PlayableAsset_t5AD1606B76C9753A7F4C6B1061193F581023F137 * ___cutScene0, const RuntimeMethod* method);
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719 (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, RuntimeObject* ___routine0, const RuntimeMethod* method);
// UnityEngine.Transform GameManager::get_CameraTarget()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * GameManager_get_CameraTarget_mBBC82927B5A0D320D5B38CA1B94C04FDB1EBC4F8_inline (GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * __this, const RuntimeMethod* method);
// System.Boolean GameManager::get_OrbitCameraTarget()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool GameManager_get_OrbitCameraTarget_m9B1FB59BEA2FEF5094B7EB93548C815087CA0E11_inline (GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_right()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_right_mA8EB1882CCE8759766544448308C88D9200F06CE (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::RotateAround(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_RotateAround_m1F93A7A1807BE407BD23EC1BA49F03AD22FCE4BE (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___point0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___axis1, float ___angle2, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::LookAt(UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_LookAt_m49185D782014D16DA747C1296BEBAC3FB3CEDC1F (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___target0, const RuntimeMethod* method);
// System.Boolean GameManager::get_OrbitCameraTarget2()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool GameManager_get_OrbitCameraTarget2_m8967752F71CD50D7C56AB9F21BC4D5576A1DF23B_inline (GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * __this, const RuntimeMethod* method);
// System.Void GameManager::set_CameraTarget(UnityEngine.Transform)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void GameManager_set_CameraTarget_m40A31328DAA4F5876B52EDE84970FE6B7E686919_inline (GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * __this, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___value0, const RuntimeMethod* method);
// System.Void GameManager/<PlayCutScene>d__46::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CPlayCutSceneU3Ed__46__ctor_m4E4D899A2AEC184002EB6F6615F66D0C16DB3B84 (U3CPlayCutSceneU3Ed__46_tC2D2594FAEFE8718E713CAC6338A1701F09CADAA * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Boolean WorldMap::AddInteraction(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WorldMap_AddInteraction_mF2CC83A625FCFB963745D016D72BC262AB4A94F4 (WorldMap_t01A07DDC9EA386C88D5814CF18E0F37CAF05270C * __this, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___interaction0, const RuntimeMethod* method);
// System.Boolean Gate::get_Locked()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Gate_get_Locked_mD0F7FDCDBBF9A7B92DB3A64C563342F48D302F4B_inline (Gate_tAA47F9BD94C2B1C12EADC5ECCAF1CAD07A035F45 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_localScale()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_localScale_mD9DF6CA81108C2A6002B5EA2BE25A6CD2723D046 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::Lerp(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_Lerp_m8E095584FFA10CF1D3EABCD04F4C83FB82EC5524_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, float ___t2, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_localScale_mF4D1611E48D1BA7566A1E166DC2DACF3ADD8BA3A (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// System.Boolean Interaction::HasKey(Key/KeyType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Interaction_HasKey_m0E72EF86A1EF62EB1478BD83D9D20B904CBE76A1 (Interaction_t41E997DF6BD70C3EAB8669FCC65D603292EF42A1 * __this, int32_t ___key0, const RuntimeMethod* method);
// System.Void Gate::set_Locked(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Gate_set_Locked_mC4655A379C367E8C658ED505B433698BC32E7765_inline (Gate_tAA47F9BD94C2B1C12EADC5ECCAF1CAD07A035F45 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::Log(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8 (RuntimeObject * ___message0, const RuntimeMethod* method);
// System.Void Interaction::set_Instance(Interaction)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Interaction_set_Instance_m1C2D19F892D930D83F98A19E2D65EF47701A7C51_inline (Interaction_t41E997DF6BD70C3EAB8669FCC65D603292EF42A1 * ___value0, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.GameObject::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * GameObject_get_gameObject_mD5FFECF7C3AC5039E847DF7A8842478539B701D6 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Physics::SphereCast(UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Physics_SphereCast_m2E200DFD71A597968D4F50B8A6284F2E622B4C57 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___origin0, float ___radius1, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___direction2, RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * ___hitInfo3, float ___maxDistance4, const RuntimeMethod* method);
// UnityEngine.Collider UnityEngine.RaycastHit::get_collider()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * RaycastHit_get_collider_m13A3DE16FBC631E0A1F987E0B22CE70AF8AB539E (RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Component::TryGetComponent<IInteraction>(!!0&)
inline bool Component_TryGetComponent_TisIInteraction_t1C1C3E6C29AFB203F5021CA7300B6FEA0EFE91FE_mEC43AA2983C9283A4104774AD183EECD789EFD70 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, RuntimeObject** ___component0, const RuntimeMethod* method)
{
	return ((  bool (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, RuntimeObject**, const RuntimeMethod*))Component_TryGetComponent_TisRuntimeObject_m409D64ADC25ED175ADC7ED4C3F7ABF5439AD63AF_gshared)(__this, ___component0, method);
}
// UnityEngine.Color UnityEngine.Color::get_green()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  Color_get_green_mFF9BD42534D385A0717B1EAD083ADF08712984B9 (const RuntimeMethod* method);
// System.Void UnityEngine.Debug::DrawRay(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Color,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_DrawRay_m3954B3FFA675C0660ED438E8B705B45EDE393C60 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___start0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___dir1, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___color2, float ___duration3, const RuntimeMethod* method);
// System.Void InteractionMenu::Activate(IInteraction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InteractionMenu_Activate_m45B84D655B2FCBC3A0429DDEB4F1B8393A397160 (InteractionMenu_t7C2715221BAC6E0BB83B07EF82F68C6AA34129CB * __this, RuntimeObject* ___interaction0, const RuntimeMethod* method);
// System.Void InteractionMenu::Deactivate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InteractionMenu_Deactivate_m912B95CC8E501BE8859EB5BB477A9D0A5DCFE10A (InteractionMenu_t7C2715221BAC6E0BB83B07EF82F68C6AA34129CB * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<Key/KeyType>::Contains(!0)
inline bool List_1_Contains_m69468F32EA1D0A3E5BC817F0CE7F0560332383E6 (List_1_tD99207A373A130469C76CB8634AADFB31FF317AE * __this, int32_t ___item0, const RuntimeMethod* method)
{
	return ((  bool (*) (List_1_tD99207A373A130469C76CB8634AADFB31FF317AE *, int32_t, const RuntimeMethod*))List_1_Contains_m793EB605F2702C16DE665C690BFA9B9675529D94_gshared)(__this, ___item0, method);
}
// System.Void System.Collections.Generic.List`1<Key/KeyType>::Add(!0)
inline void List_1_Add_m6470E90BACC0611067C594D4D8259D1BC40DBB5A (List_1_tD99207A373A130469C76CB8634AADFB31FF317AE * __this, int32_t ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_tD99207A373A130469C76CB8634AADFB31FF317AE *, int32_t, const RuntimeMethod*))List_1_Add_mBA0FDF41792A78B3EB9E395D711706E268313F0F_gshared)(__this, ___item0, method);
}
// System.Void System.Collections.Generic.List`1<Key/KeyType>::.ctor()
inline void List_1__ctor_m612E7D379DA89488D1FC2AB1FB9E2BD2AAA9AABA (List_1_tD99207A373A130469C76CB8634AADFB31FF317AE * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tD99207A373A130469C76CB8634AADFB31FF317AE *, const RuntimeMethod*))List_1__ctor_mF1D0377D81949B2ADB6104D9994F7CEE1A1E5040_gshared)(__this, method);
}
// System.Void InteractionMenu::set_Instance(InteractionMenu)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void InteractionMenu_set_Instance_m64EBB469675FBC80A34E3E4457B021DE07FED4EA_inline (InteractionMenu_t7C2715221BAC6E0BB83B07EF82F68C6AA34129CB * ___value0, const RuntimeMethod* method);
// System.String UnityEngine.Application::get_dataPath()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Application_get_dataPath_m026509C15A1E58FC6461EE7EC336EC18C9C2271E (const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B (String_t* ___str00, String_t* ___str11, const RuntimeMethod* method);
// System.Boolean System.IO.Directory::Exists(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Directory_Exists_m17E38B91F6D9A0064D614FF2237BBFC0127468FE (String_t* ___path0, const RuntimeMethod* method);
// System.IO.DirectoryInfo System.IO.Directory::CreateDirectory(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR DirectoryInfo_t4EF3610F45F0D234800D01ADA8F3F476AE0CF5CD * Directory_CreateDirectory_m38040338519C48CE52137CC146372A153D5C6A7A (String_t* ___path0, const RuntimeMethod* method);
// System.Void InteractionMenu::UpdateUISelection()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InteractionMenu_UpdateUISelection_mCDE9D19A0556DB8B15CBC355E369A362C18D2A2D (InteractionMenu_t7C2715221BAC6E0BB83B07EF82F68C6AA34129CB * __this, const RuntimeMethod* method);
// System.Void System.IO.DirectoryInfo::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DirectoryInfo__ctor_m5F307F7E646135FC323F81EA93C36CC0CF6023A6 (DirectoryInfo_t4EF3610F45F0D234800D01ADA8F3F476AE0CF5CD * __this, String_t* ___path0, const RuntimeMethod* method);
// System.IO.FileInfo[] System.IO.DirectoryInfo::GetFiles(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR FileInfoU5BU5D_tB6A5BC6AD9BEC388BF3236B4E8FBB7760F96080B* DirectoryInfo_GetFiles_mB4AE0AEC3ABF4A3541A0B68C46C802A78730C5FC (DirectoryInfo_t4EF3610F45F0D234800D01ADA8F3F476AE0CF5CD * __this, String_t* ___searchPattern0, const RuntimeMethod* method);
// System.Boolean System.String::Contains(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_Contains_mA26BDCCE8F191E8965EB8EEFC18BB4D0F85A075A (String_t* __this, String_t* ___value0, const RuntimeMethod* method);
// System.Boolean InteractionMenu::GetInteractionAsType<IFauna>(InteractionType&)
inline bool InteractionMenu_GetInteractionAsType_TisIFauna_tECBAA4EFAA9D07BFB7CB55CEE86755A301872FC5_m4E764DDABB046069A791E256957F04B862FD2182 (InteractionMenu_t7C2715221BAC6E0BB83B07EF82F68C6AA34129CB * __this, RuntimeObject** ___target0, const RuntimeMethod* method)
{
	return ((  bool (*) (InteractionMenu_t7C2715221BAC6E0BB83B07EF82F68C6AA34129CB *, RuntimeObject**, const RuntimeMethod*))InteractionMenu_GetInteractionAsType_TisRuntimeObject_mA693C5C52F4B137C964546489620C7088B3F44EE_gshared)(__this, ___target0, method);
}
// UnityEngine.Color UnityEngine.Color::get_black()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  Color_get_black_m67E91EB7017FC74D9AB5ADEF6B6929B7EFC9A982 (const RuntimeMethod* method);
// System.Boolean InteractionMenu::GetInteractionAsType<IKey>(InteractionType&)
inline bool InteractionMenu_GetInteractionAsType_TisIKey_t524F44506E887E37C88431701D58645C41BD5F3C_m244F7034AC80DC277E1467E43379BC7EFEFD5F00 (InteractionMenu_t7C2715221BAC6E0BB83B07EF82F68C6AA34129CB * __this, RuntimeObject** ___target0, const RuntimeMethod* method)
{
	return ((  bool (*) (InteractionMenu_t7C2715221BAC6E0BB83B07EF82F68C6AA34129CB *, RuntimeObject**, const RuntimeMethod*))InteractionMenu_GetInteractionAsType_TisRuntimeObject_mA693C5C52F4B137C964546489620C7088B3F44EE_gshared)(__this, ___target0, method);
}
// System.Boolean InteractionMenu::GetInteractionAsType<IGate>(InteractionType&)
inline bool InteractionMenu_GetInteractionAsType_TisIGate_tC633C584B29457590CF59B3448AEE38AC9FB33C6_mDD834F8EB0D1D7DCE6A48654C2AD746D46BB8279 (InteractionMenu_t7C2715221BAC6E0BB83B07EF82F68C6AA34129CB * __this, RuntimeObject** ___target0, const RuntimeMethod* method)
{
	return ((  bool (*) (InteractionMenu_t7C2715221BAC6E0BB83B07EF82F68C6AA34129CB *, RuntimeObject**, const RuntimeMethod*))InteractionMenu_GetInteractionAsType_TisRuntimeObject_mA693C5C52F4B137C964546489620C7088B3F44EE_gshared)(__this, ___target0, method);
}
// System.Boolean InteractionMenu::GetInteractionAsType<IShip>(InteractionType&)
inline bool InteractionMenu_GetInteractionAsType_TisIShip_tC6FB125A646115EE3BCEDE1326BF546935215C99_m22472493C6CA081B6709F1EAE1AD3E87BDAFA312 (InteractionMenu_t7C2715221BAC6E0BB83B07EF82F68C6AA34129CB * __this, RuntimeObject** ___target0, const RuntimeMethod* method)
{
	return ((  bool (*) (InteractionMenu_t7C2715221BAC6E0BB83B07EF82F68C6AA34129CB *, RuntimeObject**, const RuntimeMethod*))InteractionMenu_GetInteractionAsType_TisRuntimeObject_mA693C5C52F4B137C964546489620C7088B3F44EE_gshared)(__this, ___target0, method);
}
// ToolTip ToolTip::get_Instance()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ToolTip_t044257731DC5931927D3AB0671C17515BD8E251E * ToolTip_get_Instance_m5CB773BB76F41361836DE8051081FD5CDF562969_inline (const RuntimeMethod* method);
// System.Void ToolTip::Toggle(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ToolTip_Toggle_m7BB7DD0FB5AA7DD002A5173E3391AF8457627F6C (ToolTip_t044257731DC5931927D3AB0671C17515BD8E251E * __this, String_t* ___message0, const RuntimeMethod* method);
// System.Void InteractionMenu::Photograph(IFauna)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InteractionMenu_Photograph_mDF8C7A662AA42E89A39027BBAB69C54692736571 (InteractionMenu_t7C2715221BAC6E0BB83B07EF82F68C6AA34129CB * __this, RuntimeObject* ___fauna0, const RuntimeMethod* method);
// System.Boolean Interaction::Addkey(Key/KeyType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Interaction_Addkey_mCC3293ED3A6334A742C2196DF288E220BF1073A5 (Interaction_t41E997DF6BD70C3EAB8669FCC65D603292EF42A1 * __this, int32_t ___key0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponentInChildren<UnityEngine.UI.Dropdown>()
inline Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96 * GameObject_GetComponentInChildren_TisDropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96_m8B8487C221C1219A04D3F96C76A0E7CFFA9B4CD2 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96 * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponentInChildren_TisRuntimeObject_mC8FC6687C66150FA89090C2A7733B2EE2E1315FD_gshared)(__this, method);
}
// !!0 UnityEngine.GameObject::GetComponentInChildren<UnityEngine.UI.RawImage>()
inline RawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A * GameObject_GetComponentInChildren_TisRawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A_mBE7B553FDA545118EB6051F0473AAEC7F78D8CAB (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  RawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponentInChildren_TisRuntimeObject_mC8FC6687C66150FA89090C2A7733B2EE2E1315FD_gshared)(__this, method);
}
// System.Void UnityEngine.UI.Dropdown::ClearOptions()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dropdown_ClearOptions_m7F59A8B054698715921D2B0E37EB1808BE53C23C (Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96 * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData> UnityEngine.UI.Dropdown::get_options()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_tAF6577A540702C9F6C407DE69A8FAFB502339DC4 * Dropdown_get_options_mF427A2157CDD901C12F1B160C4D1F8207D7111D0 (Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.UI.Dropdown/OptionData::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OptionData__ctor_m5AF14BD8BBF6118AC51A7A9A38AE3AB2DE3C2675 (OptionData_t5F665DC13C1E4307727D66ECC1100B3A77E3E857 * __this, String_t* ___text0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>::Add(!0)
inline void List_1_Add_mC2611000DA97145D1DFC867ECA604FBB3E8628EC (List_1_tAF6577A540702C9F6C407DE69A8FAFB502339DC4 * __this, OptionData_t5F665DC13C1E4307727D66ECC1100B3A77E3E857 * ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_tAF6577A540702C9F6C407DE69A8FAFB502339DC4 *, OptionData_t5F665DC13C1E4307727D66ECC1100B3A77E3E857 *, const RuntimeMethod*))List_1_Add_mE5B3CBB3A625606D9BC4337FEAAF1D66BCB6F96E_gshared)(__this, ___item0, method);
}
// System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Texture2D__ctor_m7D64AB4C55A01F2EE57483FD9EF826607DF9E4B4 (Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * __this, int32_t ___width0, int32_t ___height1, const RuntimeMethod* method);
// System.Byte[] System.IO.File::ReadAllBytes(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* File_ReadAllBytes_mFB47FB50E938AE90CC822442D30E896441D95829 (String_t* ___path0, const RuntimeMethod* method);
// System.Boolean UnityEngine.ImageConversion::LoadImage(UnityEngine.Texture2D,System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ImageConversion_LoadImage_m5BB4FBA0565E698ED5C6F25F7A0A5F83CADD7477 (Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___tex0, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___data1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Texture2D>::Add(!0)
inline void List_1_Add_m9AF452292436C834FC154E6457CDD42B18FB310F (List_1_t67CA4414F3746D817D6D1A1D16FD9E7C85CED2D7 * __this, Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t67CA4414F3746D817D6D1A1D16FD9E7C85CED2D7 *, Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF *, const RuntimeMethod*))List_1_Add_mE5B3CBB3A625606D9BC4337FEAAF1D66BCB6F96E_gshared)(__this, ___item0, method);
}
// !0 System.Collections.Generic.List`1<UnityEngine.Texture2D>::get_Item(System.Int32)
inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * List_1_get_Item_m69B62B1F3FDC9F7B1D0906604855E457795130AB_inline (List_1_t67CA4414F3746D817D6D1A1D16FD9E7C85CED2D7 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * (*) (List_1_t67CA4414F3746D817D6D1A1D16FD9E7C85CED2D7 *, int32_t, const RuntimeMethod*))List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline)(__this, ___index0, method);
}
// System.Void UnityEngine.UI.RawImage::set_texture(UnityEngine.Texture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RawImage_set_texture_m1D7BAE6CB629C36894B664D9F5D68CACA88B8D99 (RawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A * __this, Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * ___value0, const RuntimeMethod* method);
// System.Void MainMenu/<LoadGame>d__7::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CLoadGameU3Ed__7__ctor_m57695A7D16F59DB4E5587AE9E06B571D1940260F (U3CLoadGameU3Ed__7_tC6D4D2F6FFFF4C3DA7A96BE826554FAE56FB9D74 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Collections.IEnumerator MainMenu::LoadGame()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* MainMenu_LoadGame_m75C2A4F48DA3B9EB7A2ADF6872C66FC286639600 (MainMenu_tEB11F5A993C42E93B585FBB65C9E92EC91C5707C * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Texture2D>::get_Count()
inline int32_t List_1_get_Count_mC0601A2601297DDBF0B461A2A8B8EF9528741B88_inline (List_1_t67CA4414F3746D817D6D1A1D16FD9E7C85CED2D7 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t67CA4414F3746D817D6D1A1D16FD9E7C85CED2D7 *, const RuntimeMethod*))List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline)(__this, method);
}
// System.Int32 UnityEngine.UI.Dropdown::get_value()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Dropdown_get_value_mFBF47E0C72050C5CB96B8B6D33F41BA2D1368F26_inline (Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Application::Quit()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Application_Quit_m8D720E5092786C2EE32310D85FE61C253D3B1F2A (const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Texture2D>::.ctor()
inline void List_1__ctor_mD04CAD03BDE2013E799D002230D9D16EFD84FEFC (List_1_t67CA4414F3746D817D6D1A1D16FD9E7C85CED2D7 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t67CA4414F3746D817D6D1A1D16FD9E7C85CED2D7 *, const RuntimeMethod*))List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared)(__this, method);
}
// System.Void UnityEngine.Cursor::set_visible(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Cursor_set_visible_m4747F0DC20D06D1932EC740C5CCC738C1664903D (bool ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Cursor::set_lockState(UnityEngine.CursorLockMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Cursor_set_lockState_mC0739186A04F4C278F02E8C1714D99B491E3A217 (int32_t ___value0, const RuntimeMethod* method);
// System.Void MouseLook::set_Instance(MouseLook)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void MouseLook_set_Instance_mA80F2A4EF6E41B80058108D5AFF17C03E4F6251D_inline (MouseLook_t1E1EE79B13022184F3EF9AC8FE8C6771053890DA * ___value0, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Transform::get_parent()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * Transform_get_parent_m7D06005D9CB55F90F39D42F6A2AF9C7BC80745C9 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// System.Void MouseLook::set_CursorToggle(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MouseLook_set_CursorToggle_m2DBA5589D35ED1938A52174EC990627601603468 (MouseLook_t1E1EE79B13022184F3EF9AC8FE8C6771053890DA * __this, bool ___value0, const RuntimeMethod* method);
// System.Boolean MouseLook::get_LookEnabled()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool MouseLook_get_LookEnabled_m872F548C1ADDCAE36BCFFC9743F4F6E240A703C5_inline (MouseLook_t1E1EE79B13022184F3EF9AC8FE8C6771053890DA * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Input::GetAxisRaw(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Input_GetAxisRaw_mC07AC23FD8D04A69CDB07C6399C93FFFAEB0FC5B (String_t* ___axisName0, const RuntimeMethod* method);
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * __this, float ___x0, float ___y1, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Multiply(UnityEngine.Vector2,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Vector2_op_Multiply_mC7A7802352867555020A90205EBABA56EE5E36CB_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___a0, float ___d1, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::Lerp(UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Vector2_Lerp_mC9A8AB816281F4447B7B62264595C16751ED355B_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___a0, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___b1, float ___t2, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Addition(UnityEngine.Vector2,UnityEngine.Vector2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Vector2_op_Addition_m5EACC2AEA80FEE29F380397CF1F4B11D04BE71CC_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___a0, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___b1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_right()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_get_right_mF5A51F81961474E0A7A31C2757FD00921FB79C44 (const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::AngleAxis(System.Single,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  Quaternion_AngleAxis_m4644D20F58ADF03E9EA297CB4A845E5BCDA1E398 (float ___angle0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___axis1, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localRotation(UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_localRotation_m1A9101457EC4653AFC93FCC4065A29F2C78FA62C (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___value0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_up()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_up_mAB753D250A30C78924D5D22B0821F1D254525C31 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_rotation(UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_rotation_m1B5F3D4CE984AB31254615C9C71B0E54978583B4 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___value0, const RuntimeMethod* method);
// System.Void PlayerController::set_Instance(PlayerController)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void PlayerController_set_Instance_m1DBDE7741593D098536751F470647F587A7A0D0A_inline (PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9 * ___value0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.CharacterController>()
inline CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E * Component_GetComponent_TisCharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E_m3DB1DD5819F96D7C7F6F19C12138AC48D21DBBF2 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_up()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_get_up_m38AECA68388D446CFADDD022B0B867293044EA50 (const RuntimeMethod* method);
// UnityEngine.CollisionFlags UnityEngine.CharacterController::Move(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t CharacterController_Move_mE0EBC32C72A0BEC18EDEBE748D44309A4BA32E60 (CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___motion0, const RuntimeMethod* method);
// System.Void UnityEngine.Collider::set_enabled(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Collider_set_enabled_m047B4D830755CD36671F7A60BFAA9C0D61F6C4A1 (Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * __this, bool ___value0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.AI.NavMeshAgent>()
inline NavMeshAgent_tB9746B6C38013341DB63973CA7ED657494EFB41B * Component_GetComponent_TisNavMeshAgent_tB9746B6C38013341DB63973CA7ED657494EFB41B_m15077FF184192C84FCF6B2A1FC8ECF53C9220F2F (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  NavMeshAgent_tB9746B6C38013341DB63973CA7ED657494EFB41B * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.Void Ship::set_Agent(UnityEngine.AI.NavMeshAgent)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Ship_set_Agent_m99D21CDD3A46A0B76541A0FBC9B8DA8BA9858A8D_inline (Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * __this, NavMeshAgent_tB9746B6C38013341DB63973CA7ED657494EFB41B * ___value0, const RuntimeMethod* method);
// System.Void ShipWait::.ctor(Ship)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShipWait__ctor_mC7C641CCA9F94A3C7D6BBF10F32B8F2B77496FA5 (ShipWait_tB05589ED6695523DD921B075C449CE03E0B836FE * __this, Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * ___instance0, const RuntimeMethod* method);
// System.Void ShipMove::.ctor(Ship)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShipMove__ctor_mB007B621D62FBD63A71C4B9A679A3E2707A8C639 (ShipMove_t761409814A6DA69DE131EA554AEC499CB81D3953 * __this, Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * ___instance0, const RuntimeMethod* method);
// System.Void FiniteStateMachine.StateMachine::.ctor(FiniteStateMachine.IState)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StateMachine__ctor_m4362A8F7E5321C0B2F5D8E85462C1643F863E457 (StateMachine_t8E24C2687A18E15B27A6A8FEED2DFE22D910119F * __this, RuntimeObject* ___initState0, const RuntimeMethod* method);
// System.Void Ship::set_StateMachine(FiniteStateMachine.StateMachine)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Ship_set_StateMachine_mEB49B2626B6B51769D40F7CC9FC3FA2D59961F13_inline (Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * __this, StateMachine_t8E24C2687A18E15B27A6A8FEED2DFE22D910119F * ___value0, const RuntimeMethod* method);
// System.Void PlayerController::MoveToPosition(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerController_MoveToPosition_m9E1B5DA225A7BE1C6E859C6294A44281E5480876 (PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___targetPos0, const RuntimeMethod* method);
// FiniteStateMachine.StateMachine Ship::get_StateMachine()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR StateMachine_t8E24C2687A18E15B27A6A8FEED2DFE22D910119F * Ship_get_StateMachine_m9110695EE50E4C7260E64164574C5394CFF403C1_inline (Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * __this, const RuntimeMethod* method);
// System.Void FiniteStateMachine.StateMachine::OnUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StateMachine_OnUpdate_m5C824B8C704BF7B7E99B53B7BC0A17D05EFE3DA6 (StateMachine_t8E24C2687A18E15B27A6A8FEED2DFE22D910119F * __this, const RuntimeMethod* method);
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SceneManager_LoadScene_m5550E6368A6D0E37DACEDA3C5E4BA331836BC3C5 (int32_t ___sceneBuildIndex0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Bounds::get_center()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Bounds_get_center_m78CD262996DD859F71DAFFF39228EBE0C422F485 (Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Bounds::get_size()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Bounds_get_size_mB1C37E89879C7810BC9F4210033D9277DAFE2C14 (Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 * __this, const RuntimeMethod* method);
// FiniteStateMachine.IState FiniteStateMachine.StateMachine::get_CurrentState()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject* StateMachine_get_CurrentState_mCDB0B1FC6472E599268D5E92F31C0D890613B326_inline (StateMachine_t8E24C2687A18E15B27A6A8FEED2DFE22D910119F * __this, const RuntimeMethod* method);
// StateType FiniteStateMachine.StateMachine::GetCurrentStateAsType<ShipState>()
inline ShipState_tF56B93F7FFD3E22AB4F4ECDF9A73D50A3FE508A5 * StateMachine_GetCurrentStateAsType_TisShipState_tF56B93F7FFD3E22AB4F4ECDF9A73D50A3FE508A5_m955D4460B9487D2450FF1C450DEB3B39C73F6255 (StateMachine_t8E24C2687A18E15B27A6A8FEED2DFE22D910119F * __this, const RuntimeMethod* method)
{
	return ((  ShipState_tF56B93F7FFD3E22AB4F4ECDF9A73D50A3FE508A5 * (*) (StateMachine_t8E24C2687A18E15B27A6A8FEED2DFE22D910119F *, const RuntimeMethod*))StateMachine_GetCurrentStateAsType_TisRuntimeObject_m461BA580D5A4CC0A0D536C14160EDF25F93699E3_gshared)(__this, method);
}
// System.Void ShipState::.ctor(Ship)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShipState__ctor_mEA5C98A1E34AE5F3ADF5D3940A212D61561FBD48 (ShipState_tF56B93F7FFD3E22AB4F4ECDF9A73D50A3FE508A5 * __this, Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * ___instance0, const RuntimeMethod* method);
// UnityEngine.Vector3 ShipMove::GetRandomPosInBounds()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ShipMove_GetRandomPosInBounds_mF73CF65739E672B709F7304EE16A896E94E1F7E4 (ShipMove_t761409814A6DA69DE131EA554AEC499CB81D3953 * __this, const RuntimeMethod* method);
// Ship ShipState::get_Instance()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * ShipState_get_Instance_m11696AE1492C8B96116FF2687CE13EA7B45428A0_inline (ShipState_tF56B93F7FFD3E22AB4F4ECDF9A73D50A3FE508A5 * __this, const RuntimeMethod* method);
// UnityEngine.AI.NavMeshAgent Ship::get_Agent()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR NavMeshAgent_tB9746B6C38013341DB63973CA7ED657494EFB41B * Ship_get_Agent_m2733EC0E5F1D092B7FB03B5BA882B9FAD8C4CF96_inline (Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.AI.NavMeshAgent::SetDestination(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool NavMeshAgent_SetDestination_m244EFBCDB717576303DA711EE39572B865F43747 (NavMeshAgent_tB9746B6C38013341DB63973CA7ED657494EFB41B * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___target0, const RuntimeMethod* method);
// System.Single UnityEngine.Vector3::Distance(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Vector3_Distance_mB648A79E4A1BAAFBF7B029644638C0D715480677 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method);
// System.Single UnityEngine.AI.NavMeshAgent::get_stoppingDistance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float NavMeshAgent_get_stoppingDistance_mE2F58A8DB9C8402F0373576AB91690E8B34C1EA6 (NavMeshAgent_tB9746B6C38013341DB63973CA7ED657494EFB41B * __this, const RuntimeMethod* method);
// System.Void FiniteStateMachine.StateMachine::SetState(FiniteStateMachine.IState)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StateMachine_SetState_m7BA252E835DBB9FBFF89545B6634AFD0DEB659A0 (StateMachine_t8E24C2687A18E15B27A6A8FEED2DFE22D910119F * __this, RuntimeObject* ___newState0, const RuntimeMethod* method);
// System.Void UnityEngine.Gizmos::DrawSphere(UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Gizmos_DrawSphere_m50414CF8E502F4D93FC133091DA5E39543D69E91 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___center0, float ___radius1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Bounds::get_extents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Bounds_get_extents_mA54D7497D65DCF21CA952FA754B9D1086305FF02 (Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 * __this, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void ShipState::set_Instance(Ship)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void ShipState_set_Instance_m37F9D75A65E59E0500535234B7F46301E108BFCF_inline (ShipState_tF56B93F7FFD3E22AB4F4ECDF9A73D50A3FE508A5 * __this, Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * ___value0, const RuntimeMethod* method);
// UnityEngine.Vector3 SoloFish::GetRandomPosInBounds()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  SoloFish_GetRandomPosInBounds_m8622D90A0BB64C591B451299165E119524C6B3E1 (SoloFish_t248C1128425014659AF96F9294BAC7D1799179C2 * __this, const RuntimeMethod* method);
// System.Void SoloFish::MoveStep()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SoloFish_MoveStep_m75074289C1B548F4A42BFC3A0E377F3B58031E3E (SoloFish_t248C1128425014659AF96F9294BAC7D1799179C2 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Gizmos::DrawWireSphere(UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Gizmos_DrawWireSphere_m96C425145BBD85CF0192F9DDB3D1A8C69429B78B (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___center0, float ___radius1, const RuntimeMethod* method);
// UnityEngine.Vector3 SoloFish::Bounds()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  SoloFish_Bounds_m456DD94835D1E087A1A45D1258B1496EBB7778A7 (SoloFish_t248C1128425014659AF96F9294BAC7D1799179C2 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 SoloFish::ObstacleAvoidance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  SoloFish_ObstacleAvoidance_m5D27E74C36F7F0C727F4AEA88E249EF022CD779C (SoloFish_t248C1128425014659AF96F9294BAC7D1799179C2 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::DrawRay(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_DrawRay_m918D1131BACEBD7CCEA9D9BFDF3279F6CD56E121 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___start0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___dir1, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___color2, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_UnaryNegation(UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_UnaryNegation_m362EA356F4CADEDB39F965A0DBDED6EA890925F7_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, const RuntimeMethod* method);
// System.Void FiniteStateMachine.StateMachine::set_CurrentState(FiniteStateMachine.IState)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void StateMachine_set_CurrentState_m1B411CA93F64340414238167A0DF7B8DBD57B8C8_inline (StateMachine_t8E24C2687A18E15B27A6A8FEED2DFE22D910119F * __this, RuntimeObject* ___value0, const RuntimeMethod* method);
// System.Void System.NotImplementedException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotImplementedException__ctor_mA2E9CE7F00CB335581A296D2596082D57E45BA83 (NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6 * __this, const RuntimeMethod* method);
// System.Void ToolTip::set_Instance(ToolTip)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void ToolTip_set_Instance_mD51AE6A1572D2CFFED1C930D088CB363C31B30D2_inline (ToolTip_t044257731DC5931927D3AB0671C17515BD8E251E * ___value0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponentInChildren<UnityEngine.UI.Text>()
inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * Component_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mFB5C182E24F496A866F116D3F75CBD8616A46AB3 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponentInChildren_TisRuntimeObject_mB377B32275A969E0D1A738DBC693DE8EB3593642_gshared)(__this, method);
}
// System.Void WorldMap::set_Instance(WorldMap)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void WorldMap_set_Instance_mDDE558CAD26A7D4680B28F1C7FA3AB7F3A47A7B6_inline (WorldMap_t01A07DDC9EA386C88D5814CF18E0F37CAF05270C * ___value0, const RuntimeMethod* method);
// System.Void WorldMap::UpdateLegendSelection()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WorldMap_UpdateLegendSelection_m96C6070EF674FED8B832E12C9ED1403EFAD2B33A (WorldMap_t01A07DDC9EA386C88D5814CF18E0F37CAF05270C * __this, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::get_white()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  Color_get_white_mB21E47D20959C3AEC41AF8BA04F63AC89FAF319E (const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject>::.ctor(System.Collections.Generic.IEnumerable`1<!0>)
inline void List_1__ctor_m6C09283D126DAC46B64EE124C67FADA66797752F (List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * __this, RuntimeObject* ___collection0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 *, RuntimeObject*, const RuntimeMethod*))List_1__ctor_m1D864B65CCD0498EC4BFFBDA8F8D04AE5333195A_gshared)(__this, ___collection0, method);
}
// UnityEngine.Color UnityEngine.Color::get_grey()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  Color_get_grey_mB2E29B47327F20233856F933DC00ACADEBFDBDFA (const RuntimeMethod* method);
// System.Boolean UnityEngine.GameObject::TryGetComponent<IInteraction>(!!0&)
inline bool GameObject_TryGetComponent_TisIInteraction_t1C1C3E6C29AFB203F5021CA7300B6FEA0EFE91FE_m619A9E6EA0CC7690AEDF0BA79C3534C945F5906B (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, RuntimeObject** ___component0, const RuntimeMethod* method)
{
	return ((  bool (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, RuntimeObject**, const RuntimeMethod*))GameObject_TryGetComponent_TisRuntimeObject_mC7639C3E3E2A65695113BCF25718826CC392D6CB_gshared)(__this, ___component0, method);
}
// System.Void GameManager::TogglePlayer(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameManager_TogglePlayer_m6D6635AAAE3C34217AAF3AE377EC5B97AE4CAA47 (GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * __this, bool ___toggle0, const RuntimeMethod* method);
// System.Void UnityEngine.Playables.PlayableDirector::set_playableAsset(UnityEngine.Playables.PlayableAsset)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayableDirector_set_playableAsset_m6A7954E07B87A8764468294D1264572301656CE3 (PlayableDirector_t63912C98A4EC9D4701E88B633E27E0D97209BA38 * __this, PlayableAsset_t5AD1606B76C9753A7F4C6B1061193F581023F137 * ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Playables.PlayableDirector::Play()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayableDirector_Play_m13D31AD720EEE25E93A21394B225EA10300C47C4 (PlayableDirector_t63912C98A4EC9D4701E88B633E27E0D97209BA38 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.WaitForEndOfFrame::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WaitForEndOfFrame__ctor_mEA41FB4A9236A64D566330BBE25F9902DEBB2EEA (WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4 * __this, const RuntimeMethod* method);
// System.Double UnityEngine.Playables.PlayableDirector::get_time()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR double PlayableDirector_get_time_m6E6BEDB6E9FF4A8CD48F73FB64F353B5787E735F (PlayableDirector_t63912C98A4EC9D4701E88B633E27E0D97209BA38 * __this, const RuntimeMethod* method);
// UnityEngine.Playables.PlayableAsset UnityEngine.Playables.PlayableDirector::get_playableAsset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PlayableAsset_t5AD1606B76C9753A7F4C6B1061193F581023F137 * PlayableDirector_get_playableAsset_mAD7C43BF96CA5F6553D52DA32C2D14C8501CE45A (PlayableDirector_t63912C98A4EC9D4701E88B633E27E0D97209BA38 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Playables.PlayableDirector::Stop()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayableDirector_Stop_m4C9FCBB20CDD4BF75DF910F3BE713251D9548C96 (PlayableDirector_t63912C98A4EC9D4701E88B633E27E0D97209BA38 * __this, const RuntimeMethod* method);
// System.Void System.NotSupportedException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.WaitForSeconds::.ctor(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WaitForSeconds__ctor_mD298C4CB9532BBBDE172FC40F3397E30504038D4 (WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 * __this, float ___seconds0, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Clamp01(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_Clamp01_m2296D75F0F1292D5C8181C57007A1CA45F440C4C (float ___value0, const RuntimeMethod* method);
// System.Void System.ThrowHelper::ThrowArgumentOutOfRangeException()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929 (const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// ActionMenu ActionMenu::get_Instance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ActionMenu_tE3C589CB5B0B532FF5504CF0D558D489281BED01 * ActionMenu_get_Instance_m0A358E0ED4326BDF16E867E22F1E5BF5B9AD2DC0 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ActionMenu_tE3C589CB5B0B532FF5504CF0D558D489281BED01_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static ActionMenu Instance { get; private set; }
		ActionMenu_tE3C589CB5B0B532FF5504CF0D558D489281BED01 * L_0 = ((ActionMenu_tE3C589CB5B0B532FF5504CF0D558D489281BED01_StaticFields*)il2cpp_codegen_static_fields_for(ActionMenu_tE3C589CB5B0B532FF5504CF0D558D489281BED01_il2cpp_TypeInfo_var))->get_U3CInstanceU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void ActionMenu::set_Instance(ActionMenu)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ActionMenu_set_Instance_m4D0D66C264341B224B923BEA333A7257A7B8E82F (ActionMenu_tE3C589CB5B0B532FF5504CF0D558D489281BED01 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ActionMenu_tE3C589CB5B0B532FF5504CF0D558D489281BED01_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static ActionMenu Instance { get; private set; }
		ActionMenu_tE3C589CB5B0B532FF5504CF0D558D489281BED01 * L_0 = ___value0;
		((ActionMenu_tE3C589CB5B0B532FF5504CF0D558D489281BED01_StaticFields*)il2cpp_codegen_static_fields_for(ActionMenu_tE3C589CB5B0B532FF5504CF0D558D489281BED01_il2cpp_TypeInfo_var))->set_U3CInstanceU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.Void ActionMenu::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ActionMenu_Awake_mF389F85308C8A469685278C9EB1A0BA881E714A2 (ActionMenu_tE3C589CB5B0B532FF5504CF0D558D489281BED01 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if(Instance == null)
		ActionMenu_tE3C589CB5B0B532FF5504CF0D558D489281BED01 * L_0;
		L_0 = ActionMenu_get_Instance_m0A358E0ED4326BDF16E867E22F1E5BF5B9AD2DC0_inline(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		// Instance = this;
		ActionMenu_set_Instance_m4D0D66C264341B224B923BEA333A7257A7B8E82F_inline(__this, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0014:
	{
		// enabled = false;
		Behaviour_set_enabled_mDE415591B28853D1CD764C53CB499A2142247F32(__this, (bool)0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void ActionMenu::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ActionMenu_Start_mCC99B103AA695673F44496113AA07AA4F5A79AE8 (ActionMenu_tE3C589CB5B0B532FF5504CF0D558D489281BED01 * __this, const RuntimeMethod* method)
{
	{
		// Deactivate();
		int32_t L_0;
		L_0 = ActionMenu_Deactivate_mFA0E81D3F5C0B0FC0267242D68B3B0BA21F36D85(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void ActionMenu::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ActionMenu_Update_mD53D9CBD0C575A7E145C71E92415F1E002222471 (ActionMenu_tE3C589CB5B0B532FF5504CF0D558D489281BED01 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE5368AE200A37FDFED8AA9B3CB8C7E26E81CA911);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// if (gameObject.activeSelf == true && timer == -1)
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0;
		L_0 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		bool L_1;
		L_1 = GameObject_get_activeSelf_m4865097C24FB29F3C31F5C30619AF242297F23EE(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0080;
		}
	}
	{
		float L_2 = __this->get_timer_7();
		if ((!(((float)L_2) == ((float)(-1.0f)))))
		{
			goto IL_0080;
		}
	}
	{
		// int axis = Mathf.FloorToInt(Input.GetAxis("Menu Selection"));  //scroll wheel usage to navigate options
		float L_3;
		L_3 = Input_GetAxis_m939297DEB2ECF8D8D09AD66EB69979AAD2B62326(_stringLiteralE5368AE200A37FDFED8AA9B3CB8C7E26E81CA911, /*hidden argument*/NULL);
		int32_t L_4;
		L_4 = Mathf_FloorToInt_m9164D538D17B8C3C8A6C4E4FA95032F757D9091E(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		// if (axis != 0)
		int32_t L_5 = V_0;
		if (!L_5)
		{
			goto IL_0080;
		}
	}
	{
		// timer = 0;
		__this->set_timer_7((0.0f));
		// selection += axis;
		int32_t L_6 = __this->get_selection_6();
		int32_t L_7 = V_0;
		__this->set_selection_6(((int32_t)il2cpp_codegen_add((int32_t)L_6, (int32_t)L_7)));
		// if(selection < 0)
		int32_t L_8 = __this->get_selection_6();
		if ((((int32_t)L_8) >= ((int32_t)0)))
		{
			goto IL_0061;
		}
	}
	{
		// selection = menuOptions.Length - 1;
		ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224* L_9 = __this->get_menuOptions_4();
		__this->set_selection_6(((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_9)->max_length))), (int32_t)1)));
		// }
		goto IL_007a;
	}

IL_0061:
	{
		// else if(selection > menuOptions.Length - 1)
		int32_t L_10 = __this->get_selection_6();
		ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224* L_11 = __this->get_menuOptions_4();
		if ((((int32_t)L_10) <= ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_11)->max_length))), (int32_t)1)))))
		{
			goto IL_007a;
		}
	}
	{
		// selection = 0;
		__this->set_selection_6(0);
	}

IL_007a:
	{
		// UpdateUISelection();
		ActionMenu_UpdateUISelection_mA81DEC42B1B5D91EF6D7FDC89FC3C5BDBD2FC85F(__this, /*hidden argument*/NULL);
	}

IL_0080:
	{
		// if(timer > -1)
		float L_12 = __this->get_timer_7();
		if ((!(((float)L_12) > ((float)(-1.0f)))))
		{
			goto IL_00b7;
		}
	}
	{
		// timer += Time.deltaTime;
		float L_13 = __this->get_timer_7();
		float L_14;
		L_14 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		__this->set_timer_7(((float)il2cpp_codegen_add((float)L_13, (float)L_14)));
		// if(timer > 0.2f)
		float L_15 = __this->get_timer_7();
		if ((!(((float)L_15) > ((float)(0.200000003f)))))
		{
			goto IL_00b7;
		}
	}
	{
		// timer = -1;
		__this->set_timer_7((-1.0f));
	}

IL_00b7:
	{
		// }
		return;
	}
}
// System.Void ActionMenu::UpdateUISelection()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ActionMenu_UpdateUISelection_mA81DEC42B1B5D91EF6D7FDC89FC3C5BDBD2FC85F (ActionMenu_tE3C589CB5B0B532FF5504CF0D558D489281BED01 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral11E7C4352B11858AEFB14F47D96849AC6696EC2B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA887E0FF7814A894C42824B8F164C9E3F0509345);
		s_Il2CppMethodInitialized = true;
	}
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  V_1;
	memset((&V_1), 0, sizeof(V_1));
	int32_t V_2 = 0;
	{
		// ColorUtility.TryParseHtmlString("#FFA047", out selectedOrange);
		bool L_0;
		L_0 = ColorUtility_TryParseHtmlString_m69BEFAF655920930399471B79CF668FC3BAD4069(_stringLiteral11E7C4352B11858AEFB14F47D96849AC6696EC2B, (Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 *)(&V_0), /*hidden argument*/NULL);
		// ColorUtility.TryParseHtmlString("#3A74A6", out unselectedBlue);
		bool L_1;
		L_1 = ColorUtility_TryParseHtmlString_m69BEFAF655920930399471B79CF668FC3BAD4069(_stringLiteralA887E0FF7814A894C42824B8F164C9E3F0509345, (Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 *)(&V_1), /*hidden argument*/NULL);
		// for(int i = 0; i <menuOptions.Length; i++)
		V_2 = 0;
		goto IL_0049;
	}

IL_001e:
	{
		// if(i == selection)
		int32_t L_2 = V_2;
		int32_t L_3 = __this->get_selection_6();
		if ((!(((uint32_t)L_2) == ((uint32_t)L_3))))
		{
			goto IL_0037;
		}
	}
	{
		// menuOptions[i].color = selectedOrange;
		ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224* L_4 = __this->get_menuOptions_4();
		int32_t L_5 = V_2;
		int32_t L_6 = L_5;
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_7 = (L_4)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_6));
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_8 = V_0;
		VirtActionInvoker1< Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_7, L_8);
		// }
		goto IL_0045;
	}

IL_0037:
	{
		// menuOptions[i].color = unselectedBlue;
		ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224* L_9 = __this->get_menuOptions_4();
		int32_t L_10 = V_2;
		int32_t L_11 = L_10;
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_12 = (L_9)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_11));
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_13 = V_1;
		VirtActionInvoker1< Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_12, L_13);
	}

IL_0045:
	{
		// for(int i = 0; i <menuOptions.Length; i++)
		int32_t L_14 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_14, (int32_t)1));
	}

IL_0049:
	{
		// for(int i = 0; i <menuOptions.Length; i++)
		int32_t L_15 = V_2;
		ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224* L_16 = __this->get_menuOptions_4();
		if ((((int32_t)L_15) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_16)->max_length))))))
		{
			goto IL_001e;
		}
	}
	{
		// }
		return;
	}
}
// System.Void ActionMenu::Activate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ActionMenu_Activate_mD3CE0F110A26F537889E78930F7F78B0969FCC4D (ActionMenu_tE3C589CB5B0B532FF5504CF0D558D489281BED01 * __this, const RuntimeMethod* method)
{
	{
		// gameObject.SetActive(true);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0;
		L_0 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_0, (bool)1, /*hidden argument*/NULL);
		// selection = 0;
		__this->set_selection_6(0);
		// UpdateUISelection();
		ActionMenu_UpdateUISelection_mA81DEC42B1B5D91EF6D7FDC89FC3C5BDBD2FC85F(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Int32 ActionMenu::Deactivate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ActionMenu_Deactivate_mFA0E81D3F5C0B0FC0267242D68B3B0BA21F36D85 (ActionMenu_tE3C589CB5B0B532FF5504CF0D558D489281BED01 * __this, const RuntimeMethod* method)
{
	{
		// gameObject.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0;
		L_0 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_0, (bool)0, /*hidden argument*/NULL);
		// int s = selection;
		int32_t L_1 = __this->get_selection_6();
		// selection = -1;
		__this->set_selection_6((-1));
		// return s;
		return L_1;
	}
}
// System.Void ActionMenu::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ActionMenu__ctor_m94CC62F0105BC9ED7DE5851FDE69845E7291D8D5 (ActionMenu_tE3C589CB5B0B532FF5504CF0D558D489281BED01 * __this, const RuntimeMethod* method)
{
	{
		// private int selection = -1;
		__this->set_selection_6((-1));
		// private float timer = -1;
		__this->set_timer_7((-1.0f));
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Actions Actions::get_Instance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Actions_tF16D3C582B8D8DF5DD96B758A4D691F304BE44BB * Actions_get_Instance_mFCF21C897955B911E7D3B36BE438793642556B1C (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Actions_tF16D3C582B8D8DF5DD96B758A4D691F304BE44BB_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static Actions Instance { get; private set; }
		Actions_tF16D3C582B8D8DF5DD96B758A4D691F304BE44BB * L_0 = ((Actions_tF16D3C582B8D8DF5DD96B758A4D691F304BE44BB_StaticFields*)il2cpp_codegen_static_fields_for(Actions_tF16D3C582B8D8DF5DD96B758A4D691F304BE44BB_il2cpp_TypeInfo_var))->get_U3CInstanceU3Ek__BackingField_11();
		return L_0;
	}
}
// System.Void Actions::set_Instance(Actions)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Actions_set_Instance_m1F264080CD39D9EF3210325A20AEC5721E281845 (Actions_tF16D3C582B8D8DF5DD96B758A4D691F304BE44BB * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Actions_tF16D3C582B8D8DF5DD96B758A4D691F304BE44BB_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static Actions Instance { get; private set; }
		Actions_tF16D3C582B8D8DF5DD96B758A4D691F304BE44BB * L_0 = ___value0;
		((Actions_tF16D3C582B8D8DF5DD96B758A4D691F304BE44BB_StaticFields*)il2cpp_codegen_static_fields_for(Actions_tF16D3C582B8D8DF5DD96B758A4D691F304BE44BB_il2cpp_TypeInfo_var))->set_U3CInstanceU3Ek__BackingField_11(L_0);
		return;
	}
}
// System.Void Actions::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Actions_Awake_m9F5BFB055871E0A1D7F18FDACB86E65C58524BED (Actions_tF16D3C582B8D8DF5DD96B758A4D691F304BE44BB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if(Instance == null)
		Actions_tF16D3C582B8D8DF5DD96B758A4D691F304BE44BB * L_0;
		L_0 = Actions_get_Instance_mFCF21C897955B911E7D3B36BE438793642556B1C_inline(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		// Instance = this;
		Actions_set_Instance_m1F264080CD39D9EF3210325A20AEC5721E281845_inline(__this, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0014:
	{
		// enabled = false;
		Behaviour_set_enabled_mDE415591B28853D1CD764C53CB499A2142247F32(__this, (bool)0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Actions::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Actions_Start_m7093D52055EE9FCF9145A5F06DC7D89609D29B0C (Actions_tF16D3C582B8D8DF5DD96B758A4D691F304BE44BB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m3DD76DE838FA83DF972E0486A296345EB3A7DDF3_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_mC197A913A8DF8E125DEFD1C13605BC1DFD23C2F7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// for(int i = 0; i < iconLimit; i++)
		V_0 = 0;
		goto IL_0071;
	}

IL_0004:
	{
		// spawnedSonarIcons.Add(Instantiate(sonarIconPrefab, ActionMenu.Instance.transform.root, false));
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_0 = __this->get_spawnedSonarIcons_14();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1 = __this->get_sonarIconPrefab_10();
		ActionMenu_tE3C589CB5B0B532FF5504CF0D558D489281BED01 * L_2;
		L_2 = ActionMenu_get_Instance_m0A358E0ED4326BDF16E867E22F1E5BF5B9AD2DC0_inline(/*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_3;
		L_3 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_2, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_4;
		L_4 = Transform_get_root_mDEB1F3B4DB26B32CEED6DFFF734F85C79C4DDA91(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5;
		L_5 = Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_mC197A913A8DF8E125DEFD1C13605BC1DFD23C2F7(L_1, L_4, (bool)0, /*hidden argument*/Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_mC197A913A8DF8E125DEFD1C13605BC1DFD23C2F7_RuntimeMethod_var);
		List_1_Add_m3DD76DE838FA83DF972E0486A296345EB3A7DDF3(L_0, L_5, /*hidden argument*/List_1_Add_m3DD76DE838FA83DF972E0486A296345EB3A7DDF3_RuntimeMethod_var);
		// spawnedSonarIcons[i].transform.position = Vector3.zero;
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_6 = __this->get_spawnedSonarIcons_14();
		int32_t L_7 = V_0;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_8;
		L_8 = List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_inline(L_6, L_7, /*hidden argument*/List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_RuntimeMethod_var);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_9;
		L_9 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_8, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10;
		L_10 = Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6(/*hidden argument*/NULL);
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_9, L_10, /*hidden argument*/NULL);
		// spawnedSonarIcons[i].transform.SetAsFirstSibling();
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_11 = __this->get_spawnedSonarIcons_14();
		int32_t L_12 = V_0;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_13;
		L_13 = List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_inline(L_11, L_12, /*hidden argument*/List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_RuntimeMethod_var);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_14;
		L_14 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_13, /*hidden argument*/NULL);
		Transform_SetAsFirstSibling_mD5C02831BA6C7C3408CD491191EAF760ECB7E754(L_14, /*hidden argument*/NULL);
		// spawnedSonarIcons[i].SetActive(false);
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_15 = __this->get_spawnedSonarIcons_14();
		int32_t L_16 = V_0;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_17;
		L_17 = List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_inline(L_15, L_16, /*hidden argument*/List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_RuntimeMethod_var);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_17, (bool)0, /*hidden argument*/NULL);
		// for(int i = 0; i < iconLimit; i++)
		int32_t L_18 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_18, (int32_t)1));
	}

IL_0071:
	{
		// for(int i = 0; i < iconLimit; i++)
		int32_t L_19 = V_0;
		int32_t L_20 = __this->get_iconLimit_8();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_0004;
		}
	}
	{
		// Map(false);
		Actions_Map_m76DFCD66B1AA4F860F1956778B07ADE092E771D9(__this, (bool)0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Actions::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Actions_Update_mA16AB2E471F8E5369DF9018D706C24F192DD7924 (Actions_tF16D3C582B8D8DF5DD96B758A4D691F304BE44BB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDD86881DBA7E6B755DFE2849A5B0579CC3D828CD);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	ColliderU5BU5D_t5124940293343DB3D31DA41C593709905906E486* V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_4;
	memset((&V_4), 0, sizeof(V_4));
	{
		// if (WorldMap.Instance.legend.activeSelf == false)
		WorldMap_t01A07DDC9EA386C88D5814CF18E0F37CAF05270C * L_0;
		L_0 = WorldMap_get_Instance_m119402AD39383BC71435F9F36C3219A2281289CA_inline(/*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1 = L_0->get_legend_5();
		bool L_2;
		L_2 = GameObject_get_activeSelf_m4865097C24FB29F3C31F5C30619AF242297F23EE(L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0094;
		}
	}
	{
		// if (ActionMenu.Instance.gameObject.activeSelf == false)
		ActionMenu_tE3C589CB5B0B532FF5504CF0D558D489281BED01 * L_3;
		L_3 = ActionMenu_get_Instance_m0A358E0ED4326BDF16E867E22F1E5BF5B9AD2DC0_inline(/*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_4;
		L_4 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_3, /*hidden argument*/NULL);
		bool L_5;
		L_5 = GameObject_get_activeSelf_m4865097C24FB29F3C31F5C30619AF242297F23EE(L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_004e;
		}
	}
	{
		// if (InteractionMenu.Instance.gameObject.activeSelf == false)
		InteractionMenu_t7C2715221BAC6E0BB83B07EF82F68C6AA34129CB * L_6;
		L_6 = InteractionMenu_get_Instance_m60C03978FEDDC7D708EB1C5C30EBD081344385FF_inline(/*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_7;
		L_7 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_6, /*hidden argument*/NULL);
		bool L_8;
		L_8 = GameObject_get_activeSelf_m4865097C24FB29F3C31F5C30619AF242297F23EE(L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_00a7;
		}
	}
	{
		// if (Input.GetButtonDown("Action") == true)
		bool L_9;
		L_9 = Input_GetButtonDown_m2001112EBCA3D5C7B0344EF62C896667F7E67DDF(_stringLiteralDD86881DBA7E6B755DFE2849A5B0579CC3D828CD, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_00a7;
		}
	}
	{
		// ActionMenu.Instance.Activate();
		ActionMenu_tE3C589CB5B0B532FF5504CF0D558D489281BED01 * L_10;
		L_10 = ActionMenu_get_Instance_m0A358E0ED4326BDF16E867E22F1E5BF5B9AD2DC0_inline(/*hidden argument*/NULL);
		ActionMenu_Activate_mD3CE0F110A26F537889E78930F7F78B0969FCC4D(L_10, /*hidden argument*/NULL);
		// }
		goto IL_00a7;
	}

IL_004e:
	{
		// if (Input.GetButtonUp("Action") == true)
		bool L_11;
		L_11 = Input_GetButtonUp_m15AA6B42BD0DDCC7802346E49F30653D750260DD(_stringLiteralDD86881DBA7E6B755DFE2849A5B0579CC3D828CD, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_00a7;
		}
	}
	{
		// int selection = ActionMenu.Instance.Deactivate();
		ActionMenu_tE3C589CB5B0B532FF5504CF0D558D489281BED01 * L_12;
		L_12 = ActionMenu_get_Instance_m0A358E0ED4326BDF16E867E22F1E5BF5B9AD2DC0_inline(/*hidden argument*/NULL);
		int32_t L_13;
		L_13 = ActionMenu_Deactivate_mFA0E81D3F5C0B0FC0267242D68B3B0BA21F36D85(L_12, /*hidden argument*/NULL);
		V_0 = L_13;
		int32_t L_14 = V_0;
		switch (L_14)
		{
			case 0:
			{
				goto IL_0079;
			}
			case 1:
			{
				goto IL_0082;
			}
			case 2:
			{
				goto IL_008b;
			}
		}
	}
	{
		goto IL_00a7;
	}

IL_0079:
	{
		// Boost(true);
		Actions_Boost_m5CB5C10BC32C2DE52E94274EAEF5F486716E49E4(__this, (bool)1, /*hidden argument*/NULL);
		// break;
		goto IL_00a7;
	}

IL_0082:
	{
		// Sonar(true);
		Actions_Sonar_m9974B6F2FE0C34C6FADC1792DD56F5013D247130(__this, (bool)1, /*hidden argument*/NULL);
		// break;
		goto IL_00a7;
	}

IL_008b:
	{
		// Map(true);
		Actions_Map_m76DFCD66B1AA4F860F1956778B07ADE092E771D9(__this, (bool)1, /*hidden argument*/NULL);
		// break;
		goto IL_00a7;
	}

IL_0094:
	{
		// else if(Input.GetButtonDown("Action") == true)
		bool L_15;
		L_15 = Input_GetButtonDown_m2001112EBCA3D5C7B0344EF62C896667F7E67DDF(_stringLiteralDD86881DBA7E6B755DFE2849A5B0579CC3D828CD, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_00a7;
		}
	}
	{
		// Map(false);
		Actions_Map_m76DFCD66B1AA4F860F1956778B07ADE092E771D9(__this, (bool)0, /*hidden argument*/NULL);
	}

IL_00a7:
	{
		// if(boostTimer > -1)
		float L_16 = __this->get_boostTimer_13();
		if ((!(((float)L_16) > ((float)(-1.0f)))))
		{
			goto IL_00db;
		}
	}
	{
		// boostTimer += Time.deltaTime;
		float L_17 = __this->get_boostTimer_13();
		float L_18;
		L_18 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		__this->set_boostTimer_13(((float)il2cpp_codegen_add((float)L_17, (float)L_18)));
		// if(boostTimer > boostTime)
		float L_19 = __this->get_boostTimer_13();
		float L_20 = __this->get_boostTime_5();
		if ((!(((float)L_19) > ((float)L_20))))
		{
			goto IL_00db;
		}
	}
	{
		// Boost(false); // deactivate boost
		Actions_Boost_m5CB5C10BC32C2DE52E94274EAEF5F486716E49E4(__this, (bool)0, /*hidden argument*/NULL);
	}

IL_00db:
	{
		// if(sonarTimer > -1)  //sonar timer
		float L_21 = __this->get_sonarTimer_12();
		if ((!(((float)L_21) > ((float)(-1.0f)))))
		{
			goto IL_0208;
		}
	}
	{
		// Collider[] closeInteractions = Physics.OverlapSphere(transform.position, sonarRadius, interactionLayer);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_22;
		L_22 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_23;
		L_23 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_22, /*hidden argument*/NULL);
		float L_24 = __this->get_sonarRadius_7();
		LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  L_25 = __this->get_interactionLayer_9();
		int32_t L_26;
		L_26 = LayerMask_op_Implicit_mD89E9970822613D8D19B2EBCA36C79391C287BE0(L_25, /*hidden argument*/NULL);
		ColliderU5BU5D_t5124940293343DB3D31DA41C593709905906E486* L_27;
		L_27 = Physics_OverlapSphere_mE1FC40C646B1468905057516601DB49DD41E0223(L_23, L_24, L_26, /*hidden argument*/NULL);
		V_1 = L_27;
		// int interactionCount = closeInteractions.Length;
		ColliderU5BU5D_t5124940293343DB3D31DA41C593709905906E486* L_28 = V_1;
		V_2 = ((int32_t)((int32_t)(((RuntimeArray*)L_28)->max_length)));
		// for(int i = 0; i < iconLimit; i++)
		V_3 = 0;
		goto IL_01d5;
	}

IL_0118:
	{
		// if(i < interactionCount)
		int32_t L_29 = V_3;
		int32_t L_30 = V_2;
		if ((((int32_t)L_29) >= ((int32_t)L_30)))
		{
			goto IL_01ac;
		}
	}
	{
		// Vector3 screenPos = Camera.main.WorldToScreenPoint(closeInteractions[i].transform.position); //Z value becomes Depth
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_31;
		L_31 = Camera_get_main_mC337C621B91591CEF89504C97EF64D717C12871C(/*hidden argument*/NULL);
		ColliderU5BU5D_t5124940293343DB3D31DA41C593709905906E486* L_32 = V_1;
		int32_t L_33 = V_3;
		int32_t L_34 = L_33;
		Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * L_35 = (L_32)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_34));
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_36;
		L_36 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_35, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_37;
		L_37 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_36, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_38;
		L_38 = Camera_WorldToScreenPoint_m44710195E7736CE9DE5A9B05E32059A9A950F95C(L_31, L_37, /*hidden argument*/NULL);
		V_4 = L_38;
		// if(screenPos.z > 0) //if icon is visible on screen (negative Z means object is not visible on screen)
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_39 = V_4;
		float L_40 = L_39.get_z_4();
		if ((!(((float)L_40) > ((float)(0.0f)))))
		{
			goto IL_0185;
		}
	}
	{
		// spawnedSonarIcons[i].transform.position = screenPos;
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_41 = __this->get_spawnedSonarIcons_14();
		int32_t L_42 = V_3;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_43;
		L_43 = List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_inline(L_41, L_42, /*hidden argument*/List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_RuntimeMethod_var);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_44;
		L_44 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_43, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_45 = V_4;
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_44, L_45, /*hidden argument*/NULL);
		// if(spawnedSonarIcons[i].activeSelf == false)
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_46 = __this->get_spawnedSonarIcons_14();
		int32_t L_47 = V_3;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_48;
		L_48 = List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_inline(L_46, L_47, /*hidden argument*/List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_RuntimeMethod_var);
		bool L_49;
		L_49 = GameObject_get_activeSelf_m4865097C24FB29F3C31F5C30619AF242297F23EE(L_48, /*hidden argument*/NULL);
		if (L_49)
		{
			goto IL_01d1;
		}
	}
	{
		// spawnedSonarIcons[i].SetActive(true);
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_50 = __this->get_spawnedSonarIcons_14();
		int32_t L_51 = V_3;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_52;
		L_52 = List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_inline(L_50, L_51, /*hidden argument*/List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_RuntimeMethod_var);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_52, (bool)1, /*hidden argument*/NULL);
		// }
		goto IL_01d1;
	}

IL_0185:
	{
		// else if(spawnedSonarIcons[i].activeSelf == true)  //if icon is active and not on screen
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_53 = __this->get_spawnedSonarIcons_14();
		int32_t L_54 = V_3;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_55;
		L_55 = List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_inline(L_53, L_54, /*hidden argument*/List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_RuntimeMethod_var);
		bool L_56;
		L_56 = GameObject_get_activeSelf_m4865097C24FB29F3C31F5C30619AF242297F23EE(L_55, /*hidden argument*/NULL);
		if (!L_56)
		{
			goto IL_01d1;
		}
	}
	{
		// spawnedSonarIcons[i].SetActive(false);
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_57 = __this->get_spawnedSonarIcons_14();
		int32_t L_58 = V_3;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_59;
		L_59 = List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_inline(L_57, L_58, /*hidden argument*/List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_RuntimeMethod_var);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_59, (bool)0, /*hidden argument*/NULL);
		// }
		goto IL_01d1;
	}

IL_01ac:
	{
		// else if (spawnedSonarIcons[i].activeSelf == true) //if icon is active but no corresponding interaction
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_60 = __this->get_spawnedSonarIcons_14();
		int32_t L_61 = V_3;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_62;
		L_62 = List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_inline(L_60, L_61, /*hidden argument*/List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_RuntimeMethod_var);
		bool L_63;
		L_63 = GameObject_get_activeSelf_m4865097C24FB29F3C31F5C30619AF242297F23EE(L_62, /*hidden argument*/NULL);
		if (!L_63)
		{
			goto IL_01d1;
		}
	}
	{
		// spawnedSonarIcons[i].SetActive(false);  //turns off icon
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_64 = __this->get_spawnedSonarIcons_14();
		int32_t L_65 = V_3;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_66;
		L_66 = List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_inline(L_64, L_65, /*hidden argument*/List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_RuntimeMethod_var);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_66, (bool)0, /*hidden argument*/NULL);
	}

IL_01d1:
	{
		// for(int i = 0; i < iconLimit; i++)
		int32_t L_67 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_67, (int32_t)1));
	}

IL_01d5:
	{
		// for(int i = 0; i < iconLimit; i++)
		int32_t L_68 = V_3;
		int32_t L_69 = __this->get_iconLimit_8();
		if ((((int32_t)L_68) < ((int32_t)L_69)))
		{
			goto IL_0118;
		}
	}
	{
		// sonarTimer += Time.deltaTime;
		float L_70 = __this->get_sonarTimer_12();
		float L_71;
		L_71 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		__this->set_sonarTimer_12(((float)il2cpp_codegen_add((float)L_70, (float)L_71)));
		// if(sonarTimer > sonarTime)
		float L_72 = __this->get_sonarTimer_12();
		float L_73 = __this->get_sonarTime_6();
		if ((!(((float)L_72) > ((float)L_73))))
		{
			goto IL_0208;
		}
	}
	{
		// Sonar(false);
		Actions_Sonar_m9974B6F2FE0C34C6FADC1792DD56F5013D247130(__this, (bool)0, /*hidden argument*/NULL);
	}

IL_0208:
	{
		// }
		return;
	}
}
// System.Void Actions::Boost(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Actions_Boost_m5CB5C10BC32C2DE52E94274EAEF5F486716E49E4 (Actions_tF16D3C582B8D8DF5DD96B758A4D691F304BE44BB * __this, bool ___toggle0, const RuntimeMethod* method)
{
	{
		// if(toggle == true)  //turn on functionality
		bool L_0 = ___toggle0;
		if (!L_0)
		{
			goto IL_0033;
		}
	}
	{
		// if(boostTimer == -1)
		float L_1 = __this->get_boostTimer_13();
		if ((!(((float)L_1) == ((float)(-1.0f)))))
		{
			goto IL_0062;
		}
	}
	{
		// boostTimer = 0;
		__this->set_boostTimer_13((0.0f));
		// PlayerController.Instance.speed *= boostMultiplier;
		PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9 * L_2;
		L_2 = PlayerController_get_Instance_m8C373614456176CE37B49E24BA9B7E019C5A0440_inline(/*hidden argument*/NULL);
		PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9 * L_3 = L_2;
		float L_4 = L_3->get_speed_4();
		float L_5 = __this->get_boostMultiplier_4();
		L_3->set_speed_4(((float)il2cpp_codegen_multiply((float)L_4, (float)L_5)));
		// }
		return;
	}

IL_0033:
	{
		// if (boostTimer == -1)
		float L_6 = __this->get_boostTimer_13();
		if ((!(((float)L_6) == ((float)(-1.0f)))))
		{
			goto IL_0062;
		}
	}
	{
		// boostTimer = -1;
		__this->set_boostTimer_13((-1.0f));
		// PlayerController.Instance.speed /= boostMultiplier;
		PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9 * L_7;
		L_7 = PlayerController_get_Instance_m8C373614456176CE37B49E24BA9B7E019C5A0440_inline(/*hidden argument*/NULL);
		PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9 * L_8 = L_7;
		float L_9 = L_8->get_speed_4();
		float L_10 = __this->get_boostMultiplier_4();
		L_8->set_speed_4(((float)((float)L_9/(float)L_10)));
	}

IL_0062:
	{
		// }
		return;
	}
}
// System.Void Actions::Sonar(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Actions_Sonar_m9974B6F2FE0C34C6FADC1792DD56F5013D247130 (Actions_tF16D3C582B8D8DF5DD96B758A4D691F304BE44BB * __this, bool ___toggle0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m4B68F0A4E0441A036D7E39BC7E639536164196D9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mF39107B3A55F66C83EBCA798CBC93AC4C990DBD7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_mB38DBEFCD264B4682A190F8592464C0658F702B7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m3616D04A85546C8251A6C376656CDB5358D893F6_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m6206A8F48C7A98B9CFC23A13548CB215717573BD_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 1> __leave_targets;
	{
		// if(toggle == true) //activate
		bool L_0 = ___toggle0;
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		// if(sonarTimer == -1)
		float L_1 = __this->get_sonarTimer_12();
		if ((!(((float)L_1) == ((float)(-1.0f)))))
		{
			goto IL_0069;
		}
	}
	{
		// sonarTimer = 0;
		__this->set_sonarTimer_12((0.0f));
		// }
		return;
	}

IL_001c:
	{
		// sonarTimer = -1;
		__this->set_sonarTimer_12((-1.0f));
		// if(spawnedSonarIcons.Count > 0)
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_2 = __this->get_spawnedSonarIcons_14();
		int32_t L_3;
		L_3 = List_1_get_Count_m6206A8F48C7A98B9CFC23A13548CB215717573BD_inline(L_2, /*hidden argument*/List_1_get_Count_m6206A8F48C7A98B9CFC23A13548CB215717573BD_RuntimeMethod_var);
		if ((((int32_t)L_3) <= ((int32_t)0)))
		{
			goto IL_0069;
		}
	}
	{
		// foreach (GameObject go in spawnedSonarIcons)
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_4 = __this->get_spawnedSonarIcons_14();
		Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14  L_5;
		L_5 = List_1_GetEnumerator_m3616D04A85546C8251A6C376656CDB5358D893F6(L_4, /*hidden argument*/List_1_GetEnumerator_m3616D04A85546C8251A6C376656CDB5358D893F6_RuntimeMethod_var);
		V_0 = L_5;
	}

IL_0041:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0050;
		}

IL_0043:
		{
			// foreach (GameObject go in spawnedSonarIcons)
			GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_6;
			L_6 = Enumerator_get_Current_mB38DBEFCD264B4682A190F8592464C0658F702B7_inline((Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14 *)(&V_0), /*hidden argument*/Enumerator_get_Current_mB38DBEFCD264B4682A190F8592464C0658F702B7_RuntimeMethod_var);
			// go.SetActive(false);
			GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_6, (bool)0, /*hidden argument*/NULL);
		}

IL_0050:
		{
			// foreach (GameObject go in spawnedSonarIcons)
			bool L_7;
			L_7 = Enumerator_MoveNext_mF39107B3A55F66C83EBCA798CBC93AC4C990DBD7((Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14 *)(&V_0), /*hidden argument*/Enumerator_MoveNext_mF39107B3A55F66C83EBCA798CBC93AC4C990DBD7_RuntimeMethod_var);
			if (L_7)
			{
				goto IL_0043;
			}
		}

IL_0059:
		{
			IL2CPP_LEAVE(0x69, FINALLY_005b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_005b;
	}

FINALLY_005b:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m4B68F0A4E0441A036D7E39BC7E639536164196D9((Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14 *)(&V_0), /*hidden argument*/Enumerator_Dispose_m4B68F0A4E0441A036D7E39BC7E639536164196D9_RuntimeMethod_var);
		IL2CPP_END_FINALLY(91)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(91)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x69, IL_0069)
	}

IL_0069:
	{
		// }
		return;
	}
}
// System.Void Actions::Map(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Actions_Map_m76DFCD66B1AA4F860F1956778B07ADE092E771D9 (Actions_tF16D3C582B8D8DF5DD96B758A4D691F304BE44BB * __this, bool ___toggle0, const RuntimeMethod* method)
{
	{
		// if(toggle == true)
		bool L_0 = ___toggle0;
		if (!L_0)
		{
			goto IL_003d;
		}
	}
	{
		// PlayerController.Instance.enabled = false;  //turn off player movement
		PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9 * L_1;
		L_1 = PlayerController_get_Instance_m8C373614456176CE37B49E24BA9B7E019C5A0440_inline(/*hidden argument*/NULL);
		Behaviour_set_enabled_mDE415591B28853D1CD764C53CB499A2142247F32(L_1, (bool)0, /*hidden argument*/NULL);
		// MouseLook.Instance.enabled = false;  //turn off mouselook
		MouseLook_t1E1EE79B13022184F3EF9AC8FE8C6771053890DA * L_2;
		L_2 = MouseLook_get_Instance_m4F889A9C207EAE72011AF1BA9DA70046C5404386_inline(/*hidden argument*/NULL);
		Behaviour_set_enabled_mDE415591B28853D1CD764C53CB499A2142247F32(L_2, (bool)0, /*hidden argument*/NULL);
		// Interaction.Instance.enabled = false;  //turn off interaction
		Interaction_t41E997DF6BD70C3EAB8669FCC65D603292EF42A1 * L_3;
		L_3 = Interaction_get_Instance_m49A31B95D41F5321AA7036861F8330D6BB09A940_inline(/*hidden argument*/NULL);
		Behaviour_set_enabled_mDE415591B28853D1CD764C53CB499A2142247F32(L_3, (bool)0, /*hidden argument*/NULL);
		// Sonar(false);  //deactivate sonar
		Actions_Sonar_m9974B6F2FE0C34C6FADC1792DD56F5013D247130(__this, (bool)0, /*hidden argument*/NULL);
		// Boost(false);  //deactivate boost
		Actions_Boost_m5CB5C10BC32C2DE52E94274EAEF5F486716E49E4(__this, (bool)0, /*hidden argument*/NULL);
		// WorldMap.Instance.Activate();  //activate map
		WorldMap_t01A07DDC9EA386C88D5814CF18E0F37CAF05270C * L_4;
		L_4 = WorldMap_get_Instance_m119402AD39383BC71435F9F36C3219A2281289CA_inline(/*hidden argument*/NULL);
		WorldMap_Activate_m270FF00B4D2A645DB02BFEE99F12E78D75054808(L_4, /*hidden argument*/NULL);
		// }
		return;
	}

IL_003d:
	{
		// PlayerController.Instance.enabled = true;  //turn on player movement
		PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9 * L_5;
		L_5 = PlayerController_get_Instance_m8C373614456176CE37B49E24BA9B7E019C5A0440_inline(/*hidden argument*/NULL);
		Behaviour_set_enabled_mDE415591B28853D1CD764C53CB499A2142247F32(L_5, (bool)1, /*hidden argument*/NULL);
		// MouseLook.Instance.enabled = true;  //turn on mouselook
		MouseLook_t1E1EE79B13022184F3EF9AC8FE8C6771053890DA * L_6;
		L_6 = MouseLook_get_Instance_m4F889A9C207EAE72011AF1BA9DA70046C5404386_inline(/*hidden argument*/NULL);
		Behaviour_set_enabled_mDE415591B28853D1CD764C53CB499A2142247F32(L_6, (bool)1, /*hidden argument*/NULL);
		// Interaction.Instance.enabled = true;  //turn on interaction
		Interaction_t41E997DF6BD70C3EAB8669FCC65D603292EF42A1 * L_7;
		L_7 = Interaction_get_Instance_m49A31B95D41F5321AA7036861F8330D6BB09A940_inline(/*hidden argument*/NULL);
		Behaviour_set_enabled_mDE415591B28853D1CD764C53CB499A2142247F32(L_7, (bool)1, /*hidden argument*/NULL);
		// WorldMap.Instance.Deactivate();  //deactivate map
		WorldMap_t01A07DDC9EA386C88D5814CF18E0F37CAF05270C * L_8;
		L_8 = WorldMap_get_Instance_m119402AD39383BC71435F9F36C3219A2281289CA_inline(/*hidden argument*/NULL);
		WorldMap_Deactivate_m45F3C427C11E7AA6FC6C11B1346F94BA221F8641(L_8, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Actions::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Actions__ctor_mA309EB283343FDDB8A1FE0D25D6EB563A75F8AFD (Actions_tF16D3C582B8D8DF5DD96B758A4D691F304BE44BB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m859B0EE8491FDDEB1A3F7115D334B863E025BBC8_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public float boostMultiplier = 5;
		__this->set_boostMultiplier_4((5.0f));
		// public float boostTime = 3;
		__this->set_boostTime_5((3.0f));
		// public float sonarTime = 3;
		__this->set_sonarTime_6((3.0f));
		// public float sonarRadius = 15;
		__this->set_sonarRadius_7((15.0f));
		// public int iconLimit = 5;
		__this->set_iconLimit_8(5);
		// private float sonarTimer = -1;
		__this->set_sonarTimer_12((-1.0f));
		// private float boostTimer = -1;
		__this->set_boostTimer_13((-1.0f));
		// private List<GameObject> spawnedSonarIcons = new List<GameObject>();
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_0 = (List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 *)il2cpp_codegen_object_new(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5_il2cpp_TypeInfo_var);
		List_1__ctor_m859B0EE8491FDDEB1A3F7115D334B863E025BBC8(L_0, /*hidden argument*/List_1__ctor_m859B0EE8491FDDEB1A3F7115D334B863E025BBC8_RuntimeMethod_var);
		__this->set_spawnedSonarIcons_14(L_0);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String Fish::Inspect()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Fish_Inspect_mE6F49E168DC57D78B1CA0B6EE9F12AC1CBEC011F (Fish_t45F7EC0A7EE404E12CC412CF93723EA8799CA46C * __this, const RuntimeMethod* method)
{
	{
		// return tooltipText;
		String_t* L_0 = __this->get_tooltipText_4();
		return L_0;
	}
}
// System.Void Fish::Photograph(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Fish_Photograph_mCA93E8601A6B867BF98917CDD6806BF7B4F9B127 (Fish_t45F7EC0A7EE404E12CC412CF93723EA8799CA46C * __this, String_t* ___folderPath0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA15C898F015A9B0BC3268E8883CD03008A56DE26);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF7725728A0F11C8DECA5441C8D7FF63F5803B2C0);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  V_1;
	memset((&V_1), 0, sizeof(V_1));
	int32_t V_2 = 0;
	TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  V_3;
	memset((&V_3), 0, sizeof(V_3));
	{
		// if(folderPath != null)
		String_t* L_0 = ___folderPath0;
		if (!L_0)
		{
			goto IL_006e;
		}
	}
	{
		// string date = System.DateTime.Now.Day.ToString() + System.DateTime.Now.Month.ToString() + System.DateTime.Now.TimeOfDay.ToString().Replace(':', '-');
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_il2cpp_TypeInfo_var);
		DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  L_1;
		L_1 = DateTime_get_Now_mCAC695993D6E2C57B900C83BEF3F8B18BC4EBC2C(/*hidden argument*/NULL);
		V_1 = L_1;
		int32_t L_2;
		L_2 = DateTime_get_Day_m9D698CA2A7D1FBEE7BEC0A982A119239F513CBA8((DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405 *)(&V_1), /*hidden argument*/NULL);
		V_2 = L_2;
		String_t* L_3;
		L_3 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&V_2), /*hidden argument*/NULL);
		DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  L_4;
		L_4 = DateTime_get_Now_mCAC695993D6E2C57B900C83BEF3F8B18BC4EBC2C(/*hidden argument*/NULL);
		V_1 = L_4;
		int32_t L_5;
		L_5 = DateTime_get_Month_m46CC2AED3F24A91104919B1F6B5103DD1F8BBAE8((DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405 *)(&V_1), /*hidden argument*/NULL);
		V_2 = L_5;
		String_t* L_6;
		L_6 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&V_2), /*hidden argument*/NULL);
		DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  L_7;
		L_7 = DateTime_get_Now_mCAC695993D6E2C57B900C83BEF3F8B18BC4EBC2C(/*hidden argument*/NULL);
		V_1 = L_7;
		TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  L_8;
		L_8 = DateTime_get_TimeOfDay_mE6A177963C8D8AA8AA2830239F1C7B3D11AFC645((DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405 *)(&V_1), /*hidden argument*/NULL);
		V_3 = L_8;
		String_t* L_9;
		L_9 = TimeSpan_ToString_mB89DE4C354B8A29F627C22FA7EA31E94B1DA884B((TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203 *)(&V_3), /*hidden argument*/NULL);
		String_t* L_10;
		L_10 = String_Replace_mD912844A1141FE8771F55255C4A8D29C80C47618(L_9, ((int32_t)58), ((int32_t)45), /*hidden argument*/NULL);
		String_t* L_11;
		L_11 = String_Concat_m89EAB4C6A96B0E5C3F87300D6BE78D386B9EFC44(L_3, L_6, L_10, /*hidden argument*/NULL);
		V_0 = L_11;
		// ScreenCapture.CaptureScreenshot(folderPath + $"/UWW_{date}.png", 2);
		String_t* L_12 = ___folderPath0;
		String_t* L_13 = V_0;
		String_t* L_14;
		L_14 = String_Concat_m37A5BF26F8F8F1892D60D727303B23FB604FEE78(L_12, _stringLiteralF7725728A0F11C8DECA5441C8D7FF63F5803B2C0, L_13, _stringLiteralA15C898F015A9B0BC3268E8883CD03008A56DE26, /*hidden argument*/NULL);
		ScreenCapture_CaptureScreenshot_mB0DE010388F478603FB22C2DE37E2E01C5F49A28(L_14, 2, /*hidden argument*/NULL);
	}

IL_006e:
	{
		// if(GameManager.Instance.TargetFound == false && (Object)GameManager.Target == this) // if this is target fish, and target fish has notbeen found
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_15;
		L_15 = GameManager_get_Instance_m758C0D98BBDEF0C2DCA163A5D60B22DBB5B07232_inline(/*hidden argument*/NULL);
		bool L_16;
		L_16 = GameManager_get_TargetFound_mD646125D22C8183B642D2ECD48C64FE59D1D12AE_inline(L_15, /*hidden argument*/NULL);
		if (L_16)
		{
			goto IL_0097;
		}
	}
	{
		RuntimeObject* L_17;
		L_17 = GameManager_get_Target_m3ACB72AC913A603DF141C5D74A3F5C3B42D25B8D_inline(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_18;
		L_18 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(((Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)CastclassClass((RuntimeObject*)L_17, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var)), __this, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_0097;
		}
	}
	{
		// GameManager.Instance.TargetFound = true; // target has been found = true
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_19;
		L_19 = GameManager_get_Instance_m758C0D98BBDEF0C2DCA163A5D60B22DBB5B07232_inline(/*hidden argument*/NULL);
		GameManager_set_TargetFound_mA5DAB51223A80F7D092FF2967C48AC6EB5464CB2_inline(L_19, (bool)1, /*hidden argument*/NULL);
	}

IL_0097:
	{
		// }
		return;
	}
}
// System.Void Fish::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Fish__ctor_mEDE54A2E8EAB7C21444968366A1E9E12AC8D4A48 (Fish_t45F7EC0A7EE404E12CC412CF93723EA8799CA46C * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void FlockAgent::Initialize(FlockManager,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FlockAgent_Initialize_m8D1C04B1942FF5534A44DAC4526F9A8EF16CA1A6 (FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04 * __this, FlockManager_t241CFB1B2E6593247EFA52A9E2CB2A111400F1F8 * ___manager0, float ___initSpeed1, const RuntimeMethod* method)
{
	{
		// flockInstance = manager;
		FlockManager_t241CFB1B2E6593247EFA52A9E2CB2A111400F1F8 * L_0 = ___manager0;
		__this->set_flockInstance_12(L_0);
		// speed = initSpeed;
		float L_1 = ___initSpeed1;
		__this->set_speed_9(L_1);
		// }
		return;
	}
}
// System.Void FlockAgent::Move()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FlockAgent_Move_mE006DB28962C8459F5677FCE8F3B9DA4302F4C99 (FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04 * __this, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_1;
	memset((&V_1), 0, sizeof(V_1));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_2;
	memset((&V_2), 0, sizeof(V_2));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_3;
	memset((&V_3), 0, sizeof(V_3));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_4;
	memset((&V_4), 0, sizeof(V_4));
	{
		// FindNeighbors();
		FlockAgent_FindNeighbors_m084145F3ABC4617EA0A00724C318BEBD459B1BBE(__this, /*hidden argument*/NULL);
		// CalculateSpeed();
		FlockAgent_CalculateSpeed_m0CA1C3F5ACDD0DA9A4A121596281AA5AEC6646B1(__this, /*hidden argument*/NULL);
		// Vector3 cohesion = Cohese() * flockInstance.cohesion;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0;
		L_0 = FlockAgent_Cohese_mCBA014DB1CABC0C892246D2B8C48DC8B8CCC7D48(__this, /*hidden argument*/NULL);
		FlockManager_t241CFB1B2E6593247EFA52A9E2CB2A111400F1F8 * L_1 = __this->get_flockInstance_12();
		float L_2 = L_1->get_cohesion_13();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3;
		L_3 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_0, L_2, /*hidden argument*/NULL);
		// Vector3 separation = Separate() * flockInstance.separation;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4;
		L_4 = FlockAgent_Separate_mD2D03FDAC8EB6894C85FED42F19C5BD69AA1DBC2(__this, /*hidden argument*/NULL);
		FlockManager_t241CFB1B2E6593247EFA52A9E2CB2A111400F1F8 * L_5 = __this->get_flockInstance_12();
		float L_6 = L_5->get_separation_14();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7;
		L_7 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_4, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		// Vector3 alignment = Align() * flockInstance.alignment;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8;
		L_8 = FlockAgent_Align_m7890991F9B43B074A1420EB79A42EC79A7659EBB(__this, /*hidden argument*/NULL);
		FlockManager_t241CFB1B2E6593247EFA52A9E2CB2A111400F1F8 * L_9 = __this->get_flockInstance_12();
		float L_10 = L_9->get_alignment_15();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_11;
		L_11 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_8, L_10, /*hidden argument*/NULL);
		V_1 = L_11;
		// Vector3 bounds = Bounds() * flockInstance.bounds;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		L_12 = FlockAgent_Bounds_m4300D54C6A0AF7BF4FB2B650EB878EA72A996CD1(__this, /*hidden argument*/NULL);
		FlockManager_t241CFB1B2E6593247EFA52A9E2CB2A111400F1F8 * L_13 = __this->get_flockInstance_12();
		float L_14 = L_13->get_bounds_16();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_15;
		L_15 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_12, L_14, /*hidden argument*/NULL);
		V_2 = L_15;
		// Vector3 avoidance = ObstacleAvoidance() * flockInstance.avoidance;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_16;
		L_16 = FlockAgent_ObstacleAvoidance_m630F88E9A64AE156AA32C14C11538797BCB8BDDD(__this, /*hidden argument*/NULL);
		FlockManager_t241CFB1B2E6593247EFA52A9E2CB2A111400F1F8 * L_17 = __this->get_flockInstance_12();
		float L_18 = L_17->get_avoidance_17();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_19;
		L_19 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_16, L_18, /*hidden argument*/NULL);
		V_3 = L_19;
		// Vector3 motion = cohesion + separation + alignment + bounds + avoidance;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_20 = V_0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_21;
		L_21 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_3, L_20, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_22 = V_1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_23;
		L_23 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_21, L_22, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_24 = V_2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_25;
		L_25 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_23, L_24, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_26 = V_3;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_27;
		L_27 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_25, L_26, /*hidden argument*/NULL);
		V_4 = L_27;
		// motion = Vector3.SmoothDamp(transform.forward, motion, ref velocity, smoothDamp);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_28;
		L_28 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_29;
		L_29 = Transform_get_forward_mD850B9ECF892009E3485408DC0D375165B7BF053(L_28, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_30 = V_4;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_31 = __this->get_address_of_velocity_10();
		float L_32 = __this->get_smoothDamp_6();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_33;
		L_33 = Vector3_SmoothDamp_m4655944DBD5B44125F8F4B5A15E31DE94CB0F627(L_29, L_30, (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)L_31, L_32, /*hidden argument*/NULL);
		V_4 = L_33;
		// motion = motion.normalized * speed;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_34;
		L_34 = Vector3_get_normalized_m2FA6DF38F97BDA4CCBDAE12B9FE913A241DAC8D5((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_4), /*hidden argument*/NULL);
		float L_35 = __this->get_speed_9();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_36;
		L_36 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_34, L_35, /*hidden argument*/NULL);
		V_4 = L_36;
		// if(motion == Vector3.zero)
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_37 = V_4;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_38;
		L_38 = Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6(/*hidden argument*/NULL);
		bool L_39;
		L_39 = Vector3_op_Equality_m8A98C7F38641110A2F90445EF8E98ECE14B08296(L_37, L_38, /*hidden argument*/NULL);
		if (!L_39)
		{
			goto IL_00e7;
		}
	}
	{
		// motion = transform.forward;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_40;
		L_40 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_41;
		L_41 = Transform_get_forward_mD850B9ECF892009E3485408DC0D375165B7BF053(L_40, /*hidden argument*/NULL);
		V_4 = L_41;
	}

IL_00e7:
	{
		// transform.forward = motion; // redirect orientation
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_42;
		L_42 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_43 = V_4;
		Transform_set_forward_mAE46B156F55F2F90AB495B17F7C20BF59A5D7D4D(L_42, L_43, /*hidden argument*/NULL);
		// transform.position += motion * Time.deltaTime; // move position
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_44;
		L_44 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_45 = L_44;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_46;
		L_46 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_45, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_47 = V_4;
		float L_48;
		L_48 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_49;
		L_49 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_47, L_48, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_50;
		L_50 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_46, L_49, /*hidden argument*/NULL);
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_45, L_50, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void FlockAgent::FindNeighbors()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FlockAgent_FindNeighbors_m084145F3ABC4617EA0A00724C318BEBD459B1BBE (FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mF1329DCD6488AD8E4E570B756253384836CC2E82_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Clear_m2BD50CE116D1680EF02FA4859D6802F72B45324F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04 * V_1 = NULL;
	float G_B4_0 = 0.0f;
	float G_B3_0 = 0.0f;
	float G_B6_0 = 0.0f;
	float G_B5_0 = 0.0f;
	{
		// cohesionNeighbours.Clear();
		List_1_t50CCFC552038CBD55842D81AC8F7EE04848C7255 * L_0 = __this->get_cohesionNeighbours_13();
		List_1_Clear_m2BD50CE116D1680EF02FA4859D6802F72B45324F(L_0, /*hidden argument*/List_1_Clear_m2BD50CE116D1680EF02FA4859D6802F72B45324F_RuntimeMethod_var);
		// seperationNeighbours.Clear();
		List_1_t50CCFC552038CBD55842D81AC8F7EE04848C7255 * L_1 = __this->get_seperationNeighbours_14();
		List_1_Clear_m2BD50CE116D1680EF02FA4859D6802F72B45324F(L_1, /*hidden argument*/List_1_Clear_m2BD50CE116D1680EF02FA4859D6802F72B45324F_RuntimeMethod_var);
		// alignmentNeighbours.Clear();
		List_1_t50CCFC552038CBD55842D81AC8F7EE04848C7255 * L_2 = __this->get_alignmentNeighbours_15();
		List_1_Clear_m2BD50CE116D1680EF02FA4859D6802F72B45324F(L_2, /*hidden argument*/List_1_Clear_m2BD50CE116D1680EF02FA4859D6802F72B45324F_RuntimeMethod_var);
		// for(int i = 0; i < flockInstance.SpawnedAgents.Length; i++)
		V_0 = 0;
		goto IL_00d7;
	}

IL_0028:
	{
		// FlockAgent agent = flockInstance.SpawnedAgents[i];
		FlockManager_t241CFB1B2E6593247EFA52A9E2CB2A111400F1F8 * L_3 = __this->get_flockInstance_12();
		FlockAgentU5BU5D_t012E3C58876A70268E6BFEDBF651E5B8D23AAF84* L_4;
		L_4 = FlockManager_get_SpawnedAgents_m050E5346A4490CAAD7EB7B9F8A7E6399405B4C7A_inline(L_3, /*hidden argument*/NULL);
		int32_t L_5 = V_0;
		int32_t L_6 = L_5;
		FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04 * L_7 = (L_4)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_6));
		V_1 = L_7;
		// if(agent != this)
		FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04 * L_8 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_9;
		L_9 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_8, __this, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_00d3;
		}
	}
	{
		// float distanceSqr = Vector3.SqrMagnitude(agent.transform.position - transform.position);
		FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04 * L_10 = V_1;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_11;
		L_11 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_10, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		L_12 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_11, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_13;
		L_13 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_14;
		L_14 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_13, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_15;
		L_15 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_12, L_14, /*hidden argument*/NULL);
		float L_16;
		L_16 = Vector3_SqrMagnitude_m286141DAE9BF2AD40B0FA58C1D97B5773327A5B9_inline(L_15, /*hidden argument*/NULL);
		// if(distanceSqr <= flockInstance.cohesionDistance * flockInstance.cohesionDistance)
		float L_17 = L_16;
		FlockManager_t241CFB1B2E6593247EFA52A9E2CB2A111400F1F8 * L_18 = __this->get_flockInstance_12();
		float L_19 = L_18->get_cohesionDistance_8();
		FlockManager_t241CFB1B2E6593247EFA52A9E2CB2A111400F1F8 * L_20 = __this->get_flockInstance_12();
		float L_21 = L_20->get_cohesionDistance_8();
		G_B3_0 = L_17;
		if ((!(((float)L_17) <= ((float)((float)il2cpp_codegen_multiply((float)L_19, (float)L_21))))))
		{
			G_B4_0 = L_17;
			goto IL_0088;
		}
	}
	{
		// cohesionNeighbours.Add(agent);
		List_1_t50CCFC552038CBD55842D81AC8F7EE04848C7255 * L_22 = __this->get_cohesionNeighbours_13();
		FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04 * L_23 = V_1;
		List_1_Add_mF1329DCD6488AD8E4E570B756253384836CC2E82(L_22, L_23, /*hidden argument*/List_1_Add_mF1329DCD6488AD8E4E570B756253384836CC2E82_RuntimeMethod_var);
		G_B4_0 = G_B3_0;
	}

IL_0088:
	{
		// if (distanceSqr <= flockInstance.cohesionDistance * flockInstance.cohesionDistance)
		float L_24 = G_B4_0;
		FlockManager_t241CFB1B2E6593247EFA52A9E2CB2A111400F1F8 * L_25 = __this->get_flockInstance_12();
		float L_26 = L_25->get_cohesionDistance_8();
		FlockManager_t241CFB1B2E6593247EFA52A9E2CB2A111400F1F8 * L_27 = __this->get_flockInstance_12();
		float L_28 = L_27->get_cohesionDistance_8();
		G_B5_0 = L_24;
		if ((!(((float)L_24) <= ((float)((float)il2cpp_codegen_multiply((float)L_26, (float)L_28))))))
		{
			G_B6_0 = L_24;
			goto IL_00ae;
		}
	}
	{
		// seperationNeighbours.Add(agent);
		List_1_t50CCFC552038CBD55842D81AC8F7EE04848C7255 * L_29 = __this->get_seperationNeighbours_14();
		FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04 * L_30 = V_1;
		List_1_Add_mF1329DCD6488AD8E4E570B756253384836CC2E82(L_29, L_30, /*hidden argument*/List_1_Add_mF1329DCD6488AD8E4E570B756253384836CC2E82_RuntimeMethod_var);
		G_B6_0 = G_B5_0;
	}

IL_00ae:
	{
		// if (distanceSqr <= flockInstance.cohesionDistance * flockInstance.cohesionDistance)
		FlockManager_t241CFB1B2E6593247EFA52A9E2CB2A111400F1F8 * L_31 = __this->get_flockInstance_12();
		float L_32 = L_31->get_cohesionDistance_8();
		FlockManager_t241CFB1B2E6593247EFA52A9E2CB2A111400F1F8 * L_33 = __this->get_flockInstance_12();
		float L_34 = L_33->get_cohesionDistance_8();
		if ((!(((float)G_B6_0) <= ((float)((float)il2cpp_codegen_multiply((float)L_32, (float)L_34))))))
		{
			goto IL_00d3;
		}
	}
	{
		// alignmentNeighbours.Add(agent);
		List_1_t50CCFC552038CBD55842D81AC8F7EE04848C7255 * L_35 = __this->get_alignmentNeighbours_15();
		FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04 * L_36 = V_1;
		List_1_Add_mF1329DCD6488AD8E4E570B756253384836CC2E82(L_35, L_36, /*hidden argument*/List_1_Add_mF1329DCD6488AD8E4E570B756253384836CC2E82_RuntimeMethod_var);
	}

IL_00d3:
	{
		// for(int i = 0; i < flockInstance.SpawnedAgents.Length; i++)
		int32_t L_37 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_37, (int32_t)1));
	}

IL_00d7:
	{
		// for(int i = 0; i < flockInstance.SpawnedAgents.Length; i++)
		int32_t L_38 = V_0;
		FlockManager_t241CFB1B2E6593247EFA52A9E2CB2A111400F1F8 * L_39 = __this->get_flockInstance_12();
		FlockAgentU5BU5D_t012E3C58876A70268E6BFEDBF651E5B8D23AAF84* L_40;
		L_40 = FlockManager_get_SpawnedAgents_m050E5346A4490CAAD7EB7B9F8A7E6399405B4C7A_inline(L_39, /*hidden argument*/NULL);
		if ((((int32_t)L_38) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_40)->max_length))))))
		{
			goto IL_0028;
		}
	}
	{
		// }
		return;
	}
}
// System.Void FlockAgent::CalculateSpeed()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FlockAgent_CalculateSpeed_m0CA1C3F5ACDD0DA9A4A121596281AA5AEC6646B1 (FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mB64A4B531A3EB080CDD6EBBD4D31F9BCB6730F76_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m8036333D84A83C0B1348EFFA8C2CA6EBCB99E363_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// if(cohesionNeighbours.Count == 0)
		List_1_t50CCFC552038CBD55842D81AC8F7EE04848C7255 * L_0 = __this->get_cohesionNeighbours_13();
		int32_t L_1;
		L_1 = List_1_get_Count_mB64A4B531A3EB080CDD6EBBD4D31F9BCB6730F76_inline(L_0, /*hidden argument*/List_1_get_Count_mB64A4B531A3EB080CDD6EBBD4D31F9BCB6730F76_RuntimeMethod_var);
		if (L_1)
		{
			goto IL_000e;
		}
	}
	{
		// return;
		return;
	}

IL_000e:
	{
		// speed = 0;
		__this->set_speed_9((0.0f));
		// for(int i = 0; i < cohesionNeighbours.Count; i++)
		V_0 = 0;
		goto IL_003f;
	}

IL_001d:
	{
		// speed += cohesionNeighbours[i].speed;
		float L_2 = __this->get_speed_9();
		List_1_t50CCFC552038CBD55842D81AC8F7EE04848C7255 * L_3 = __this->get_cohesionNeighbours_13();
		int32_t L_4 = V_0;
		FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04 * L_5;
		L_5 = List_1_get_Item_m8036333D84A83C0B1348EFFA8C2CA6EBCB99E363_inline(L_3, L_4, /*hidden argument*/List_1_get_Item_m8036333D84A83C0B1348EFFA8C2CA6EBCB99E363_RuntimeMethod_var);
		float L_6 = L_5->get_speed_9();
		__this->set_speed_9(((float)il2cpp_codegen_add((float)L_2, (float)L_6)));
		// for(int i = 0; i < cohesionNeighbours.Count; i++)
		int32_t L_7 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)1));
	}

IL_003f:
	{
		// for(int i = 0; i < cohesionNeighbours.Count; i++)
		int32_t L_8 = V_0;
		List_1_t50CCFC552038CBD55842D81AC8F7EE04848C7255 * L_9 = __this->get_cohesionNeighbours_13();
		int32_t L_10;
		L_10 = List_1_get_Count_mB64A4B531A3EB080CDD6EBBD4D31F9BCB6730F76_inline(L_9, /*hidden argument*/List_1_get_Count_mB64A4B531A3EB080CDD6EBBD4D31F9BCB6730F76_RuntimeMethod_var);
		if ((((int32_t)L_8) < ((int32_t)L_10)))
		{
			goto IL_001d;
		}
	}
	{
		// speed /= cohesionNeighbours.Count;
		float L_11 = __this->get_speed_9();
		List_1_t50CCFC552038CBD55842D81AC8F7EE04848C7255 * L_12 = __this->get_cohesionNeighbours_13();
		int32_t L_13;
		L_13 = List_1_get_Count_mB64A4B531A3EB080CDD6EBBD4D31F9BCB6730F76_inline(L_12, /*hidden argument*/List_1_get_Count_mB64A4B531A3EB080CDD6EBBD4D31F9BCB6730F76_RuntimeMethod_var);
		__this->set_speed_9(((float)((float)L_11/(float)((float)((float)L_13)))));
		// speed = Mathf.Clamp(speed, flockInstance.speed.x, flockInstance.speed.y);
		float L_14 = __this->get_speed_9();
		FlockManager_t241CFB1B2E6593247EFA52A9E2CB2A111400F1F8 * L_15 = __this->get_flockInstance_12();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_16 = L_15->get_address_of_speed_7();
		float L_17 = L_16->get_x_0();
		FlockManager_t241CFB1B2E6593247EFA52A9E2CB2A111400F1F8 * L_18 = __this->get_flockInstance_12();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_19 = L_18->get_address_of_speed_7();
		float L_20 = L_19->get_y_1();
		float L_21;
		L_21 = Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87(L_14, L_17, L_20, /*hidden argument*/NULL);
		__this->set_speed_9(L_21);
		// }
		return;
	}
}
// UnityEngine.Vector3 FlockAgent::Cohese()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  FlockAgent_Cohese_mCBA014DB1CABC0C892246D2B8C48DC8B8CCC7D48 (FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mB64A4B531A3EB080CDD6EBBD4D31F9BCB6730F76_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m8036333D84A83C0B1348EFFA8C2CA6EBCB99E363_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		// Vector3 vector = Vector3.zero;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0;
		L_0 = Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6(/*hidden argument*/NULL);
		V_0 = L_0;
		// int neighboursInView = 0;
		V_1 = 0;
		// if(cohesionNeighbours.Count == 0)
		List_1_t50CCFC552038CBD55842D81AC8F7EE04848C7255 * L_1 = __this->get_cohesionNeighbours_13();
		int32_t L_2;
		L_2 = List_1_get_Count_mB64A4B531A3EB080CDD6EBBD4D31F9BCB6730F76_inline(L_1, /*hidden argument*/List_1_get_Count_mB64A4B531A3EB080CDD6EBBD4D31F9BCB6730F76_RuntimeMethod_var);
		if (L_2)
		{
			goto IL_0017;
		}
	}
	{
		// return vector;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3 = V_0;
		return L_3;
	}

IL_0017:
	{
		// for(int i = 0; i < cohesionNeighbours.Count; i++)
		V_2 = 0;
		goto IL_005e;
	}

IL_001b:
	{
		// if(VectorInView(cohesionNeighbours[i].transform.position) == true)
		List_1_t50CCFC552038CBD55842D81AC8F7EE04848C7255 * L_4 = __this->get_cohesionNeighbours_13();
		int32_t L_5 = V_2;
		FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04 * L_6;
		L_6 = List_1_get_Item_m8036333D84A83C0B1348EFFA8C2CA6EBCB99E363_inline(L_4, L_5, /*hidden argument*/List_1_get_Item_m8036333D84A83C0B1348EFFA8C2CA6EBCB99E363_RuntimeMethod_var);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_7;
		L_7 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_6, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8;
		L_8 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_7, /*hidden argument*/NULL);
		bool L_9;
		L_9 = FlockAgent_VectorInView_m40DF4A6C365ADD655A97BB18413186B022867CE5(__this, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_005a;
		}
	}
	{
		// neighboursInView++;
		int32_t L_10 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1));
		// vector += cohesionNeighbours[i].transform.position;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_11 = V_0;
		List_1_t50CCFC552038CBD55842D81AC8F7EE04848C7255 * L_12 = __this->get_cohesionNeighbours_13();
		int32_t L_13 = V_2;
		FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04 * L_14;
		L_14 = List_1_get_Item_m8036333D84A83C0B1348EFFA8C2CA6EBCB99E363_inline(L_12, L_13, /*hidden argument*/List_1_get_Item_m8036333D84A83C0B1348EFFA8C2CA6EBCB99E363_RuntimeMethod_var);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_15;
		L_15 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_14, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_16;
		L_16 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_15, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_17;
		L_17 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_11, L_16, /*hidden argument*/NULL);
		V_0 = L_17;
	}

IL_005a:
	{
		// for(int i = 0; i < cohesionNeighbours.Count; i++)
		int32_t L_18 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_18, (int32_t)1));
	}

IL_005e:
	{
		// for(int i = 0; i < cohesionNeighbours.Count; i++)
		int32_t L_19 = V_2;
		List_1_t50CCFC552038CBD55842D81AC8F7EE04848C7255 * L_20 = __this->get_cohesionNeighbours_13();
		int32_t L_21;
		L_21 = List_1_get_Count_mB64A4B531A3EB080CDD6EBBD4D31F9BCB6730F76_inline(L_20, /*hidden argument*/List_1_get_Count_mB64A4B531A3EB080CDD6EBBD4D31F9BCB6730F76_RuntimeMethod_var);
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_001b;
		}
	}
	{
		// if(neighboursInView == 0)
		int32_t L_22 = V_1;
		if (L_22)
		{
			goto IL_0071;
		}
	}
	{
		// return vector;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_23 = V_0;
		return L_23;
	}

IL_0071:
	{
		// vector /= neighboursInView;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_24 = V_0;
		int32_t L_25 = V_1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_26;
		L_26 = Vector3_op_Division_mE5ACBFB168FED529587457A83BA98B7DB32E2A05_inline(L_24, ((float)((float)L_25)), /*hidden argument*/NULL);
		V_0 = L_26;
		// vector -= transform.position;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_27 = V_0;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_28;
		L_28 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_29;
		L_29 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_28, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_30;
		L_30 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_27, L_29, /*hidden argument*/NULL);
		V_0 = L_30;
		// return vector.normalized;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_31;
		L_31 = Vector3_get_normalized_m2FA6DF38F97BDA4CCBDAE12B9FE913A241DAC8D5((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_0), /*hidden argument*/NULL);
		return L_31;
	}
}
// UnityEngine.Vector3 FlockAgent::Separate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  FlockAgent_Separate_mD2D03FDAC8EB6894C85FED42F19C5BD69AA1DBC2 (FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mB64A4B531A3EB080CDD6EBBD4D31F9BCB6730F76_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m8036333D84A83C0B1348EFFA8C2CA6EBCB99E363_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		// Vector3 vector = transform.forward;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0;
		L_0 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1;
		L_1 = Transform_get_forward_mD850B9ECF892009E3485408DC0D375165B7BF053(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		// if(seperationNeighbours.Count == 0)
		List_1_t50CCFC552038CBD55842D81AC8F7EE04848C7255 * L_2 = __this->get_seperationNeighbours_14();
		int32_t L_3;
		L_3 = List_1_get_Count_mB64A4B531A3EB080CDD6EBBD4D31F9BCB6730F76_inline(L_2, /*hidden argument*/List_1_get_Count_mB64A4B531A3EB080CDD6EBBD4D31F9BCB6730F76_RuntimeMethod_var);
		if (L_3)
		{
			goto IL_001b;
		}
	}
	{
		// return vector;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = V_0;
		return L_4;
	}

IL_001b:
	{
		// int neighboursinview = 0;
		V_1 = 0;
		// for(int i = 0; i < seperationNeighbours.Count; i++)
		V_2 = 0;
		goto IL_0074;
	}

IL_0021:
	{
		// if(VectorInView(seperationNeighbours[i].transform.position) == true)
		List_1_t50CCFC552038CBD55842D81AC8F7EE04848C7255 * L_5 = __this->get_seperationNeighbours_14();
		int32_t L_6 = V_2;
		FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04 * L_7;
		L_7 = List_1_get_Item_m8036333D84A83C0B1348EFFA8C2CA6EBCB99E363_inline(L_5, L_6, /*hidden argument*/List_1_get_Item_m8036333D84A83C0B1348EFFA8C2CA6EBCB99E363_RuntimeMethod_var);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_8;
		L_8 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_7, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9;
		L_9 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_8, /*hidden argument*/NULL);
		bool L_10;
		L_10 = FlockAgent_VectorInView_m40DF4A6C365ADD655A97BB18413186B022867CE5(__this, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0070;
		}
	}
	{
		// neighboursinview ++;
		int32_t L_11 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_11, (int32_t)1));
		// vector += transform.position - seperationNeighbours[i].transform.position;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12 = V_0;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_13;
		L_13 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_14;
		L_14 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_13, /*hidden argument*/NULL);
		List_1_t50CCFC552038CBD55842D81AC8F7EE04848C7255 * L_15 = __this->get_seperationNeighbours_14();
		int32_t L_16 = V_2;
		FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04 * L_17;
		L_17 = List_1_get_Item_m8036333D84A83C0B1348EFFA8C2CA6EBCB99E363_inline(L_15, L_16, /*hidden argument*/List_1_get_Item_m8036333D84A83C0B1348EFFA8C2CA6EBCB99E363_RuntimeMethod_var);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_18;
		L_18 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_17, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_19;
		L_19 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_18, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_20;
		L_20 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_14, L_19, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_21;
		L_21 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_12, L_20, /*hidden argument*/NULL);
		V_0 = L_21;
	}

IL_0070:
	{
		// for(int i = 0; i < seperationNeighbours.Count; i++)
		int32_t L_22 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_22, (int32_t)1));
	}

IL_0074:
	{
		// for(int i = 0; i < seperationNeighbours.Count; i++)
		int32_t L_23 = V_2;
		List_1_t50CCFC552038CBD55842D81AC8F7EE04848C7255 * L_24 = __this->get_seperationNeighbours_14();
		int32_t L_25;
		L_25 = List_1_get_Count_mB64A4B531A3EB080CDD6EBBD4D31F9BCB6730F76_inline(L_24, /*hidden argument*/List_1_get_Count_mB64A4B531A3EB080CDD6EBBD4D31F9BCB6730F76_RuntimeMethod_var);
		if ((((int32_t)L_23) < ((int32_t)L_25)))
		{
			goto IL_0021;
		}
	}
	{
		// vector /= neighboursinview;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_26 = V_0;
		int32_t L_27 = V_1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_28;
		L_28 = Vector3_op_Division_mE5ACBFB168FED529587457A83BA98B7DB32E2A05_inline(L_26, ((float)((float)L_27)), /*hidden argument*/NULL);
		V_0 = L_28;
		// return vector.normalized;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_29;
		L_29 = Vector3_get_normalized_m2FA6DF38F97BDA4CCBDAE12B9FE913A241DAC8D5((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_0), /*hidden argument*/NULL);
		return L_29;
	}
}
// UnityEngine.Vector3 FlockAgent::Align()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  FlockAgent_Align_m7890991F9B43B074A1420EB79A42EC79A7659EBB (FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mB64A4B531A3EB080CDD6EBBD4D31F9BCB6730F76_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m8036333D84A83C0B1348EFFA8C2CA6EBCB99E363_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		// Vector3 vector = transform.forward;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0;
		L_0 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1;
		L_1 = Transform_get_forward_mD850B9ECF892009E3485408DC0D375165B7BF053(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		// if(alignmentNeighbours.Count == 0)
		List_1_t50CCFC552038CBD55842D81AC8F7EE04848C7255 * L_2 = __this->get_alignmentNeighbours_15();
		int32_t L_3;
		L_3 = List_1_get_Count_mB64A4B531A3EB080CDD6EBBD4D31F9BCB6730F76_inline(L_2, /*hidden argument*/List_1_get_Count_mB64A4B531A3EB080CDD6EBBD4D31F9BCB6730F76_RuntimeMethod_var);
		if (L_3)
		{
			goto IL_001b;
		}
	}
	{
		// return vector;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = V_0;
		return L_4;
	}

IL_001b:
	{
		// int neighboursinview = 0;
		V_1 = 0;
		// for(int i = 0; i < alignmentNeighbours.Count; i++)
		V_2 = 0;
		goto IL_0064;
	}

IL_0021:
	{
		// if(VectorInView(alignmentNeighbours[i].transform.position) == true)
		List_1_t50CCFC552038CBD55842D81AC8F7EE04848C7255 * L_5 = __this->get_alignmentNeighbours_15();
		int32_t L_6 = V_2;
		FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04 * L_7;
		L_7 = List_1_get_Item_m8036333D84A83C0B1348EFFA8C2CA6EBCB99E363_inline(L_5, L_6, /*hidden argument*/List_1_get_Item_m8036333D84A83C0B1348EFFA8C2CA6EBCB99E363_RuntimeMethod_var);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_8;
		L_8 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_7, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9;
		L_9 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_8, /*hidden argument*/NULL);
		bool L_10;
		L_10 = FlockAgent_VectorInView_m40DF4A6C365ADD655A97BB18413186B022867CE5(__this, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0060;
		}
	}
	{
		// neighboursinview ++;
		int32_t L_11 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_11, (int32_t)1));
		// vector += alignmentNeighbours[i].transform.position;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12 = V_0;
		List_1_t50CCFC552038CBD55842D81AC8F7EE04848C7255 * L_13 = __this->get_alignmentNeighbours_15();
		int32_t L_14 = V_2;
		FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04 * L_15;
		L_15 = List_1_get_Item_m8036333D84A83C0B1348EFFA8C2CA6EBCB99E363_inline(L_13, L_14, /*hidden argument*/List_1_get_Item_m8036333D84A83C0B1348EFFA8C2CA6EBCB99E363_RuntimeMethod_var);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_16;
		L_16 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_15, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_17;
		L_17 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_16, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_18;
		L_18 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_12, L_17, /*hidden argument*/NULL);
		V_0 = L_18;
	}

IL_0060:
	{
		// for(int i = 0; i < alignmentNeighbours.Count; i++)
		int32_t L_19 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_19, (int32_t)1));
	}

IL_0064:
	{
		// for(int i = 0; i < alignmentNeighbours.Count; i++)
		int32_t L_20 = V_2;
		List_1_t50CCFC552038CBD55842D81AC8F7EE04848C7255 * L_21 = __this->get_alignmentNeighbours_15();
		int32_t L_22;
		L_22 = List_1_get_Count_mB64A4B531A3EB080CDD6EBBD4D31F9BCB6730F76_inline(L_21, /*hidden argument*/List_1_get_Count_mB64A4B531A3EB080CDD6EBBD4D31F9BCB6730F76_RuntimeMethod_var);
		if ((((int32_t)L_20) < ((int32_t)L_22)))
		{
			goto IL_0021;
		}
	}
	{
		// vector /= neighboursinview;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_23 = V_0;
		int32_t L_24 = V_1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_25;
		L_25 = Vector3_op_Division_mE5ACBFB168FED529587457A83BA98B7DB32E2A05_inline(L_23, ((float)((float)L_24)), /*hidden argument*/NULL);
		V_0 = L_25;
		// return vector.normalized;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_26;
		L_26 = Vector3_get_normalized_m2FA6DF38F97BDA4CCBDAE12B9FE913A241DAC8D5((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_0), /*hidden argument*/NULL);
		return L_26;
	}
}
// UnityEngine.Vector3 FlockAgent::Bounds()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  FlockAgent_Bounds_m4300D54C6A0AF7BF4FB2B650EB878EA72A996CD1 (FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04 * __this, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// Vector3 centerOffset = flockInstance.transform.position - transform.position;
		FlockManager_t241CFB1B2E6593247EFA52A9E2CB2A111400F1F8 * L_0 = __this->get_flockInstance_12();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1;
		L_1 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_0, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		L_2 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_1, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_3;
		L_3 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4;
		L_4 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_3, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5;
		L_5 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_2, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		// bool withinBounds = (centerOffset.magnitude >= flockInstance.boundsDistance * 0.9);
		float L_6;
		L_6 = Vector3_get_magnitude_mDDD40612220D8104E77E993E18A101A69A944991((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_0), /*hidden argument*/NULL);
		FlockManager_t241CFB1B2E6593247EFA52A9E2CB2A111400F1F8 * L_7 = __this->get_flockInstance_12();
		float L_8 = L_7->get_boundsDistance_11();
		// if(withinBounds == true)
		if (!((((int32_t)((!(((double)((double)((double)L_6))) >= ((double)((double)il2cpp_codegen_multiply((double)((double)((double)L_8)), (double)(0.90000000000000002))))))? 1 : 0)) == ((int32_t)0))? 1 : 0))
		{
			goto IL_004e;
		}
	}
	{
		// return centerOffset.normalized;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9;
		L_9 = Vector3_get_normalized_m2FA6DF38F97BDA4CCBDAE12B9FE913A241DAC8D5((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_0), /*hidden argument*/NULL);
		return L_9;
	}

IL_004e:
	{
		// return Vector3.zero;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10;
		L_10 = Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6(/*hidden argument*/NULL);
		return L_10;
	}
}
// UnityEngine.Vector3 FlockAgent::ObstacleAvoidance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  FlockAgent_ObstacleAvoidance_m630F88E9A64AE156AA32C14C11538797BCB8BDDD (FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04 * __this, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		// Vector3 vector = Vector3.zero;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0;
		L_0 = Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6(/*hidden argument*/NULL);
		V_0 = L_0;
		// if(Physics.Raycast(transform.position, transform.forward, out RaycastHit hit, flockInstance.obstacleDistance, obstacleMask) == true)
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1;
		L_1 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		L_2 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_1, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_3;
		L_3 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4;
		L_4 = Transform_get_forward_mD850B9ECF892009E3485408DC0D375165B7BF053(L_3, /*hidden argument*/NULL);
		FlockManager_t241CFB1B2E6593247EFA52A9E2CB2A111400F1F8 * L_5 = __this->get_flockInstance_12();
		float L_6 = L_5->get_obstacleDistance_12();
		LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  L_7 = __this->get_obstacleMask_7();
		int32_t L_8;
		L_8 = LayerMask_op_Implicit_mD89E9970822613D8D19B2EBCA36C79391C287BE0(L_7, /*hidden argument*/NULL);
		bool L_9;
		L_9 = Physics_Raycast_mCBD5F7D498C246713EDDBB446E97205DA206C49C(L_2, L_4, (RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)(&V_1), L_6, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0044;
		}
	}
	{
		// vector = AvoidObstacle();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10;
		L_10 = FlockAgent_AvoidObstacle_m9274B9DABEC71E0C1CFCABC33E63019C2BE14B6B(__this, /*hidden argument*/NULL);
		V_0 = L_10;
		// }
		goto IL_004f;
	}

IL_0044:
	{
		// currentAvoidance = Vector3.zero;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_11;
		L_11 = Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6(/*hidden argument*/NULL);
		__this->set_currentAvoidance_11(L_11);
	}

IL_004f:
	{
		// return vector;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12 = V_0;
		return L_12;
	}
}
// UnityEngine.Vector3 FlockAgent::AvoidObstacle()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  FlockAgent_AvoidObstacle_m9274B9DABEC71E0C1CFCABC33E63019C2BE14B6B (FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_1;
	memset((&V_1), 0, sizeof(V_1));
	RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89  V_2;
	memset((&V_2), 0, sizeof(V_2));
	int32_t V_3 = 0;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_4;
	memset((&V_4), 0, sizeof(V_4));
	RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89  V_5;
	memset((&V_5), 0, sizeof(V_5));
	float V_6 = 0.0f;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_7;
	memset((&V_7), 0, sizeof(V_7));
	{
		// if(currentAvoidance != Vector3.zero)
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = __this->get_currentAvoidance_11();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1;
		L_1 = Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6(/*hidden argument*/NULL);
		bool L_2;
		L_2 = Vector3_op_Inequality_m15190A795B416EB699E69E6190DE6F1C1F208710(L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_004e;
		}
	}
	{
		// if(Physics.Raycast(transform.position, transform.forward, out RaycastHit hit, flockInstance.obstacleDistance, obstacleMask) == false)
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_3;
		L_3 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4;
		L_4 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_3, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_5;
		L_5 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6;
		L_6 = Transform_get_forward_mD850B9ECF892009E3485408DC0D375165B7BF053(L_5, /*hidden argument*/NULL);
		FlockManager_t241CFB1B2E6593247EFA52A9E2CB2A111400F1F8 * L_7 = __this->get_flockInstance_12();
		float L_8 = L_7->get_obstacleDistance_12();
		LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  L_9 = __this->get_obstacleMask_7();
		int32_t L_10;
		L_10 = LayerMask_op_Implicit_mD89E9970822613D8D19B2EBCA36C79391C287BE0(L_9, /*hidden argument*/NULL);
		bool L_11;
		L_11 = Physics_Raycast_mCBD5F7D498C246713EDDBB446E97205DA206C49C(L_4, L_6, (RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)(&V_2), L_8, L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_004e;
		}
	}
	{
		// return currentAvoidance;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12 = __this->get_currentAvoidance_11();
		return L_12;
	}

IL_004e:
	{
		// float maxDist = int.MinValue;
		V_0 = (-2.14748365E+09f);
		// Vector3 dir = Vector3.zero;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13;
		L_13 = Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6(/*hidden argument*/NULL);
		V_1 = L_13;
		// for(int i = 0; i < collisionRays.Length; i++)
		V_3 = 0;
		goto IL_00f6;
	}

IL_0061:
	{
		// Vector3 currentDir = transform.TransformDirection(collisionRays[i].normalized);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_14;
		L_14 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_15 = __this->get_collisionRays_8();
		int32_t L_16 = V_3;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_17;
		L_17 = Vector3_get_normalized_m2FA6DF38F97BDA4CCBDAE12B9FE913A241DAC8D5((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)((L_15)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_16))), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_18;
		L_18 = Transform_TransformDirection_m6B5E3F0A7C6323159DEC6D9BC035FB53ADD96E91(L_14, L_17, /*hidden argument*/NULL);
		V_4 = L_18;
		// if(Physics.Raycast(transform.position, currentDir, out RaycastHit hit, flockInstance.obstacleDistance, obstacleMask) == true)
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_19;
		L_19 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_20;
		L_20 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_19, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_21 = V_4;
		FlockManager_t241CFB1B2E6593247EFA52A9E2CB2A111400F1F8 * L_22 = __this->get_flockInstance_12();
		float L_23 = L_22->get_obstacleDistance_12();
		LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  L_24 = __this->get_obstacleMask_7();
		int32_t L_25;
		L_25 = LayerMask_op_Implicit_mD89E9970822613D8D19B2EBCA36C79391C287BE0(L_24, /*hidden argument*/NULL);
		bool L_26;
		L_26 = Physics_Raycast_mCBD5F7D498C246713EDDBB446E97205DA206C49C(L_20, L_21, (RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)(&V_5), L_23, L_25, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_00da;
		}
	}
	{
		// float sqrDist = (hit.point - transform.position).sqrMagnitude;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_27;
		L_27 = RaycastHit_get_point_m32F7282CBB2E13393A33BAD046BDA218E48DD21E((RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)(&V_5), /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_28;
		L_28 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_29;
		L_29 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_28, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_30;
		L_30 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_27, L_29, /*hidden argument*/NULL);
		V_7 = L_30;
		float L_31;
		L_31 = Vector3_get_sqrMagnitude_mC567EE6DF411501A8FE1F23A0038862630B88249((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_7), /*hidden argument*/NULL);
		V_6 = L_31;
		// if(sqrDist > maxDist)
		float L_32 = V_6;
		float L_33 = V_0;
		if ((!(((float)L_32) > ((float)L_33))))
		{
			goto IL_00f2;
		}
	}
	{
		// maxDist = sqrDist;
		float L_34 = V_6;
		V_0 = L_34;
		// dir = currentDir;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_35 = V_4;
		V_1 = L_35;
		// }
		goto IL_00f2;
	}

IL_00da:
	{
		// dir = currentDir;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_36 = V_4;
		V_1 = L_36;
		// currentAvoidance = currentDir.normalized;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_37;
		L_37 = Vector3_get_normalized_m2FA6DF38F97BDA4CCBDAE12B9FE913A241DAC8D5((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_4), /*hidden argument*/NULL);
		__this->set_currentAvoidance_11(L_37);
		// return dir.normalized;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_38;
		L_38 = Vector3_get_normalized_m2FA6DF38F97BDA4CCBDAE12B9FE913A241DAC8D5((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_1), /*hidden argument*/NULL);
		return L_38;
	}

IL_00f2:
	{
		// for(int i = 0; i < collisionRays.Length; i++)
		int32_t L_39 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_39, (int32_t)1));
	}

IL_00f6:
	{
		// for(int i = 0; i < collisionRays.Length; i++)
		int32_t L_40 = V_3;
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_41 = __this->get_collisionRays_8();
		if ((((int32_t)L_40) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_41)->max_length))))))
		{
			goto IL_0061;
		}
	}
	{
		// return dir.normalized;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_42;
		L_42 = Vector3_get_normalized_m2FA6DF38F97BDA4CCBDAE12B9FE913A241DAC8D5((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_1), /*hidden argument*/NULL);
		return L_42;
	}
}
// System.Boolean FlockAgent::VectorInView(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool FlockAgent_VectorInView_m40DF4A6C365ADD655A97BB18413186B022867CE5 (FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position0, const RuntimeMethod* method)
{
	{
		// return Vector3.Angle(transform.forward, position - transform.position) <= viewAngle;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0;
		L_0 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1;
		L_1 = Transform_get_forward_mD850B9ECF892009E3485408DC0D375165B7BF053(L_0, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___position0;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_3;
		L_3 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4;
		L_4 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_3, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5;
		L_5 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_2, L_4, /*hidden argument*/NULL);
		float L_6;
		L_6 = Vector3_Angle_m3715AB03A36C59D8CF08F8D71E2F46454EB884C1(L_1, L_5, /*hidden argument*/NULL);
		float L_7 = __this->get_viewAngle_5();
		return (bool)((((int32_t)((!(((float)L_6) <= ((float)L_7)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void FlockAgent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FlockAgent__ctor_m7901AE28864B436FF807D1A533D94F577A6FA977 (FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m98321E90C6288A40B856F52CFF529E59A738E641_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t50CCFC552038CBD55842D81AC8F7EE04848C7255_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private List<FlockAgent> cohesionNeighbours = new List<FlockAgent>();
		List_1_t50CCFC552038CBD55842D81AC8F7EE04848C7255 * L_0 = (List_1_t50CCFC552038CBD55842D81AC8F7EE04848C7255 *)il2cpp_codegen_object_new(List_1_t50CCFC552038CBD55842D81AC8F7EE04848C7255_il2cpp_TypeInfo_var);
		List_1__ctor_m98321E90C6288A40B856F52CFF529E59A738E641(L_0, /*hidden argument*/List_1__ctor_m98321E90C6288A40B856F52CFF529E59A738E641_RuntimeMethod_var);
		__this->set_cohesionNeighbours_13(L_0);
		// private List<FlockAgent> seperationNeighbours = new List<FlockAgent>();
		List_1_t50CCFC552038CBD55842D81AC8F7EE04848C7255 * L_1 = (List_1_t50CCFC552038CBD55842D81AC8F7EE04848C7255 *)il2cpp_codegen_object_new(List_1_t50CCFC552038CBD55842D81AC8F7EE04848C7255_il2cpp_TypeInfo_var);
		List_1__ctor_m98321E90C6288A40B856F52CFF529E59A738E641(L_1, /*hidden argument*/List_1__ctor_m98321E90C6288A40B856F52CFF529E59A738E641_RuntimeMethod_var);
		__this->set_seperationNeighbours_14(L_1);
		// private List<FlockAgent> alignmentNeighbours = new List<FlockAgent>();
		List_1_t50CCFC552038CBD55842D81AC8F7EE04848C7255 * L_2 = (List_1_t50CCFC552038CBD55842D81AC8F7EE04848C7255 *)il2cpp_codegen_object_new(List_1_t50CCFC552038CBD55842D81AC8F7EE04848C7255_il2cpp_TypeInfo_var);
		List_1__ctor_m98321E90C6288A40B856F52CFF529E59A738E641(L_2, /*hidden argument*/List_1__ctor_m98321E90C6288A40B856F52CFF529E59A738E641_RuntimeMethod_var);
		__this->set_alignmentNeighbours_15(L_2);
		Fish__ctor_mEDE54A2E8EAB7C21444968366A1E9E12AC8D4A48(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// FlockAgent[] FlockManager::get_SpawnedAgents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR FlockAgentU5BU5D_t012E3C58876A70268E6BFEDBF651E5B8D23AAF84* FlockManager_get_SpawnedAgents_m050E5346A4490CAAD7EB7B9F8A7E6399405B4C7A (FlockManager_t241CFB1B2E6593247EFA52A9E2CB2A111400F1F8 * __this, const RuntimeMethod* method)
{
	{
		// public FlockAgent[] SpawnedAgents { get; private set; }
		FlockAgentU5BU5D_t012E3C58876A70268E6BFEDBF651E5B8D23AAF84* L_0 = __this->get_U3CSpawnedAgentsU3Ek__BackingField_18();
		return L_0;
	}
}
// System.Void FlockManager::set_SpawnedAgents(FlockAgent[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FlockManager_set_SpawnedAgents_m86395D548FB2FC61178CBDF82A5C5629A8716BB6 (FlockManager_t241CFB1B2E6593247EFA52A9E2CB2A111400F1F8 * __this, FlockAgentU5BU5D_t012E3C58876A70268E6BFEDBF651E5B8D23AAF84* ___value0, const RuntimeMethod* method)
{
	{
		// public FlockAgent[] SpawnedAgents { get; private set; }
		FlockAgentU5BU5D_t012E3C58876A70268E6BFEDBF651E5B8D23AAF84* L_0 = ___value0;
		__this->set_U3CSpawnedAgentsU3Ek__BackingField_18(L_0);
		return;
	}
}
// System.Void FlockManager::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FlockManager_Start_m2CAA7577F414C6299641BA18F29B39E7DCB5EC37 (FlockManager_t241CFB1B2E6593247EFA52A9E2CB2A111400F1F8 * __this, const RuntimeMethod* method)
{
	{
		// SpawnAgents();
		FlockManager_SpawnAgents_m075D462688CF50D13A0F934C0989C1CFCE54B3B3(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void FlockManager::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FlockManager_Update_m9A5DF45FD8F6018A6BF35881B36217BD505B9717 (FlockManager_t241CFB1B2E6593247EFA52A9E2CB2A111400F1F8 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		// for (int i = 0; i < SpawnedAgents.Length; i++)
		V_0 = 0;
		goto IL_0015;
	}

IL_0004:
	{
		// SpawnedAgents[i].Move();
		FlockAgentU5BU5D_t012E3C58876A70268E6BFEDBF651E5B8D23AAF84* L_0;
		L_0 = FlockManager_get_SpawnedAgents_m050E5346A4490CAAD7EB7B9F8A7E6399405B4C7A_inline(__this, /*hidden argument*/NULL);
		int32_t L_1 = V_0;
		int32_t L_2 = L_1;
		FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04 * L_3 = (L_0)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_2));
		FlockAgent_Move_mE006DB28962C8459F5677FCE8F3B9DA4302F4C99(L_3, /*hidden argument*/NULL);
		// for (int i = 0; i < SpawnedAgents.Length; i++)
		int32_t L_4 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)1));
	}

IL_0015:
	{
		// for (int i = 0; i < SpawnedAgents.Length; i++)
		int32_t L_5 = V_0;
		FlockAgentU5BU5D_t012E3C58876A70268E6BFEDBF651E5B8D23AAF84* L_6;
		L_6 = FlockManager_get_SpawnedAgents_m050E5346A4490CAAD7EB7B9F8A7E6399405B4C7A_inline(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_5) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_6)->max_length))))))
		{
			goto IL_0004;
		}
	}
	{
		// }
		return;
	}
}
// System.Void FlockManager::SpawnAgents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FlockManager_SpawnAgents_m075D462688CF50D13A0F934C0989C1CFCE54B3B3 (FlockManager_t241CFB1B2E6593247EFA52A9E2CB2A111400F1F8 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FlockAgentU5BU5D_t012E3C58876A70268E6BFEDBF651E5B8D23AAF84_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_Instantiate_TisFlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04_mB3A05C03EA7F057D8762864B17A537999578B839_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_1;
	memset((&V_1), 0, sizeof(V_1));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_2;
	memset((&V_2), 0, sizeof(V_2));
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  V_3;
	memset((&V_3), 0, sizeof(V_3));
	{
		// SpawnedAgents = new FlockAgent[size];
		int32_t L_0 = __this->get_size_5();
		FlockAgentU5BU5D_t012E3C58876A70268E6BFEDBF651E5B8D23AAF84* L_1 = (FlockAgentU5BU5D_t012E3C58876A70268E6BFEDBF651E5B8D23AAF84*)(FlockAgentU5BU5D_t012E3C58876A70268E6BFEDBF651E5B8D23AAF84*)SZArrayNew(FlockAgentU5BU5D_t012E3C58876A70268E6BFEDBF651E5B8D23AAF84_il2cpp_TypeInfo_var, (uint32_t)L_0);
		FlockManager_set_SpawnedAgents_m86395D548FB2FC61178CBDF82A5C5629A8716BB6_inline(__this, L_1, /*hidden argument*/NULL);
		// for (int i = 0; i < size; i++)
		V_0 = 0;
		goto IL_00cb;
	}

IL_0018:
	{
		// Vector3 random = Random.insideUnitSphere;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		L_2 = Random_get_insideUnitSphere_m43E5AE1F6A6CFA892BAE6E3ED71BEBFCE308CE90(/*hidden argument*/NULL);
		V_1 = L_2;
		// random = new Vector3(random.x * spawnBounds.x, random.y * spawnBounds.y, random.z * spawnBounds.z);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3 = V_1;
		float L_4 = L_3.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_5 = __this->get_address_of_spawnBounds_6();
		float L_6 = L_5->get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7 = V_1;
		float L_8 = L_7.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_9 = __this->get_address_of_spawnBounds_6();
		float L_10 = L_9->get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_11 = V_1;
		float L_12 = L_11.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_13 = __this->get_address_of_spawnBounds_6();
		float L_14 = L_13->get_z_4();
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_1), ((float)il2cpp_codegen_multiply((float)L_4, (float)L_6)), ((float)il2cpp_codegen_multiply((float)L_8, (float)L_10)), ((float)il2cpp_codegen_multiply((float)L_12, (float)L_14)), /*hidden argument*/NULL);
		// Vector3 spawnPos = transform.position + random;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_15;
		L_15 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_16;
		L_16 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_15, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_17 = V_1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_18;
		L_18 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_16, L_17, /*hidden argument*/NULL);
		V_2 = L_18;
		// Quaternion rot = Quaternion.Euler(0, Random.Range(0, 360), 0);
		int32_t L_19;
		L_19 = Random_Range_m4B3A0037ACA057F33C94508F908546B9317D996A(0, ((int32_t)360), /*hidden argument*/NULL);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_20;
		L_20 = Quaternion_Euler_m37BF99FFFA09F4B3F83DC066641B82C59B19A9C3((0.0f), ((float)((float)L_19)), (0.0f), /*hidden argument*/NULL);
		V_3 = L_20;
		// SpawnedAgents[i] = Instantiate(agentPrefab, spawnPos, rot);
		FlockAgentU5BU5D_t012E3C58876A70268E6BFEDBF651E5B8D23AAF84* L_21;
		L_21 = FlockManager_get_SpawnedAgents_m050E5346A4490CAAD7EB7B9F8A7E6399405B4C7A_inline(__this, /*hidden argument*/NULL);
		int32_t L_22 = V_0;
		FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04 * L_23 = __this->get_agentPrefab_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_24 = V_2;
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_25 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04 * L_26;
		L_26 = Object_Instantiate_TisFlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04_mB3A05C03EA7F057D8762864B17A537999578B839(L_23, L_24, L_25, /*hidden argument*/Object_Instantiate_TisFlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04_mB3A05C03EA7F057D8762864B17A537999578B839_RuntimeMethod_var);
		ArrayElementTypeCheck (L_21, L_26);
		(L_21)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(L_22), (FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04 *)L_26);
		// SpawnedAgents[i].Initialize(this, Random.Range(speed.x, speed.y));
		FlockAgentU5BU5D_t012E3C58876A70268E6BFEDBF651E5B8D23AAF84* L_27;
		L_27 = FlockManager_get_SpawnedAgents_m050E5346A4490CAAD7EB7B9F8A7E6399405B4C7A_inline(__this, /*hidden argument*/NULL);
		int32_t L_28 = V_0;
		int32_t L_29 = L_28;
		FlockAgent_t4F2FE65A76E2CB0FE7F03F3E7DFA8F1FC8419D04 * L_30 = (L_27)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_29));
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_31 = __this->get_address_of_speed_7();
		float L_32 = L_31->get_x_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_33 = __this->get_address_of_speed_7();
		float L_34 = L_33->get_y_1();
		float L_35;
		L_35 = Random_Range_mC15372D42A9ABDCAC3DE82E114D60A40C9C311D2(L_32, L_34, /*hidden argument*/NULL);
		FlockAgent_Initialize_m8D1C04B1942FF5534A44DAC4526F9A8EF16CA1A6(L_30, __this, L_35, /*hidden argument*/NULL);
		// for (int i = 0; i < size; i++)
		int32_t L_36 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_36, (int32_t)1));
	}

IL_00cb:
	{
		// for (int i = 0; i < size; i++)
		int32_t L_37 = V_0;
		int32_t L_38 = __this->get_size_5();
		if ((((int32_t)L_37) < ((int32_t)L_38)))
		{
			goto IL_0018;
		}
	}
	{
		// }
		return;
	}
}
// System.Void FlockManager::OnDrawGizmos()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FlockManager_OnDrawGizmos_m33A47EA1DF5486A1A47B8869073F955F986B5529 (FlockManager_t241CFB1B2E6593247EFA52A9E2CB2A111400F1F8 * __this, const RuntimeMethod* method)
{
	{
		// Gizmos.color = Color.red;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_0;
		L_0 = Color_get_red_m9BD55EBF7A74A515330FA5F7AC7A67C8A8913DD8(/*hidden argument*/NULL);
		Gizmos_set_color_m937ACC6288C81BAFFC3449FAA03BB4F680F4E74F(L_0, /*hidden argument*/NULL);
		// Gizmos.DrawWireCube(transform.position, spawnBounds);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1;
		L_1 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		L_2 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_1, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3 = __this->get_spawnBounds_6();
		Gizmos_DrawWireCube_mC526244E50C6E5793D4066C9C99023D5FF8424BF(L_2, L_3, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void FlockManager::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FlockManager__ctor_m317ABCE65877D367E812F102E2D7D3A49FA28DFF (FlockManager_t241CFB1B2E6593247EFA52A9E2CB2A111400F1F8 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// GameManager GameManager::get_Instance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * GameManager_get_Instance_m758C0D98BBDEF0C2DCA163A5D60B22DBB5B07232 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static GameManager Instance { get; private set; }
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_0 = ((GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_StaticFields*)il2cpp_codegen_static_fields_for(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_il2cpp_TypeInfo_var))->get_U3CInstanceU3Ek__BackingField_16();
		return L_0;
	}
}
// System.Void GameManager::set_Instance(GameManager)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameManager_set_Instance_mAEF75ABE6135BDD61B9A3AED1606C0449787C5C7 (GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static GameManager Instance { get; private set; }
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_0 = ___value0;
		((GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_StaticFields*)il2cpp_codegen_static_fields_for(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_il2cpp_TypeInfo_var))->set_U3CInstanceU3Ek__BackingField_16(L_0);
		return;
	}
}
// IFauna GameManager::get_Target()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* GameManager_get_Target_m3ACB72AC913A603DF141C5D74A3F5C3B42D25B8D (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static IFauna Target { get; private set; }
		RuntimeObject* L_0 = ((GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_StaticFields*)il2cpp_codegen_static_fields_for(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_il2cpp_TypeInfo_var))->get_U3CTargetU3Ek__BackingField_17();
		return L_0;
	}
}
// System.Void GameManager::set_Target(IFauna)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameManager_set_Target_mB22AB1128EBC08C3818777166428FF6DEEE341E6 (RuntimeObject* ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static IFauna Target { get; private set; }
		RuntimeObject* L_0 = ___value0;
		((GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_StaticFields*)il2cpp_codegen_static_fields_for(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_il2cpp_TypeInfo_var))->set_U3CTargetU3Ek__BackingField_17(L_0);
		return;
	}
}
// System.Boolean GameManager::get_TargetFound()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GameManager_get_TargetFound_mD646125D22C8183B642D2ECD48C64FE59D1D12AE (GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * __this, const RuntimeMethod* method)
{
	{
		// public bool TargetFound { get; set; } = false;
		bool L_0 = __this->get_U3CTargetFoundU3Ek__BackingField_18();
		return L_0;
	}
}
// System.Void GameManager::set_TargetFound(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameManager_set_TargetFound_mA5DAB51223A80F7D092FF2967C48AC6EB5464CB2 (GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// public bool TargetFound { get; set; } = false;
		bool L_0 = ___value0;
		__this->set_U3CTargetFoundU3Ek__BackingField_18(L_0);
		return;
	}
}
// System.Boolean GameManager::get_OrbitCameraTarget()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GameManager_get_OrbitCameraTarget_m9B1FB59BEA2FEF5094B7EB93548C815087CA0E11 (GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * __this, const RuntimeMethod* method)
{
	{
		// public bool OrbitCameraTarget { get; set; } = false;
		bool L_0 = __this->get_U3COrbitCameraTargetU3Ek__BackingField_19();
		return L_0;
	}
}
// System.Void GameManager::set_OrbitCameraTarget(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameManager_set_OrbitCameraTarget_m30E1AB1FE69E612022E9ECBB93579479690F219B (GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// public bool OrbitCameraTarget { get; set; } = false;
		bool L_0 = ___value0;
		__this->set_U3COrbitCameraTargetU3Ek__BackingField_19(L_0);
		return;
	}
}
// System.Boolean GameManager::get_OrbitCameraTarget2()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GameManager_get_OrbitCameraTarget2_m8967752F71CD50D7C56AB9F21BC4D5576A1DF23B (GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * __this, const RuntimeMethod* method)
{
	{
		// public bool OrbitCameraTarget2 { get; set; } = false;
		bool L_0 = __this->get_U3COrbitCameraTarget2U3Ek__BackingField_20();
		return L_0;
	}
}
// System.Void GameManager::set_OrbitCameraTarget2(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameManager_set_OrbitCameraTarget2_m82942D4D17D5A3922A44BD6398ED44370EC6F7D4 (GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// public bool OrbitCameraTarget2 { get; set; } = false;
		bool L_0 = ___value0;
		__this->set_U3COrbitCameraTarget2U3Ek__BackingField_20(L_0);
		return;
	}
}
// System.Boolean GameManager::get_PanToCameraTarget()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GameManager_get_PanToCameraTarget_m650916A4853E8626442E0A99A7CFEE2C7C168A8C (GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * __this, const RuntimeMethod* method)
{
	{
		// public bool PanToCameraTarget { get; set; } = false;
		bool L_0 = __this->get_U3CPanToCameraTargetU3Ek__BackingField_21();
		return L_0;
	}
}
// System.Void GameManager::set_PanToCameraTarget(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameManager_set_PanToCameraTarget_mD06BE8F791C2C13FC8FC4DE7AA41E8D6C3FA773F (GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// public bool PanToCameraTarget { get; set; } = false;
		bool L_0 = ___value0;
		__this->set_U3CPanToCameraTargetU3Ek__BackingField_21(L_0);
		return;
	}
}
// UnityEngine.Transform GameManager::get_CameraTarget()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * GameManager_get_CameraTarget_mBBC82927B5A0D320D5B38CA1B94C04FDB1EBC4F8 (GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * __this, const RuntimeMethod* method)
{
	{
		// public Transform CameraTarget { get; private set; }
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0 = __this->get_U3CCameraTargetU3Ek__BackingField_22();
		return L_0;
	}
}
// System.Void GameManager::set_CameraTarget(UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameManager_set_CameraTarget_m40A31328DAA4F5876B52EDE84970FE6B7E686919 (GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * __this, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___value0, const RuntimeMethod* method)
{
	{
		// public Transform CameraTarget { get; private set; }
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0 = ___value0;
		__this->set_U3CCameraTargetU3Ek__BackingField_22(L_0);
		return;
	}
}
// System.Void GameManager::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameManager_Awake_m22F42B2A82708B10F652CAD8F2E0A4767110FF30 (GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisIFauna_tECBAA4EFAA9D07BFB7CB55CEE86755A301872FC5_m3DDC9275C8F6A20642A992058DD3465A7B43896A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_TryGetComponent_TisSoloFish_t248C1128425014659AF96F9294BAC7D1799179C2_m77EB52771C7FC7A5EF16539BFE0797509D5DA199_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Remove_m4FBB736F89C431F0D19B655E23269EAB321B863A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Remove_mD36BF07C31C1DF947856EFECE89BAF4D6A24DEB7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m6206A8F48C7A98B9CFC23A13548CB215717573BD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m82AF14687C6FA2B1572D859A551E3ADBCBADC3C0_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m8B222F262DF0C4B49E12B4E87AB2162202744499_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m81B599A0051F8F4543E5C73A11585E96E940943B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * V_0 = NULL;
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * V_1 = NULL;
	SoloFish_t248C1128425014659AF96F9294BAC7D1799179C2 * V_2 = NULL;
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * G_B8_0 = NULL;
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * G_B7_0 = NULL;
	{
		// if(Instance == null)
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_0;
		L_0 = GameManager_get_Instance_m758C0D98BBDEF0C2DCA163A5D60B22DBB5B07232_inline(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0015;
		}
	}
	{
		// Instance = this;
		GameManager_set_Instance_mAEF75ABE6135BDD61B9A3AED1606C0449787C5C7_inline(__this, /*hidden argument*/NULL);
		// }
		goto IL_001c;
	}

IL_0015:
	{
		// enabled = false;
		Behaviour_set_enabled_mDE415591B28853D1CD764C53CB499A2142247F32(__this, (bool)0, /*hidden argument*/NULL);
	}

IL_001c:
	{
		// GameObject targetFish = fishPrefabs[Random.Range(0, fishPrefabs.Count)];
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_2 = __this->get_fishPrefabs_4();
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_3 = __this->get_fishPrefabs_4();
		int32_t L_4;
		L_4 = List_1_get_Count_m6206A8F48C7A98B9CFC23A13548CB215717573BD_inline(L_3, /*hidden argument*/List_1_get_Count_m6206A8F48C7A98B9CFC23A13548CB215717573BD_RuntimeMethod_var);
		int32_t L_5;
		L_5 = Random_Range_m4B3A0037ACA057F33C94508F908546B9317D996A(0, L_4, /*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_6;
		L_6 = List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_inline(L_2, L_5, /*hidden argument*/List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_RuntimeMethod_var);
		V_0 = L_6;
		goto IL_0058;
	}

IL_003b:
	{
		// targetFish = fishPrefabs[Random.Range(0, fishPrefabs.Count)];
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_7 = __this->get_fishPrefabs_4();
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_8 = __this->get_fishPrefabs_4();
		int32_t L_9;
		L_9 = List_1_get_Count_m6206A8F48C7A98B9CFC23A13548CB215717573BD_inline(L_8, /*hidden argument*/List_1_get_Count_m6206A8F48C7A98B9CFC23A13548CB215717573BD_RuntimeMethod_var);
		int32_t L_10;
		L_10 = Random_Range_m4B3A0037ACA057F33C94508F908546B9317D996A(0, L_9, /*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_11;
		L_11 = List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_inline(L_7, L_10, /*hidden argument*/List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_RuntimeMethod_var);
		V_0 = L_11;
	}

IL_0058:
	{
		// while(targetFish.GetComponent<IFauna>() == null)
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_12 = V_0;
		RuntimeObject* L_13;
		L_13 = GameObject_GetComponent_TisIFauna_tECBAA4EFAA9D07BFB7CB55CEE86755A301872FC5_m3DDC9275C8F6A20642A992058DD3465A7B43896A(L_12, /*hidden argument*/GameObject_GetComponent_TisIFauna_tECBAA4EFAA9D07BFB7CB55CEE86755A301872FC5_m3DDC9275C8F6A20642A992058DD3465A7B43896A_RuntimeMethod_var);
		if (!L_13)
		{
			goto IL_003b;
		}
	}
	{
		// fishPrefabs.Remove(targetFish);
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_14 = __this->get_fishPrefabs_4();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_15 = V_0;
		bool L_16;
		L_16 = List_1_Remove_mD36BF07C31C1DF947856EFECE89BAF4D6A24DEB7(L_14, L_15, /*hidden argument*/List_1_Remove_mD36BF07C31C1DF947856EFECE89BAF4D6A24DEB7_RuntimeMethod_var);
		// Transform spawn = ZoneCSpawns[Random.Range(0, ZoneCSpawns.Count)];
		List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * L_17 = __this->get_ZoneCSpawns_7();
		List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * L_18 = __this->get_ZoneCSpawns_7();
		int32_t L_19;
		L_19 = List_1_get_Count_m82AF14687C6FA2B1572D859A551E3ADBCBADC3C0_inline(L_18, /*hidden argument*/List_1_get_Count_m82AF14687C6FA2B1572D859A551E3ADBCBADC3C0_RuntimeMethod_var);
		int32_t L_20;
		L_20 = Random_Range_m4B3A0037ACA057F33C94508F908546B9317D996A(0, L_19, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_21;
		L_21 = List_1_get_Item_m8B222F262DF0C4B49E12B4E87AB2162202744499_inline(L_17, L_20, /*hidden argument*/List_1_get_Item_m8B222F262DF0C4B49E12B4E87AB2162202744499_RuntimeMethod_var);
		V_1 = L_21;
		// GameObject spawnedFish = Instantiate(targetFish, spawn.transform.position, spawn.transform.rotation);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_22 = V_0;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_23 = V_1;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_24;
		L_24 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_23, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_25;
		L_25 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_24, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_26 = V_1;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_27;
		L_27 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_26, /*hidden argument*/NULL);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_28;
		L_28 = Transform_get_rotation_m4AA3858C00DF4C9614B80352558C4C37D08D2200(L_27, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_29;
		L_29 = Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m81B599A0051F8F4543E5C73A11585E96E940943B(L_22, L_25, L_28, /*hidden argument*/Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m81B599A0051F8F4543E5C73A11585E96E940943B_RuntimeMethod_var);
		// if (spawnedFish.TryGetComponent(out SoloFish sFish) == true)
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_30 = L_29;
		bool L_31;
		L_31 = GameObject_TryGetComponent_TisSoloFish_t248C1128425014659AF96F9294BAC7D1799179C2_m77EB52771C7FC7A5EF16539BFE0797509D5DA199(L_30, (SoloFish_t248C1128425014659AF96F9294BAC7D1799179C2 **)(&V_2), /*hidden argument*/GameObject_TryGetComponent_TisSoloFish_t248C1128425014659AF96F9294BAC7D1799179C2_m77EB52771C7FC7A5EF16539BFE0797509D5DA199_RuntimeMethod_var);
		G_B7_0 = L_30;
		if (!L_31)
		{
			G_B8_0 = L_30;
			goto IL_00c1;
		}
	}
	{
		// sFish.boundBox.center = spawn.position;
		SoloFish_t248C1128425014659AF96F9294BAC7D1799179C2 * L_32 = V_2;
		Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 * L_33 = L_32->get_address_of_boundBox_5();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_34 = V_1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_35;
		L_35 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_34, /*hidden argument*/NULL);
		Bounds_set_center_mAC54A53224BBEFE37A4387DCBD0EF3774751221E((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)L_33, L_35, /*hidden argument*/NULL);
		G_B8_0 = G_B7_0;
	}

IL_00c1:
	{
		// spawnedFish.name = targetFish.name;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_36 = G_B8_0;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_37 = V_0;
		String_t* L_38;
		L_38 = Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB(L_37, /*hidden argument*/NULL);
		Object_set_name_m87C4006618ADB325ABE5439DF159E10DD8DD0781(L_36, L_38, /*hidden argument*/NULL);
		// targetText.text = targetFish.name;
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_39 = __this->get_targetText_8();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_40 = V_0;
		String_t* L_41;
		L_41 = Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB(L_40, /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_39, L_41);
		// Target = spawnedFish.GetComponent<IFauna>();
		RuntimeObject* L_42;
		L_42 = GameObject_GetComponent_TisIFauna_tECBAA4EFAA9D07BFB7CB55CEE86755A301872FC5_m3DDC9275C8F6A20642A992058DD3465A7B43896A(L_36, /*hidden argument*/GameObject_GetComponent_TisIFauna_tECBAA4EFAA9D07BFB7CB55CEE86755A301872FC5_m3DDC9275C8F6A20642A992058DD3465A7B43896A_RuntimeMethod_var);
		GameManager_set_Target_mB22AB1128EBC08C3818777166428FF6DEEE341E6_inline(L_42, /*hidden argument*/NULL);
		// ZoneCSpawns.Remove(spawn);
		List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * L_43 = __this->get_ZoneCSpawns_7();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_44 = V_1;
		bool L_45;
		L_45 = List_1_Remove_m4FBB736F89C431F0D19B655E23269EAB321B863A(L_43, L_44, /*hidden argument*/List_1_Remove_m4FBB736F89C431F0D19B655E23269EAB321B863A_RuntimeMethod_var);
		// if (fishPrefabs.Count > 0  && ZoneASpawns.Count > 0)
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_46 = __this->get_fishPrefabs_4();
		int32_t L_47;
		L_47 = List_1_get_Count_m6206A8F48C7A98B9CFC23A13548CB215717573BD_inline(L_46, /*hidden argument*/List_1_get_Count_m6206A8F48C7A98B9CFC23A13548CB215717573BD_RuntimeMethod_var);
		if ((((int32_t)L_47) <= ((int32_t)0)))
		{
			goto IL_011d;
		}
	}
	{
		List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * L_48 = __this->get_ZoneASpawns_5();
		int32_t L_49;
		L_49 = List_1_get_Count_m82AF14687C6FA2B1572D859A551E3ADBCBADC3C0_inline(L_48, /*hidden argument*/List_1_get_Count_m82AF14687C6FA2B1572D859A551E3ADBCBADC3C0_RuntimeMethod_var);
		if ((((int32_t)L_49) <= ((int32_t)0)))
		{
			goto IL_011d;
		}
	}
	{
		// SpawnZone(ZoneASpawns);
		List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * L_50 = __this->get_ZoneASpawns_5();
		GameManager_SpawnZone_m095B9AA853D168B937A20973F44CE3D7D5B3B8CE(__this, L_50, /*hidden argument*/NULL);
	}

IL_011d:
	{
		// if (fishPrefabs.Count > 0 && ZoneBSpawns.Count > 0)
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_51 = __this->get_fishPrefabs_4();
		int32_t L_52;
		L_52 = List_1_get_Count_m6206A8F48C7A98B9CFC23A13548CB215717573BD_inline(L_51, /*hidden argument*/List_1_get_Count_m6206A8F48C7A98B9CFC23A13548CB215717573BD_RuntimeMethod_var);
		if ((((int32_t)L_52) <= ((int32_t)0)))
		{
			goto IL_0145;
		}
	}
	{
		List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * L_53 = __this->get_ZoneBSpawns_6();
		int32_t L_54;
		L_54 = List_1_get_Count_m82AF14687C6FA2B1572D859A551E3ADBCBADC3C0_inline(L_53, /*hidden argument*/List_1_get_Count_m82AF14687C6FA2B1572D859A551E3ADBCBADC3C0_RuntimeMethod_var);
		if ((((int32_t)L_54) <= ((int32_t)0)))
		{
			goto IL_0145;
		}
	}
	{
		// SpawnZone(ZoneBSpawns);
		List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * L_55 = __this->get_ZoneBSpawns_6();
		GameManager_SpawnZone_m095B9AA853D168B937A20973F44CE3D7D5B3B8CE(__this, L_55, /*hidden argument*/NULL);
	}

IL_0145:
	{
		// if (fishPrefabs.Count > 0 && ZoneCSpawns.Count > 0)
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_56 = __this->get_fishPrefabs_4();
		int32_t L_57;
		L_57 = List_1_get_Count_m6206A8F48C7A98B9CFC23A13548CB215717573BD_inline(L_56, /*hidden argument*/List_1_get_Count_m6206A8F48C7A98B9CFC23A13548CB215717573BD_RuntimeMethod_var);
		if ((((int32_t)L_57) <= ((int32_t)0)))
		{
			goto IL_016d;
		}
	}
	{
		List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * L_58 = __this->get_ZoneCSpawns_7();
		int32_t L_59;
		L_59 = List_1_get_Count_m82AF14687C6FA2B1572D859A551E3ADBCBADC3C0_inline(L_58, /*hidden argument*/List_1_get_Count_m82AF14687C6FA2B1572D859A551E3ADBCBADC3C0_RuntimeMethod_var);
		if ((((int32_t)L_59) <= ((int32_t)0)))
		{
			goto IL_016d;
		}
	}
	{
		// SpawnZone(ZoneCSpawns);
		List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * L_60 = __this->get_ZoneCSpawns_7();
		GameManager_SpawnZone_m095B9AA853D168B937A20973F44CE3D7D5B3B8CE(__this, L_60, /*hidden argument*/NULL);
	}

IL_016d:
	{
		// }
		return;
	}
}
// System.Void GameManager::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameManager_Start_m26461AEF27E44DB8FECCBC19D6C9E228B658BF8E (GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if(cutsceneDirector != null && introCutScene != null)
		PlayableDirector_t63912C98A4EC9D4701E88B633E27E0D97209BA38 * L_0 = __this->get_cutsceneDirector_11();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		PlayableAsset_t5AD1606B76C9753A7F4C6B1061193F581023F137 * L_2 = __this->get_introCutScene_12();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_3;
		L_3 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_2, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002f;
		}
	}
	{
		// StartCoroutine(PlayCutScene(introCutScene));
		PlayableAsset_t5AD1606B76C9753A7F4C6B1061193F581023F137 * L_4 = __this->get_introCutScene_12();
		RuntimeObject* L_5;
		L_5 = GameManager_PlayCutScene_mEE157DD535C601E8C248C3286884E2622B9E5851(__this, L_4, /*hidden argument*/NULL);
		Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * L_6;
		L_6 = MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719(__this, L_5, /*hidden argument*/NULL);
	}

IL_002f:
	{
		// }
		return;
	}
}
// System.Void GameManager::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameManager_Update_mC9303BA7C3117BD861F49F8E36151CC52117E6C1 (GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if(CameraTarget != null)
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0;
		L_0 = GameManager_get_CameraTarget_mBBC82927B5A0D320D5B38CA1B94C04FDB1EBC4F8_inline(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00af;
		}
	}
	{
		// if(OrbitCameraTarget == true) //tells the camera what to do, in this care rotate around the object its set to target and to move itself to look at it
		bool L_2;
		L_2 = GameManager_get_OrbitCameraTarget_m9B1FB59BEA2FEF5094B7EB93548C815087CA0E11_inline(__this, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0060;
		}
	}
	{
		// cutSceneCamera.transform.RotateAround(CameraTarget.position, CameraTarget.right, 15 * Time.deltaTime);
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_3 = __this->get_cutSceneCamera_9();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_4;
		L_4 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_3, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_5;
		L_5 = GameManager_get_CameraTarget_mBBC82927B5A0D320D5B38CA1B94C04FDB1EBC4F8_inline(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6;
		L_6 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_5, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_7;
		L_7 = GameManager_get_CameraTarget_mBBC82927B5A0D320D5B38CA1B94C04FDB1EBC4F8_inline(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8;
		L_8 = Transform_get_right_mA8EB1882CCE8759766544448308C88D9200F06CE(L_7, /*hidden argument*/NULL);
		float L_9;
		L_9 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		Transform_RotateAround_m1F93A7A1807BE407BD23EC1BA49F03AD22FCE4BE(L_4, L_6, L_8, ((float)il2cpp_codegen_multiply((float)(15.0f), (float)L_9)), /*hidden argument*/NULL);
		// cutSceneCamera.transform.LookAt(CameraTarget);
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_10 = __this->get_cutSceneCamera_9();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_11;
		L_11 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_10, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_12;
		L_12 = GameManager_get_CameraTarget_mBBC82927B5A0D320D5B38CA1B94C04FDB1EBC4F8_inline(__this, /*hidden argument*/NULL);
		Transform_LookAt_m49185D782014D16DA747C1296BEBAC3FB3CEDC1F(L_11, L_12, /*hidden argument*/NULL);
	}

IL_0060:
	{
		// if(OrbitCameraTarget2 == true) //tells the camera what to do, in this care rotate around the object its set to target and to move itself to look at it
		bool L_13;
		L_13 = GameManager_get_OrbitCameraTarget2_m8967752F71CD50D7C56AB9F21BC4D5576A1DF23B_inline(__this, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_00af;
		}
	}
	{
		// cutSceneCamera.transform.RotateAround(CameraTarget.position, CameraTarget.right, 15 * Time.deltaTime);
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_14 = __this->get_cutSceneCamera_9();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_15;
		L_15 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_14, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_16;
		L_16 = GameManager_get_CameraTarget_mBBC82927B5A0D320D5B38CA1B94C04FDB1EBC4F8_inline(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_17;
		L_17 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_16, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_18;
		L_18 = GameManager_get_CameraTarget_mBBC82927B5A0D320D5B38CA1B94C04FDB1EBC4F8_inline(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_19;
		L_19 = Transform_get_right_mA8EB1882CCE8759766544448308C88D9200F06CE(L_18, /*hidden argument*/NULL);
		float L_20;
		L_20 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		Transform_RotateAround_m1F93A7A1807BE407BD23EC1BA49F03AD22FCE4BE(L_15, L_17, L_19, ((float)il2cpp_codegen_multiply((float)(15.0f), (float)L_20)), /*hidden argument*/NULL);
		// cutSceneCamera.transform.LookAt(CameraTarget);
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_21 = __this->get_cutSceneCamera_9();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_22;
		L_22 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_21, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_23;
		L_23 = GameManager_get_CameraTarget_mBBC82927B5A0D320D5B38CA1B94C04FDB1EBC4F8_inline(__this, /*hidden argument*/NULL);
		Transform_LookAt_m49185D782014D16DA747C1296BEBAC3FB3CEDC1F(L_22, L_23, /*hidden argument*/NULL);
	}

IL_00af:
	{
		// }
		return;
	}
}
// System.Void GameManager::SpawnZone(System.Collections.Generic.List`1<UnityEngine.Transform>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameManager_SpawnZone_m095B9AA853D168B937A20973F44CE3D7D5B3B8CE (GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * __this, List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * ___spawnList0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_TryGetComponent_TisSoloFish_t248C1128425014659AF96F9294BAC7D1799179C2_m77EB52771C7FC7A5EF16539BFE0797509D5DA199_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Remove_m4FBB736F89C431F0D19B655E23269EAB321B863A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Remove_mD36BF07C31C1DF947856EFECE89BAF4D6A24DEB7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m6206A8F48C7A98B9CFC23A13548CB215717573BD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m82AF14687C6FA2B1572D859A551E3ADBCBADC3C0_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m8B222F262DF0C4B49E12B4E87AB2162202744499_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m81B599A0051F8F4543E5C73A11585E96E940943B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * V_3 = NULL;
	SoloFish_t248C1128425014659AF96F9294BAC7D1799179C2 * V_4 = NULL;
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * G_B3_0 = NULL;
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * G_B2_0 = NULL;
	{
		// int zoneCount = spawnList.Count;
		List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * L_0 = ___spawnList0;
		int32_t L_1;
		L_1 = List_1_get_Count_m82AF14687C6FA2B1572D859A551E3ADBCBADC3C0_inline(L_0, /*hidden argument*/List_1_get_Count_m82AF14687C6FA2B1572D859A551E3ADBCBADC3C0_RuntimeMethod_var);
		V_0 = L_1;
		// for(int i = 0; i < zoneCount; i++)
		V_1 = 0;
		goto IL_00b0;
	}

IL_000e:
	{
		// int f = Random.Range(0, fishPrefabs.Count);
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_2 = __this->get_fishPrefabs_4();
		int32_t L_3;
		L_3 = List_1_get_Count_m6206A8F48C7A98B9CFC23A13548CB215717573BD_inline(L_2, /*hidden argument*/List_1_get_Count_m6206A8F48C7A98B9CFC23A13548CB215717573BD_RuntimeMethod_var);
		int32_t L_4;
		L_4 = Random_Range_m4B3A0037ACA057F33C94508F908546B9317D996A(0, L_3, /*hidden argument*/NULL);
		V_2 = L_4;
		// Transform spawn = spawnList[Random.Range(0, spawnList.Count)];
		List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * L_5 = ___spawnList0;
		List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * L_6 = ___spawnList0;
		int32_t L_7;
		L_7 = List_1_get_Count_m82AF14687C6FA2B1572D859A551E3ADBCBADC3C0_inline(L_6, /*hidden argument*/List_1_get_Count_m82AF14687C6FA2B1572D859A551E3ADBCBADC3C0_RuntimeMethod_var);
		int32_t L_8;
		L_8 = Random_Range_m4B3A0037ACA057F33C94508F908546B9317D996A(0, L_7, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_9;
		L_9 = List_1_get_Item_m8B222F262DF0C4B49E12B4E87AB2162202744499_inline(L_5, L_8, /*hidden argument*/List_1_get_Item_m8B222F262DF0C4B49E12B4E87AB2162202744499_RuntimeMethod_var);
		V_3 = L_9;
		// GameObject fish = Instantiate(fishPrefabs[f], spawn.transform.position, spawn.transform.rotation);
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_10 = __this->get_fishPrefabs_4();
		int32_t L_11 = V_2;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_12;
		L_12 = List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_inline(L_10, L_11, /*hidden argument*/List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_RuntimeMethod_var);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_13 = V_3;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_14;
		L_14 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_13, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_15;
		L_15 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_14, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_16 = V_3;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_17;
		L_17 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_16, /*hidden argument*/NULL);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_18;
		L_18 = Transform_get_rotation_m4AA3858C00DF4C9614B80352558C4C37D08D2200(L_17, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_19;
		L_19 = Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m81B599A0051F8F4543E5C73A11585E96E940943B(L_12, L_15, L_18, /*hidden argument*/Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m81B599A0051F8F4543E5C73A11585E96E940943B_RuntimeMethod_var);
		// if(fish.TryGetComponent(out SoloFish sFish) == true)
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_20 = L_19;
		bool L_21;
		L_21 = GameObject_TryGetComponent_TisSoloFish_t248C1128425014659AF96F9294BAC7D1799179C2_m77EB52771C7FC7A5EF16539BFE0797509D5DA199(L_20, (SoloFish_t248C1128425014659AF96F9294BAC7D1799179C2 **)(&V_4), /*hidden argument*/GameObject_TryGetComponent_TisSoloFish_t248C1128425014659AF96F9294BAC7D1799179C2_m77EB52771C7FC7A5EF16539BFE0797509D5DA199_RuntimeMethod_var);
		G_B2_0 = L_20;
		if (!L_21)
		{
			G_B3_0 = L_20;
			goto IL_0076;
		}
	}
	{
		// sFish.boundBox.center = spawn.position;
		SoloFish_t248C1128425014659AF96F9294BAC7D1799179C2 * L_22 = V_4;
		Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 * L_23 = L_22->get_address_of_boundBox_5();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_24 = V_3;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_25;
		L_25 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_24, /*hidden argument*/NULL);
		Bounds_set_center_mAC54A53224BBEFE37A4387DCBD0EF3774751221E((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)L_23, L_25, /*hidden argument*/NULL);
		G_B3_0 = G_B2_0;
	}

IL_0076:
	{
		// fish.name = fishPrefabs[f].name;
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_26 = __this->get_fishPrefabs_4();
		int32_t L_27 = V_2;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_28;
		L_28 = List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_inline(L_26, L_27, /*hidden argument*/List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_RuntimeMethod_var);
		String_t* L_29;
		L_29 = Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB(L_28, /*hidden argument*/NULL);
		Object_set_name_m87C4006618ADB325ABE5439DF159E10DD8DD0781(G_B3_0, L_29, /*hidden argument*/NULL);
		// fishPrefabs.Remove(fishPrefabs[f]);
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_30 = __this->get_fishPrefabs_4();
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_31 = __this->get_fishPrefabs_4();
		int32_t L_32 = V_2;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_33;
		L_33 = List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_inline(L_31, L_32, /*hidden argument*/List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_RuntimeMethod_var);
		bool L_34;
		L_34 = List_1_Remove_mD36BF07C31C1DF947856EFECE89BAF4D6A24DEB7(L_30, L_33, /*hidden argument*/List_1_Remove_mD36BF07C31C1DF947856EFECE89BAF4D6A24DEB7_RuntimeMethod_var);
		// spawnList.Remove(spawn);
		List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * L_35 = ___spawnList0;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_36 = V_3;
		bool L_37;
		L_37 = List_1_Remove_m4FBB736F89C431F0D19B655E23269EAB321B863A(L_35, L_36, /*hidden argument*/List_1_Remove_m4FBB736F89C431F0D19B655E23269EAB321B863A_RuntimeMethod_var);
		// for(int i = 0; i < zoneCount; i++)
		int32_t L_38 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_38, (int32_t)1));
	}

IL_00b0:
	{
		// for(int i = 0; i < zoneCount; i++)
		int32_t L_39 = V_1;
		int32_t L_40 = V_0;
		if ((((int32_t)L_39) < ((int32_t)L_40)))
		{
			goto IL_000e;
		}
	}
	{
		// }
		return;
	}
}
// System.Void GameManager::TogglePlayer(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameManager_TogglePlayer_m6D6635AAAE3C34217AAF3AE377EC5B97AE4CAA47 (GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * __this, bool ___toggle0, const RuntimeMethod* method)
{
	{
		// PlayerController.Instance.enabled = toggle;
		PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9 * L_0;
		L_0 = PlayerController_get_Instance_m8C373614456176CE37B49E24BA9B7E019C5A0440_inline(/*hidden argument*/NULL);
		bool L_1 = ___toggle0;
		Behaviour_set_enabled_mDE415591B28853D1CD764C53CB499A2142247F32(L_0, L_1, /*hidden argument*/NULL);
		// MouseLook.Instance.gameObject.SetActive(toggle);
		MouseLook_t1E1EE79B13022184F3EF9AC8FE8C6771053890DA * L_2;
		L_2 = MouseLook_get_Instance_m4F889A9C207EAE72011AF1BA9DA70046C5404386_inline(/*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_3;
		L_3 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_2, /*hidden argument*/NULL);
		bool L_4 = ___toggle0;
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_3, L_4, /*hidden argument*/NULL);
		// Interaction.Instance.enabled = toggle;
		Interaction_t41E997DF6BD70C3EAB8669FCC65D603292EF42A1 * L_5;
		L_5 = Interaction_get_Instance_m49A31B95D41F5321AA7036861F8330D6BB09A940_inline(/*hidden argument*/NULL);
		bool L_6 = ___toggle0;
		Behaviour_set_enabled_mDE415591B28853D1CD764C53CB499A2142247F32(L_5, L_6, /*hidden argument*/NULL);
		// Actions.Instance.enabled = toggle;
		Actions_tF16D3C582B8D8DF5DD96B758A4D691F304BE44BB * L_7;
		L_7 = Actions_get_Instance_mFCF21C897955B911E7D3B36BE438793642556B1C_inline(/*hidden argument*/NULL);
		bool L_8 = ___toggle0;
		Behaviour_set_enabled_mDE415591B28853D1CD764C53CB499A2142247F32(L_7, L_8, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void GameManager::PositionCinematicCamera(UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameManager_PositionCinematicCamera_mA50236AF87D5EB57368C70AF4CFA8B1B243C8F16 (GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * __this, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___target0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// CameraTarget = target;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0 = ___target0;
		GameManager_set_CameraTarget_m40A31328DAA4F5876B52EDE84970FE6B7E686919_inline(__this, L_0, /*hidden argument*/NULL);
		// if(target != null)
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1 = ___target0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_1, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0031;
		}
	}
	{
		// cutSceneCamera.transform.position = target.position + cameraOffset;
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_3 = __this->get_cutSceneCamera_9();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_4;
		L_4 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_3, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_5 = ___target0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6;
		L_6 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_5, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7 = __this->get_cameraOffset_10();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8;
		L_8 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_6, L_7, /*hidden argument*/NULL);
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_4, L_8, /*hidden argument*/NULL);
	}

IL_0031:
	{
		// }
		return;
	}
}
// System.Collections.IEnumerator GameManager::PlayCutScene(UnityEngine.Playables.PlayableAsset)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* GameManager_PlayCutScene_mEE157DD535C601E8C248C3286884E2622B9E5851 (GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * __this, PlayableAsset_t5AD1606B76C9753A7F4C6B1061193F581023F137 * ___cutScene0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPlayCutSceneU3Ed__46_tC2D2594FAEFE8718E713CAC6338A1701F09CADAA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CPlayCutSceneU3Ed__46_tC2D2594FAEFE8718E713CAC6338A1701F09CADAA * L_0 = (U3CPlayCutSceneU3Ed__46_tC2D2594FAEFE8718E713CAC6338A1701F09CADAA *)il2cpp_codegen_object_new(U3CPlayCutSceneU3Ed__46_tC2D2594FAEFE8718E713CAC6338A1701F09CADAA_il2cpp_TypeInfo_var);
		U3CPlayCutSceneU3Ed__46__ctor_m4E4D899A2AEC184002EB6F6615F66D0C16DB3B84(L_0, 0, /*hidden argument*/NULL);
		U3CPlayCutSceneU3Ed__46_tC2D2594FAEFE8718E713CAC6338A1701F09CADAA * L_1 = L_0;
		L_1->set_U3CU3E4__this_2(__this);
		U3CPlayCutSceneU3Ed__46_tC2D2594FAEFE8718E713CAC6338A1701F09CADAA * L_2 = L_1;
		PlayableAsset_t5AD1606B76C9753A7F4C6B1061193F581023F137 * L_3 = ___cutScene0;
		L_2->set_cutScene_3(L_3);
		return L_2;
	}
}
// System.Void GameManager::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameManager__ctor_mE8666F6D0CA9C31E16B719F79780DC4B0245B64D (GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean Gate::get_Locked()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Gate_get_Locked_mD0F7FDCDBBF9A7B92DB3A64C563342F48D302F4B (Gate_tAA47F9BD94C2B1C12EADC5ECCAF1CAD07A035F45 * __this, const RuntimeMethod* method)
{
	{
		// public bool Locked { get; private set; } = true;
		bool L_0 = __this->get_U3CLockedU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void Gate::set_Locked(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Gate_set_Locked_mC4655A379C367E8C658ED505B433698BC32E7765 (Gate_tAA47F9BD94C2B1C12EADC5ECCAF1CAD07A035F45 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// public bool Locked { get; private set; } = true;
		bool L_0 = ___value0;
		__this->set_U3CLockedU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.Void Gate::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Gate_Awake_m9E870AA2F62C390A3CA0F5AB8E75B744519FF1FC (Gate_tAA47F9BD94C2B1C12EADC5ECCAF1CAD07A035F45 * __this, const RuntimeMethod* method)
{
	{
		// gateOpen = Vector3.zero;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0;
		L_0 = Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6(/*hidden argument*/NULL);
		__this->set_gateOpen_9(L_0);
		// }
		return;
	}
}
// System.Void Gate::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Gate_Start_m27BE6AC1FF73604405C96D634D19AF13DD9A1234 (Gate_tAA47F9BD94C2B1C12EADC5ECCAF1CAD07A035F45 * __this, const RuntimeMethod* method)
{
	{
		// WorldMap.Instance.AddInteraction(gameObject);
		WorldMap_t01A07DDC9EA386C88D5814CF18E0F37CAF05270C * L_0;
		L_0 = WorldMap_get_Instance_m119402AD39383BC71435F9F36C3219A2281289CA_inline(/*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1;
		L_1 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		bool L_2;
		L_2 = WorldMap_AddInteraction_mF2CC83A625FCFB963745D016D72BC262AB4A94F4(L_0, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Gate::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Gate_Update_mBA10874B7CBA45BC8F8BAFB54DD6DAE131DC3A85 (Gate_tAA47F9BD94C2B1C12EADC5ECCAF1CAD07A035F45 * __this, const RuntimeMethod* method)
{
	{
		// if(Locked == false && (transform.localScale.z > 0))
		bool L_0;
		L_0 = Gate_get_Locked_mD0F7FDCDBBF9A7B92DB3A64C563342F48D302F4B_inline(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_004c;
		}
	}
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1;
		L_1 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		L_2 = Transform_get_localScale_mD9DF6CA81108C2A6002B5EA2BE25A6CD2723D046(L_1, /*hidden argument*/NULL);
		float L_3 = L_2.get_z_4();
		if ((!(((float)L_3) > ((float)(0.0f)))))
		{
			goto IL_004c;
		}
	}
	{
		// transform.localScale = Vector3.Lerp(transform.localScale, gateOpen, openTime * Time.deltaTime); //this scales the object down (the gate) to the pre defined zero state transforming it from its current size, over the variable time of openTime
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_4;
		L_4 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_5;
		L_5 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6;
		L_6 = Transform_get_localScale_mD9DF6CA81108C2A6002B5EA2BE25A6CD2723D046(L_5, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7 = __this->get_gateOpen_9();
		float L_8 = __this->get_openTime_10();
		float L_9;
		L_9 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10;
		L_10 = Vector3_Lerp_m8E095584FFA10CF1D3EABCD04F4C83FB82EC5524_inline(L_6, L_7, ((float)il2cpp_codegen_multiply((float)L_8, (float)L_9)), /*hidden argument*/NULL);
		Transform_set_localScale_mF4D1611E48D1BA7566A1E166DC2DACF3ADD8BA3A(L_4, L_10, /*hidden argument*/NULL);
	}

IL_004c:
	{
		// }
		return;
	}
}
// System.String Gate::Inspect()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Gate_Inspect_m60870B3FA73AADCC9875C320E9D12E3A13411E03 (Gate_tAA47F9BD94C2B1C12EADC5ECCAF1CAD07A035F45 * __this, const RuntimeMethod* method)
{
	{
		// return tooltipText;
		String_t* L_0 = __this->get_tooltipText_4();
		return L_0;
	}
}
// System.Boolean Gate::Unlock()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Gate_Unlock_m4D20EBE5FC66340D63BB8D9157A0C09DC6CDC5F4 (Gate_tAA47F9BD94C2B1C12EADC5ECCAF1CAD07A035F45 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KeyType_t9EEB0AD19791311F275DCC1CE12BD7AAD39A3C13_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4D43BD5775F2AB357D4F679F6BE3377EF52F5BAD);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA91C7F3122FF007BB627E35A579A9AC513F8A14A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC9E357C460CFAAA857877CCC0114469CFFEF676B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD8126D7FB6A15D1DF1ED742A3550A992581F56F8);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE8D08BFECF9F5FBAAB31AB2F1B32784ADAB8F601);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF19A0A1611C3B19235821CD4D354C47A2B95313F);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if(Locked == true)  //checks that the gate being interacted with is locked
		bool L_0;
		L_0 = Gate_get_Locked_mD0F7FDCDBBF9A7B92DB3A64C563342F48D302F4B_inline(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_00b0;
		}
	}
	{
		// if(Interaction.Instance.HasKey(type) == true) //checks that you have the key
		Interaction_t41E997DF6BD70C3EAB8669FCC65D603292EF42A1 * L_1;
		L_1 = Interaction_get_Instance_m49A31B95D41F5321AA7036861F8330D6BB09A940_inline(/*hidden argument*/NULL);
		int32_t L_2 = __this->get_type_5();
		bool L_3;
		L_3 = Interaction_HasKey_m0E72EF86A1EF62EB1478BD83D9D20B904CBE76A1(L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0099;
		}
	}
	{
		// Locked = false;
		Gate_set_Locked_mC4655A379C367E8C658ED505B433698BC32E7765_inline(__this, (bool)0, /*hidden argument*/NULL);
		// Debug.Log("Cutscene " + type + "has been triggered");
		int32_t* L_4 = __this->get_address_of_type_5();
		RuntimeObject * L_5 = Box(KeyType_t9EEB0AD19791311F275DCC1CE12BD7AAD39A3C13_il2cpp_TypeInfo_var, L_4);
		String_t* L_6;
		L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_5);
		*L_4 = *(int32_t*)UnBox(L_5);
		String_t* L_7;
		L_7 = String_Concat_m89EAB4C6A96B0E5C3F87300D6BE78D386B9EFC44(_stringLiteralC9E357C460CFAAA857877CCC0114469CFFEF676B, L_6, _stringLiteralA91C7F3122FF007BB627E35A579A9AC513F8A14A, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(L_7, /*hidden argument*/NULL);
		// if( type == Key.KeyType.a)
		int32_t L_8 = __this->get_type_5();
		if (L_8)
		{
			goto IL_0075;
		}
	}
	{
		// manager.StartCoroutine(manager.PlayCutScene(manager.gateACutScene));  //plays the cutscene for gate A
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_9 = __this->get_manager_7();
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_10 = __this->get_manager_7();
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_11 = __this->get_manager_7();
		PlayableAsset_t5AD1606B76C9753A7F4C6B1061193F581023F137 * L_12 = L_11->get_gateACutScene_13();
		RuntimeObject* L_13;
		L_13 = GameManager_PlayCutScene_mEE157DD535C601E8C248C3286884E2622B9E5851(L_10, L_12, /*hidden argument*/NULL);
		Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * L_14;
		L_14 = MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719(L_9, L_13, /*hidden argument*/NULL);
		// }
		goto IL_0097;
	}

IL_0075:
	{
		// manager.StartCoroutine(manager.PlayCutScene(manager.gateBCutScene));  //plays the cutscene
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_15 = __this->get_manager_7();
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_16 = __this->get_manager_7();
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_17 = __this->get_manager_7();
		PlayableAsset_t5AD1606B76C9753A7F4C6B1061193F581023F137 * L_18 = L_17->get_gateBCutScene_14();
		RuntimeObject* L_19;
		L_19 = GameManager_PlayCutScene_mEE157DD535C601E8C248C3286884E2622B9E5851(L_16, L_18, /*hidden argument*/NULL);
		Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * L_20;
		L_20 = MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719(L_15, L_19, /*hidden argument*/NULL);
	}

IL_0097:
	{
		// return true;
		return (bool)1;
	}

IL_0099:
	{
		// tooltipText = "You dont have the key, you need to find it";
		__this->set_tooltipText_4(_stringLiteralD8126D7FB6A15D1DF1ED742A3550A992581F56F8);
		// Debug.Log("Key is missing");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(_stringLiteralE8D08BFECF9F5FBAAB31AB2F1B32784ADAB8F601, /*hidden argument*/NULL);
		// }
		goto IL_00c5;
	}

IL_00b0:
	{
		// tooltipText = "This gate was already unlocked";
		__this->set_tooltipText_4(_stringLiteral4D43BD5775F2AB357D4F679F6BE3377EF52F5BAD);
		// Debug.Log("is Already Unlocked");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(_stringLiteralF19A0A1611C3B19235821CD4D354C47A2B95313F, /*hidden argument*/NULL);
	}

IL_00c5:
	{
		// return false;
		return (bool)0;
	}
}
// System.Void Gate::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Gate__ctor_mEACCC67CFE6FC196994848EE1207B13283FB518E (Gate_tAA47F9BD94C2B1C12EADC5ECCAF1CAD07A035F45 * __this, const RuntimeMethod* method)
{
	{
		// public bool Locked { get; private set; } = true;
		__this->set_U3CLockedU3Ek__BackingField_6((bool)1);
		// private float openTime = 2.5f;
		__this->set_openTime_10((2.5f));
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Interaction Interaction::get_Instance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Interaction_t41E997DF6BD70C3EAB8669FCC65D603292EF42A1 * Interaction_get_Instance_m49A31B95D41F5321AA7036861F8330D6BB09A940 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Interaction_t41E997DF6BD70C3EAB8669FCC65D603292EF42A1_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static Interaction Instance { get; private set; }
		Interaction_t41E997DF6BD70C3EAB8669FCC65D603292EF42A1 * L_0 = ((Interaction_t41E997DF6BD70C3EAB8669FCC65D603292EF42A1_StaticFields*)il2cpp_codegen_static_fields_for(Interaction_t41E997DF6BD70C3EAB8669FCC65D603292EF42A1_il2cpp_TypeInfo_var))->get_U3CInstanceU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void Interaction::set_Instance(Interaction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Interaction_set_Instance_m1C2D19F892D930D83F98A19E2D65EF47701A7C51 (Interaction_t41E997DF6BD70C3EAB8669FCC65D603292EF42A1 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Interaction_t41E997DF6BD70C3EAB8669FCC65D603292EF42A1_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static Interaction Instance { get; private set; }
		Interaction_t41E997DF6BD70C3EAB8669FCC65D603292EF42A1 * L_0 = ___value0;
		((Interaction_t41E997DF6BD70C3EAB8669FCC65D603292EF42A1_StaticFields*)il2cpp_codegen_static_fields_for(Interaction_t41E997DF6BD70C3EAB8669FCC65D603292EF42A1_il2cpp_TypeInfo_var))->set_U3CInstanceU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.Void Interaction::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Interaction_Awake_m07EB608CF485E6B7DE48D22C0FF5DF5558C9BE45 (Interaction_t41E997DF6BD70C3EAB8669FCC65D603292EF42A1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if(Instance == null)
		Interaction_t41E997DF6BD70C3EAB8669FCC65D603292EF42A1 * L_0;
		L_0 = Interaction_get_Instance_m49A31B95D41F5321AA7036861F8330D6BB09A940_inline(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		// Instance = this;
		Interaction_set_Instance_m1C2D19F892D930D83F98A19E2D65EF47701A7C51_inline(__this, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0014:
	{
		// enabled = false;
		Behaviour_set_enabled_mDE415591B28853D1CD764C53CB499A2142247F32(__this, (bool)0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Interaction::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Interaction_Update_m19CE4CF0CFFA37FF15C650076DB2F1CF3F677899 (Interaction_t41E997DF6BD70C3EAB8669FCC65D603292EF42A1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_TryGetComponent_TisIInteraction_t1C1C3E6C29AFB203F5021CA7300B6FEA0EFE91FE_mEC43AA2983C9283A4104774AD183EECD789EFD70_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralAF8E649A3EF1AB74A43C9F88015FA68AC719215A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF5260CA4F761D522A3E8A6463DF1FAC59EBA2525);
		s_Il2CppMethodInitialized = true;
	}
	RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89  V_0;
	memset((&V_0), 0, sizeof(V_0));
	RuntimeObject* V_1 = NULL;
	{
		// if (InteractionMenu.Instance.gameObject.activeSelf == false)
		InteractionMenu_t7C2715221BAC6E0BB83B07EF82F68C6AA34129CB * L_0;
		L_0 = InteractionMenu_get_Instance_m60C03978FEDDC7D708EB1C5C30EBD081344385FF_inline(/*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1;
		L_1 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_0, /*hidden argument*/NULL);
		bool L_2;
		L_2 = GameObject_get_activeSelf_m4865097C24FB29F3C31F5C30619AF242297F23EE(L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_00d0;
		}
	}
	{
		// if (ActionMenu.Instance.gameObject.activeSelf == false && WorldMap.Instance.legend.gameObject.activeSelf == false)
		ActionMenu_tE3C589CB5B0B532FF5504CF0D558D489281BED01 * L_3;
		L_3 = ActionMenu_get_Instance_m0A358E0ED4326BDF16E867E22F1E5BF5B9AD2DC0_inline(/*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_4;
		L_4 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_3, /*hidden argument*/NULL);
		bool L_5;
		L_5 = GameObject_get_activeSelf_m4865097C24FB29F3C31F5C30619AF242297F23EE(L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_00e6;
		}
	}
	{
		WorldMap_t01A07DDC9EA386C88D5814CF18E0F37CAF05270C * L_6;
		L_6 = WorldMap_get_Instance_m119402AD39383BC71435F9F36C3219A2281289CA_inline(/*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_7 = L_6->get_legend_5();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_8;
		L_8 = GameObject_get_gameObject_mD5FFECF7C3AC5039E847DF7A8842478539B701D6(L_7, /*hidden argument*/NULL);
		bool L_9;
		L_9 = GameObject_get_activeSelf_m4865097C24FB29F3C31F5C30619AF242297F23EE(L_8, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_00e6;
		}
	}
	{
		// if (Input.GetButtonDown("Interaction") == true)
		bool L_10;
		L_10 = Input_GetButtonDown_m2001112EBCA3D5C7B0344EF62C896667F7E67DDF(_stringLiteralAF8E649A3EF1AB74A43C9F88015FA68AC719215A, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_00e6;
		}
	}
	{
		// if (Physics.SphereCast(transform.position, 0.5f, transform.forward, out RaycastHit hit, distance) == true)
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_11;
		L_11 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		L_12 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_11, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_13;
		L_13 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_14;
		L_14 = Transform_get_forward_mD850B9ECF892009E3485408DC0D375165B7BF053(L_13, /*hidden argument*/NULL);
		float L_15 = __this->get_distance_4();
		bool L_16;
		L_16 = Physics_SphereCast_m2E200DFD71A597968D4F50B8A6284F2E622B4C57(L_12, (0.5f), L_14, (RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)(&V_0), L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00e6;
		}
	}
	{
		// if (hit.collider.TryGetComponent(out IInteraction interaction) == true)
		Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * L_17;
		L_17 = RaycastHit_get_collider_m13A3DE16FBC631E0A1F987E0B22CE70AF8AB539E((RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)(&V_0), /*hidden argument*/NULL);
		bool L_18;
		L_18 = Component_TryGetComponent_TisIInteraction_t1C1C3E6C29AFB203F5021CA7300B6FEA0EFE91FE_mEC43AA2983C9283A4104774AD183EECD789EFD70(L_17, (RuntimeObject**)(&V_1), /*hidden argument*/Component_TryGetComponent_TisIInteraction_t1C1C3E6C29AFB203F5021CA7300B6FEA0EFE91FE_mEC43AA2983C9283A4104774AD183EECD789EFD70_RuntimeMethod_var);
		if (!L_18)
		{
			goto IL_00e6;
		}
	}
	{
		// Debug.DrawRay(transform.position, transform.forward * distance, Color.green, 0.25f);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_19;
		L_19 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_20;
		L_20 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_19, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_21;
		L_21 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_22;
		L_22 = Transform_get_forward_mD850B9ECF892009E3485408DC0D375165B7BF053(L_21, /*hidden argument*/NULL);
		float L_23 = __this->get_distance_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_24;
		L_24 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_22, L_23, /*hidden argument*/NULL);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_25;
		L_25 = Color_get_green_mFF9BD42534D385A0717B1EAD083ADF08712984B9(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_DrawRay_m3954B3FFA675C0660ED438E8B705B45EDE393C60(L_20, L_24, L_25, (0.25f), /*hidden argument*/NULL);
		// Debug.Log("Interaction detected");
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(_stringLiteralF5260CA4F761D522A3E8A6463DF1FAC59EBA2525, /*hidden argument*/NULL);
		// InteractionMenu.Instance.Activate(interaction);
		InteractionMenu_t7C2715221BAC6E0BB83B07EF82F68C6AA34129CB * L_26;
		L_26 = InteractionMenu_get_Instance_m60C03978FEDDC7D708EB1C5C30EBD081344385FF_inline(/*hidden argument*/NULL);
		RuntimeObject* L_27 = V_1;
		InteractionMenu_Activate_m45B84D655B2FCBC3A0429DDEB4F1B8393A397160(L_26, L_27, /*hidden argument*/NULL);
		// }
		return;
	}

IL_00d0:
	{
		// if(Input.GetButtonUp("Interaction") == true)
		bool L_28;
		L_28 = Input_GetButtonUp_m15AA6B42BD0DDCC7802346E49F30653D750260DD(_stringLiteralAF8E649A3EF1AB74A43C9F88015FA68AC719215A, /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_00e6;
		}
	}
	{
		// InteractionMenu.Instance.Deactivate();
		InteractionMenu_t7C2715221BAC6E0BB83B07EF82F68C6AA34129CB * L_29;
		L_29 = InteractionMenu_get_Instance_m60C03978FEDDC7D708EB1C5C30EBD081344385FF_inline(/*hidden argument*/NULL);
		InteractionMenu_Deactivate_m912B95CC8E501BE8859EB5BB477A9D0A5DCFE10A(L_29, /*hidden argument*/NULL);
	}

IL_00e6:
	{
		// }
		return;
	}
}
// System.Boolean Interaction::Addkey(Key/KeyType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Interaction_Addkey_mCC3293ED3A6334A742C2196DF288E220BF1073A5 (Interaction_t41E997DF6BD70C3EAB8669FCC65D603292EF42A1 * __this, int32_t ___key0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m6470E90BACC0611067C594D4D8259D1BC40DBB5A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Contains_m69468F32EA1D0A3E5BC817F0CE7F0560332383E6_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if(keyRing.Contains(key) == false)
		List_1_tD99207A373A130469C76CB8634AADFB31FF317AE * L_0 = __this->get_keyRing_6();
		int32_t L_1 = ___key0;
		bool L_2;
		L_2 = List_1_Contains_m69468F32EA1D0A3E5BC817F0CE7F0560332383E6(L_0, L_1, /*hidden argument*/List_1_Contains_m69468F32EA1D0A3E5BC817F0CE7F0560332383E6_RuntimeMethod_var);
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		// keyRing.Add(key);
		List_1_tD99207A373A130469C76CB8634AADFB31FF317AE * L_3 = __this->get_keyRing_6();
		int32_t L_4 = ___key0;
		List_1_Add_m6470E90BACC0611067C594D4D8259D1BC40DBB5A(L_3, L_4, /*hidden argument*/List_1_Add_m6470E90BACC0611067C594D4D8259D1BC40DBB5A_RuntimeMethod_var);
		// return true;
		return (bool)1;
	}

IL_001c:
	{
		// return false;
		return (bool)0;
	}
}
// System.Boolean Interaction::HasKey(Key/KeyType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Interaction_HasKey_m0E72EF86A1EF62EB1478BD83D9D20B904CBE76A1 (Interaction_t41E997DF6BD70C3EAB8669FCC65D603292EF42A1 * __this, int32_t ___key0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Contains_m69468F32EA1D0A3E5BC817F0CE7F0560332383E6_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return keyRing.Contains(key);
		List_1_tD99207A373A130469C76CB8634AADFB31FF317AE * L_0 = __this->get_keyRing_6();
		int32_t L_1 = ___key0;
		bool L_2;
		L_2 = List_1_Contains_m69468F32EA1D0A3E5BC817F0CE7F0560332383E6(L_0, L_1, /*hidden argument*/List_1_Contains_m69468F32EA1D0A3E5BC817F0CE7F0560332383E6_RuntimeMethod_var);
		return L_2;
	}
}
// System.Void Interaction::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Interaction__ctor_m70406E11A94FE15ADD18C51085F78B5463FACEB8 (Interaction_t41E997DF6BD70C3EAB8669FCC65D603292EF42A1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m612E7D379DA89488D1FC2AB1FB9E2BD2AAA9AABA_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_tD99207A373A130469C76CB8634AADFB31FF317AE_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public float distance = 2;
		__this->set_distance_4((2.0f));
		// private List<Key.KeyType> keyRing = new List<Key.KeyType>();
		List_1_tD99207A373A130469C76CB8634AADFB31FF317AE * L_0 = (List_1_tD99207A373A130469C76CB8634AADFB31FF317AE *)il2cpp_codegen_object_new(List_1_tD99207A373A130469C76CB8634AADFB31FF317AE_il2cpp_TypeInfo_var);
		List_1__ctor_m612E7D379DA89488D1FC2AB1FB9E2BD2AAA9AABA(L_0, /*hidden argument*/List_1__ctor_m612E7D379DA89488D1FC2AB1FB9E2BD2AAA9AABA_RuntimeMethod_var);
		__this->set_keyRing_6(L_0);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// InteractionMenu InteractionMenu::get_Instance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InteractionMenu_t7C2715221BAC6E0BB83B07EF82F68C6AA34129CB * InteractionMenu_get_Instance_m60C03978FEDDC7D708EB1C5C30EBD081344385FF (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InteractionMenu_t7C2715221BAC6E0BB83B07EF82F68C6AA34129CB_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static InteractionMenu Instance { get; private set; }
		InteractionMenu_t7C2715221BAC6E0BB83B07EF82F68C6AA34129CB * L_0 = ((InteractionMenu_t7C2715221BAC6E0BB83B07EF82F68C6AA34129CB_StaticFields*)il2cpp_codegen_static_fields_for(InteractionMenu_t7C2715221BAC6E0BB83B07EF82F68C6AA34129CB_il2cpp_TypeInfo_var))->get_U3CInstanceU3Ek__BackingField_10();
		return L_0;
	}
}
// System.Void InteractionMenu::set_Instance(InteractionMenu)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InteractionMenu_set_Instance_m64EBB469675FBC80A34E3E4457B021DE07FED4EA (InteractionMenu_t7C2715221BAC6E0BB83B07EF82F68C6AA34129CB * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InteractionMenu_t7C2715221BAC6E0BB83B07EF82F68C6AA34129CB_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static InteractionMenu Instance { get; private set; }
		InteractionMenu_t7C2715221BAC6E0BB83B07EF82F68C6AA34129CB * L_0 = ___value0;
		((InteractionMenu_t7C2715221BAC6E0BB83B07EF82F68C6AA34129CB_StaticFields*)il2cpp_codegen_static_fields_for(InteractionMenu_t7C2715221BAC6E0BB83B07EF82F68C6AA34129CB_il2cpp_TypeInfo_var))->set_U3CInstanceU3Ek__BackingField_10(L_0);
		return;
	}
}
// System.Void InteractionMenu::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InteractionMenu_Awake_mDD193A70CB13CF5429EFF02481B45135665A2889 (InteractionMenu_t7C2715221BAC6E0BB83B07EF82F68C6AA34129CB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralCEB37FDDA4CCFF50204045296E416398C098BA6A);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if(Instance == null)
		InteractionMenu_t7C2715221BAC6E0BB83B07EF82F68C6AA34129CB * L_0;
		L_0 = InteractionMenu_get_Instance_m60C03978FEDDC7D708EB1C5C30EBD081344385FF_inline(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0015;
		}
	}
	{
		// Instance = this;
		InteractionMenu_set_Instance_m64EBB469675FBC80A34E3E4457B021DE07FED4EA_inline(__this, /*hidden argument*/NULL);
		// }
		goto IL_001c;
	}

IL_0015:
	{
		// enabled = false;
		Behaviour_set_enabled_mDE415591B28853D1CD764C53CB499A2142247F32(__this, (bool)0, /*hidden argument*/NULL);
	}

IL_001c:
	{
		// if (Directory.Exists(Application.dataPath + "/Resources/PhotoGallery") == false)
		String_t* L_2;
		L_2 = Application_get_dataPath_m026509C15A1E58FC6461EE7EC336EC18C9C2271E(/*hidden argument*/NULL);
		String_t* L_3;
		L_3 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(L_2, _stringLiteralCEB37FDDA4CCFF50204045296E416398C098BA6A, /*hidden argument*/NULL);
		bool L_4;
		L_4 = Directory_Exists_m17E38B91F6D9A0064D614FF2237BBFC0127468FE(L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0047;
		}
	}
	{
		// Directory.CreateDirectory(Application.dataPath + "/Resources/PhotoGallery");
		String_t* L_5;
		L_5 = Application_get_dataPath_m026509C15A1E58FC6461EE7EC336EC18C9C2271E(/*hidden argument*/NULL);
		String_t* L_6;
		L_6 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(L_5, _stringLiteralCEB37FDDA4CCFF50204045296E416398C098BA6A, /*hidden argument*/NULL);
		DirectoryInfo_t4EF3610F45F0D234800D01ADA8F3F476AE0CF5CD * L_7;
		L_7 = Directory_CreateDirectory_m38040338519C48CE52137CC146372A153D5C6A7A(L_6, /*hidden argument*/NULL);
	}

IL_0047:
	{
		// }
		return;
	}
}
// System.Void InteractionMenu::UpdateUISelection()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InteractionMenu_UpdateUISelection_mCDE9D19A0556DB8B15CBC355E369A362C18D2A2D (InteractionMenu_t7C2715221BAC6E0BB83B07EF82F68C6AA34129CB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral11E7C4352B11858AEFB14F47D96849AC6696EC2B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA887E0FF7814A894C42824B8F164C9E3F0509345);
		s_Il2CppMethodInitialized = true;
	}
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  V_1;
	memset((&V_1), 0, sizeof(V_1));
	int32_t V_2 = 0;
	{
		// ColorUtility.TryParseHtmlString("#FFA047", out selectedOrange);
		bool L_0;
		L_0 = ColorUtility_TryParseHtmlString_m69BEFAF655920930399471B79CF668FC3BAD4069(_stringLiteral11E7C4352B11858AEFB14F47D96849AC6696EC2B, (Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 *)(&V_0), /*hidden argument*/NULL);
		// ColorUtility.TryParseHtmlString("#3A74A6", out unselectedBlue);
		bool L_1;
		L_1 = ColorUtility_TryParseHtmlString_m69BEFAF655920930399471B79CF668FC3BAD4069(_stringLiteralA887E0FF7814A894C42824B8F164C9E3F0509345, (Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 *)(&V_1), /*hidden argument*/NULL);
		// switch (optionIndex)
		int32_t L_2 = __this->get_optionIndex_8();
		V_2 = L_2;
		int32_t L_3 = V_2;
		if (!L_3)
		{
			goto IL_0029;
		}
	}
	{
		int32_t L_4 = V_2;
		if ((((int32_t)L_4) == ((int32_t)1)))
		{
			goto IL_0042;
		}
	}
	{
		return;
	}

IL_0029:
	{
		// optionA.color = selectedOrange;
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_5 = __this->get_optionA_4();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_6 = V_0;
		VirtActionInvoker1< Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_5, L_6);
		// optionB.color = unselectedBlue;
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_7 = __this->get_optionB_5();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_8 = V_1;
		VirtActionInvoker1< Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_7, L_8);
		// break;
		return;
	}

IL_0042:
	{
		// optionA.color = unselectedBlue;
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_9 = __this->get_optionA_4();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_10 = V_1;
		VirtActionInvoker1< Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_9, L_10);
		// optionB.color = selectedOrange;
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_11 = __this->get_optionB_5();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_12 = V_0;
		VirtActionInvoker1< Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_11, L_12);
		// }
		return;
	}
}
// System.Void InteractionMenu::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InteractionMenu_Start_mCFC55383AE7C50EEEBA53097C75A503474091A25 (InteractionMenu_t7C2715221BAC6E0BB83B07EF82F68C6AA34129CB * __this, const RuntimeMethod* method)
{
	{
		// Deactivate();
		InteractionMenu_Deactivate_m912B95CC8E501BE8859EB5BB477A9D0A5DCFE10A(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void InteractionMenu::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InteractionMenu_Update_m89548D247EF54D378478EBF006756F060DD3F368 (InteractionMenu_t7C2715221BAC6E0BB83B07EF82F68C6AA34129CB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE5368AE200A37FDFED8AA9B3CB8C7E26E81CA911);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// if(gameObject.activeSelf == true && timer == -1)
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0;
		L_0 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		bool L_1;
		L_1 = GameObject_get_activeSelf_m4865097C24FB29F3C31F5C30619AF242297F23EE(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_006e;
		}
	}
	{
		float L_2 = __this->get_timer_9();
		if ((!(((float)L_2) == ((float)(-1.0f)))))
		{
			goto IL_006e;
		}
	}
	{
		// int axis = Mathf.FloorToInt(Input.GetAxis("Menu Selection"));
		float L_3;
		L_3 = Input_GetAxis_m939297DEB2ECF8D8D09AD66EB69979AAD2B62326(_stringLiteralE5368AE200A37FDFED8AA9B3CB8C7E26E81CA911, /*hidden argument*/NULL);
		int32_t L_4;
		L_4 = Mathf_FloorToInt_m9164D538D17B8C3C8A6C4E4FA95032F757D9091E(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		// if(axis != 0)
		int32_t L_5 = V_0;
		if (!L_5)
		{
			goto IL_006e;
		}
	}
	{
		// timer = 0;
		__this->set_timer_9((0.0f));
		// optionIndex += axis;
		int32_t L_6 = __this->get_optionIndex_8();
		int32_t L_7 = V_0;
		__this->set_optionIndex_8(((int32_t)il2cpp_codegen_add((int32_t)L_6, (int32_t)L_7)));
		// if(optionIndex < 0)
		int32_t L_8 = __this->get_optionIndex_8();
		if ((((int32_t)L_8) >= ((int32_t)0)))
		{
			goto IL_0058;
		}
	}
	{
		// optionIndex = 1;
		__this->set_optionIndex_8(1);
		// }
		goto IL_0068;
	}

IL_0058:
	{
		// else if(optionIndex > 1)
		int32_t L_9 = __this->get_optionIndex_8();
		if ((((int32_t)L_9) <= ((int32_t)1)))
		{
			goto IL_0068;
		}
	}
	{
		// optionIndex = 0;
		__this->set_optionIndex_8(0);
	}

IL_0068:
	{
		// UpdateUISelection();
		InteractionMenu_UpdateUISelection_mCDE9D19A0556DB8B15CBC355E369A362C18D2A2D(__this, /*hidden argument*/NULL);
	}

IL_006e:
	{
		// if(timer > - 1)
		float L_10 = __this->get_timer_9();
		if ((!(((float)L_10) > ((float)(-1.0f)))))
		{
			goto IL_00a5;
		}
	}
	{
		// timer += Time.deltaTime;
		float L_11 = __this->get_timer_9();
		float L_12;
		L_12 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		__this->set_timer_9(((float)il2cpp_codegen_add((float)L_11, (float)L_12)));
		// if(timer > 0.25f)
		float L_13 = __this->get_timer_9();
		if ((!(((float)L_13) > ((float)(0.25f)))))
		{
			goto IL_00a5;
		}
	}
	{
		// timer = -1f;
		__this->set_timer_9((-1.0f));
	}

IL_00a5:
	{
		// }
		return;
	}
}
// System.Void InteractionMenu::Photograph(IFauna)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InteractionMenu_Photograph_mDF8C7A662AA42E89A39027BBAB69C54692736571 (InteractionMenu_t7C2715221BAC6E0BB83B07EF82F68C6AA34129CB * __this, RuntimeObject* ___fauna0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DirectoryInfo_t4EF3610F45F0D234800D01ADA8F3F476AE0CF5CD_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IFauna_tECBAA4EFAA9D07BFB7CB55CEE86755A301872FC5_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralCEB37FDDA4CCFF50204045296E416398C098BA6A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDCB6AFB2D0F0FAD8B76294039022E7626E5D71BC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF4164D1DF7A1099D188B845751FF90B8C55A023D);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	FileInfoU5BU5D_tB6A5BC6AD9BEC388BF3236B4E8FBB7760F96080B* V_2 = NULL;
	int32_t V_3 = 0;
	{
		// string gallery = Application.dataPath + "/Resources/PhotoGallery"; //sets the datapath for where these files will go
		String_t* L_0;
		L_0 = Application_get_dataPath_m026509C15A1E58FC6461EE7EC336EC18C9C2271E(/*hidden argument*/NULL);
		String_t* L_1;
		L_1 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(L_0, _stringLiteralCEB37FDDA4CCFF50204045296E416398C098BA6A, /*hidden argument*/NULL);
		V_0 = L_1;
		// DirectoryInfo dir = new DirectoryInfo(gallery);
		String_t* L_2 = V_0;
		DirectoryInfo_t4EF3610F45F0D234800D01ADA8F3F476AE0CF5CD * L_3 = (DirectoryInfo_t4EF3610F45F0D234800D01ADA8F3F476AE0CF5CD *)il2cpp_codegen_object_new(DirectoryInfo_t4EF3610F45F0D234800D01ADA8F3F476AE0CF5CD_il2cpp_TypeInfo_var);
		DirectoryInfo__ctor_m5F307F7E646135FC323F81EA93C36CC0CF6023A6(L_3, L_2, /*hidden argument*/NULL);
		// FileInfo[] files = dir.GetFiles("*.png");
		FileInfoU5BU5D_tB6A5BC6AD9BEC388BF3236B4E8FBB7760F96080B* L_4;
		L_4 = DirectoryInfo_GetFiles_mB4AE0AEC3ABF4A3541A0B68C46C802A78730C5FC(L_3, _stringLiteralDCB6AFB2D0F0FAD8B76294039022E7626E5D71BC, /*hidden argument*/NULL);
		// int count = 0;
		V_1 = 0;
		// foreach (FileInfo info in files)
		V_2 = L_4;
		V_3 = 0;
		goto IL_0043;
	}

IL_0027:
	{
		// foreach (FileInfo info in files)
		FileInfoU5BU5D_tB6A5BC6AD9BEC388BF3236B4E8FBB7760F96080B* L_5 = V_2;
		int32_t L_6 = V_3;
		int32_t L_7 = L_6;
		FileInfo_t6C8B2EAA1E23F9E6D7C287C58E4EEEB2049ABAB9 * L_8 = (L_5)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_7));
		// if(info.Name.Contains("UWW_") == true)  //checks the count of images and adds 1 more
		String_t* L_9;
		L_9 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.IO.FileSystemInfo::get_Name() */, L_8);
		bool L_10;
		L_10 = String_Contains_mA26BDCCE8F191E8965EB8EEFC18BB4D0F85A075A(L_9, _stringLiteralF4164D1DF7A1099D188B845751FF90B8C55A023D, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_003f;
		}
	}
	{
		// count++;
		int32_t L_11 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_11, (int32_t)1));
	}

IL_003f:
	{
		int32_t L_12 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_12, (int32_t)1));
	}

IL_0043:
	{
		// foreach (FileInfo info in files)
		int32_t L_13 = V_3;
		FileInfoU5BU5D_tB6A5BC6AD9BEC388BF3236B4E8FBB7760F96080B* L_14 = V_2;
		if ((((int32_t)L_13) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_14)->max_length))))))
		{
			goto IL_0027;
		}
	}
	{
		// if(count < 30)  //if less than 30 adds a photograph to the gallery
		int32_t L_15 = V_1;
		if ((((int32_t)L_15) >= ((int32_t)((int32_t)30))))
		{
			goto IL_0056;
		}
	}
	{
		// fauna.Photograph(gallery);
		RuntimeObject* L_16 = ___fauna0;
		String_t* L_17 = V_0;
		InterfaceActionInvoker1< String_t* >::Invoke(0 /* System.Void IFauna::Photograph(System.String) */, IFauna_tECBAA4EFAA9D07BFB7CB55CEE86755A301872FC5_il2cpp_TypeInfo_var, L_16, L_17);
		// }
		return;
	}

IL_0056:
	{
		// fauna.Photograph();
		RuntimeObject* L_18 = ___fauna0;
		InterfaceActionInvoker1< String_t* >::Invoke(0 /* System.Void IFauna::Photograph(System.String) */, IFauna_tECBAA4EFAA9D07BFB7CB55CEE86755A301872FC5_il2cpp_TypeInfo_var, L_18, (String_t*)NULL);
		// }
		return;
	}
}
// System.Void InteractionMenu::Activate(IInteraction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InteractionMenu_Activate_m45B84D655B2FCBC3A0429DDEB4F1B8393A397160 (InteractionMenu_t7C2715221BAC6E0BB83B07EF82F68C6AA34129CB * __this, RuntimeObject* ___interaction0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InteractionMenu_GetInteractionAsType_TisIFauna_tECBAA4EFAA9D07BFB7CB55CEE86755A301872FC5_m4E764DDABB046069A791E256957F04B862FD2182_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InteractionMenu_GetInteractionAsType_TisIGate_tC633C584B29457590CF59B3448AEE38AC9FB33C6_mDD834F8EB0D1D7DCE6A48654C2AD746D46BB8279_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InteractionMenu_GetInteractionAsType_TisIKey_t524F44506E887E37C88431701D58645C41BD5F3C_m244F7034AC80DC277E1467E43379BC7EFEFD5F00_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InteractionMenu_GetInteractionAsType_TisIShip_tC6FB125A646115EE3BCEDE1326BF546935215C99_m22472493C6CA081B6709F1EAE1AD3E87BDAFA312_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralAA8142B38C59D66391C7A7D71BCD21A8EE61FC8B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC09F0D4A7B660E1924D2300CB0765C6FFCB6FDE4);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralCCDD2BB80FDD5E8E6A309332CA959EBF10BABCEC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE27F31821C3D549C27ECCCC1928221B8D933E5BF);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	RuntimeObject* V_2 = NULL;
	RuntimeObject* V_3 = NULL;
	{
		// currentInteraction = interaction;
		RuntimeObject* L_0 = ___interaction0;
		__this->set_currentInteraction_7(L_0);
		// if (GetInteractionAsType(out IFauna fauna) == true)
		bool L_1;
		L_1 = InteractionMenu_GetInteractionAsType_TisIFauna_tECBAA4EFAA9D07BFB7CB55CEE86755A301872FC5_m4E764DDABB046069A791E256957F04B862FD2182(__this, (RuntimeObject**)(&V_0), /*hidden argument*/InteractionMenu_GetInteractionAsType_TisIFauna_tECBAA4EFAA9D07BFB7CB55CEE86755A301872FC5_m4E764DDABB046069A791E256957F04B862FD2182_RuntimeMethod_var);
		if (!L_1)
		{
			goto IL_0036;
		}
	}
	{
		// optionBText.text = "Photograph";
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_2 = __this->get_optionBText_6();
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_2, _stringLiteralAA8142B38C59D66391C7A7D71BCD21A8EE61FC8B);
		// optionBText.color = Color.black;
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_3 = __this->get_optionBText_6();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_4;
		L_4 = Color_get_black_m67E91EB7017FC74D9AB5ADEF6B6929B7EFC9A982(/*hidden argument*/NULL);
		VirtActionInvoker1< Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_3, L_4);
		// }
		goto IL_00b8;
	}

IL_0036:
	{
		// else if (GetInteractionAsType(out IKey key) == true)
		bool L_5;
		L_5 = InteractionMenu_GetInteractionAsType_TisIKey_t524F44506E887E37C88431701D58645C41BD5F3C_m244F7034AC80DC277E1467E43379BC7EFEFD5F00(__this, (RuntimeObject**)(&V_1), /*hidden argument*/InteractionMenu_GetInteractionAsType_TisIKey_t524F44506E887E37C88431701D58645C41BD5F3C_m244F7034AC80DC277E1467E43379BC7EFEFD5F00_RuntimeMethod_var);
		if (!L_5)
		{
			goto IL_0062;
		}
	}
	{
		// optionBText.text = "Pick Up";
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_6 = __this->get_optionBText_6();
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_6, _stringLiteralE27F31821C3D549C27ECCCC1928221B8D933E5BF);
		// optionBText.color = Color.black;
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_7 = __this->get_optionBText_6();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_8;
		L_8 = Color_get_black_m67E91EB7017FC74D9AB5ADEF6B6929B7EFC9A982(/*hidden argument*/NULL);
		VirtActionInvoker1< Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_7, L_8);
		// }
		goto IL_00b8;
	}

IL_0062:
	{
		// else if (GetInteractionAsType(out IGate gate) == true)
		bool L_9;
		L_9 = InteractionMenu_GetInteractionAsType_TisIGate_tC633C584B29457590CF59B3448AEE38AC9FB33C6_mDD834F8EB0D1D7DCE6A48654C2AD746D46BB8279(__this, (RuntimeObject**)(&V_2), /*hidden argument*/InteractionMenu_GetInteractionAsType_TisIGate_tC633C584B29457590CF59B3448AEE38AC9FB33C6_mDD834F8EB0D1D7DCE6A48654C2AD746D46BB8279_RuntimeMethod_var);
		if (!L_9)
		{
			goto IL_008e;
		}
	}
	{
		// optionBText.text = "Unlock";
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_10 = __this->get_optionBText_6();
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_10, _stringLiteralCCDD2BB80FDD5E8E6A309332CA959EBF10BABCEC);
		// optionBText.color = Color.black;
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_11 = __this->get_optionBText_6();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_12;
		L_12 = Color_get_black_m67E91EB7017FC74D9AB5ADEF6B6929B7EFC9A982(/*hidden argument*/NULL);
		VirtActionInvoker1< Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_11, L_12);
		// }
		goto IL_00b8;
	}

IL_008e:
	{
		// else if (GetInteractionAsType(out IShip ship) == true)
		bool L_13;
		L_13 = InteractionMenu_GetInteractionAsType_TisIShip_tC6FB125A646115EE3BCEDE1326BF546935215C99_m22472493C6CA081B6709F1EAE1AD3E87BDAFA312(__this, (RuntimeObject**)(&V_3), /*hidden argument*/InteractionMenu_GetInteractionAsType_TisIShip_tC6FB125A646115EE3BCEDE1326BF546935215C99_m22472493C6CA081B6709F1EAE1AD3E87BDAFA312_RuntimeMethod_var);
		if (!L_13)
		{
			goto IL_00b8;
		}
	}
	{
		// optionBText.text = "Leave";
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_14 = __this->get_optionBText_6();
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_14, _stringLiteralC09F0D4A7B660E1924D2300CB0765C6FFCB6FDE4);
		// optionBText.color = Color.black;
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_15 = __this->get_optionBText_6();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_16;
		L_16 = Color_get_black_m67E91EB7017FC74D9AB5ADEF6B6929B7EFC9A982(/*hidden argument*/NULL);
		VirtActionInvoker1< Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_15, L_16);
	}

IL_00b8:
	{
		// optionIndex = 0;
		__this->set_optionIndex_8(0);
		// UpdateUISelection();
		InteractionMenu_UpdateUISelection_mCDE9D19A0556DB8B15CBC355E369A362C18D2A2D(__this, /*hidden argument*/NULL);
		// gameObject.SetActive(true);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_17;
		L_17 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_17, (bool)1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void InteractionMenu::Deactivate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InteractionMenu_Deactivate_m912B95CC8E501BE8859EB5BB477A9D0A5DCFE10A (InteractionMenu_t7C2715221BAC6E0BB83B07EF82F68C6AA34129CB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IGate_tC633C584B29457590CF59B3448AEE38AC9FB33C6_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IInteraction_t1C1C3E6C29AFB203F5021CA7300B6FEA0EFE91FE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IKey_t524F44506E887E37C88431701D58645C41BD5F3C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IShip_tC6FB125A646115EE3BCEDE1326BF546935215C99_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InteractionMenu_GetInteractionAsType_TisIFauna_tECBAA4EFAA9D07BFB7CB55CEE86755A301872FC5_m4E764DDABB046069A791E256957F04B862FD2182_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InteractionMenu_GetInteractionAsType_TisIGate_tC633C584B29457590CF59B3448AEE38AC9FB33C6_mDD834F8EB0D1D7DCE6A48654C2AD746D46BB8279_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InteractionMenu_GetInteractionAsType_TisIKey_t524F44506E887E37C88431701D58645C41BD5F3C_m244F7034AC80DC277E1467E43379BC7EFEFD5F00_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InteractionMenu_GetInteractionAsType_TisIShip_tC6FB125A646115EE3BCEDE1326BF546935215C99_m22472493C6CA081B6709F1EAE1AD3E87BDAFA312_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	RuntimeObject* V_2 = NULL;
	RuntimeObject* V_3 = NULL;
	{
		// if(optionIndex == 0)
		int32_t L_0 = __this->get_optionIndex_8();
		if (L_0)
		{
			goto IL_001f;
		}
	}
	{
		// ToolTip.Instance.Toggle(currentInteraction.Inspect());
		ToolTip_t044257731DC5931927D3AB0671C17515BD8E251E * L_1;
		L_1 = ToolTip_get_Instance_m5CB773BB76F41361836DE8051081FD5CDF562969_inline(/*hidden argument*/NULL);
		RuntimeObject* L_2 = __this->get_currentInteraction_7();
		String_t* L_3;
		L_3 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String IInteraction::Inspect() */, IInteraction_t1C1C3E6C29AFB203F5021CA7300B6FEA0EFE91FE_il2cpp_TypeInfo_var, L_2);
		ToolTip_Toggle_m7BB7DD0FB5AA7DD002A5173E3391AF8457627F6C(L_1, L_3, /*hidden argument*/NULL);
		// }
		goto IL_0070;
	}

IL_001f:
	{
		// else if(optionIndex == 1)
		int32_t L_4 = __this->get_optionIndex_8();
		if ((!(((uint32_t)L_4) == ((uint32_t)1))))
		{
			goto IL_0070;
		}
	}
	{
		// if (GetInteractionAsType(out IFauna fauna) == true)
		bool L_5;
		L_5 = InteractionMenu_GetInteractionAsType_TisIFauna_tECBAA4EFAA9D07BFB7CB55CEE86755A301872FC5_m4E764DDABB046069A791E256957F04B862FD2182(__this, (RuntimeObject**)(&V_0), /*hidden argument*/InteractionMenu_GetInteractionAsType_TisIFauna_tECBAA4EFAA9D07BFB7CB55CEE86755A301872FC5_m4E764DDABB046069A791E256957F04B862FD2182_RuntimeMethod_var);
		if (!L_5)
		{
			goto IL_003b;
		}
	}
	{
		// Photograph(fauna);
		RuntimeObject* L_6 = V_0;
		InteractionMenu_Photograph_mDF8C7A662AA42E89A39027BBAB69C54692736571(__this, L_6, /*hidden argument*/NULL);
		// }
		goto IL_0070;
	}

IL_003b:
	{
		// else if (GetInteractionAsType(out IKey key) == true)
		bool L_7;
		L_7 = InteractionMenu_GetInteractionAsType_TisIKey_t524F44506E887E37C88431701D58645C41BD5F3C_m244F7034AC80DC277E1467E43379BC7EFEFD5F00(__this, (RuntimeObject**)(&V_1), /*hidden argument*/InteractionMenu_GetInteractionAsType_TisIKey_t524F44506E887E37C88431701D58645C41BD5F3C_m244F7034AC80DC277E1467E43379BC7EFEFD5F00_RuntimeMethod_var);
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		// key.Pickup();
		RuntimeObject* L_8 = V_1;
		InterfaceActionInvoker0::Invoke(0 /* System.Void IKey::Pickup() */, IKey_t524F44506E887E37C88431701D58645C41BD5F3C_il2cpp_TypeInfo_var, L_8);
		// }
		goto IL_0070;
	}

IL_004d:
	{
		// else if(GetInteractionAsType(out IGate gate) == true)
		bool L_9;
		L_9 = InteractionMenu_GetInteractionAsType_TisIGate_tC633C584B29457590CF59B3448AEE38AC9FB33C6_mDD834F8EB0D1D7DCE6A48654C2AD746D46BB8279(__this, (RuntimeObject**)(&V_2), /*hidden argument*/InteractionMenu_GetInteractionAsType_TisIGate_tC633C584B29457590CF59B3448AEE38AC9FB33C6_mDD834F8EB0D1D7DCE6A48654C2AD746D46BB8279_RuntimeMethod_var);
		if (!L_9)
		{
			goto IL_0060;
		}
	}
	{
		// gate.Unlock();
		RuntimeObject* L_10 = V_2;
		bool L_11;
		L_11 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean IGate::Unlock() */, IGate_tC633C584B29457590CF59B3448AEE38AC9FB33C6_il2cpp_TypeInfo_var, L_10);
		// }
		goto IL_0070;
	}

IL_0060:
	{
		// else if(GetInteractionAsType(out IShip ship) == true)
		bool L_12;
		L_12 = InteractionMenu_GetInteractionAsType_TisIShip_tC6FB125A646115EE3BCEDE1326BF546935215C99_m22472493C6CA081B6709F1EAE1AD3E87BDAFA312(__this, (RuntimeObject**)(&V_3), /*hidden argument*/InteractionMenu_GetInteractionAsType_TisIShip_tC6FB125A646115EE3BCEDE1326BF546935215C99_m22472493C6CA081B6709F1EAE1AD3E87BDAFA312_RuntimeMethod_var);
		if (!L_12)
		{
			goto IL_0070;
		}
	}
	{
		// ship.Leave();
		RuntimeObject* L_13 = V_3;
		InterfaceActionInvoker0::Invoke(0 /* System.Void IShip::Leave() */, IShip_tC6FB125A646115EE3BCEDE1326BF546935215C99_il2cpp_TypeInfo_var, L_13);
	}

IL_0070:
	{
		// gameObject.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_14;
		L_14 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_14, (bool)0, /*hidden argument*/NULL);
		// optionIndex = -1;
		__this->set_optionIndex_8((-1));
		// currentInteraction = null;
		__this->set_currentInteraction_7((RuntimeObject*)NULL);
		// }
		return;
	}
}
// System.Void InteractionMenu::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InteractionMenu__ctor_m7D2580EE5A91D03AC0C8191AACF351421AD3C3EC (InteractionMenu_t7C2715221BAC6E0BB83B07EF82F68C6AA34129CB * __this, const RuntimeMethod* method)
{
	{
		// private int optionIndex = -1;
		__this->set_optionIndex_8((-1));
		// private float timer = -1;
		__this->set_timer_9((-1.0f));
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Key::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Key_Start_m32134F86EACDABA7FFF8FB47E9DA60B2D7F71B4A (Key_tB313BF742406084E17C47FE2B934490E0261AC8E * __this, const RuntimeMethod* method)
{
	{
		// WorldMap.Instance.AddInteraction(gameObject);
		WorldMap_t01A07DDC9EA386C88D5814CF18E0F37CAF05270C * L_0;
		L_0 = WorldMap_get_Instance_m119402AD39383BC71435F9F36C3219A2281289CA_inline(/*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1;
		L_1 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		bool L_2;
		L_2 = WorldMap_AddInteraction_mF2CC83A625FCFB963745D016D72BC262AB4A94F4(L_0, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Key::Pickup()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Key_Pickup_m2A0EA27BF1A8B8D1EFA82F77FD5829B902E8F026 (Key_tB313BF742406084E17C47FE2B934490E0261AC8E * __this, const RuntimeMethod* method)
{
	{
		// if(Interaction.Instance.Addkey(type) == true)
		Interaction_t41E997DF6BD70C3EAB8669FCC65D603292EF42A1 * L_0;
		L_0 = Interaction_get_Instance_m49A31B95D41F5321AA7036861F8330D6BB09A940_inline(/*hidden argument*/NULL);
		int32_t L_1 = __this->get_type_4();
		bool L_2;
		L_2 = Interaction_Addkey_mCC3293ED3A6334A742C2196DF288E220BF1073A5(L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		// gameObject.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_3;
		L_3 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_3, (bool)0, /*hidden argument*/NULL);
	}

IL_001e:
	{
		// }
		return;
	}
}
// System.String Key::Inspect()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Key_Inspect_m1443EDF14E5EFD3C262BBFFF0A08D28DF9191365 (Key_tB313BF742406084E17C47FE2B934490E0261AC8E * __this, const RuntimeMethod* method)
{
	{
		// return tooltipText;
		String_t* L_0 = __this->get_tooltipText_5();
		return L_0;
	}
}
// System.Void Key::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Key__ctor_mDED000F0CC8C37C5B2A6916661E52F7735A49725 (Key_tB313BF742406084E17C47FE2B934490E0261AC8E * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MainMenu::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainMenu_Awake_mA8CB83E7D49CE72C6D0DE5D7D6313F8305A1A4FE (MainMenu_tEB11F5A993C42E93B585FBB65C9E92EC91C5707C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DirectoryInfo_t4EF3610F45F0D234800D01ADA8F3F476AE0CF5CD_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponentInChildren_TisDropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96_m8B8487C221C1219A04D3F96C76A0E7CFFA9B4CD2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponentInChildren_TisRawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A_mBE7B553FDA545118EB6051F0473AAEC7F78D8CAB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m9AF452292436C834FC154E6457CDD42B18FB310F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mC2611000DA97145D1DFC867ECA604FBB3E8628EC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m69B62B1F3FDC9F7B1D0906604855E457795130AB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&OptionData_t5F665DC13C1E4307727D66ECC1100B3A77E3E857_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral521B13C1C2102F7779BF79109F54CF81F4D7ECE7);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral87657B66CDDAD40B19EA8B71A88ED298D2AF6F2F);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	FileInfoU5BU5D_tB6A5BC6AD9BEC388BF3236B4E8FBB7760F96080B* V_1 = NULL;
	FileInfoU5BU5D_tB6A5BC6AD9BEC388BF3236B4E8FBB7760F96080B* V_2 = NULL;
	int32_t V_3 = 0;
	FileInfo_t6C8B2EAA1E23F9E6D7C287C58E4EEEB2049ABAB9 * V_4 = NULL;
	Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * V_5 = NULL;
	{
		// galleryDropdown = galleryPanel.GetComponentInChildren<Dropdown>();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = __this->get_galleryPanel_4();
		Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96 * L_1;
		L_1 = GameObject_GetComponentInChildren_TisDropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96_m8B8487C221C1219A04D3F96C76A0E7CFFA9B4CD2(L_0, /*hidden argument*/GameObject_GetComponentInChildren_TisDropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96_m8B8487C221C1219A04D3F96C76A0E7CFFA9B4CD2_RuntimeMethod_var);
		__this->set_galleryDropdown_6(L_1);
		// galleryDisplay = galleryPanel.GetComponentInChildren<RawImage>();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2 = __this->get_galleryPanel_4();
		RawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A * L_3;
		L_3 = GameObject_GetComponentInChildren_TisRawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A_mBE7B553FDA545118EB6051F0473AAEC7F78D8CAB(L_2, /*hidden argument*/GameObject_GetComponentInChildren_TisRawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A_mBE7B553FDA545118EB6051F0473AAEC7F78D8CAB_RuntimeMethod_var);
		__this->set_galleryDisplay_7(L_3);
		// galleryDropdown.ClearOptions();
		Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96 * L_4 = __this->get_galleryDropdown_6();
		Dropdown_ClearOptions_m7F59A8B054698715921D2B0E37EB1808BE53C23C(L_4, /*hidden argument*/NULL);
		// string galleryFolder = Application.dataPath + "/Resources/Photogallery";
		String_t* L_5;
		L_5 = Application_get_dataPath_m026509C15A1E58FC6461EE7EC336EC18C9C2271E(/*hidden argument*/NULL);
		String_t* L_6;
		L_6 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(L_5, _stringLiteral87657B66CDDAD40B19EA8B71A88ED298D2AF6F2F, /*hidden argument*/NULL);
		V_0 = L_6;
		// if (Directory.Exists(galleryFolder) == false)
		String_t* L_7 = V_0;
		bool L_8;
		L_8 = Directory_Exists_m17E38B91F6D9A0064D614FF2237BBFC0127468FE(L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_004c;
		}
	}
	{
		// Directory.CreateDirectory(galleryFolder);
		String_t* L_9 = V_0;
		DirectoryInfo_t4EF3610F45F0D234800D01ADA8F3F476AE0CF5CD * L_10;
		L_10 = Directory_CreateDirectory_m38040338519C48CE52137CC146372A153D5C6A7A(L_9, /*hidden argument*/NULL);
	}

IL_004c:
	{
		// DirectoryInfo dir = new DirectoryInfo(galleryFolder);
		String_t* L_11 = V_0;
		DirectoryInfo_t4EF3610F45F0D234800D01ADA8F3F476AE0CF5CD * L_12 = (DirectoryInfo_t4EF3610F45F0D234800D01ADA8F3F476AE0CF5CD *)il2cpp_codegen_object_new(DirectoryInfo_t4EF3610F45F0D234800D01ADA8F3F476AE0CF5CD_il2cpp_TypeInfo_var);
		DirectoryInfo__ctor_m5F307F7E646135FC323F81EA93C36CC0CF6023A6(L_12, L_11, /*hidden argument*/NULL);
		// FileInfo[] ScreenshotInfos = dir.GetFiles("UWW_?*.png");
		FileInfoU5BU5D_tB6A5BC6AD9BEC388BF3236B4E8FBB7760F96080B* L_13;
		L_13 = DirectoryInfo_GetFiles_mB4AE0AEC3ABF4A3541A0B68C46C802A78730C5FC(L_12, _stringLiteral521B13C1C2102F7779BF79109F54CF81F4D7ECE7, /*hidden argument*/NULL);
		V_1 = L_13;
		// if(ScreenshotInfos.Length > 0)
		FileInfoU5BU5D_tB6A5BC6AD9BEC388BF3236B4E8FBB7760F96080B* L_14 = V_1;
		if (!(((RuntimeArray*)L_14)->max_length))
		{
			goto IL_00dc;
		}
	}
	{
		// foreach (FileInfo info in ScreenshotInfos)
		FileInfoU5BU5D_tB6A5BC6AD9BEC388BF3236B4E8FBB7760F96080B* L_15 = V_1;
		V_2 = L_15;
		V_3 = 0;
		goto IL_00be;
	}

IL_0067:
	{
		// foreach (FileInfo info in ScreenshotInfos)
		FileInfoU5BU5D_tB6A5BC6AD9BEC388BF3236B4E8FBB7760F96080B* L_16 = V_2;
		int32_t L_17 = V_3;
		int32_t L_18 = L_17;
		FileInfo_t6C8B2EAA1E23F9E6D7C287C58E4EEEB2049ABAB9 * L_19 = (L_16)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_18));
		V_4 = L_19;
		// galleryDropdown.options.Add(new Dropdown.OptionData(info.Name));
		Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96 * L_20 = __this->get_galleryDropdown_6();
		List_1_tAF6577A540702C9F6C407DE69A8FAFB502339DC4 * L_21;
		L_21 = Dropdown_get_options_mF427A2157CDD901C12F1B160C4D1F8207D7111D0(L_20, /*hidden argument*/NULL);
		FileInfo_t6C8B2EAA1E23F9E6D7C287C58E4EEEB2049ABAB9 * L_22 = V_4;
		String_t* L_23;
		L_23 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.IO.FileSystemInfo::get_Name() */, L_22);
		OptionData_t5F665DC13C1E4307727D66ECC1100B3A77E3E857 * L_24 = (OptionData_t5F665DC13C1E4307727D66ECC1100B3A77E3E857 *)il2cpp_codegen_object_new(OptionData_t5F665DC13C1E4307727D66ECC1100B3A77E3E857_il2cpp_TypeInfo_var);
		OptionData__ctor_m5AF14BD8BBF6118AC51A7A9A38AE3AB2DE3C2675(L_24, L_23, /*hidden argument*/NULL);
		List_1_Add_mC2611000DA97145D1DFC867ECA604FBB3E8628EC(L_21, L_24, /*hidden argument*/List_1_Add_mC2611000DA97145D1DFC867ECA604FBB3E8628EC_RuntimeMethod_var);
		// Texture2D texture = new Texture2D(960, 540);
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_25 = (Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF *)il2cpp_codegen_object_new(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF_il2cpp_TypeInfo_var);
		Texture2D__ctor_m7D64AB4C55A01F2EE57483FD9EF826607DF9E4B4(L_25, ((int32_t)960), ((int32_t)540), /*hidden argument*/NULL);
		V_5 = L_25;
		// texture.LoadImage(File.ReadAllBytes(info.FullName));
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_26 = V_5;
		FileInfo_t6C8B2EAA1E23F9E6D7C287C58E4EEEB2049ABAB9 * L_27 = V_4;
		String_t* L_28;
		L_28 = VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String System.IO.FileSystemInfo::get_FullName() */, L_27);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_29;
		L_29 = File_ReadAllBytes_mFB47FB50E938AE90CC822442D30E896441D95829(L_28, /*hidden argument*/NULL);
		bool L_30;
		L_30 = ImageConversion_LoadImage_m5BB4FBA0565E698ED5C6F25F7A0A5F83CADD7477(L_26, L_29, /*hidden argument*/NULL);
		// screenshots.Add(texture);
		List_1_t67CA4414F3746D817D6D1A1D16FD9E7C85CED2D7 * L_31 = __this->get_screenshots_8();
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_32 = V_5;
		List_1_Add_m9AF452292436C834FC154E6457CDD42B18FB310F(L_31, L_32, /*hidden argument*/List_1_Add_m9AF452292436C834FC154E6457CDD42B18FB310F_RuntimeMethod_var);
		int32_t L_33 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_33, (int32_t)1));
	}

IL_00be:
	{
		// foreach (FileInfo info in ScreenshotInfos)
		int32_t L_34 = V_3;
		FileInfoU5BU5D_tB6A5BC6AD9BEC388BF3236B4E8FBB7760F96080B* L_35 = V_2;
		if ((((int32_t)L_34) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_35)->max_length))))))
		{
			goto IL_0067;
		}
	}
	{
		// galleryDisplay.texture = screenshots[0];
		RawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A * L_36 = __this->get_galleryDisplay_7();
		List_1_t67CA4414F3746D817D6D1A1D16FD9E7C85CED2D7 * L_37 = __this->get_screenshots_8();
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_38;
		L_38 = List_1_get_Item_m69B62B1F3FDC9F7B1D0906604855E457795130AB_inline(L_37, 0, /*hidden argument*/List_1_get_Item_m69B62B1F3FDC9F7B1D0906604855E457795130AB_RuntimeMethod_var);
		RawImage_set_texture_m1D7BAE6CB629C36894B664D9F5D68CACA88B8D99(L_36, L_38, /*hidden argument*/NULL);
		// }
		return;
	}

IL_00dc:
	{
		// galleryDisplay.gameObject.SetActive(false);
		RawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A * L_39 = __this->get_galleryDisplay_7();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_40;
		L_40 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_39, /*hidden argument*/NULL);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_40, (bool)0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MainMenu::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainMenu_Start_m3B552CE289B1D7E5343961C8461C484EA61DB621 (MainMenu_tEB11F5A993C42E93B585FBB65C9E92EC91C5707C * __this, const RuntimeMethod* method)
{
	{
		// galleryPanel.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = __this->get_galleryPanel_4();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_0, (bool)0, /*hidden argument*/NULL);
		// infoPanel.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1 = __this->get_infoPanel_5();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_1, (bool)0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Collections.IEnumerator MainMenu::LoadGame()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* MainMenu_LoadGame_m75C2A4F48DA3B9EB7A2ADF6872C66FC286639600 (MainMenu_tEB11F5A993C42E93B585FBB65C9E92EC91C5707C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLoadGameU3Ed__7_tC6D4D2F6FFFF4C3DA7A96BE826554FAE56FB9D74_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CLoadGameU3Ed__7_tC6D4D2F6FFFF4C3DA7A96BE826554FAE56FB9D74 * L_0 = (U3CLoadGameU3Ed__7_tC6D4D2F6FFFF4C3DA7A96BE826554FAE56FB9D74 *)il2cpp_codegen_object_new(U3CLoadGameU3Ed__7_tC6D4D2F6FFFF4C3DA7A96BE826554FAE56FB9D74_il2cpp_TypeInfo_var);
		U3CLoadGameU3Ed__7__ctor_m57695A7D16F59DB4E5587AE9E06B571D1940260F(L_0, 0, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void MainMenu::StartGame()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainMenu_StartGame_m8EFE49FE95784A266A683D0CCAAE7C2274981049 (MainMenu_tEB11F5A993C42E93B585FBB65C9E92EC91C5707C * __this, const RuntimeMethod* method)
{
	{
		// StartCoroutine(LoadGame());
		RuntimeObject* L_0;
		L_0 = MainMenu_LoadGame_m75C2A4F48DA3B9EB7A2ADF6872C66FC286639600(__this, /*hidden argument*/NULL);
		Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * L_1;
		L_1 = MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719(__this, L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MainMenu::Info()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainMenu_Info_m9B449123D7739667B9D5569898441C2A89400461 (MainMenu_tEB11F5A993C42E93B585FBB65C9E92EC91C5707C * __this, const RuntimeMethod* method)
{
	{
		// if(galleryPanel.activeSelf == true)
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = __this->get_galleryPanel_4();
		bool L_1;
		L_1 = GameObject_get_activeSelf_m4865097C24FB29F3C31F5C30619AF242297F23EE(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		// galleryPanel.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2 = __this->get_galleryPanel_4();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_2, (bool)0, /*hidden argument*/NULL);
	}

IL_0019:
	{
		// infoPanel.SetActive(!infoPanel.activeSelf);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_3 = __this->get_infoPanel_5();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_4 = __this->get_infoPanel_5();
		bool L_5;
		L_5 = GameObject_get_activeSelf_m4865097C24FB29F3C31F5C30619AF242297F23EE(L_4, /*hidden argument*/NULL);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_3, (bool)((((int32_t)L_5) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MainMenu::Gallery()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainMenu_Gallery_m3FC2EDFC3E9D08BBD78F49732962217470E974B2 (MainMenu_tEB11F5A993C42E93B585FBB65C9E92EC91C5707C * __this, const RuntimeMethod* method)
{
	{
		// if (infoPanel.activeSelf == true)
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = __this->get_infoPanel_5();
		bool L_1;
		L_1 = GameObject_get_activeSelf_m4865097C24FB29F3C31F5C30619AF242297F23EE(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		// infoPanel.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2 = __this->get_infoPanel_5();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_2, (bool)0, /*hidden argument*/NULL);
	}

IL_0019:
	{
		// galleryPanel.SetActive(!galleryPanel.activeSelf);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_3 = __this->get_galleryPanel_4();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_4 = __this->get_galleryPanel_4();
		bool L_5;
		L_5 = GameObject_get_activeSelf_m4865097C24FB29F3C31F5C30619AF242297F23EE(L_4, /*hidden argument*/NULL);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_3, (bool)((((int32_t)L_5) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MainMenu::SelectGalleryImage()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainMenu_SelectGalleryImage_mBE00416F61B0A0B8B5648DE827A479FFE9CC8B69 (MainMenu_tEB11F5A993C42E93B585FBB65C9E92EC91C5707C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mC0601A2601297DDBF0B461A2A8B8EF9528741B88_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m69B62B1F3FDC9F7B1D0906604855E457795130AB_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if(screenshots.Count > 0)
		List_1_t67CA4414F3746D817D6D1A1D16FD9E7C85CED2D7 * L_0 = __this->get_screenshots_8();
		int32_t L_1;
		L_1 = List_1_get_Count_mC0601A2601297DDBF0B461A2A8B8EF9528741B88_inline(L_0, /*hidden argument*/List_1_get_Count_mC0601A2601297DDBF0B461A2A8B8EF9528741B88_RuntimeMethod_var);
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_0052;
		}
	}
	{
		// if (galleryDisplay.gameObject.activeSelf == false)
		RawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A * L_2 = __this->get_galleryDisplay_7();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_3;
		L_3 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_2, /*hidden argument*/NULL);
		bool L_4;
		L_4 = GameObject_get_activeSelf_m4865097C24FB29F3C31F5C30619AF242297F23EE(L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0031;
		}
	}
	{
		// galleryDisplay.gameObject.SetActive(true);
		RawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A * L_5 = __this->get_galleryDisplay_7();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_6;
		L_6 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_5, /*hidden argument*/NULL);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_6, (bool)1, /*hidden argument*/NULL);
	}

IL_0031:
	{
		// galleryDisplay.texture = screenshots[galleryDropdown.value];
		RawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A * L_7 = __this->get_galleryDisplay_7();
		List_1_t67CA4414F3746D817D6D1A1D16FD9E7C85CED2D7 * L_8 = __this->get_screenshots_8();
		Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96 * L_9 = __this->get_galleryDropdown_6();
		int32_t L_10;
		L_10 = Dropdown_get_value_mFBF47E0C72050C5CB96B8B6D33F41BA2D1368F26_inline(L_9, /*hidden argument*/NULL);
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_11;
		L_11 = List_1_get_Item_m69B62B1F3FDC9F7B1D0906604855E457795130AB_inline(L_8, L_10, /*hidden argument*/List_1_get_Item_m69B62B1F3FDC9F7B1D0906604855E457795130AB_RuntimeMethod_var);
		RawImage_set_texture_m1D7BAE6CB629C36894B664D9F5D68CACA88B8D99(L_7, L_11, /*hidden argument*/NULL);
	}

IL_0052:
	{
		// }
		return;
	}
}
// System.Void MainMenu::QuitToDesktop()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainMenu_QuitToDesktop_m4BA7361022A9F78B96A3EE6C4B117CECF2D53D31 (MainMenu_tEB11F5A993C42E93B585FBB65C9E92EC91C5707C * __this, const RuntimeMethod* method)
{
	{
		// Application.Quit();
		Application_Quit_m8D720E5092786C2EE32310D85FE61C253D3B1F2A(/*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MainMenu::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainMenu__ctor_m4D77CEC8F91682A2D9492AE815F89B178BF9717D (MainMenu_tEB11F5A993C42E93B585FBB65C9E92EC91C5707C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mD04CAD03BDE2013E799D002230D9D16EFD84FEFC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t67CA4414F3746D817D6D1A1D16FD9E7C85CED2D7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private List<Texture2D> screenshots = new List<Texture2D>();
		List_1_t67CA4414F3746D817D6D1A1D16FD9E7C85CED2D7 * L_0 = (List_1_t67CA4414F3746D817D6D1A1D16FD9E7C85CED2D7 *)il2cpp_codegen_object_new(List_1_t67CA4414F3746D817D6D1A1D16FD9E7C85CED2D7_il2cpp_TypeInfo_var);
		List_1__ctor_mD04CAD03BDE2013E799D002230D9D16EFD84FEFC(L_0, /*hidden argument*/List_1__ctor_mD04CAD03BDE2013E799D002230D9D16EFD84FEFC_RuntimeMethod_var);
		__this->set_screenshots_8(L_0);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// MouseLook MouseLook::get_Instance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MouseLook_t1E1EE79B13022184F3EF9AC8FE8C6771053890DA * MouseLook_get_Instance_m4F889A9C207EAE72011AF1BA9DA70046C5404386 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MouseLook_t1E1EE79B13022184F3EF9AC8FE8C6771053890DA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static MouseLook Instance { get; private set; }
		MouseLook_t1E1EE79B13022184F3EF9AC8FE8C6771053890DA * L_0 = ((MouseLook_t1E1EE79B13022184F3EF9AC8FE8C6771053890DA_StaticFields*)il2cpp_codegen_static_fields_for(MouseLook_t1E1EE79B13022184F3EF9AC8FE8C6771053890DA_il2cpp_TypeInfo_var))->get_U3CInstanceU3Ek__BackingField_10();
		return L_0;
	}
}
// System.Void MouseLook::set_Instance(MouseLook)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MouseLook_set_Instance_mA80F2A4EF6E41B80058108D5AFF17C03E4F6251D (MouseLook_t1E1EE79B13022184F3EF9AC8FE8C6771053890DA * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MouseLook_t1E1EE79B13022184F3EF9AC8FE8C6771053890DA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static MouseLook Instance { get; private set; }
		MouseLook_t1E1EE79B13022184F3EF9AC8FE8C6771053890DA * L_0 = ___value0;
		((MouseLook_t1E1EE79B13022184F3EF9AC8FE8C6771053890DA_StaticFields*)il2cpp_codegen_static_fields_for(MouseLook_t1E1EE79B13022184F3EF9AC8FE8C6771053890DA_il2cpp_TypeInfo_var))->set_U3CInstanceU3Ek__BackingField_10(L_0);
		return;
	}
}
// System.Boolean MouseLook::get_LookEnabled()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool MouseLook_get_LookEnabled_m872F548C1ADDCAE36BCFFC9743F4F6E240A703C5 (MouseLook_t1E1EE79B13022184F3EF9AC8FE8C6771053890DA * __this, const RuntimeMethod* method)
{
	{
		// public bool LookEnabled { get; set; } = true;
		bool L_0 = __this->get_U3CLookEnabledU3Ek__BackingField_11();
		return L_0;
	}
}
// System.Void MouseLook::set_LookEnabled(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MouseLook_set_LookEnabled_mE39640BB02C6774A01AE2C1E80613702480B2C99 (MouseLook_t1E1EE79B13022184F3EF9AC8FE8C6771053890DA * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// public bool LookEnabled { get; set; } = true;
		bool L_0 = ___value0;
		__this->set_U3CLookEnabledU3Ek__BackingField_11(L_0);
		return;
	}
}
// System.Void MouseLook::set_CursorToggle(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MouseLook_set_CursorToggle_m2DBA5589D35ED1938A52174EC990627601603468 (MouseLook_t1E1EE79B13022184F3EF9AC8FE8C6771053890DA * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// Cursor.visible = value;
		bool L_0 = ___value0;
		Cursor_set_visible_m4747F0DC20D06D1932EC740C5CCC738C1664903D(L_0, /*hidden argument*/NULL);
		// if(value == true)
		bool L_1 = ___value0;
		if (!L_1)
		{
			goto IL_0010;
		}
	}
	{
		// Cursor.lockState = CursorLockMode.None;
		Cursor_set_lockState_mC0739186A04F4C278F02E8C1714D99B491E3A217(0, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0010:
	{
		// Cursor.lockState = CursorLockMode.Locked;
		Cursor_set_lockState_mC0739186A04F4C278F02E8C1714D99B491E3A217(1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MouseLook::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MouseLook_Awake_m10C7098FFAA05035E7FB675ECCC5EF1FD79B8B95 (MouseLook_t1E1EE79B13022184F3EF9AC8FE8C6771053890DA * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if(Instance == null)
		MouseLook_t1E1EE79B13022184F3EF9AC8FE8C6771053890DA * L_0;
		L_0 = MouseLook_get_Instance_m4F889A9C207EAE72011AF1BA9DA70046C5404386_inline(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0015;
		}
	}
	{
		// Instance = this;
		MouseLook_set_Instance_mA80F2A4EF6E41B80058108D5AFF17C03E4F6251D_inline(__this, /*hidden argument*/NULL);
		// }
		goto IL_001c;
	}

IL_0015:
	{
		// enabled = false;
		Behaviour_set_enabled_mDE415591B28853D1CD764C53CB499A2142247F32(__this, (bool)0, /*hidden argument*/NULL);
	}

IL_001c:
	{
		// character = transform.parent;  //set reference to character transform
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_2;
		L_2 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_3;
		L_3 = Transform_get_parent_m7D06005D9CB55F90F39D42F6A2AF9C7BC80745C9(L_2, /*hidden argument*/NULL);
		__this->set_character_6(L_3);
		// CursorToggle = false;  //turn cursor off
		MouseLook_set_CursorToggle_m2DBA5589D35ED1938A52174EC990627601603468(__this, (bool)0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MouseLook::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MouseLook_Update_m3D49361C94E0433BB35499708EE783B4543D83D5 (MouseLook_t1E1EE79B13022184F3EF9AC8FE8C6771053890DA * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral16DD21BE77B115D392226EB71A2D3A9FDC29E3F0);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral88BEE283254D7094E258B3A88730F4CC4F1E4AC7);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if(LookEnabled == true)
		bool L_0;
		L_0 = MouseLook_get_LookEnabled_m872F548C1ADDCAE36BCFFC9743F4F6E240A703C5_inline(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_00ec;
		}
	}
	{
		// mouseDir = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));//get cursor position, multiply with sensitivity
		float L_1;
		L_1 = Input_GetAxisRaw_mC07AC23FD8D04A69CDB07C6399C93FFFAEB0FC5B(_stringLiteral88BEE283254D7094E258B3A88730F4CC4F1E4AC7, /*hidden argument*/NULL);
		float L_2;
		L_2 = Input_GetAxisRaw_mC07AC23FD8D04A69CDB07C6399C93FFFAEB0FC5B(_stringLiteral16DD21BE77B115D392226EB71A2D3A9FDC29E3F0, /*hidden argument*/NULL);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_3;
		memset((&L_3), 0, sizeof(L_3));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_3), L_1, L_2, /*hidden argument*/NULL);
		__this->set_mouseDir_7(L_3);
		// mouseDir *= sensitivity; //multiple cursor direction with sensitivity
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_4 = __this->get_mouseDir_7();
		float L_5 = __this->get_sensitivity_4();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_6;
		L_6 = Vector2_op_Multiply_mC7A7802352867555020A90205EBABA56EE5E36CB_inline(L_4, L_5, /*hidden argument*/NULL);
		__this->set_mouseDir_7(L_6);
		// smoothing = Vector2.Lerp(smoothing, mouseDir, 1 / drag);  //interpolate between smoothing vector and mouse direction over the drag ratio
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_7 = __this->get_smoothing_8();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_8 = __this->get_mouseDir_7();
		float L_9 = __this->get_drag_5();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_10;
		L_10 = Vector2_Lerp_mC9A8AB816281F4447B7B62264595C16751ED355B_inline(L_7, L_8, ((float)((float)(1.0f)/(float)L_9)), /*hidden argument*/NULL);
		__this->set_smoothing_8(L_10);
		// result += smoothing;  //apply smoothing to the result
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_11 = __this->get_result_9();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_12 = __this->get_smoothing_8();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_13;
		L_13 = Vector2_op_Addition_m5EACC2AEA80FEE29F380397CF1F4B11D04BE71CC_inline(L_11, L_12, /*hidden argument*/NULL);
		__this->set_result_9(L_13);
		// result.y = Mathf.Clamp(result.y, -70, 70);  //clamp the Y value
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_14 = __this->get_address_of_result_9();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_15 = __this->get_address_of_result_9();
		float L_16 = L_15->get_y_1();
		float L_17;
		L_17 = Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87(L_16, (-70.0f), (70.0f), /*hidden argument*/NULL);
		L_14->set_y_1(L_17);
		// transform.localRotation = Quaternion.AngleAxis(-result.y, Vector3.right);  //apply angle rotation for the camera
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_18;
		L_18 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_19 = __this->get_address_of_result_9();
		float L_20 = L_19->get_y_1();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_21;
		L_21 = Vector3_get_right_mF5A51F81961474E0A7A31C2757FD00921FB79C44(/*hidden argument*/NULL);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_22;
		L_22 = Quaternion_AngleAxis_m4644D20F58ADF03E9EA297CB4A845E5BCDA1E398(((-L_20)), L_21, /*hidden argument*/NULL);
		Transform_set_localRotation_m1A9101457EC4653AFC93FCC4065A29F2C78FA62C(L_18, L_22, /*hidden argument*/NULL);
		// character.rotation = Quaternion.AngleAxis(result.x, character.transform.up);  //apply angle rotation for the parent
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_23 = __this->get_character_6();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_24 = __this->get_address_of_result_9();
		float L_25 = L_24->get_x_0();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_26 = __this->get_character_6();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_27;
		L_27 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_26, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_28;
		L_28 = Transform_get_up_mAB753D250A30C78924D5D22B0821F1D254525C31(L_27, /*hidden argument*/NULL);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_29;
		L_29 = Quaternion_AngleAxis_m4644D20F58ADF03E9EA297CB4A845E5BCDA1E398(L_25, L_28, /*hidden argument*/NULL);
		Transform_set_rotation_m1B5F3D4CE984AB31254615C9C71B0E54978583B4(L_23, L_29, /*hidden argument*/NULL);
	}

IL_00ec:
	{
		// }
		return;
	}
}
// System.Void MouseLook::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MouseLook__ctor_mD12D8075DDEA2085341B59FF8BA9FB353613200B (MouseLook_t1E1EE79B13022184F3EF9AC8FE8C6771053890DA * __this, const RuntimeMethod* method)
{
	{
		// public float sensitivity = 2.5f;
		__this->set_sensitivity_4((2.5f));
		// public float drag = 1.5f;
		__this->set_drag_5((1.5f));
		// public bool LookEnabled { get; set; } = true;
		__this->set_U3CLookEnabledU3Ek__BackingField_11((bool)1);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void NewBehaviourScript::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NewBehaviourScript_Start_m783F84A617DADC4574B0BF1524481E6B96C65661 (NewBehaviourScript_t5ED8D4BD4668DBECFEFD841BD2A8CC1D8FCF38CB * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void NewBehaviourScript::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NewBehaviourScript_Update_m411C4D5C2D993FD70092FDA0FE2AC4786F8AC001 (NewBehaviourScript_t5ED8D4BD4668DBECFEFD841BD2A8CC1D8FCF38CB * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void NewBehaviourScript::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NewBehaviourScript__ctor_m437970EA37D66BDF32972F4CC0F65B95E5961FAA (NewBehaviourScript_t5ED8D4BD4668DBECFEFD841BD2A8CC1D8FCF38CB * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// PlayerController PlayerController::get_Instance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9 * PlayerController_get_Instance_m8C373614456176CE37B49E24BA9B7E019C5A0440 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static PlayerController Instance { get; private set; }
		PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9 * L_0 = ((PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_StaticFields*)il2cpp_codegen_static_fields_for(PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_il2cpp_TypeInfo_var))->get_U3CInstanceU3Ek__BackingField_8();
		return L_0;
	}
}
// System.Void PlayerController::set_Instance(PlayerController)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerController_set_Instance_m1DBDE7741593D098536751F470647F587A7A0D0A (PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static PlayerController Instance { get; private set; }
		PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9 * L_0 = ___value0;
		((PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_StaticFields*)il2cpp_codegen_static_fields_for(PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_il2cpp_TypeInfo_var))->set_U3CInstanceU3Ek__BackingField_8(L_0);
		return;
	}
}
// System.Void PlayerController::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerController_Awake_m23059564DCFDD324EA9DB966473251896647CD23 (PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisCharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E_m3DB1DD5819F96D7C7F6F19C12138AC48D21DBBF2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (Instance == null)
		PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9 * L_0;
		L_0 = PlayerController_get_Instance_m8C373614456176CE37B49E24BA9B7E019C5A0440_inline(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0015;
		}
	}
	{
		// Instance = this;
		PlayerController_set_Instance_m1DBDE7741593D098536751F470647F587A7A0D0A_inline(__this, /*hidden argument*/NULL);
		// }
		goto IL_001c;
	}

IL_0015:
	{
		// enabled = false;
		Behaviour_set_enabled_mDE415591B28853D1CD764C53CB499A2142247F32(__this, (bool)0, /*hidden argument*/NULL);
	}

IL_001c:
	{
		// controller = GetComponent<CharacterController>(); //referance the character controller
		CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E * L_2;
		L_2 = Component_GetComponent_TisCharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E_m3DB1DD5819F96D7C7F6F19C12138AC48D21DBBF2(__this, /*hidden argument*/Component_GetComponent_TisCharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E_m3DB1DD5819F96D7C7F6F19C12138AC48D21DBBF2_RuntimeMethod_var);
		__this->set_controller_7(L_2);
		// }
		return;
	}
}
// System.Void PlayerController::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerController_Update_mB31159CAD7DD2329859472554BC9154A83D8E794 (PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral265E15F1F86F1C766555899D5771CF29055DE75A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7F8C014BD4810CC276D0F9F81A1E759C7B098B1E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF88D019D310EFCC979975957D01E97BE7C0390BC);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// motion = Vector3.zero;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0;
		L_0 = Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6(/*hidden argument*/NULL);
		__this->set_motion_6(L_0);
		// input = new Vector3(Input.GetAxisRaw("Horizontal"),
		//     Input.GetAxisRaw("Depth"),
		//     Input.GetAxisRaw("Vertical"));//get inputs
		float L_1;
		L_1 = Input_GetAxisRaw_mC07AC23FD8D04A69CDB07C6399C93FFFAEB0FC5B(_stringLiteral7F8C014BD4810CC276D0F9F81A1E759C7B098B1E, /*hidden argument*/NULL);
		float L_2;
		L_2 = Input_GetAxisRaw_mC07AC23FD8D04A69CDB07C6399C93FFFAEB0FC5B(_stringLiteralF88D019D310EFCC979975957D01E97BE7C0390BC, /*hidden argument*/NULL);
		float L_3;
		L_3 = Input_GetAxisRaw_mC07AC23FD8D04A69CDB07C6399C93FFFAEB0FC5B(_stringLiteral265E15F1F86F1C766555899D5771CF29055DE75A, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4;
		memset((&L_4), 0, sizeof(L_4));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_4), L_1, L_2, L_3, /*hidden argument*/NULL);
		__this->set_input_5(L_4);
		// motion += transform.forward.normalized * input.z;  //forward motion
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5 = __this->get_motion_6();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_6;
		L_6 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7;
		L_7 = Transform_get_forward_mD850B9ECF892009E3485408DC0D375165B7BF053(L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8;
		L_8 = Vector3_get_normalized_m2FA6DF38F97BDA4CCBDAE12B9FE913A241DAC8D5((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_0), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_9 = __this->get_address_of_input_5();
		float L_10 = L_9->get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_11;
		L_11 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_8, L_10, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		L_12 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_5, L_11, /*hidden argument*/NULL);
		__this->set_motion_6(L_12);
		// motion += transform.right.normalized * input.x;  //horizontal motion
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = __this->get_motion_6();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_14;
		L_14 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_15;
		L_15 = Transform_get_right_mA8EB1882CCE8759766544448308C88D9200F06CE(L_14, /*hidden argument*/NULL);
		V_0 = L_15;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_16;
		L_16 = Vector3_get_normalized_m2FA6DF38F97BDA4CCBDAE12B9FE913A241DAC8D5((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_0), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_17 = __this->get_address_of_input_5();
		float L_18 = L_17->get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_19;
		L_19 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_16, L_18, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_20;
		L_20 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_13, L_19, /*hidden argument*/NULL);
		__this->set_motion_6(L_20);
		// motion += Vector3.up.normalized * input.y;  //vertical motion
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_21 = __this->get_motion_6();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_22;
		L_22 = Vector3_get_up_m38AECA68388D446CFADDD022B0B867293044EA50(/*hidden argument*/NULL);
		V_0 = L_22;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_23;
		L_23 = Vector3_get_normalized_m2FA6DF38F97BDA4CCBDAE12B9FE913A241DAC8D5((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_0), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_24 = __this->get_address_of_input_5();
		float L_25 = L_24->get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_26;
		L_26 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_23, L_25, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_27;
		L_27 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_21, L_26, /*hidden argument*/NULL);
		__this->set_motion_6(L_27);
		// controller.Move(motion * speed * Time.deltaTime);  //apply movement
		CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E * L_28 = __this->get_controller_7();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_29 = __this->get_motion_6();
		float L_30 = __this->get_speed_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_31;
		L_31 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_29, L_30, /*hidden argument*/NULL);
		float L_32;
		L_32 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_33;
		L_33 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_31, L_32, /*hidden argument*/NULL);
		int32_t L_34;
		L_34 = CharacterController_Move_mE0EBC32C72A0BEC18EDEBE748D44309A4BA32E60(L_28, L_33, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayerController::MoveToPosition(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerController_MoveToPosition_m9E1B5DA225A7BE1C6E859C6294A44281E5480876 (PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___targetPos0, const RuntimeMethod* method)
{
	{
		// controller.enabled = false;  //disabled the players character controlled to not mess with math just in case
		CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E * L_0 = __this->get_controller_7();
		Collider_set_enabled_m047B4D830755CD36671F7A60BFAA9C0D61F6C4A1(L_0, (bool)0, /*hidden argument*/NULL);
		// transform.position = targetPos;  //moves hte player
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1;
		L_1 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___targetPos0;
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_1, L_2, /*hidden argument*/NULL);
		// controller.enabled = true;  //turns player controller back on
		CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E * L_3 = __this->get_controller_7();
		Collider_set_enabled_m047B4D830755CD36671F7A60BFAA9C0D61F6C4A1(L_3, (bool)1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayerController::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerController__ctor_mF30385729DAFDFCB895C4939F6051DCE6C0327FB (PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9 * __this, const RuntimeMethod* method)
{
	{
		// public float speed = 3f;
		__this->set_speed_4((3.0f));
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// FiniteStateMachine.StateMachine Ship::get_StateMachine()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StateMachine_t8E24C2687A18E15B27A6A8FEED2DFE22D910119F * Ship_get_StateMachine_m9110695EE50E4C7260E64164574C5394CFF403C1 (Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * __this, const RuntimeMethod* method)
{
	{
		// public StateMachine StateMachine { get; private set; }
		StateMachine_t8E24C2687A18E15B27A6A8FEED2DFE22D910119F * L_0 = __this->get_U3CStateMachineU3Ek__BackingField_9();
		return L_0;
	}
}
// System.Void Ship::set_StateMachine(FiniteStateMachine.StateMachine)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Ship_set_StateMachine_mEB49B2626B6B51769D40F7CC9FC3FA2D59961F13 (Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * __this, StateMachine_t8E24C2687A18E15B27A6A8FEED2DFE22D910119F * ___value0, const RuntimeMethod* method)
{
	{
		// public StateMachine StateMachine { get; private set; }
		StateMachine_t8E24C2687A18E15B27A6A8FEED2DFE22D910119F * L_0 = ___value0;
		__this->set_U3CStateMachineU3Ek__BackingField_9(L_0);
		return;
	}
}
// UnityEngine.AI.NavMeshAgent Ship::get_Agent()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NavMeshAgent_tB9746B6C38013341DB63973CA7ED657494EFB41B * Ship_get_Agent_m2733EC0E5F1D092B7FB03B5BA882B9FAD8C4CF96 (Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * __this, const RuntimeMethod* method)
{
	{
		// public NavMeshAgent Agent { get; private set; }
		NavMeshAgent_tB9746B6C38013341DB63973CA7ED657494EFB41B * L_0 = __this->get_U3CAgentU3Ek__BackingField_10();
		return L_0;
	}
}
// System.Void Ship::set_Agent(UnityEngine.AI.NavMeshAgent)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Ship_set_Agent_m99D21CDD3A46A0B76541A0FBC9B8DA8BA9858A8D (Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * __this, NavMeshAgent_tB9746B6C38013341DB63973CA7ED657494EFB41B * ___value0, const RuntimeMethod* method)
{
	{
		// public NavMeshAgent Agent { get; private set; }
		NavMeshAgent_tB9746B6C38013341DB63973CA7ED657494EFB41B * L_0 = ___value0;
		__this->set_U3CAgentU3Ek__BackingField_10(L_0);
		return;
	}
}
// System.Void Ship::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Ship_Awake_m956DC332783F1DAA6F7F13588816260AD621C9A2 (Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisNavMeshAgent_tB9746B6C38013341DB63973CA7ED657494EFB41B_m15077FF184192C84FCF6B2A1FC8ECF53C9220F2F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ShipMove_t761409814A6DA69DE131EA554AEC499CB81D3953_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ShipWait_tB05589ED6695523DD921B075C449CE03E0B836FE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StateMachine_t8E24C2687A18E15B27A6A8FEED2DFE22D910119F_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Agent = GetComponent<NavMeshAgent>();
		NavMeshAgent_tB9746B6C38013341DB63973CA7ED657494EFB41B * L_0;
		L_0 = Component_GetComponent_TisNavMeshAgent_tB9746B6C38013341DB63973CA7ED657494EFB41B_m15077FF184192C84FCF6B2A1FC8ECF53C9220F2F(__this, /*hidden argument*/Component_GetComponent_TisNavMeshAgent_tB9746B6C38013341DB63973CA7ED657494EFB41B_m15077FF184192C84FCF6B2A1FC8ECF53C9220F2F_RuntimeMethod_var);
		Ship_set_Agent_m99D21CDD3A46A0B76541A0FBC9B8DA8BA9858A8D_inline(__this, L_0, /*hidden argument*/NULL);
		// waitState = new ShipWait(this) { waitTime = waitState.waitTime};
		ShipWait_tB05589ED6695523DD921B075C449CE03E0B836FE * L_1 = (ShipWait_tB05589ED6695523DD921B075C449CE03E0B836FE *)il2cpp_codegen_object_new(ShipWait_tB05589ED6695523DD921B075C449CE03E0B836FE_il2cpp_TypeInfo_var);
		ShipWait__ctor_mC7C641CCA9F94A3C7D6BBF10F32B8F2B77496FA5(L_1, __this, /*hidden argument*/NULL);
		ShipWait_tB05589ED6695523DD921B075C449CE03E0B836FE * L_2 = L_1;
		ShipWait_tB05589ED6695523DD921B075C449CE03E0B836FE * L_3 = __this->get_waitState_7();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_4 = L_3->get_waitTime_1();
		L_2->set_waitTime_1(L_4);
		__this->set_waitState_7(L_2);
		// moveState = new ShipMove(this);
		ShipMove_t761409814A6DA69DE131EA554AEC499CB81D3953 * L_5 = (ShipMove_t761409814A6DA69DE131EA554AEC499CB81D3953 *)il2cpp_codegen_object_new(ShipMove_t761409814A6DA69DE131EA554AEC499CB81D3953_il2cpp_TypeInfo_var);
		ShipMove__ctor_mB007B621D62FBD63A71C4B9A679A3E2707A8C639(L_5, __this, /*hidden argument*/NULL);
		__this->set_moveState_8(L_5);
		// StateMachine = new StateMachine(waitState);
		ShipWait_tB05589ED6695523DD921B075C449CE03E0B836FE * L_6 = __this->get_waitState_7();
		StateMachine_t8E24C2687A18E15B27A6A8FEED2DFE22D910119F * L_7 = (StateMachine_t8E24C2687A18E15B27A6A8FEED2DFE22D910119F *)il2cpp_codegen_object_new(StateMachine_t8E24C2687A18E15B27A6A8FEED2DFE22D910119F_il2cpp_TypeInfo_var);
		StateMachine__ctor_m4362A8F7E5321C0B2F5D8E85462C1643F863E457(L_7, L_6, /*hidden argument*/NULL);
		Ship_set_StateMachine_mEB49B2626B6B51769D40F7CC9FC3FA2D59961F13_inline(__this, L_7, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Ship::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Ship_Start_m809FEE5ACEBDF1EC3E7A57D31FF5D6D994E0F4BF (Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * __this, const RuntimeMethod* method)
{
	{
		// PlayerController.Instance.MoveToPosition(transform.position + playerSpawnOffset);
		PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9 * L_0;
		L_0 = PlayerController_get_Instance_m8C373614456176CE37B49E24BA9B7E019C5A0440_inline(/*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1;
		L_1 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		L_2 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_1, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3 = __this->get_playerSpawnOffset_5();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4;
		L_4 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_2, L_3, /*hidden argument*/NULL);
		PlayerController_MoveToPosition_m9E1B5DA225A7BE1C6E859C6294A44281E5480876(L_0, L_4, /*hidden argument*/NULL);
		// WorldMap.Instance.AddInteraction(gameObject);
		WorldMap_t01A07DDC9EA386C88D5814CF18E0F37CAF05270C * L_5;
		L_5 = WorldMap_get_Instance_m119402AD39383BC71435F9F36C3219A2281289CA_inline(/*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_6;
		L_6 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		bool L_7;
		L_7 = WorldMap_AddInteraction_mF2CC83A625FCFB963745D016D72BC262AB4A94F4(L_5, L_6, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Ship::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Ship_Update_mC04E9DB38367AC83C98CCF1C377D7C442FAE2A7E (Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * __this, const RuntimeMethod* method)
{
	{
		// StateMachine.OnUpdate();
		StateMachine_t8E24C2687A18E15B27A6A8FEED2DFE22D910119F * L_0;
		L_0 = Ship_get_StateMachine_m9110695EE50E4C7260E64164574C5394CFF403C1_inline(__this, /*hidden argument*/NULL);
		StateMachine_OnUpdate_m5C824B8C704BF7B7E99B53B7BC0A17D05EFE3DA6(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.String Ship::Inspect()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Ship_Inspect_mCB84D7CA931A5BFB47F385CFF0C8EFD4284BCFFF (Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * __this, const RuntimeMethod* method)
{
	{
		// return tooltipText;
		String_t* L_0 = __this->get_tooltipText_4();
		return L_0;
	}
}
// System.Void Ship::Leave()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Ship_Leave_m126EADEB1988B84E8ED2F86F7E20A0373C5AF64B (Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3E006858D3E74664A4AB5E9754647D20A4D98A94);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4D2CACA605B9C43AD760EA775DD96667ED22EDA4);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		// if(GameManager.Instance.TargetFound == true)  //if target fish found
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_0;
		L_0 = GameManager_get_Instance_m758C0D98BBDEF0C2DCA163A5D60B22DBB5B07232_inline(/*hidden argument*/NULL);
		bool L_1;
		L_1 = GameManager_get_TargetFound_mD646125D22C8183B642D2ECD48C64FE59D1D12AE_inline(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001e;
		}
	}
	{
		// MouseLook.Instance.CursorToggle = true;
		MouseLook_t1E1EE79B13022184F3EF9AC8FE8C6771053890DA * L_2;
		L_2 = MouseLook_get_Instance_m4F889A9C207EAE72011AF1BA9DA70046C5404386_inline(/*hidden argument*/NULL);
		MouseLook_set_CursorToggle_m2DBA5589D35ED1938A52174EC990627601603468(L_2, (bool)1, /*hidden argument*/NULL);
		// SceneManager.LoadScene(0);
		IL2CPP_RUNTIME_CLASS_INIT(SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_m5550E6368A6D0E37DACEDA3C5E4BA331836BC3C5(0, /*hidden argument*/NULL);
		// }
		return;
	}

IL_001e:
	{
		// string message = $"You need to find the target fish'{GameManager.Instance.targetText.text}' before you can leave";
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_3;
		L_3 = GameManager_get_Instance_m758C0D98BBDEF0C2DCA163A5D60B22DBB5B07232_inline(/*hidden argument*/NULL);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_4 = L_3->get_targetText_8();
		String_t* L_5;
		L_5 = VirtFuncInvoker0< String_t* >::Invoke(74 /* System.String UnityEngine.UI.Text::get_text() */, L_4);
		String_t* L_6;
		L_6 = String_Concat_m89EAB4C6A96B0E5C3F87300D6BE78D386B9EFC44(_stringLiteral4D2CACA605B9C43AD760EA775DD96667ED22EDA4, L_5, _stringLiteral3E006858D3E74664A4AB5E9754647D20A4D98A94, /*hidden argument*/NULL);
		V_0 = L_6;
		// ToolTip.Instance.Toggle(message);
		ToolTip_t044257731DC5931927D3AB0671C17515BD8E251E * L_7;
		L_7 = ToolTip_get_Instance_m5CB773BB76F41361836DE8051081FD5CDF562969_inline(/*hidden argument*/NULL);
		String_t* L_8 = V_0;
		ToolTip_Toggle_m7BB7DD0FB5AA7DD002A5173E3391AF8457627F6C(L_7, L_8, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Ship::OnDrawGizmos()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Ship_OnDrawGizmos_m9D86874A855361682803C23AB3ED843A9EAB0D49 (Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StateMachine_GetCurrentStateAsType_TisShipState_tF56B93F7FFD3E22AB4F4ECDF9A73D50A3FE508A5_m955D4460B9487D2450FF1C450DEB3B39C73F6255_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Gizmos.DrawWireCube(boundBox.center, boundBox.size);
		Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 * L_0 = __this->get_address_of_boundBox_6();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1;
		L_1 = Bounds_get_center_m78CD262996DD859F71DAFFF39228EBE0C422F485((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)L_0, /*hidden argument*/NULL);
		Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 * L_2 = __this->get_address_of_boundBox_6();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3;
		L_3 = Bounds_get_size_mB1C37E89879C7810BC9F4210033D9277DAFE2C14((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)L_2, /*hidden argument*/NULL);
		Gizmos_DrawWireCube_mC526244E50C6E5793D4066C9C99023D5FF8424BF(L_1, L_3, /*hidden argument*/NULL);
		// if(StateMachine != null && StateMachine.CurrentState != null)
		StateMachine_t8E24C2687A18E15B27A6A8FEED2DFE22D910119F * L_4;
		L_4 = Ship_get_StateMachine_m9110695EE50E4C7260E64164574C5394CFF403C1_inline(__this, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0040;
		}
	}
	{
		StateMachine_t8E24C2687A18E15B27A6A8FEED2DFE22D910119F * L_5;
		L_5 = Ship_get_StateMachine_m9110695EE50E4C7260E64164574C5394CFF403C1_inline(__this, /*hidden argument*/NULL);
		RuntimeObject* L_6;
		L_6 = StateMachine_get_CurrentState_mCDB0B1FC6472E599268D5E92F31C0D890613B326_inline(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0040;
		}
	}
	{
		// StateMachine.GetCurrentStateAsType<ShipState>().DrawGizmos();
		StateMachine_t8E24C2687A18E15B27A6A8FEED2DFE22D910119F * L_7;
		L_7 = Ship_get_StateMachine_m9110695EE50E4C7260E64164574C5394CFF403C1_inline(__this, /*hidden argument*/NULL);
		ShipState_tF56B93F7FFD3E22AB4F4ECDF9A73D50A3FE508A5 * L_8;
		L_8 = StateMachine_GetCurrentStateAsType_TisShipState_tF56B93F7FFD3E22AB4F4ECDF9A73D50A3FE508A5_m955D4460B9487D2450FF1C450DEB3B39C73F6255(L_7, /*hidden argument*/StateMachine_GetCurrentStateAsType_TisShipState_tF56B93F7FFD3E22AB4F4ECDF9A73D50A3FE508A5_m955D4460B9487D2450FF1C450DEB3B39C73F6255_RuntimeMethod_var);
		VirtActionInvoker0::Invoke(10 /* System.Void ShipState::DrawGizmos() */, L_8);
	}

IL_0040:
	{
		// }
		return;
	}
}
// System.Void Ship::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Ship__ctor_mA4FC970EB2F615E5B00A86D2B89DCBAC88C9CC1E (Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ShipMove::.ctor(Ship)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShipMove__ctor_mB007B621D62FBD63A71C4B9A679A3E2707A8C639 (ShipMove_t761409814A6DA69DE131EA554AEC499CB81D3953 * __this, Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * ___instance0, const RuntimeMethod* method)
{
	{
		// public ShipMove(Ship instance) : base(instance)
		Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * L_0 = ___instance0;
		ShipState__ctor_mEA5C98A1E34AE5F3ADF5D3940A212D61561FBD48(__this, L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void ShipMove::OnEnter()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShipMove_OnEnter_mAF5CB80592877AC4484E83F9027248012294BF6D (ShipMove_t761409814A6DA69DE131EA554AEC499CB81D3953 * __this, const RuntimeMethod* method)
{
	{
		// targetPos = GetRandomPosInBounds();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0;
		L_0 = ShipMove_GetRandomPosInBounds_mF73CF65739E672B709F7304EE16A896E94E1F7E4(__this, /*hidden argument*/NULL);
		__this->set_targetPos_1(L_0);
		// Instance.Agent.SetDestination(targetPos);
		Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * L_1;
		L_1 = ShipState_get_Instance_m11696AE1492C8B96116FF2687CE13EA7B45428A0_inline(__this, /*hidden argument*/NULL);
		NavMeshAgent_tB9746B6C38013341DB63973CA7ED657494EFB41B * L_2;
		L_2 = Ship_get_Agent_m2733EC0E5F1D092B7FB03B5BA882B9FAD8C4CF96_inline(L_1, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3 = __this->get_targetPos_1();
		bool L_4;
		L_4 = NavMeshAgent_SetDestination_m244EFBCDB717576303DA711EE39572B865F43747(L_2, L_3, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void ShipMove::OnUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShipMove_OnUpdate_mCD2173519CDBE3734589F653C812EE130F678485 (ShipMove_t761409814A6DA69DE131EA554AEC499CB81D3953 * __this, const RuntimeMethod* method)
{
	{
		// if(Vector3.Distance(Instance.transform.position, targetPos) < Instance.Agent.stoppingDistance)
		Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * L_0;
		L_0 = ShipState_get_Instance_m11696AE1492C8B96116FF2687CE13EA7B45428A0_inline(__this, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1;
		L_1 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_0, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		L_2 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_1, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3 = __this->get_targetPos_1();
		float L_4;
		L_4 = Vector3_Distance_mB648A79E4A1BAAFBF7B029644638C0D715480677(L_2, L_3, /*hidden argument*/NULL);
		Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * L_5;
		L_5 = ShipState_get_Instance_m11696AE1492C8B96116FF2687CE13EA7B45428A0_inline(__this, /*hidden argument*/NULL);
		NavMeshAgent_tB9746B6C38013341DB63973CA7ED657494EFB41B * L_6;
		L_6 = Ship_get_Agent_m2733EC0E5F1D092B7FB03B5BA882B9FAD8C4CF96_inline(L_5, /*hidden argument*/NULL);
		float L_7;
		L_7 = NavMeshAgent_get_stoppingDistance_mE2F58A8DB9C8402F0373576AB91690E8B34C1EA6(L_6, /*hidden argument*/NULL);
		if ((!(((float)L_4) < ((float)L_7))))
		{
			goto IL_0048;
		}
	}
	{
		// Instance.StateMachine.SetState(Instance.waitState);
		Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * L_8;
		L_8 = ShipState_get_Instance_m11696AE1492C8B96116FF2687CE13EA7B45428A0_inline(__this, /*hidden argument*/NULL);
		StateMachine_t8E24C2687A18E15B27A6A8FEED2DFE22D910119F * L_9;
		L_9 = Ship_get_StateMachine_m9110695EE50E4C7260E64164574C5394CFF403C1_inline(L_8, /*hidden argument*/NULL);
		Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * L_10;
		L_10 = ShipState_get_Instance_m11696AE1492C8B96116FF2687CE13EA7B45428A0_inline(__this, /*hidden argument*/NULL);
		ShipWait_tB05589ED6695523DD921B075C449CE03E0B836FE * L_11 = L_10->get_waitState_7();
		StateMachine_SetState_m7BA252E835DBB9FBFF89545B6634AFD0DEB659A0(L_9, L_11, /*hidden argument*/NULL);
	}

IL_0048:
	{
		// }
		return;
	}
}
// System.Void ShipMove::DrawGizmos()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShipMove_DrawGizmos_m798CCF44823A7E5864E8948948B2ECBAE0E57807 (ShipMove_t761409814A6DA69DE131EA554AEC499CB81D3953 * __this, const RuntimeMethod* method)
{
	{
		// Gizmos.color = Color.red;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_0;
		L_0 = Color_get_red_m9BD55EBF7A74A515330FA5F7AC7A67C8A8913DD8(/*hidden argument*/NULL);
		Gizmos_set_color_m937ACC6288C81BAFFC3449FAA03BB4F680F4E74F(L_0, /*hidden argument*/NULL);
		// Gizmos.DrawSphere(targetPos, 1);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1 = __this->get_targetPos_1();
		Gizmos_DrawSphere_m50414CF8E502F4D93FC133091DA5E39543D69E91(L_1, (1.0f), /*hidden argument*/NULL);
		// }
		return;
	}
}
// UnityEngine.Vector3 ShipMove::GetRandomPosInBounds()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ShipMove_GetRandomPosInBounds_mF73CF65739E672B709F7304EE16A896E94E1F7E4 (ShipMove_t761409814A6DA69DE131EA554AEC499CB81D3953 * __this, const RuntimeMethod* method)
{
	{
		// return new Vector3(Random.Range(-Instance.boundBox.extents.x, Instance.boundBox.extents.x), Instance.transform.position.y, Random.Range(-Instance.boundBox.extents.z, Instance.boundBox.extents.z));
		Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * L_0;
		L_0 = ShipState_get_Instance_m11696AE1492C8B96116FF2687CE13EA7B45428A0_inline(__this, /*hidden argument*/NULL);
		Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 * L_1 = L_0->get_address_of_boundBox_6();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		L_2 = Bounds_get_extents_mA54D7497D65DCF21CA952FA754B9D1086305FF02((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)L_1, /*hidden argument*/NULL);
		float L_3 = L_2.get_x_2();
		Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * L_4;
		L_4 = ShipState_get_Instance_m11696AE1492C8B96116FF2687CE13EA7B45428A0_inline(__this, /*hidden argument*/NULL);
		Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 * L_5 = L_4->get_address_of_boundBox_6();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6;
		L_6 = Bounds_get_extents_mA54D7497D65DCF21CA952FA754B9D1086305FF02((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)L_5, /*hidden argument*/NULL);
		float L_7 = L_6.get_x_2();
		float L_8;
		L_8 = Random_Range_mC15372D42A9ABDCAC3DE82E114D60A40C9C311D2(((-L_3)), L_7, /*hidden argument*/NULL);
		Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * L_9;
		L_9 = ShipState_get_Instance_m11696AE1492C8B96116FF2687CE13EA7B45428A0_inline(__this, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_10;
		L_10 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_9, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_11;
		L_11 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_10, /*hidden argument*/NULL);
		float L_12 = L_11.get_y_3();
		Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * L_13;
		L_13 = ShipState_get_Instance_m11696AE1492C8B96116FF2687CE13EA7B45428A0_inline(__this, /*hidden argument*/NULL);
		Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 * L_14 = L_13->get_address_of_boundBox_6();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_15;
		L_15 = Bounds_get_extents_mA54D7497D65DCF21CA952FA754B9D1086305FF02((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)L_14, /*hidden argument*/NULL);
		float L_16 = L_15.get_z_4();
		Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * L_17;
		L_17 = ShipState_get_Instance_m11696AE1492C8B96116FF2687CE13EA7B45428A0_inline(__this, /*hidden argument*/NULL);
		Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 * L_18 = L_17->get_address_of_boundBox_6();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_19;
		L_19 = Bounds_get_extents_mA54D7497D65DCF21CA952FA754B9D1086305FF02((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)L_18, /*hidden argument*/NULL);
		float L_20 = L_19.get_z_4();
		float L_21;
		L_21 = Random_Range_mC15372D42A9ABDCAC3DE82E114D60A40C9C311D2(((-L_16)), L_20, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_22;
		memset((&L_22), 0, sizeof(L_22));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_22), L_8, L_12, L_21, /*hidden argument*/NULL);
		return L_22;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Ship ShipState::get_Instance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * ShipState_get_Instance_m11696AE1492C8B96116FF2687CE13EA7B45428A0 (ShipState_tF56B93F7FFD3E22AB4F4ECDF9A73D50A3FE508A5 * __this, const RuntimeMethod* method)
{
	{
		// public Ship Instance { get; private set; }
		Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * L_0 = __this->get_U3CInstanceU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void ShipState::set_Instance(Ship)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShipState_set_Instance_m37F9D75A65E59E0500535234B7F46301E108BFCF (ShipState_tF56B93F7FFD3E22AB4F4ECDF9A73D50A3FE508A5 * __this, Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * ___value0, const RuntimeMethod* method)
{
	{
		// public Ship Instance { get; private set; }
		Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * L_0 = ___value0;
		__this->set_U3CInstanceU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Void ShipState::.ctor(Ship)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShipState__ctor_mEA5C98A1E34AE5F3ADF5D3940A212D61561FBD48 (ShipState_tF56B93F7FFD3E22AB4F4ECDF9A73D50A3FE508A5 * __this, Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * ___instance0, const RuntimeMethod* method)
{
	{
		// public ShipState(Ship instance)
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		// Instance = instance;
		Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * L_0 = ___instance0;
		ShipState_set_Instance_m37F9D75A65E59E0500535234B7F46301E108BFCF_inline(__this, L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void ShipState::OnEnter()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShipState_OnEnter_mD09267B90F3E7D321C6F73DDD37D625DB1EC3B44 (ShipState_tF56B93F7FFD3E22AB4F4ECDF9A73D50A3FE508A5 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void ShipState::OnExit()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShipState_OnExit_mD10B24E4176AF5474BEACBCB48E50368FDE8264D (ShipState_tF56B93F7FFD3E22AB4F4ECDF9A73D50A3FE508A5 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void ShipState::OnUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShipState_OnUpdate_mB9D658F5434ABD6F3AA4770908C5F361B3627D03 (ShipState_tF56B93F7FFD3E22AB4F4ECDF9A73D50A3FE508A5 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void ShipState::DrawGizmos()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShipState_DrawGizmos_m673B6C785E859D1C49481C9183CA27910A3DF0F7 (ShipState_tF56B93F7FFD3E22AB4F4ECDF9A73D50A3FE508A5 * __this, const RuntimeMethod* method)
{
	{
		// public virtual void DrawGizmos() { }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ShipWait::.ctor(Ship)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShipWait__ctor_mC7C641CCA9F94A3C7D6BBF10F32B8F2B77496FA5 (ShipWait_tB05589ED6695523DD921B075C449CE03E0B836FE * __this, Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * ___instance0, const RuntimeMethod* method)
{
	{
		// public Vector2 waitTime = new Vector2(1, 3);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0;
		memset((&L_0), 0, sizeof(L_0));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_0), (1.0f), (3.0f), /*hidden argument*/NULL);
		__this->set_waitTime_1(L_0);
		// private float timer = -1;
		__this->set_timer_3((-1.0f));
		// public ShipWait(Ship instance) : base(instance)
		Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * L_1 = ___instance0;
		ShipState__ctor_mEA5C98A1E34AE5F3ADF5D3940A212D61561FBD48(__this, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void ShipWait::OnEnter()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShipWait_OnEnter_m9E6683753E750BF44EEB10B25083DF5E6D00BDF9 (ShipWait_tB05589ED6695523DD921B075C449CE03E0B836FE * __this, const RuntimeMethod* method)
{
	{
		// time = Random.Range(waitTime.x, waitTime.y);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_0 = __this->get_address_of_waitTime_1();
		float L_1 = L_0->get_x_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_2 = __this->get_address_of_waitTime_1();
		float L_3 = L_2->get_y_1();
		float L_4;
		L_4 = Random_Range_mC15372D42A9ABDCAC3DE82E114D60A40C9C311D2(L_1, L_3, /*hidden argument*/NULL);
		__this->set_time_2(L_4);
		// timer = 0;
		__this->set_timer_3((0.0f));
		// }
		return;
	}
}
// System.Void ShipWait::OnUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShipWait_OnUpdate_mC4EC33BF6D9C15D7FD6F5B23D178774D2960C781 (ShipWait_tB05589ED6695523DD921B075C449CE03E0B836FE * __this, const RuntimeMethod* method)
{
	{
		// if(timer > -1)
		float L_0 = __this->get_timer_3();
		if ((!(((float)L_0) > ((float)(-1.0f)))))
		{
			goto IL_0048;
		}
	}
	{
		// timer += Time.deltaTime;
		float L_1 = __this->get_timer_3();
		float L_2;
		L_2 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		__this->set_timer_3(((float)il2cpp_codegen_add((float)L_1, (float)L_2)));
		// if(timer > time)
		float L_3 = __this->get_timer_3();
		float L_4 = __this->get_time_2();
		if ((!(((float)L_3) > ((float)L_4))))
		{
			goto IL_0048;
		}
	}
	{
		// Instance.StateMachine.SetState(Instance.moveState);
		Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * L_5;
		L_5 = ShipState_get_Instance_m11696AE1492C8B96116FF2687CE13EA7B45428A0_inline(__this, /*hidden argument*/NULL);
		StateMachine_t8E24C2687A18E15B27A6A8FEED2DFE22D910119F * L_6;
		L_6 = Ship_get_StateMachine_m9110695EE50E4C7260E64164574C5394CFF403C1_inline(L_5, /*hidden argument*/NULL);
		Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * L_7;
		L_7 = ShipState_get_Instance_m11696AE1492C8B96116FF2687CE13EA7B45428A0_inline(__this, /*hidden argument*/NULL);
		ShipMove_t761409814A6DA69DE131EA554AEC499CB81D3953 * L_8 = L_7->get_moveState_8();
		StateMachine_SetState_m7BA252E835DBB9FBFF89545B6634AFD0DEB659A0(L_6, L_8, /*hidden argument*/NULL);
	}

IL_0048:
	{
		// }
		return;
	}
}
// System.Void ShipWait::OnExit()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShipWait_OnExit_m253088D6C12BD0C78612108AC96BFDB0EDD682E9 (ShipWait_tB05589ED6695523DD921B075C449CE03E0B836FE * __this, const RuntimeMethod* method)
{
	{
		// timer = -1;
		__this->set_timer_3((-1.0f));
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SoloFish::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SoloFish_Update_mF84BAC95F9ECBA23F0C5E7B257508B31D1425450 (SoloFish_t248C1128425014659AF96F9294BAC7D1799179C2 * __this, const RuntimeMethod* method)
{
	{
		// if(Random.Range(0, 1000) < 10)
		int32_t L_0;
		L_0 = Random_Range_m4B3A0037ACA057F33C94508F908546B9317D996A(0, ((int32_t)1000), /*hidden argument*/NULL);
		if ((((int32_t)L_0) >= ((int32_t)((int32_t)10))))
		{
			goto IL_001b;
		}
	}
	{
		// targetPosition = GetRandomPosInBounds();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1;
		L_1 = SoloFish_GetRandomPosInBounds_m8622D90A0BB64C591B451299165E119524C6B3E1(__this, /*hidden argument*/NULL);
		__this->set_targetPosition_17(L_1);
	}

IL_001b:
	{
		// MoveStep();
		SoloFish_MoveStep_m75074289C1B548F4A42BFC3A0E377F3B58031E3E(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void SoloFish::OnDrawGizmos()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SoloFish_OnDrawGizmos_mFE0A5C49D7FA515541574BAD7297DD258BCFFD71 (SoloFish_t248C1128425014659AF96F9294BAC7D1799179C2 * __this, const RuntimeMethod* method)
{
	{
		// Gizmos.color = Color.red;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_0;
		L_0 = Color_get_red_m9BD55EBF7A74A515330FA5F7AC7A67C8A8913DD8(/*hidden argument*/NULL);
		Gizmos_set_color_m937ACC6288C81BAFFC3449FAA03BB4F680F4E74F(L_0, /*hidden argument*/NULL);
		// Gizmos.DrawWireCube(boundBox.center, boundBox.size);
		Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 * L_1 = __this->get_address_of_boundBox_5();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		L_2 = Bounds_get_center_m78CD262996DD859F71DAFFF39228EBE0C422F485((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)L_1, /*hidden argument*/NULL);
		Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 * L_3 = __this->get_address_of_boundBox_5();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4;
		L_4 = Bounds_get_size_mB1C37E89879C7810BC9F4210033D9277DAFE2C14((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)L_3, /*hidden argument*/NULL);
		Gizmos_DrawWireCube_mC526244E50C6E5793D4066C9C99023D5FF8424BF(L_2, L_4, /*hidden argument*/NULL);
		// Gizmos.DrawWireSphere(targetPosition, 1);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5 = __this->get_targetPosition_17();
		Gizmos_DrawWireSphere_m96C425145BBD85CF0192F9DDB3D1A8C69429B78B(L_5, (1.0f), /*hidden argument*/NULL);
		// }
		return;
	}
}
// UnityEngine.Vector3 SoloFish::GetRandomPosInBounds()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  SoloFish_GetRandomPosInBounds_m8622D90A0BB64C591B451299165E119524C6B3E1 (SoloFish_t248C1128425014659AF96F9294BAC7D1799179C2 * __this, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// Vector3 randomCoords = new Vector3(Random.Range(-boundBox.extents.x, boundBox.extents.x), Random.Range(-boundBox.extents.y, boundBox.extents.y), Random.Range(-boundBox.extents.z, boundBox.extents.z));
		Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 * L_0 = __this->get_address_of_boundBox_5();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1;
		L_1 = Bounds_get_extents_mA54D7497D65DCF21CA952FA754B9D1086305FF02((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)L_0, /*hidden argument*/NULL);
		float L_2 = L_1.get_x_2();
		Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 * L_3 = __this->get_address_of_boundBox_5();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4;
		L_4 = Bounds_get_extents_mA54D7497D65DCF21CA952FA754B9D1086305FF02((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)L_3, /*hidden argument*/NULL);
		float L_5 = L_4.get_x_2();
		float L_6;
		L_6 = Random_Range_mC15372D42A9ABDCAC3DE82E114D60A40C9C311D2(((-L_2)), L_5, /*hidden argument*/NULL);
		Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 * L_7 = __this->get_address_of_boundBox_5();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8;
		L_8 = Bounds_get_extents_mA54D7497D65DCF21CA952FA754B9D1086305FF02((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)L_7, /*hidden argument*/NULL);
		float L_9 = L_8.get_y_3();
		Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 * L_10 = __this->get_address_of_boundBox_5();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_11;
		L_11 = Bounds_get_extents_mA54D7497D65DCF21CA952FA754B9D1086305FF02((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)L_10, /*hidden argument*/NULL);
		float L_12 = L_11.get_y_3();
		float L_13;
		L_13 = Random_Range_mC15372D42A9ABDCAC3DE82E114D60A40C9C311D2(((-L_9)), L_12, /*hidden argument*/NULL);
		Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 * L_14 = __this->get_address_of_boundBox_5();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_15;
		L_15 = Bounds_get_extents_mA54D7497D65DCF21CA952FA754B9D1086305FF02((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)L_14, /*hidden argument*/NULL);
		float L_16 = L_15.get_z_4();
		Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 * L_17 = __this->get_address_of_boundBox_5();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_18;
		L_18 = Bounds_get_extents_mA54D7497D65DCF21CA952FA754B9D1086305FF02((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)L_17, /*hidden argument*/NULL);
		float L_19 = L_18.get_z_4();
		float L_20;
		L_20 = Random_Range_mC15372D42A9ABDCAC3DE82E114D60A40C9C311D2(((-L_16)), L_19, /*hidden argument*/NULL);
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_0), L_6, L_13, L_20, /*hidden argument*/NULL);
		// return boundBox.center + randomCoords;
		Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 * L_21 = __this->get_address_of_boundBox_5();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_22;
		L_22 = Bounds_get_center_m78CD262996DD859F71DAFFF39228EBE0C422F485((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)L_21, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_23 = V_0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_24;
		L_24 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_22, L_23, /*hidden argument*/NULL);
		return L_24;
	}
}
// System.Void SoloFish::MoveStep()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SoloFish_MoveStep_m75074289C1B548F4A42BFC3A0E377F3B58031E3E (SoloFish_t248C1128425014659AF96F9294BAC7D1799179C2 * __this, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_1;
	memset((&V_1), 0, sizeof(V_1));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_2;
	memset((&V_2), 0, sizeof(V_2));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_3;
	memset((&V_3), 0, sizeof(V_3));
	{
		// Vector3 bounds = Bounds() * boundsWeight;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0;
		L_0 = SoloFish_Bounds_m456DD94835D1E087A1A45D1258B1496EBB7778A7(__this, /*hidden argument*/NULL);
		float L_1 = __this->get_boundsWeight_12();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		L_2 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_0, L_1, /*hidden argument*/NULL);
		// Vector3 avoidance = ObstacleAvoidance() * obstacleWeight;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3;
		L_3 = SoloFish_ObstacleAvoidance_m5D27E74C36F7F0C727F4AEA88E249EF022CD779C(__this, /*hidden argument*/NULL);
		float L_4 = __this->get_obstacleWeight_13();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5;
		L_5 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		// Vector3 target = (targetPosition - transform.position).normalized * targetWeight;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = __this->get_targetPosition_17();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_7;
		L_7 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8;
		L_8 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_7, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9;
		L_9 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_6, L_8, /*hidden argument*/NULL);
		V_3 = L_9;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10;
		L_10 = Vector3_get_normalized_m2FA6DF38F97BDA4CCBDAE12B9FE913A241DAC8D5((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_3), /*hidden argument*/NULL);
		float L_11 = __this->get_targetWeight_14();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		L_12 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_10, L_11, /*hidden argument*/NULL);
		V_1 = L_12;
		// Vector3 motion = bounds + avoidance + target;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = V_0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_14;
		L_14 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_2, L_13, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_15 = V_1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_16;
		L_16 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_14, L_15, /*hidden argument*/NULL);
		V_2 = L_16;
		// motion = Vector3.SmoothDamp(transform.forward, motion, ref currentVelocity, smoothDamp);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_17;
		L_17 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_18;
		L_18 = Transform_get_forward_mD850B9ECF892009E3485408DC0D375165B7BF053(L_17, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_19 = V_2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_20 = __this->get_address_of_currentVelocity_15();
		float L_21 = __this->get_smoothDamp_6();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_22;
		L_22 = Vector3_SmoothDamp_m4655944DBD5B44125F8F4B5A15E31DE94CB0F627(L_18, L_19, (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)L_20, L_21, /*hidden argument*/NULL);
		V_2 = L_22;
		// motion = motion.normalized * speed;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_23;
		L_23 = Vector3_get_normalized_m2FA6DF38F97BDA4CCBDAE12B9FE913A241DAC8D5((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_2), /*hidden argument*/NULL);
		float L_24 = __this->get_speed_10();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_25;
		L_25 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_23, L_24, /*hidden argument*/NULL);
		V_2 = L_25;
		// if(motion == Vector3.zero)
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_26 = V_2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_27;
		L_27 = Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6(/*hidden argument*/NULL);
		bool L_28;
		L_28 = Vector3_op_Equality_m8A98C7F38641110A2F90445EF8E98ECE14B08296(L_26, L_27, /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_00a4;
		}
	}
	{
		// motion = transform.forward;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_29;
		L_29 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_30;
		L_30 = Transform_get_forward_mD850B9ECF892009E3485408DC0D375165B7BF053(L_29, /*hidden argument*/NULL);
		V_2 = L_30;
	}

IL_00a4:
	{
		// transform.forward = motion;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_31;
		L_31 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_32 = V_2;
		Transform_set_forward_mAE46B156F55F2F90AB495B17F7C20BF59A5D7D4D(L_31, L_32, /*hidden argument*/NULL);
		// transform.position += motion * Time.deltaTime;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_33;
		L_33 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_34 = L_33;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_35;
		L_35 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_34, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_36 = V_2;
		float L_37;
		L_37 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_38;
		L_38 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_36, L_37, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_39;
		L_39 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_35, L_38, /*hidden argument*/NULL);
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_34, L_39, /*hidden argument*/NULL);
		// }
		return;
	}
}
// UnityEngine.Vector3 SoloFish::ObstacleAvoidance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  SoloFish_ObstacleAvoidance_m5D27E74C36F7F0C727F4AEA88E249EF022CD779C (SoloFish_t248C1128425014659AF96F9294BAC7D1799179C2 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	float V_1 = 0.0f;
	int32_t V_2 = 0;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_3;
	memset((&V_3), 0, sizeof(V_3));
	RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89  V_4;
	memset((&V_4), 0, sizeof(V_4));
	float V_5 = 0.0f;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_6;
	memset((&V_6), 0, sizeof(V_6));
	{
		// Vector3 vector = Vector3.zero;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0;
		L_0 = Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6(/*hidden argument*/NULL);
		V_0 = L_0;
		// float largestDistance = int.MinValue;
		V_1 = (-2.14748365E+09f);
		// for (int i = 0; i < collisionRays.Length; i++)
		V_2 = 0;
		goto IL_00ea;
	}

IL_0013:
	{
		// Vector3 currentDir = transform.TransformDirection(collisionRays[i].normalized);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1;
		L_1 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_2 = __this->get_collisionRays_7();
		int32_t L_3 = V_2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4;
		L_4 = Vector3_get_normalized_m2FA6DF38F97BDA4CCBDAE12B9FE913A241DAC8D5((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)((L_2)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_3))), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5;
		L_5 = Transform_TransformDirection_m6B5E3F0A7C6323159DEC6D9BC035FB53ADD96E91(L_1, L_4, /*hidden argument*/NULL);
		V_3 = L_5;
		// if(Physics.Raycast(transform.position, currentDir, out RaycastHit hit, obstacleDistance, obstacleMask) == true)
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_6;
		L_6 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7;
		L_7 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_6, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8 = V_3;
		float L_9 = __this->get_obstacleDistance_9();
		LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  L_10 = __this->get_obstacleMask_11();
		int32_t L_11;
		L_11 = LayerMask_op_Implicit_mD89E9970822613D8D19B2EBCA36C79391C287BE0(L_10, /*hidden argument*/NULL);
		bool L_12;
		L_12 = Physics_Raycast_mCBD5F7D498C246713EDDBB446E97205DA206C49C(L_7, L_8, (RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)(&V_4), L_9, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_00b0;
		}
	}
	{
		// Debug.DrawRay(transform.position, currentDir * obstacleDistance, Color.green);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_13;
		L_13 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_14;
		L_14 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_13, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_15 = V_3;
		float L_16 = __this->get_obstacleDistance_9();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_17;
		L_17 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_15, L_16, /*hidden argument*/NULL);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_18;
		L_18 = Color_get_green_mFF9BD42534D385A0717B1EAD083ADF08712984B9(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_DrawRay_m918D1131BACEBD7CCEA9D9BFDF3279F6CD56E121(L_14, L_17, L_18, /*hidden argument*/NULL);
		// float sqrDistance = (hit.point - transform.position).sqrMagnitude;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_19;
		L_19 = RaycastHit_get_point_m32F7282CBB2E13393A33BAD046BDA218E48DD21E((RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)(&V_4), /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_20;
		L_20 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_21;
		L_21 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_20, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_22;
		L_22 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_19, L_21, /*hidden argument*/NULL);
		V_6 = L_22;
		float L_23;
		L_23 = Vector3_get_sqrMagnitude_mC567EE6DF411501A8FE1F23A0038862630B88249((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_6), /*hidden argument*/NULL);
		V_5 = L_23;
		// if(sqrDistance > largestDistance)
		float L_24 = V_5;
		float L_25 = V_1;
		if ((!(((float)L_24) > ((float)L_25))))
		{
			goto IL_00e6;
		}
	}
	{
		// largestDistance = sqrDistance;
		float L_26 = V_5;
		V_1 = L_26;
		// vector += -currentDir;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_27 = V_0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_28 = V_3;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_29;
		L_29 = Vector3_op_UnaryNegation_m362EA356F4CADEDB39F965A0DBDED6EA890925F7_inline(L_28, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_30;
		L_30 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_27, L_29, /*hidden argument*/NULL);
		V_0 = L_30;
		// }
		goto IL_00e6;
	}

IL_00b0:
	{
		// Debug.DrawRay(transform.position, currentDir * obstacleDistance, Color.red);  //is just drawing rays so you can see if they are triggering or not.
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_31;
		L_31 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_32;
		L_32 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_31, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_33 = V_3;
		float L_34 = __this->get_obstacleDistance_9();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_35;
		L_35 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_33, L_34, /*hidden argument*/NULL);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_36;
		L_36 = Color_get_red_m9BD55EBF7A74A515330FA5F7AC7A67C8A8913DD8(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_DrawRay_m918D1131BACEBD7CCEA9D9BFDF3279F6CD56E121(L_32, L_35, L_36, /*hidden argument*/NULL);
		// vector += currentDir;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_37 = V_0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_38 = V_3;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_39;
		L_39 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_37, L_38, /*hidden argument*/NULL);
		V_0 = L_39;
		// currentAvoidance = currentDir.normalized;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_40;
		L_40 = Vector3_get_normalized_m2FA6DF38F97BDA4CCBDAE12B9FE913A241DAC8D5((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_3), /*hidden argument*/NULL);
		__this->set_currentAvoidance_16(L_40);
	}

IL_00e6:
	{
		// for (int i = 0; i < collisionRays.Length; i++)
		int32_t L_41 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_41, (int32_t)1));
	}

IL_00ea:
	{
		// for (int i = 0; i < collisionRays.Length; i++)
		int32_t L_42 = V_2;
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_43 = __this->get_collisionRays_7();
		if ((((int32_t)L_42) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_43)->max_length))))))
		{
			goto IL_0013;
		}
	}
	{
		// return vector.normalized;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_44;
		L_44 = Vector3_get_normalized_m2FA6DF38F97BDA4CCBDAE12B9FE913A241DAC8D5((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_0), /*hidden argument*/NULL);
		return L_44;
	}
}
// UnityEngine.Vector3 SoloFish::Bounds()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  SoloFish_Bounds_m456DD94835D1E087A1A45D1258B1496EBB7778A7 (SoloFish_t248C1128425014659AF96F9294BAC7D1799179C2 * __this, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// Vector3 centreOffset = boundBox.center - transform.position;
		Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 * L_0 = __this->get_address_of_boundBox_5();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1;
		L_1 = Bounds_get_center_m78CD262996DD859F71DAFFF39228EBE0C422F485((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)L_0, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_2;
		L_2 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3;
		L_3 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_2, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4;
		L_4 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_1, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		// bool withinBounds = (centreOffset.magnitude >= boundsDistance * 0.9);
		float L_5;
		L_5 = Vector3_get_magnitude_mDDD40612220D8104E77E993E18A101A69A944991((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_0), /*hidden argument*/NULL);
		float L_6 = __this->get_boundsDistance_8();
		// if(withinBounds == true)
		if (!((((int32_t)((!(((double)((double)((double)L_5))) >= ((double)((double)il2cpp_codegen_multiply((double)((double)((double)L_6)), (double)(0.90000000000000002))))))? 1 : 0)) == ((int32_t)0))? 1 : 0))
		{
			goto IL_0044;
		}
	}
	{
		// return centreOffset.normalized;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7;
		L_7 = Vector3_get_normalized_m2FA6DF38F97BDA4CCBDAE12B9FE913A241DAC8D5((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_0), /*hidden argument*/NULL);
		return L_7;
	}

IL_0044:
	{
		// return Vector3.zero;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8;
		L_8 = Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6(/*hidden argument*/NULL);
		return L_8;
	}
}
// System.Void SoloFish::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SoloFish__ctor_m9EABDF66EC0910BB39AF54AB993A344F63775020 (SoloFish_t248C1128425014659AF96F9294BAC7D1799179C2 * __this, const RuntimeMethod* method)
{
	{
		Fish__ctor_mEDE54A2E8EAB7C21444968366A1E9E12AC8D4A48(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// FiniteStateMachine.IState FiniteStateMachine.StateMachine::get_CurrentState()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* StateMachine_get_CurrentState_mCDB0B1FC6472E599268D5E92F31C0D890613B326 (StateMachine_t8E24C2687A18E15B27A6A8FEED2DFE22D910119F * __this, const RuntimeMethod* method)
{
	{
		// public IState CurrentState { get; private set; }
		RuntimeObject* L_0 = __this->get_U3CCurrentStateU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void FiniteStateMachine.StateMachine::set_CurrentState(FiniteStateMachine.IState)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StateMachine_set_CurrentState_m1B411CA93F64340414238167A0DF7B8DBD57B8C8 (StateMachine_t8E24C2687A18E15B27A6A8FEED2DFE22D910119F * __this, RuntimeObject* ___value0, const RuntimeMethod* method)
{
	{
		// public IState CurrentState { get; private set; }
		RuntimeObject* L_0 = ___value0;
		__this->set_U3CCurrentStateU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Void FiniteStateMachine.StateMachine::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StateMachine__ctor_m7EFA2F5F3CA4100D082EE15109939ACAA8B16221 (StateMachine_t8E24C2687A18E15B27A6A8FEED2DFE22D910119F * __this, const RuntimeMethod* method)
{
	{
		// public StateMachine()
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void FiniteStateMachine.StateMachine::.ctor(FiniteStateMachine.IState)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StateMachine__ctor_m4362A8F7E5321C0B2F5D8E85462C1643F863E457 (StateMachine_t8E24C2687A18E15B27A6A8FEED2DFE22D910119F * __this, RuntimeObject* ___initState0, const RuntimeMethod* method)
{
	{
		// public StateMachine(IState initState)
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		// SetState(initState);
		RuntimeObject* L_0 = ___initState0;
		StateMachine_SetState_m7BA252E835DBB9FBFF89545B6634AFD0DEB659A0(__this, L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void FiniteStateMachine.StateMachine::SetState(FiniteStateMachine.IState)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StateMachine_SetState_m7BA252E835DBB9FBFF89545B6634AFD0DEB659A0 (StateMachine_t8E24C2687A18E15B27A6A8FEED2DFE22D910119F * __this, RuntimeObject* ___newState0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IState_t64095858CEB21D62A4A03489C4738FA56F5243CD_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if(CurrentState != null)
		RuntimeObject* L_0;
		L_0 = StateMachine_get_CurrentState_mCDB0B1FC6472E599268D5E92F31C0D890613B326_inline(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		// CurrentState.OnExit();
		RuntimeObject* L_1;
		L_1 = StateMachine_get_CurrentState_mCDB0B1FC6472E599268D5E92F31C0D890613B326_inline(__this, /*hidden argument*/NULL);
		InterfaceActionInvoker0::Invoke(2 /* System.Void FiniteStateMachine.IState::OnExit() */, IState_t64095858CEB21D62A4A03489C4738FA56F5243CD_il2cpp_TypeInfo_var, L_1);
	}

IL_0013:
	{
		// CurrentState = newState;
		RuntimeObject* L_2 = ___newState0;
		StateMachine_set_CurrentState_m1B411CA93F64340414238167A0DF7B8DBD57B8C8_inline(__this, L_2, /*hidden argument*/NULL);
		// CurrentState.OnEnter();
		RuntimeObject* L_3;
		L_3 = StateMachine_get_CurrentState_mCDB0B1FC6472E599268D5E92F31C0D890613B326_inline(__this, /*hidden argument*/NULL);
		InterfaceActionInvoker0::Invoke(0 /* System.Void FiniteStateMachine.IState::OnEnter() */, IState_t64095858CEB21D62A4A03489C4738FA56F5243CD_il2cpp_TypeInfo_var, L_3);
		// }
		return;
	}
}
// System.Void FiniteStateMachine.StateMachine::OnUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StateMachine_OnUpdate_m5C824B8C704BF7B7E99B53B7BC0A17D05EFE3DA6 (StateMachine_t8E24C2687A18E15B27A6A8FEED2DFE22D910119F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IState_t64095858CEB21D62A4A03489C4738FA56F5243CD_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if(CurrentState != null)
		RuntimeObject* L_0;
		L_0 = StateMachine_get_CurrentState_mCDB0B1FC6472E599268D5E92F31C0D890613B326_inline(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		// CurrentState.OnUpdate();
		RuntimeObject* L_1;
		L_1 = StateMachine_get_CurrentState_mCDB0B1FC6472E599268D5E92F31C0D890613B326_inline(__this, /*hidden argument*/NULL);
		InterfaceActionInvoker0::Invoke(1 /* System.Void FiniteStateMachine.IState::OnUpdate() */, IState_t64095858CEB21D62A4A03489C4738FA56F5243CD_il2cpp_TypeInfo_var, L_1);
	}

IL_0013:
	{
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TempInteraction::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TempInteraction_Start_mEF47E0A950D8B4009BA62B8D29562A46B409BD87 (TempInteraction_t379C5510CC31B27F05D6803C86D8ACBAAA4D0587 * __this, const RuntimeMethod* method)
{
	{
		// WorldMap.Instance.AddInteraction(gameObject);
		WorldMap_t01A07DDC9EA386C88D5814CF18E0F37CAF05270C * L_0;
		L_0 = WorldMap_get_Instance_m119402AD39383BC71435F9F36C3219A2281289CA_inline(/*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1;
		L_1 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		bool L_2;
		L_2 = WorldMap_AddInteraction_mF2CC83A625FCFB963745D016D72BC262AB4A94F4(L_0, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.String TempInteraction::Inspect()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* TempInteraction_Inspect_mD82B8EC83A055FA4946EF96D2C191736298520E3 (TempInteraction_t379C5510CC31B27F05D6803C86D8ACBAAA4D0587 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralCC112E57619A877173F6FAE64FE37E7A6BBF8CBA);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return "Fish";
		return _stringLiteralCC112E57619A877173F6FAE64FE37E7A6BBF8CBA;
	}
}
// System.Void TempInteraction::Leave()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TempInteraction_Leave_mB34E7DA19EA52B8D6922633E0BA000ABABF47032 (TempInteraction_t379C5510CC31B27F05D6803C86D8ACBAAA4D0587 * __this, const RuntimeMethod* method)
{
	{
		// throw new System.NotImplementedException();
		NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6 * L_0 = (NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6_il2cpp_TypeInfo_var)));
		NotImplementedException__ctor_mA2E9CE7F00CB335581A296D2596082D57E45BA83(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&TempInteraction_Leave_mB34E7DA19EA52B8D6922633E0BA000ABABF47032_RuntimeMethod_var)));
	}
}
// System.Void TempInteraction::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TempInteraction__ctor_m25A903DAB88BDA6319528D907AD373A55B31284D (TempInteraction_t379C5510CC31B27F05D6803C86D8ACBAAA4D0587 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TestFIsh::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TestFIsh__ctor_m28D4EF296E3A469E92FC6EBCFF88E2399CE288D6 (TestFIsh_t2AC494E6BA9A5DD9FB776A1C4D31A78DC0AD98B2 * __this, const RuntimeMethod* method)
{
	{
		// public TestFIsh()
		Fish__ctor_mEDE54A2E8EAB7C21444968366A1E9E12AC8D4A48(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void TestFIsh::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TestFIsh_Start_mB4AF08A9F80D5012BFDAFF6E861AD6439B2F1484 (TestFIsh_t2AC494E6BA9A5DD9FB776A1C4D31A78DC0AD98B2 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void TestFIsh::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TestFIsh_Update_mE8C86846C90A65D720C8DFAC85D3594A4A08BE17 (TestFIsh_t2AC494E6BA9A5DD9FB776A1C4D31A78DC0AD98B2 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// ToolTip ToolTip::get_Instance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ToolTip_t044257731DC5931927D3AB0671C17515BD8E251E * ToolTip_get_Instance_m5CB773BB76F41361836DE8051081FD5CDF562969 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ToolTip_t044257731DC5931927D3AB0671C17515BD8E251E_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static ToolTip Instance { get; private set; }
		ToolTip_t044257731DC5931927D3AB0671C17515BD8E251E * L_0 = ((ToolTip_t044257731DC5931927D3AB0671C17515BD8E251E_StaticFields*)il2cpp_codegen_static_fields_for(ToolTip_t044257731DC5931927D3AB0671C17515BD8E251E_il2cpp_TypeInfo_var))->get_U3CInstanceU3Ek__BackingField_7();
		return L_0;
	}
}
// System.Void ToolTip::set_Instance(ToolTip)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ToolTip_set_Instance_mD51AE6A1572D2CFFED1C930D088CB363C31B30D2 (ToolTip_t044257731DC5931927D3AB0671C17515BD8E251E * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ToolTip_t044257731DC5931927D3AB0671C17515BD8E251E_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static ToolTip Instance { get; private set; }
		ToolTip_t044257731DC5931927D3AB0671C17515BD8E251E * L_0 = ___value0;
		((ToolTip_t044257731DC5931927D3AB0671C17515BD8E251E_StaticFields*)il2cpp_codegen_static_fields_for(ToolTip_t044257731DC5931927D3AB0671C17515BD8E251E_il2cpp_TypeInfo_var))->set_U3CInstanceU3Ek__BackingField_7(L_0);
		return;
	}
}
// System.Void ToolTip::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ToolTip_Awake_mDE594ACEB5DD7B0AF6922A947B8112E08CD22AD4 (ToolTip_t044257731DC5931927D3AB0671C17515BD8E251E * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mFB5C182E24F496A866F116D3F75CBD8616A46AB3_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (Instance == null)
		ToolTip_t044257731DC5931927D3AB0671C17515BD8E251E * L_0;
		L_0 = ToolTip_get_Instance_m5CB773BB76F41361836DE8051081FD5CDF562969_inline(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0015;
		}
	}
	{
		// Instance = this;
		ToolTip_set_Instance_mD51AE6A1572D2CFFED1C930D088CB363C31B30D2_inline(__this, /*hidden argument*/NULL);
		// }
		goto IL_001c;
	}

IL_0015:
	{
		// enabled = false;
		Behaviour_set_enabled_mDE415591B28853D1CD764C53CB499A2142247F32(__this, (bool)0, /*hidden argument*/NULL);
	}

IL_001c:
	{
		// tooltipText = GetComponentInChildren<Text>();
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_2;
		L_2 = Component_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mFB5C182E24F496A866F116D3F75CBD8616A46AB3(__this, /*hidden argument*/Component_GetComponentInChildren_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_mFB5C182E24F496A866F116D3F75CBD8616A46AB3_RuntimeMethod_var);
		__this->set_tooltipText_5(L_2);
		// }
		return;
	}
}
// System.Void ToolTip::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ToolTip_Start_mEA60282A4DDCE048FBF93A97ADBE152A7FC6B172 (ToolTip_t044257731DC5931927D3AB0671C17515BD8E251E * __this, const RuntimeMethod* method)
{
	{
		// Toggle();
		ToolTip_Toggle_m7BB7DD0FB5AA7DD002A5173E3391AF8457627F6C(__this, (String_t*)NULL, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void ToolTip::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ToolTip_Update_m17C942983DD1AB8DD7153F31DD8125CBD2CF4C00 (ToolTip_t044257731DC5931927D3AB0671C17515BD8E251E * __this, const RuntimeMethod* method)
{
	{
		// if(timer > -1)
		float L_0 = __this->get_timer_6();
		if ((!(((float)L_0) > ((float)(-1.0f)))))
		{
			goto IL_0034;
		}
	}
	{
		// timer += Time.deltaTime;
		float L_1 = __this->get_timer_6();
		float L_2;
		L_2 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		__this->set_timer_6(((float)il2cpp_codegen_add((float)L_1, (float)L_2)));
		// if(timer > tooltipTime)
		float L_3 = __this->get_timer_6();
		float L_4 = __this->get_tooltipTime_4();
		if ((!(((float)L_3) > ((float)L_4))))
		{
			goto IL_0034;
		}
	}
	{
		// Toggle();
		ToolTip_Toggle_m7BB7DD0FB5AA7DD002A5173E3391AF8457627F6C(__this, (String_t*)NULL, /*hidden argument*/NULL);
	}

IL_0034:
	{
		// }
		return;
	}
}
// System.Void ToolTip::Toggle(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ToolTip_Toggle_m7BB7DD0FB5AA7DD002A5173E3391AF8457627F6C (ToolTip_t044257731DC5931927D3AB0671C17515BD8E251E * __this, String_t* ___message0, const RuntimeMethod* method)
{
	{
		// if(message != null)
		String_t* L_0 = ___message0;
		if (!L_0)
		{
			goto IL_0027;
		}
	}
	{
		// tooltipText.text = message;
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_1 = __this->get_tooltipText_5();
		String_t* L_2 = ___message0;
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1, L_2);
		// gameObject.SetActive(true);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_3;
		L_3 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_3, (bool)1, /*hidden argument*/NULL);
		// timer = 0;
		__this->set_timer_6((0.0f));
		// }
		return;
	}

IL_0027:
	{
		// tooltipText.text = null;
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_4 = __this->get_tooltipText_5();
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_4, (String_t*)NULL);
		// gameObject.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5;
		L_5 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_5, (bool)0, /*hidden argument*/NULL);
		// timer = -1;
		__this->set_timer_6((-1.0f));
		// }
		return;
	}
}
// System.Void ToolTip::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ToolTip__ctor_m24E99AD27DDCED6355927FB404EF7F35B72153F5 (ToolTip_t044257731DC5931927D3AB0671C17515BD8E251E * __this, const RuntimeMethod* method)
{
	{
		// public float tooltipTime = 5;
		__this->set_tooltipTime_4((5.0f));
		// private float timer = -1;
		__this->set_timer_6((-1.0f));
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// WorldMap WorldMap::get_Instance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR WorldMap_t01A07DDC9EA386C88D5814CF18E0F37CAF05270C * WorldMap_get_Instance_m119402AD39383BC71435F9F36C3219A2281289CA (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WorldMap_t01A07DDC9EA386C88D5814CF18E0F37CAF05270C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static WorldMap Instance { get; private set; }
		WorldMap_t01A07DDC9EA386C88D5814CF18E0F37CAF05270C * L_0 = ((WorldMap_t01A07DDC9EA386C88D5814CF18E0F37CAF05270C_StaticFields*)il2cpp_codegen_static_fields_for(WorldMap_t01A07DDC9EA386C88D5814CF18E0F37CAF05270C_il2cpp_TypeInfo_var))->get_U3CInstanceU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void WorldMap::set_Instance(WorldMap)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WorldMap_set_Instance_mDDE558CAD26A7D4680B28F1C7FA3AB7F3A47A7B6 (WorldMap_t01A07DDC9EA386C88D5814CF18E0F37CAF05270C * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WorldMap_t01A07DDC9EA386C88D5814CF18E0F37CAF05270C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static WorldMap Instance { get; private set; }
		WorldMap_t01A07DDC9EA386C88D5814CF18E0F37CAF05270C * L_0 = ___value0;
		((WorldMap_t01A07DDC9EA386C88D5814CF18E0F37CAF05270C_StaticFields*)il2cpp_codegen_static_fields_for(WorldMap_t01A07DDC9EA386C88D5814CF18E0F37CAF05270C_il2cpp_TypeInfo_var))->set_U3CInstanceU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Void WorldMap::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WorldMap_Awake_mA1651490B068B066B7F5D421CC73376A7206A70A (WorldMap_t01A07DDC9EA386C88D5814CF18E0F37CAF05270C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if(Instance == null)
		WorldMap_t01A07DDC9EA386C88D5814CF18E0F37CAF05270C * L_0;
		L_0 = WorldMap_get_Instance_m119402AD39383BC71435F9F36C3219A2281289CA_inline(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		// Instance = this;
		WorldMap_set_Instance_mDDE558CAD26A7D4680B28F1C7FA3AB7F3A47A7B6_inline(__this, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0014:
	{
		// enabled = false;
		Behaviour_set_enabled_mDE415591B28853D1CD764C53CB499A2142247F32(__this, (bool)0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void WorldMap::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WorldMap_Start_mF99A53DAB2AB491270FADC92F6DEFADB377C17F7 (WorldMap_t01A07DDC9EA386C88D5814CF18E0F37CAF05270C * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void WorldMap::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WorldMap_Update_m0E632030EF67DA098ABAE289E6E2181884AAF743 (WorldMap_t01A07DDC9EA386C88D5814CF18E0F37CAF05270C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m6206A8F48C7A98B9CFC23A13548CB215717573BD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE5368AE200A37FDFED8AA9B3CB8C7E26E81CA911);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_2;
	memset((&V_2), 0, sizeof(V_2));
	{
		// if(legend.activeSelf == true && timer == -1)
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = __this->get_legend_5();
		bool L_1;
		L_1 = GameObject_get_activeSelf_m4865097C24FB29F3C31F5C30619AF242297F23EE(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00e6;
		}
	}
	{
		float L_2 = __this->get_timer_11();
		if ((!(((float)L_2) == ((float)(-1.0f)))))
		{
			goto IL_00e6;
		}
	}
	{
		// float axis = Input.GetAxis("Menu Selection");
		float L_3;
		L_3 = Input_GetAxis_m939297DEB2ECF8D8D09AD66EB69979AAD2B62326(_stringLiteralE5368AE200A37FDFED8AA9B3CB8C7E26E81CA911, /*hidden argument*/NULL);
		V_0 = L_3;
		// if(axis != 0)
		float L_4 = V_0;
		if ((((float)L_4) == ((float)(0.0f))))
		{
			goto IL_008b;
		}
	}
	{
		// timer = 0;
		__this->set_timer_11((0.0f));
		// legendIndex += Mathf.FloorToInt(axis);
		int32_t L_5 = __this->get_legendIndex_12();
		float L_6 = V_0;
		int32_t L_7;
		L_7 = Mathf_FloorToInt_m9164D538D17B8C3C8A6C4E4FA95032F757D9091E(L_6, /*hidden argument*/NULL);
		__this->set_legendIndex_12(((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)L_7)));
		// if(legendIndex < 0)
		int32_t L_8 = __this->get_legendIndex_12();
		if ((((int32_t)L_8) >= ((int32_t)0)))
		{
			goto IL_006c;
		}
	}
	{
		// legendIndex = legendOptions.Length - 1;
		ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224* L_9 = __this->get_legendOptions_7();
		__this->set_legendIndex_12(((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_9)->max_length))), (int32_t)1)));
		// }
		goto IL_0085;
	}

IL_006c:
	{
		// else if(legendIndex > legendOptions.Length - 1)
		int32_t L_10 = __this->get_legendIndex_12();
		ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224* L_11 = __this->get_legendOptions_7();
		if ((((int32_t)L_10) <= ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_11)->max_length))), (int32_t)1)))))
		{
			goto IL_0085;
		}
	}
	{
		// legendIndex = 0;
		__this->set_legendIndex_12(0);
	}

IL_0085:
	{
		// UpdateLegendSelection();
		WorldMap_UpdateLegendSelection_m96C6070EF674FED8B832E12C9ED1403EFAD2B33A(__this, /*hidden argument*/NULL);
	}

IL_008b:
	{
		// for(int i = 0; i < activeInteractions.Count; i++)  //actiates map icons and sets them to relevant screenposition
		V_1 = 0;
		goto IL_00d8;
	}

IL_008f:
	{
		// Vector3 screenPos = mapCam.WorldToScreenPoint(activeInteractions[i].transform.position);
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_12 = __this->get_mapCam_6();
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_13 = __this->get_activeInteractions_14();
		int32_t L_14 = V_1;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_15;
		L_15 = List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_inline(L_13, L_14, /*hidden argument*/List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_RuntimeMethod_var);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_16;
		L_16 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_15, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_17;
		L_17 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_16, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_18;
		L_18 = Camera_WorldToScreenPoint_m44710195E7736CE9DE5A9B05E32059A9A950F95C(L_12, L_17, /*hidden argument*/NULL);
		V_2 = L_18;
		// screenPos.z = 0;
		(&V_2)->set_z_4((0.0f));
		// mapIcons[i].transform.position = screenPos;
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_19 = __this->get_mapIcons_17();
		int32_t L_20 = V_1;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_21;
		L_21 = List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_inline(L_19, L_20, /*hidden argument*/List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_RuntimeMethod_var);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_22;
		L_22 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_21, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_23 = V_2;
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_22, L_23, /*hidden argument*/NULL);
		// for(int i = 0; i < activeInteractions.Count; i++)  //actiates map icons and sets them to relevant screenposition
		int32_t L_24 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_24, (int32_t)1));
	}

IL_00d8:
	{
		// for(int i = 0; i < activeInteractions.Count; i++)  //actiates map icons and sets them to relevant screenposition
		int32_t L_25 = V_1;
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_26 = __this->get_activeInteractions_14();
		int32_t L_27;
		L_27 = List_1_get_Count_m6206A8F48C7A98B9CFC23A13548CB215717573BD_inline(L_26, /*hidden argument*/List_1_get_Count_m6206A8F48C7A98B9CFC23A13548CB215717573BD_RuntimeMethod_var);
		if ((((int32_t)L_25) < ((int32_t)L_27)))
		{
			goto IL_008f;
		}
	}

IL_00e6:
	{
		// if(timer > -1) //legend selection timer
		float L_28 = __this->get_timer_11();
		if ((!(((float)L_28) > ((float)(-1.0f)))))
		{
			goto IL_011d;
		}
	}
	{
		// timer += Time.deltaTime;
		float L_29 = __this->get_timer_11();
		float L_30;
		L_30 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		__this->set_timer_11(((float)il2cpp_codegen_add((float)L_29, (float)L_30)));
		// if(timer > 0.2f)
		float L_31 = __this->get_timer_11();
		if ((!(((float)L_31) > ((float)(0.200000003f)))))
		{
			goto IL_011d;
		}
	}
	{
		// timer = -1;
		__this->set_timer_11((-1.0f));
	}

IL_011d:
	{
		// }
		return;
	}
}
// System.Void WorldMap::UpdateLegendSelection()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WorldMap_UpdateLegendSelection_m96C6070EF674FED8B832E12C9ED1403EFAD2B33A (WorldMap_t01A07DDC9EA386C88D5814CF18E0F37CAF05270C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m3DD76DE838FA83DF972E0486A296345EB3A7DDF3_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m6C09283D126DAC46B64EE124C67FADA66797752F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m859B0EE8491FDDEB1A3F7115D334B863E025BBC8_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m6206A8F48C7A98B9CFC23A13548CB215717573BD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		// for(int i = 0; i < legendOptions.Length; i++)
		V_0 = 0;
		goto IL_0099;
	}

IL_0007:
	{
		// if(i == legendIndex)
		int32_t L_0 = V_0;
		int32_t L_1 = __this->get_legendIndex_12();
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0083;
		}
	}
	{
		// legendOptions[i].color = Color.white;
		ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224* L_2 = __this->get_legendOptions_7();
		int32_t L_3 = V_0;
		int32_t L_4 = L_3;
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_5 = (L_2)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_4));
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_6;
		L_6 = Color_get_white_mB21E47D20959C3AEC41AF8BA04F63AC89FAF319E(/*hidden argument*/NULL);
		VirtActionInvoker1< Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_5, L_6);
		int32_t L_7 = V_0;
		switch (L_7)
		{
			case 0:
			{
				goto IL_0036;
			}
			case 1:
			{
				goto IL_0049;
			}
			case 2:
			{
				goto IL_005c;
			}
		}
	}
	{
		goto IL_0095;
	}

IL_0036:
	{
		// activeInteractions = new List<GameObject>(keyInteractions);
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_8 = __this->get_keyInteractions_16();
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_9 = (List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 *)il2cpp_codegen_object_new(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5_il2cpp_TypeInfo_var);
		List_1__ctor_m6C09283D126DAC46B64EE124C67FADA66797752F(L_9, L_8, /*hidden argument*/List_1__ctor_m6C09283D126DAC46B64EE124C67FADA66797752F_RuntimeMethod_var);
		__this->set_activeInteractions_14(L_9);
		// break;
		goto IL_0095;
	}

IL_0049:
	{
		// activeInteractions = new List<GameObject>(gateInteractions);
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_10 = __this->get_gateInteractions_15();
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_11 = (List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 *)il2cpp_codegen_object_new(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5_il2cpp_TypeInfo_var);
		List_1__ctor_m6C09283D126DAC46B64EE124C67FADA66797752F(L_11, L_10, /*hidden argument*/List_1__ctor_m6C09283D126DAC46B64EE124C67FADA66797752F_RuntimeMethod_var);
		__this->set_activeInteractions_14(L_11);
		// break;
		goto IL_0095;
	}

IL_005c:
	{
		// if (ship != null)
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_12 = __this->get_ship_13();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_13;
		L_13 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_12, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0095;
		}
	}
	{
		// activeInteractions = new List<GameObject>() { ship };
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_14 = (List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 *)il2cpp_codegen_object_new(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5_il2cpp_TypeInfo_var);
		List_1__ctor_m859B0EE8491FDDEB1A3F7115D334B863E025BBC8(L_14, /*hidden argument*/List_1__ctor_m859B0EE8491FDDEB1A3F7115D334B863E025BBC8_RuntimeMethod_var);
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_15 = L_14;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_16 = __this->get_ship_13();
		List_1_Add_m3DD76DE838FA83DF972E0486A296345EB3A7DDF3(L_15, L_16, /*hidden argument*/List_1_Add_m3DD76DE838FA83DF972E0486A296345EB3A7DDF3_RuntimeMethod_var);
		__this->set_activeInteractions_14(L_15);
		// break;
		goto IL_0095;
	}

IL_0083:
	{
		// legendOptions[i].color = Color.grey;
		ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224* L_17 = __this->get_legendOptions_7();
		int32_t L_18 = V_0;
		int32_t L_19 = L_18;
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_20 = (L_17)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_19));
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_21;
		L_21 = Color_get_grey_mB2E29B47327F20233856F933DC00ACADEBFDBDFA(/*hidden argument*/NULL);
		VirtActionInvoker1< Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_20, L_21);
	}

IL_0095:
	{
		// for(int i = 0; i < legendOptions.Length; i++)
		int32_t L_22 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_22, (int32_t)1));
	}

IL_0099:
	{
		// for(int i = 0; i < legendOptions.Length; i++)
		int32_t L_23 = V_0;
		ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224* L_24 = __this->get_legendOptions_7();
		if ((((int32_t)L_23) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_24)->max_length))))))
		{
			goto IL_0007;
		}
	}
	{
		// for(int i = 0; i < mapIcons.Count; i++)
		V_1 = 0;
		goto IL_00e3;
	}

IL_00ab:
	{
		// if (i < activeInteractions.Count)
		int32_t L_25 = V_1;
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_26 = __this->get_activeInteractions_14();
		int32_t L_27;
		L_27 = List_1_get_Count_m6206A8F48C7A98B9CFC23A13548CB215717573BD_inline(L_26, /*hidden argument*/List_1_get_Count_m6206A8F48C7A98B9CFC23A13548CB215717573BD_RuntimeMethod_var);
		if ((((int32_t)L_25) >= ((int32_t)L_27)))
		{
			goto IL_00cd;
		}
	}
	{
		// mapIcons[i].SetActive(true);
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_28 = __this->get_mapIcons_17();
		int32_t L_29 = V_1;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_30;
		L_30 = List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_inline(L_28, L_29, /*hidden argument*/List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_RuntimeMethod_var);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_30, (bool)1, /*hidden argument*/NULL);
		// }
		goto IL_00df;
	}

IL_00cd:
	{
		// mapIcons[i].SetActive(false);
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_31 = __this->get_mapIcons_17();
		int32_t L_32 = V_1;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_33;
		L_33 = List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_inline(L_31, L_32, /*hidden argument*/List_1_get_Item_m1E85E7CDA3459102B9FD665388559843378C2B4C_RuntimeMethod_var);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_33, (bool)0, /*hidden argument*/NULL);
	}

IL_00df:
	{
		// for(int i = 0; i < mapIcons.Count; i++)
		int32_t L_34 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_34, (int32_t)1));
	}

IL_00e3:
	{
		// for(int i = 0; i < mapIcons.Count; i++)
		int32_t L_35 = V_1;
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_36 = __this->get_mapIcons_17();
		int32_t L_37;
		L_37 = List_1_get_Count_m6206A8F48C7A98B9CFC23A13548CB215717573BD_inline(L_36, /*hidden argument*/List_1_get_Count_m6206A8F48C7A98B9CFC23A13548CB215717573BD_RuntimeMethod_var);
		if ((((int32_t)L_35) < ((int32_t)L_37)))
		{
			goto IL_00ab;
		}
	}
	{
		// }
		return;
	}
}
// System.Boolean WorldMap::AddInteraction(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WorldMap_AddInteraction_mF2CC83A625FCFB963745D016D72BC262AB4A94F4 (WorldMap_t01A07DDC9EA386C88D5814CF18E0F37CAF05270C * __this, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___interaction0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_TryGetComponent_TisIInteraction_t1C1C3E6C29AFB203F5021CA7300B6FEA0EFE91FE_m619A9E6EA0CC7690AEDF0BA79C3534C945F5906B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IGate_tC633C584B29457590CF59B3448AEE38AC9FB33C6_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IKey_t524F44506E887E37C88431701D58645C41BD5F3C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IShip_tC6FB125A646115EE3BCEDE1326BF546935215C99_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m3DD76DE838FA83DF972E0486A296345EB3A7DDF3_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_mC197A913A8DF8E125DEFD1C13605BC1DFD23C2F7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * V_1 = NULL;
	{
		// if (interaction.TryGetComponent(out IInteraction i) == true)
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = ___interaction0;
		bool L_1;
		L_1 = GameObject_TryGetComponent_TisIInteraction_t1C1C3E6C29AFB203F5021CA7300B6FEA0EFE91FE_m619A9E6EA0CC7690AEDF0BA79C3534C945F5906B(L_0, (RuntimeObject**)(&V_0), /*hidden argument*/GameObject_TryGetComponent_TisIInteraction_t1C1C3E6C29AFB203F5021CA7300B6FEA0EFE91FE_m619A9E6EA0CC7690AEDF0BA79C3534C945F5906B_RuntimeMethod_var);
		if (!L_1)
		{
			goto IL_00b0;
		}
	}
	{
		// GameObject icon = null;
		V_1 = (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *)NULL;
		// if (i is IKey)  //type check object against key interaction interface
		RuntimeObject* L_2 = V_0;
		if (!((RuntimeObject*)IsInst((RuntimeObject*)L_2, IKey_t524F44506E887E37C88431701D58645C41BD5F3C_il2cpp_TypeInfo_var)))
		{
			goto IL_003d;
		}
	}
	{
		// keyInteractions.Add(interaction);
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_3 = __this->get_keyInteractions_16();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_4 = ___interaction0;
		List_1_Add_m3DD76DE838FA83DF972E0486A296345EB3A7DDF3(L_3, L_4, /*hidden argument*/List_1_Add_m3DD76DE838FA83DF972E0486A296345EB3A7DDF3_RuntimeMethod_var);
		// icon = Instantiate(keyIconPrefab, legend.transform, false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5 = __this->get_keyIconPrefab_8();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_6 = __this->get_legend_5();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_7;
		L_7 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_8;
		L_8 = Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_mC197A913A8DF8E125DEFD1C13605BC1DFD23C2F7(L_5, L_7, (bool)0, /*hidden argument*/Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_mC197A913A8DF8E125DEFD1C13605BC1DFD23C2F7_RuntimeMethod_var);
		V_1 = L_8;
		// }
		goto IL_0092;
	}

IL_003d:
	{
		// else if (i is IGate) //type check object against gate interaction interface
		RuntimeObject* L_9 = V_0;
		if (!((RuntimeObject*)IsInst((RuntimeObject*)L_9, IGate_tC633C584B29457590CF59B3448AEE38AC9FB33C6_il2cpp_TypeInfo_var)))
		{
			goto IL_006b;
		}
	}
	{
		// gateInteractions.Add(interaction);
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_10 = __this->get_gateInteractions_15();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_11 = ___interaction0;
		List_1_Add_m3DD76DE838FA83DF972E0486A296345EB3A7DDF3(L_10, L_11, /*hidden argument*/List_1_Add_m3DD76DE838FA83DF972E0486A296345EB3A7DDF3_RuntimeMethod_var);
		// icon = Instantiate(gateIconPrefab, legend.transform, false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_12 = __this->get_gateIconPrefab_9();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_13 = __this->get_legend_5();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_14;
		L_14 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_15;
		L_15 = Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_mC197A913A8DF8E125DEFD1C13605BC1DFD23C2F7(L_12, L_14, (bool)0, /*hidden argument*/Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_mC197A913A8DF8E125DEFD1C13605BC1DFD23C2F7_RuntimeMethod_var);
		V_1 = L_15;
		// }
		goto IL_0092;
	}

IL_006b:
	{
		// else if (i is IShip)  //type check object against ship interaction interface
		RuntimeObject* L_16 = V_0;
		if (!((RuntimeObject*)IsInst((RuntimeObject*)L_16, IShip_tC6FB125A646115EE3BCEDE1326BF546935215C99_il2cpp_TypeInfo_var)))
		{
			goto IL_0092;
		}
	}
	{
		// ship = interaction;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_17 = ___interaction0;
		__this->set_ship_13(L_17);
		// icon = Instantiate(shipIconPrefab, legend.transform, false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_18 = __this->get_shipIconPrefab_10();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_19 = __this->get_legend_5();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_20;
		L_20 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_19, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_21;
		L_21 = Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_mC197A913A8DF8E125DEFD1C13605BC1DFD23C2F7(L_18, L_20, (bool)0, /*hidden argument*/Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_mC197A913A8DF8E125DEFD1C13605BC1DFD23C2F7_RuntimeMethod_var);
		V_1 = L_21;
	}

IL_0092:
	{
		// mapIcons.Add(icon);
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_22 = __this->get_mapIcons_17();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_23 = V_1;
		List_1_Add_m3DD76DE838FA83DF972E0486A296345EB3A7DDF3(L_22, L_23, /*hidden argument*/List_1_Add_m3DD76DE838FA83DF972E0486A296345EB3A7DDF3_RuntimeMethod_var);
		// icon.transform.position = Vector3.zero;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_24 = V_1;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_25;
		L_25 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_24, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_26;
		L_26 = Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6(/*hidden argument*/NULL);
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_25, L_26, /*hidden argument*/NULL);
		// return true;
		return (bool)1;
	}

IL_00b0:
	{
		// return false;
		return (bool)0;
	}
}
// System.Void WorldMap::Activate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WorldMap_Activate_m270FF00B4D2A645DB02BFEE99F12E78D75054808 (WorldMap_t01A07DDC9EA386C88D5814CF18E0F37CAF05270C * __this, const RuntimeMethod* method)
{
	{
		// legendIndex = 0;
		__this->set_legendIndex_12(0);
		// legend.SetActive(true);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = __this->get_legend_5();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_0, (bool)1, /*hidden argument*/NULL);
		// mapCam.transform.position = new Vector3(PlayerController.Instance.transform.position.x, mapCam.transform.position.y, PlayerController.Instance.transform.position.z);
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_1 = __this->get_mapCam_6();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_2;
		L_2 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_1, /*hidden argument*/NULL);
		PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9 * L_3;
		L_3 = PlayerController_get_Instance_m8C373614456176CE37B49E24BA9B7E019C5A0440_inline(/*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_4;
		L_4 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_3, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5;
		L_5 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_4, /*hidden argument*/NULL);
		float L_6 = L_5.get_x_2();
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_7 = __this->get_mapCam_6();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_8;
		L_8 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_7, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9;
		L_9 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_8, /*hidden argument*/NULL);
		float L_10 = L_9.get_y_3();
		PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9 * L_11;
		L_11 = PlayerController_get_Instance_m8C373614456176CE37B49E24BA9B7E019C5A0440_inline(/*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_12;
		L_12 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_11, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13;
		L_13 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_12, /*hidden argument*/NULL);
		float L_14 = L_13.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_15;
		memset((&L_15), 0, sizeof(L_15));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_15), L_6, L_10, L_14, /*hidden argument*/NULL);
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_2, L_15, /*hidden argument*/NULL);
		// mapCam.gameObject.SetActive(true);
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_16 = __this->get_mapCam_6();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_17;
		L_17 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_16, /*hidden argument*/NULL);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_17, (bool)1, /*hidden argument*/NULL);
		// UpdateLegendSelection();
		WorldMap_UpdateLegendSelection_m96C6070EF674FED8B832E12C9ED1403EFAD2B33A(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void WorldMap::Deactivate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WorldMap_Deactivate_m45F3C427C11E7AA6FC6C11B1346F94BA221F8641 (WorldMap_t01A07DDC9EA386C88D5814CF18E0F37CAF05270C * __this, const RuntimeMethod* method)
{
	{
		// legend.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = __this->get_legend_5();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_0, (bool)0, /*hidden argument*/NULL);
		// mapCam.gameObject.SetActive(false);
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_1 = __this->get_mapCam_6();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2;
		L_2 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_1, /*hidden argument*/NULL);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_2, (bool)0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void WorldMap::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WorldMap__ctor_mA2D5FF343986CEAC6DC4300F4120F84801BC47E5 (WorldMap_t01A07DDC9EA386C88D5814CF18E0F37CAF05270C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m859B0EE8491FDDEB1A3F7115D334B863E025BBC8_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private float timer = -1;
		__this->set_timer_11((-1.0f));
		// private List<GameObject> activeInteractions = new List<GameObject>();
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_0 = (List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 *)il2cpp_codegen_object_new(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5_il2cpp_TypeInfo_var);
		List_1__ctor_m859B0EE8491FDDEB1A3F7115D334B863E025BBC8(L_0, /*hidden argument*/List_1__ctor_m859B0EE8491FDDEB1A3F7115D334B863E025BBC8_RuntimeMethod_var);
		__this->set_activeInteractions_14(L_0);
		// private List<GameObject> gateInteractions = new List<GameObject>();
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_1 = (List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 *)il2cpp_codegen_object_new(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5_il2cpp_TypeInfo_var);
		List_1__ctor_m859B0EE8491FDDEB1A3F7115D334B863E025BBC8(L_1, /*hidden argument*/List_1__ctor_m859B0EE8491FDDEB1A3F7115D334B863E025BBC8_RuntimeMethod_var);
		__this->set_gateInteractions_15(L_1);
		// private List<GameObject> keyInteractions = new List<GameObject>();
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_2 = (List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 *)il2cpp_codegen_object_new(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5_il2cpp_TypeInfo_var);
		List_1__ctor_m859B0EE8491FDDEB1A3F7115D334B863E025BBC8(L_2, /*hidden argument*/List_1__ctor_m859B0EE8491FDDEB1A3F7115D334B863E025BBC8_RuntimeMethod_var);
		__this->set_keyInteractions_16(L_2);
		// private List<GameObject> mapIcons = new List<GameObject>();
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_3 = (List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 *)il2cpp_codegen_object_new(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5_il2cpp_TypeInfo_var);
		List_1__ctor_m859B0EE8491FDDEB1A3F7115D334B863E025BBC8(L_3, /*hidden argument*/List_1__ctor_m859B0EE8491FDDEB1A3F7115D334B863E025BBC8_RuntimeMethod_var);
		__this->set_mapIcons_17(L_3);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void GameManager/<PlayCutScene>d__46::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CPlayCutSceneU3Ed__46__ctor_m4E4D899A2AEC184002EB6F6615F66D0C16DB3B84 (U3CPlayCutSceneU3Ed__46_tC2D2594FAEFE8718E713CAC6338A1701F09CADAA * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void GameManager/<PlayCutScene>d__46::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CPlayCutSceneU3Ed__46_System_IDisposable_Dispose_mB18D4E5DB6D1BD3E1C843F02FE1E24B1BDE7D74E (U3CPlayCutSceneU3Ed__46_tC2D2594FAEFE8718E713CAC6338A1701F09CADAA * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean GameManager/<PlayCutScene>d__46::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CPlayCutSceneU3Ed__46_MoveNext_mB515DE3B1BDBD5CA0DE9351C37D45A10916D405D (U3CPlayCutSceneU3Ed__46_tC2D2594FAEFE8718E713CAC6338A1701F09CADAA * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * V_1 = NULL;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_1 = __this->get_U3CU3E4__this_2();
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0068;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		__this->set_U3CU3E1__state_0((-1));
		// TogglePlayer(false);  //disables player controlls during the cutscene
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_4 = V_1;
		GameManager_TogglePlayer_m6D6635AAAE3C34217AAF3AE377EC5B97AE4CAA47(L_4, (bool)0, /*hidden argument*/NULL);
		// cutSceneCamera.gameObject.SetActive(true);
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_5 = V_1;
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_6 = L_5->get_cutSceneCamera_9();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_7;
		L_7 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_6, /*hidden argument*/NULL);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_7, (bool)1, /*hidden argument*/NULL);
		// cutsceneDirector.playableAsset = cutScene;
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_8 = V_1;
		PlayableDirector_t63912C98A4EC9D4701E88B633E27E0D97209BA38 * L_9 = L_8->get_cutsceneDirector_11();
		PlayableAsset_t5AD1606B76C9753A7F4C6B1061193F581023F137 * L_10 = __this->get_cutScene_3();
		PlayableDirector_set_playableAsset_m6A7954E07B87A8764468294D1264572301656CE3(L_9, L_10, /*hidden argument*/NULL);
		// cutsceneDirector.Play();  //plays cutscene
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_11 = V_1;
		PlayableDirector_t63912C98A4EC9D4701E88B633E27E0D97209BA38 * L_12 = L_11->get_cutsceneDirector_11();
		PlayableDirector_Play_m13D31AD720EEE25E93A21394B225EA10300C47C4(L_12, /*hidden argument*/NULL);
		goto IL_006f;
	}

IL_0054:
	{
		// yield return new WaitForEndOfFrame();
		WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4 * L_13 = (WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4 *)il2cpp_codegen_object_new(WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_mEA41FB4A9236A64D566330BBE25F9902DEBB2EEA(L_13, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_13);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_0068:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_006f:
	{
		// while(cutsceneDirector.time != cutsceneDirector.playableAsset.duration)
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_14 = V_1;
		PlayableDirector_t63912C98A4EC9D4701E88B633E27E0D97209BA38 * L_15 = L_14->get_cutsceneDirector_11();
		double L_16;
		L_16 = PlayableDirector_get_time_m6E6BEDB6E9FF4A8CD48F73FB64F353B5787E735F(L_15, /*hidden argument*/NULL);
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_17 = V_1;
		PlayableDirector_t63912C98A4EC9D4701E88B633E27E0D97209BA38 * L_18 = L_17->get_cutsceneDirector_11();
		PlayableAsset_t5AD1606B76C9753A7F4C6B1061193F581023F137 * L_19;
		L_19 = PlayableDirector_get_playableAsset_mAD7C43BF96CA5F6553D52DA32C2D14C8501CE45A(L_18, /*hidden argument*/NULL);
		double L_20;
		L_20 = VirtFuncInvoker0< double >::Invoke(7 /* System.Double UnityEngine.Playables.PlayableAsset::get_duration() */, L_19);
		if ((!(((double)L_16) == ((double)L_20))))
		{
			goto IL_0054;
		}
	}
	{
		// cutsceneDirector.Stop();
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_21 = V_1;
		PlayableDirector_t63912C98A4EC9D4701E88B633E27E0D97209BA38 * L_22 = L_21->get_cutsceneDirector_11();
		PlayableDirector_Stop_m4C9FCBB20CDD4BF75DF910F3BE713251D9548C96(L_22, /*hidden argument*/NULL);
		// cutSceneCamera.gameObject.SetActive(false);
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_23 = V_1;
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_24 = L_23->get_cutSceneCamera_9();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_25;
		L_25 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_24, /*hidden argument*/NULL);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_25, (bool)0, /*hidden argument*/NULL);
		// TogglePlayer(true); // re enables player controls
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_26 = V_1;
		GameManager_TogglePlayer_m6D6635AAAE3C34217AAF3AE377EC5B97AE4CAA47(L_26, (bool)1, /*hidden argument*/NULL);
		// }
		return (bool)0;
	}
}
// System.Object GameManager/<PlayCutScene>d__46::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CPlayCutSceneU3Ed__46_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF10EC54DA7CA333966D062D4013D540731306F66 (U3CPlayCutSceneU3Ed__46_tC2D2594FAEFE8718E713CAC6338A1701F09CADAA * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void GameManager/<PlayCutScene>d__46::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CPlayCutSceneU3Ed__46_System_Collections_IEnumerator_Reset_m328A12CA0CF591B501C192E4A0942571129FBCC6 (U3CPlayCutSceneU3Ed__46_tC2D2594FAEFE8718E713CAC6338A1701F09CADAA * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CPlayCutSceneU3Ed__46_System_Collections_IEnumerator_Reset_m328A12CA0CF591B501C192E4A0942571129FBCC6_RuntimeMethod_var)));
	}
}
// System.Object GameManager/<PlayCutScene>d__46::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CPlayCutSceneU3Ed__46_System_Collections_IEnumerator_get_Current_mF952E38AE8940B317C0261CB53D49F19C7DC24EA (U3CPlayCutSceneU3Ed__46_tC2D2594FAEFE8718E713CAC6338A1701F09CADAA * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MainMenu/<LoadGame>d__7::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CLoadGameU3Ed__7__ctor_m57695A7D16F59DB4E5587AE9E06B571D1940260F (U3CLoadGameU3Ed__7_tC6D4D2F6FFFF4C3DA7A96BE826554FAE56FB9D74 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void MainMenu/<LoadGame>d__7::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CLoadGameU3Ed__7_System_IDisposable_Dispose_mCA4CA8352FE4514D8F778377A7D22B29D22AE734 (U3CLoadGameU3Ed__7_tC6D4D2F6FFFF4C3DA7A96BE826554FAE56FB9D74 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean MainMenu/<LoadGame>d__7::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CLoadGameU3Ed__7_MoveNext_mE59F0C0AE491D8965AE5B3F02E8C32070F32015D (U3CLoadGameU3Ed__7_tC6D4D2F6FFFF4C3DA7A96BE826554FAE56FB9D74 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0030;
		}
	}
	{
		return (bool)0;
	}

IL_0010:
	{
		__this->set_U3CU3E1__state_0((-1));
		// yield return new WaitForSeconds(2);
		WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 * L_3 = (WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 *)il2cpp_codegen_object_new(WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_mD298C4CB9532BBBDE172FC40F3397E30504038D4(L_3, (2.0f), /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_3);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_0030:
	{
		__this->set_U3CU3E1__state_0((-1));
		// SceneManager.LoadScene(1);
		IL2CPP_RUNTIME_CLASS_INIT(SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_m5550E6368A6D0E37DACEDA3C5E4BA331836BC3C5(1, /*hidden argument*/NULL);
		// }
		return (bool)0;
	}
}
// System.Object MainMenu/<LoadGame>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CLoadGameU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3186843C05568405BEBF7CDB5C0E01AFBB1ACF99 (U3CLoadGameU3Ed__7_tC6D4D2F6FFFF4C3DA7A96BE826554FAE56FB9D74 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void MainMenu/<LoadGame>d__7::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CLoadGameU3Ed__7_System_Collections_IEnumerator_Reset_mEAC358DD13F122444F8E3E2908F91C48BFE5DC9A (U3CLoadGameU3Ed__7_tC6D4D2F6FFFF4C3DA7A96BE826554FAE56FB9D74 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CLoadGameU3Ed__7_System_Collections_IEnumerator_Reset_mEAC358DD13F122444F8E3E2908F91C48BFE5DC9A_RuntimeMethod_var)));
	}
}
// System.Object MainMenu/<LoadGame>d__7::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CLoadGameU3Ed__7_System_Collections_IEnumerator_get_Current_m23B975A7A51A041008F94718C1C759C1670C098A (U3CLoadGameU3Ed__7_tC6D4D2F6FFFF4C3DA7A96BE826554FAE56FB9D74 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ActionMenu_tE3C589CB5B0B532FF5504CF0D558D489281BED01 * ActionMenu_get_Instance_m0A358E0ED4326BDF16E867E22F1E5BF5B9AD2DC0_inline (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ActionMenu_tE3C589CB5B0B532FF5504CF0D558D489281BED01_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static ActionMenu Instance { get; private set; }
		ActionMenu_tE3C589CB5B0B532FF5504CF0D558D489281BED01 * L_0 = ((ActionMenu_tE3C589CB5B0B532FF5504CF0D558D489281BED01_StaticFields*)il2cpp_codegen_static_fields_for(ActionMenu_tE3C589CB5B0B532FF5504CF0D558D489281BED01_il2cpp_TypeInfo_var))->get_U3CInstanceU3Ek__BackingField_5();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void ActionMenu_set_Instance_m4D0D66C264341B224B923BEA333A7257A7B8E82F_inline (ActionMenu_tE3C589CB5B0B532FF5504CF0D558D489281BED01 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ActionMenu_tE3C589CB5B0B532FF5504CF0D558D489281BED01_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static ActionMenu Instance { get; private set; }
		ActionMenu_tE3C589CB5B0B532FF5504CF0D558D489281BED01 * L_0 = ___value0;
		((ActionMenu_tE3C589CB5B0B532FF5504CF0D558D489281BED01_StaticFields*)il2cpp_codegen_static_fields_for(ActionMenu_tE3C589CB5B0B532FF5504CF0D558D489281BED01_il2cpp_TypeInfo_var))->set_U3CInstanceU3Ek__BackingField_5(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Actions_tF16D3C582B8D8DF5DD96B758A4D691F304BE44BB * Actions_get_Instance_mFCF21C897955B911E7D3B36BE438793642556B1C_inline (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Actions_tF16D3C582B8D8DF5DD96B758A4D691F304BE44BB_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static Actions Instance { get; private set; }
		Actions_tF16D3C582B8D8DF5DD96B758A4D691F304BE44BB * L_0 = ((Actions_tF16D3C582B8D8DF5DD96B758A4D691F304BE44BB_StaticFields*)il2cpp_codegen_static_fields_for(Actions_tF16D3C582B8D8DF5DD96B758A4D691F304BE44BB_il2cpp_TypeInfo_var))->get_U3CInstanceU3Ek__BackingField_11();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Actions_set_Instance_m1F264080CD39D9EF3210325A20AEC5721E281845_inline (Actions_tF16D3C582B8D8DF5DD96B758A4D691F304BE44BB * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Actions_tF16D3C582B8D8DF5DD96B758A4D691F304BE44BB_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static Actions Instance { get; private set; }
		Actions_tF16D3C582B8D8DF5DD96B758A4D691F304BE44BB * L_0 = ___value0;
		((Actions_tF16D3C582B8D8DF5DD96B758A4D691F304BE44BB_StaticFields*)il2cpp_codegen_static_fields_for(Actions_tF16D3C582B8D8DF5DD96B758A4D691F304BE44BB_il2cpp_TypeInfo_var))->set_U3CInstanceU3Ek__BackingField_11(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR WorldMap_t01A07DDC9EA386C88D5814CF18E0F37CAF05270C * WorldMap_get_Instance_m119402AD39383BC71435F9F36C3219A2281289CA_inline (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WorldMap_t01A07DDC9EA386C88D5814CF18E0F37CAF05270C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static WorldMap Instance { get; private set; }
		WorldMap_t01A07DDC9EA386C88D5814CF18E0F37CAF05270C * L_0 = ((WorldMap_t01A07DDC9EA386C88D5814CF18E0F37CAF05270C_StaticFields*)il2cpp_codegen_static_fields_for(WorldMap_t01A07DDC9EA386C88D5814CF18E0F37CAF05270C_il2cpp_TypeInfo_var))->get_U3CInstanceU3Ek__BackingField_4();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR InteractionMenu_t7C2715221BAC6E0BB83B07EF82F68C6AA34129CB * InteractionMenu_get_Instance_m60C03978FEDDC7D708EB1C5C30EBD081344385FF_inline (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InteractionMenu_t7C2715221BAC6E0BB83B07EF82F68C6AA34129CB_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static InteractionMenu Instance { get; private set; }
		InteractionMenu_t7C2715221BAC6E0BB83B07EF82F68C6AA34129CB * L_0 = ((InteractionMenu_t7C2715221BAC6E0BB83B07EF82F68C6AA34129CB_StaticFields*)il2cpp_codegen_static_fields_for(InteractionMenu_t7C2715221BAC6E0BB83B07EF82F68C6AA34129CB_il2cpp_TypeInfo_var))->get_U3CInstanceU3Ek__BackingField_10();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9 * PlayerController_get_Instance_m8C373614456176CE37B49E24BA9B7E019C5A0440_inline (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static PlayerController Instance { get; private set; }
		PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9 * L_0 = ((PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_StaticFields*)il2cpp_codegen_static_fields_for(PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_il2cpp_TypeInfo_var))->get_U3CInstanceU3Ek__BackingField_8();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR MouseLook_t1E1EE79B13022184F3EF9AC8FE8C6771053890DA * MouseLook_get_Instance_m4F889A9C207EAE72011AF1BA9DA70046C5404386_inline (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MouseLook_t1E1EE79B13022184F3EF9AC8FE8C6771053890DA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static MouseLook Instance { get; private set; }
		MouseLook_t1E1EE79B13022184F3EF9AC8FE8C6771053890DA * L_0 = ((MouseLook_t1E1EE79B13022184F3EF9AC8FE8C6771053890DA_StaticFields*)il2cpp_codegen_static_fields_for(MouseLook_t1E1EE79B13022184F3EF9AC8FE8C6771053890DA_il2cpp_TypeInfo_var))->get_U3CInstanceU3Ek__BackingField_10();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Interaction_t41E997DF6BD70C3EAB8669FCC65D603292EF42A1 * Interaction_get_Instance_m49A31B95D41F5321AA7036861F8330D6BB09A940_inline (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Interaction_t41E997DF6BD70C3EAB8669FCC65D603292EF42A1_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static Interaction Instance { get; private set; }
		Interaction_t41E997DF6BD70C3EAB8669FCC65D603292EF42A1 * L_0 = ((Interaction_t41E997DF6BD70C3EAB8669FCC65D603292EF42A1_StaticFields*)il2cpp_codegen_static_fields_for(Interaction_t41E997DF6BD70C3EAB8669FCC65D603292EF42A1_il2cpp_TypeInfo_var))->get_U3CInstanceU3Ek__BackingField_5();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * GameManager_get_Instance_m758C0D98BBDEF0C2DCA163A5D60B22DBB5B07232_inline (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static GameManager Instance { get; private set; }
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_0 = ((GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_StaticFields*)il2cpp_codegen_static_fields_for(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_il2cpp_TypeInfo_var))->get_U3CInstanceU3Ek__BackingField_16();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool GameManager_get_TargetFound_mD646125D22C8183B642D2ECD48C64FE59D1D12AE_inline (GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * __this, const RuntimeMethod* method)
{
	{
		// public bool TargetFound { get; set; } = false;
		bool L_0 = __this->get_U3CTargetFoundU3Ek__BackingField_18();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject* GameManager_get_Target_m3ACB72AC913A603DF141C5D74A3F5C3B42D25B8D_inline (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static IFauna Target { get; private set; }
		RuntimeObject* L_0 = ((GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_StaticFields*)il2cpp_codegen_static_fields_for(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_il2cpp_TypeInfo_var))->get_U3CTargetU3Ek__BackingField_17();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void GameManager_set_TargetFound_mA5DAB51223A80F7D092FF2967C48AC6EB5464CB2_inline (GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// public bool TargetFound { get; set; } = false;
		bool L_0 = ___value0;
		__this->set_U3CTargetFoundU3Ek__BackingField_18(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, float ___d1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		float L_2 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3 = ___a0;
		float L_4 = L_3.get_y_3();
		float L_5 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___a0;
		float L_7 = L_6.get_z_4();
		float L_8 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9;
		memset((&L_9), 0, sizeof(L_9));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_9), ((float)il2cpp_codegen_multiply((float)L_1, (float)L_2)), ((float)il2cpp_codegen_multiply((float)L_4, (float)L_5)), ((float)il2cpp_codegen_multiply((float)L_7, (float)L_8)), /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_0021;
	}

IL_0021:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = V_0;
		return L_10;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___b1;
		float L_3 = L_2.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = ___a0;
		float L_5 = L_4.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___b1;
		float L_7 = L_6.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8 = ___a0;
		float L_9 = L_8.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = ___b1;
		float L_11 = L_10.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		memset((&L_12), 0, sizeof(L_12));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_12), ((float)il2cpp_codegen_add((float)L_1, (float)L_3)), ((float)il2cpp_codegen_add((float)L_5, (float)L_7)), ((float)il2cpp_codegen_add((float)L_9, (float)L_11)), /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0030;
	}

IL_0030:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = V_0;
		return L_13;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR FlockAgentU5BU5D_t012E3C58876A70268E6BFEDBF651E5B8D23AAF84* FlockManager_get_SpawnedAgents_m050E5346A4490CAAD7EB7B9F8A7E6399405B4C7A_inline (FlockManager_t241CFB1B2E6593247EFA52A9E2CB2A111400F1F8 * __this, const RuntimeMethod* method)
{
	{
		// public FlockAgent[] SpawnedAgents { get; private set; }
		FlockAgentU5BU5D_t012E3C58876A70268E6BFEDBF651E5B8D23AAF84* L_0 = __this->get_U3CSpawnedAgentsU3Ek__BackingField_18();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___b1;
		float L_3 = L_2.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = ___a0;
		float L_5 = L_4.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___b1;
		float L_7 = L_6.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8 = ___a0;
		float L_9 = L_8.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = ___b1;
		float L_11 = L_10.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		memset((&L_12), 0, sizeof(L_12));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_12), ((float)il2cpp_codegen_subtract((float)L_1, (float)L_3)), ((float)il2cpp_codegen_subtract((float)L_5, (float)L_7)), ((float)il2cpp_codegen_subtract((float)L_9, (float)L_11)), /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0030;
	}

IL_0030:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = V_0;
		return L_13;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Vector3_SqrMagnitude_m286141DAE9BF2AD40B0FA58C1D97B5773327A5B9_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___vector0, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___vector0;
		float L_1 = L_0.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___vector0;
		float L_3 = L_2.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = ___vector0;
		float L_5 = L_4.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___vector0;
		float L_7 = L_6.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8 = ___vector0;
		float L_9 = L_8.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = ___vector0;
		float L_11 = L_10.get_z_4();
		V_0 = ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)L_1, (float)L_3)), (float)((float)il2cpp_codegen_multiply((float)L_5, (float)L_7)))), (float)((float)il2cpp_codegen_multiply((float)L_9, (float)L_11))));
		goto IL_002d;
	}

IL_002d:
	{
		float L_12 = V_0;
		return L_12;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Division_mE5ACBFB168FED529587457A83BA98B7DB32E2A05_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, float ___d1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		float L_2 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3 = ___a0;
		float L_4 = L_3.get_y_3();
		float L_5 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___a0;
		float L_7 = L_6.get_z_4();
		float L_8 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9;
		memset((&L_9), 0, sizeof(L_9));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_9), ((float)((float)L_1/(float)L_2)), ((float)((float)L_4/(float)L_5)), ((float)((float)L_7/(float)L_8)), /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_0021;
	}

IL_0021:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = V_0;
		return L_10;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void FlockManager_set_SpawnedAgents_m86395D548FB2FC61178CBDF82A5C5629A8716BB6_inline (FlockManager_t241CFB1B2E6593247EFA52A9E2CB2A111400F1F8 * __this, FlockAgentU5BU5D_t012E3C58876A70268E6BFEDBF651E5B8D23AAF84* ___value0, const RuntimeMethod* method)
{
	{
		// public FlockAgent[] SpawnedAgents { get; private set; }
		FlockAgentU5BU5D_t012E3C58876A70268E6BFEDBF651E5B8D23AAF84* L_0 = ___value0;
		__this->set_U3CSpawnedAgentsU3Ek__BackingField_18(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_2(L_0);
		float L_1 = ___y1;
		__this->set_y_3(L_1);
		float L_2 = ___z2;
		__this->set_z_4(L_2);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void GameManager_set_Instance_mAEF75ABE6135BDD61B9A3AED1606C0449787C5C7_inline (GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static GameManager Instance { get; private set; }
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_0 = ___value0;
		((GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_StaticFields*)il2cpp_codegen_static_fields_for(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_il2cpp_TypeInfo_var))->set_U3CInstanceU3Ek__BackingField_16(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void GameManager_set_Target_mB22AB1128EBC08C3818777166428FF6DEEE341E6_inline (RuntimeObject* ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static IFauna Target { get; private set; }
		RuntimeObject* L_0 = ___value0;
		((GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_StaticFields*)il2cpp_codegen_static_fields_for(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_il2cpp_TypeInfo_var))->set_U3CTargetU3Ek__BackingField_17(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * GameManager_get_CameraTarget_mBBC82927B5A0D320D5B38CA1B94C04FDB1EBC4F8_inline (GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * __this, const RuntimeMethod* method)
{
	{
		// public Transform CameraTarget { get; private set; }
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0 = __this->get_U3CCameraTargetU3Ek__BackingField_22();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool GameManager_get_OrbitCameraTarget_m9B1FB59BEA2FEF5094B7EB93548C815087CA0E11_inline (GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * __this, const RuntimeMethod* method)
{
	{
		// public bool OrbitCameraTarget { get; set; } = false;
		bool L_0 = __this->get_U3COrbitCameraTargetU3Ek__BackingField_19();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool GameManager_get_OrbitCameraTarget2_m8967752F71CD50D7C56AB9F21BC4D5576A1DF23B_inline (GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * __this, const RuntimeMethod* method)
{
	{
		// public bool OrbitCameraTarget2 { get; set; } = false;
		bool L_0 = __this->get_U3COrbitCameraTarget2U3Ek__BackingField_20();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void GameManager_set_CameraTarget_m40A31328DAA4F5876B52EDE84970FE6B7E686919_inline (GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * __this, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___value0, const RuntimeMethod* method)
{
	{
		// public Transform CameraTarget { get; private set; }
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0 = ___value0;
		__this->set_U3CCameraTargetU3Ek__BackingField_22(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Gate_get_Locked_mD0F7FDCDBBF9A7B92DB3A64C563342F48D302F4B_inline (Gate_tAA47F9BD94C2B1C12EADC5ECCAF1CAD07A035F45 * __this, const RuntimeMethod* method)
{
	{
		// public bool Locked { get; private set; } = true;
		bool L_0 = __this->get_U3CLockedU3Ek__BackingField_6();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_Lerp_m8E095584FFA10CF1D3EABCD04F4C83FB82EC5524_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, float ___t2, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		float L_0 = ___t2;
		float L_1;
		L_1 = Mathf_Clamp01_m2296D75F0F1292D5C8181C57007A1CA45F440C4C(L_0, /*hidden argument*/NULL);
		___t2 = L_1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___a0;
		float L_3 = L_2.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = ___b1;
		float L_5 = L_4.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___a0;
		float L_7 = L_6.get_x_2();
		float L_8 = ___t2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9 = ___a0;
		float L_10 = L_9.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_11 = ___b1;
		float L_12 = L_11.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = ___a0;
		float L_14 = L_13.get_y_3();
		float L_15 = ___t2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_16 = ___a0;
		float L_17 = L_16.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_18 = ___b1;
		float L_19 = L_18.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_20 = ___a0;
		float L_21 = L_20.get_z_4();
		float L_22 = ___t2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_23;
		memset((&L_23), 0, sizeof(L_23));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_23), ((float)il2cpp_codegen_add((float)L_3, (float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_subtract((float)L_5, (float)L_7)), (float)L_8)))), ((float)il2cpp_codegen_add((float)L_10, (float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_subtract((float)L_12, (float)L_14)), (float)L_15)))), ((float)il2cpp_codegen_add((float)L_17, (float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_subtract((float)L_19, (float)L_21)), (float)L_22)))), /*hidden argument*/NULL);
		V_0 = L_23;
		goto IL_0053;
	}

IL_0053:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_24 = V_0;
		return L_24;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Gate_set_Locked_mC4655A379C367E8C658ED505B433698BC32E7765_inline (Gate_tAA47F9BD94C2B1C12EADC5ECCAF1CAD07A035F45 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// public bool Locked { get; private set; } = true;
		bool L_0 = ___value0;
		__this->set_U3CLockedU3Ek__BackingField_6(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Interaction_set_Instance_m1C2D19F892D930D83F98A19E2D65EF47701A7C51_inline (Interaction_t41E997DF6BD70C3EAB8669FCC65D603292EF42A1 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Interaction_t41E997DF6BD70C3EAB8669FCC65D603292EF42A1_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static Interaction Instance { get; private set; }
		Interaction_t41E997DF6BD70C3EAB8669FCC65D603292EF42A1 * L_0 = ___value0;
		((Interaction_t41E997DF6BD70C3EAB8669FCC65D603292EF42A1_StaticFields*)il2cpp_codegen_static_fields_for(Interaction_t41E997DF6BD70C3EAB8669FCC65D603292EF42A1_il2cpp_TypeInfo_var))->set_U3CInstanceU3Ek__BackingField_5(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void InteractionMenu_set_Instance_m64EBB469675FBC80A34E3E4457B021DE07FED4EA_inline (InteractionMenu_t7C2715221BAC6E0BB83B07EF82F68C6AA34129CB * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InteractionMenu_t7C2715221BAC6E0BB83B07EF82F68C6AA34129CB_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static InteractionMenu Instance { get; private set; }
		InteractionMenu_t7C2715221BAC6E0BB83B07EF82F68C6AA34129CB * L_0 = ___value0;
		((InteractionMenu_t7C2715221BAC6E0BB83B07EF82F68C6AA34129CB_StaticFields*)il2cpp_codegen_static_fields_for(InteractionMenu_t7C2715221BAC6E0BB83B07EF82F68C6AA34129CB_il2cpp_TypeInfo_var))->set_U3CInstanceU3Ek__BackingField_10(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ToolTip_t044257731DC5931927D3AB0671C17515BD8E251E * ToolTip_get_Instance_m5CB773BB76F41361836DE8051081FD5CDF562969_inline (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ToolTip_t044257731DC5931927D3AB0671C17515BD8E251E_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static ToolTip Instance { get; private set; }
		ToolTip_t044257731DC5931927D3AB0671C17515BD8E251E * L_0 = ((ToolTip_t044257731DC5931927D3AB0671C17515BD8E251E_StaticFields*)il2cpp_codegen_static_fields_for(ToolTip_t044257731DC5931927D3AB0671C17515BD8E251E_il2cpp_TypeInfo_var))->get_U3CInstanceU3Ek__BackingField_7();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Dropdown_get_value_mFBF47E0C72050C5CB96B8B6D33F41BA2D1368F26_inline (Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96 * __this, const RuntimeMethod* method)
{
	{
		// return m_Value;
		int32_t L_0 = __this->get_m_Value_25();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void MouseLook_set_Instance_mA80F2A4EF6E41B80058108D5AFF17C03E4F6251D_inline (MouseLook_t1E1EE79B13022184F3EF9AC8FE8C6771053890DA * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MouseLook_t1E1EE79B13022184F3EF9AC8FE8C6771053890DA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static MouseLook Instance { get; private set; }
		MouseLook_t1E1EE79B13022184F3EF9AC8FE8C6771053890DA * L_0 = ___value0;
		((MouseLook_t1E1EE79B13022184F3EF9AC8FE8C6771053890DA_StaticFields*)il2cpp_codegen_static_fields_for(MouseLook_t1E1EE79B13022184F3EF9AC8FE8C6771053890DA_il2cpp_TypeInfo_var))->set_U3CInstanceU3Ek__BackingField_10(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool MouseLook_get_LookEnabled_m872F548C1ADDCAE36BCFFC9743F4F6E240A703C5_inline (MouseLook_t1E1EE79B13022184F3EF9AC8FE8C6771053890DA * __this, const RuntimeMethod* method)
{
	{
		// public bool LookEnabled { get; set; } = true;
		bool L_0 = __this->get_U3CLookEnabledU3Ek__BackingField_11();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * __this, float ___x0, float ___y1, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_0(L_0);
		float L_1 = ___y1;
		__this->set_y_1(L_1);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Vector2_op_Multiply_mC7A7802352867555020A90205EBABA56EE5E36CB_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___a0, float ___d1, const RuntimeMethod* method)
{
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0 = ___a0;
		float L_1 = L_0.get_x_0();
		float L_2 = ___d1;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_3 = ___a0;
		float L_4 = L_3.get_y_1();
		float L_5 = ___d1;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_6;
		memset((&L_6), 0, sizeof(L_6));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_6), ((float)il2cpp_codegen_multiply((float)L_1, (float)L_2)), ((float)il2cpp_codegen_multiply((float)L_4, (float)L_5)), /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0019;
	}

IL_0019:
	{
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_7 = V_0;
		return L_7;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Vector2_Lerp_mC9A8AB816281F4447B7B62264595C16751ED355B_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___a0, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___b1, float ___t2, const RuntimeMethod* method)
{
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		float L_0 = ___t2;
		float L_1;
		L_1 = Mathf_Clamp01_m2296D75F0F1292D5C8181C57007A1CA45F440C4C(L_0, /*hidden argument*/NULL);
		___t2 = L_1;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_2 = ___a0;
		float L_3 = L_2.get_x_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_4 = ___b1;
		float L_5 = L_4.get_x_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_6 = ___a0;
		float L_7 = L_6.get_x_0();
		float L_8 = ___t2;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_9 = ___a0;
		float L_10 = L_9.get_y_1();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_11 = ___b1;
		float L_12 = L_11.get_y_1();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_13 = ___a0;
		float L_14 = L_13.get_y_1();
		float L_15 = ___t2;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_16;
		memset((&L_16), 0, sizeof(L_16));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_16), ((float)il2cpp_codegen_add((float)L_3, (float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_subtract((float)L_5, (float)L_7)), (float)L_8)))), ((float)il2cpp_codegen_add((float)L_10, (float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_subtract((float)L_12, (float)L_14)), (float)L_15)))), /*hidden argument*/NULL);
		V_0 = L_16;
		goto IL_003d;
	}

IL_003d:
	{
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_17 = V_0;
		return L_17;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Vector2_op_Addition_m5EACC2AEA80FEE29F380397CF1F4B11D04BE71CC_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___a0, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___b1, const RuntimeMethod* method)
{
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0 = ___a0;
		float L_1 = L_0.get_x_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_2 = ___b1;
		float L_3 = L_2.get_x_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_4 = ___a0;
		float L_5 = L_4.get_y_1();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_6 = ___b1;
		float L_7 = L_6.get_y_1();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_8;
		memset((&L_8), 0, sizeof(L_8));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_8), ((float)il2cpp_codegen_add((float)L_1, (float)L_3)), ((float)il2cpp_codegen_add((float)L_5, (float)L_7)), /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0023;
	}

IL_0023:
	{
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_9 = V_0;
		return L_9;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void PlayerController_set_Instance_m1DBDE7741593D098536751F470647F587A7A0D0A_inline (PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static PlayerController Instance { get; private set; }
		PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9 * L_0 = ___value0;
		((PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_StaticFields*)il2cpp_codegen_static_fields_for(PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_il2cpp_TypeInfo_var))->set_U3CInstanceU3Ek__BackingField_8(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Ship_set_Agent_m99D21CDD3A46A0B76541A0FBC9B8DA8BA9858A8D_inline (Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * __this, NavMeshAgent_tB9746B6C38013341DB63973CA7ED657494EFB41B * ___value0, const RuntimeMethod* method)
{
	{
		// public NavMeshAgent Agent { get; private set; }
		NavMeshAgent_tB9746B6C38013341DB63973CA7ED657494EFB41B * L_0 = ___value0;
		__this->set_U3CAgentU3Ek__BackingField_10(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Ship_set_StateMachine_mEB49B2626B6B51769D40F7CC9FC3FA2D59961F13_inline (Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * __this, StateMachine_t8E24C2687A18E15B27A6A8FEED2DFE22D910119F * ___value0, const RuntimeMethod* method)
{
	{
		// public StateMachine StateMachine { get; private set; }
		StateMachine_t8E24C2687A18E15B27A6A8FEED2DFE22D910119F * L_0 = ___value0;
		__this->set_U3CStateMachineU3Ek__BackingField_9(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR StateMachine_t8E24C2687A18E15B27A6A8FEED2DFE22D910119F * Ship_get_StateMachine_m9110695EE50E4C7260E64164574C5394CFF403C1_inline (Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * __this, const RuntimeMethod* method)
{
	{
		// public StateMachine StateMachine { get; private set; }
		StateMachine_t8E24C2687A18E15B27A6A8FEED2DFE22D910119F * L_0 = __this->get_U3CStateMachineU3Ek__BackingField_9();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject* StateMachine_get_CurrentState_mCDB0B1FC6472E599268D5E92F31C0D890613B326_inline (StateMachine_t8E24C2687A18E15B27A6A8FEED2DFE22D910119F * __this, const RuntimeMethod* method)
{
	{
		// public IState CurrentState { get; private set; }
		RuntimeObject* L_0 = __this->get_U3CCurrentStateU3Ek__BackingField_0();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * ShipState_get_Instance_m11696AE1492C8B96116FF2687CE13EA7B45428A0_inline (ShipState_tF56B93F7FFD3E22AB4F4ECDF9A73D50A3FE508A5 * __this, const RuntimeMethod* method)
{
	{
		// public Ship Instance { get; private set; }
		Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * L_0 = __this->get_U3CInstanceU3Ek__BackingField_0();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR NavMeshAgent_tB9746B6C38013341DB63973CA7ED657494EFB41B * Ship_get_Agent_m2733EC0E5F1D092B7FB03B5BA882B9FAD8C4CF96_inline (Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * __this, const RuntimeMethod* method)
{
	{
		// public NavMeshAgent Agent { get; private set; }
		NavMeshAgent_tB9746B6C38013341DB63973CA7ED657494EFB41B * L_0 = __this->get_U3CAgentU3Ek__BackingField_10();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void ShipState_set_Instance_m37F9D75A65E59E0500535234B7F46301E108BFCF_inline (ShipState_tF56B93F7FFD3E22AB4F4ECDF9A73D50A3FE508A5 * __this, Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * ___value0, const RuntimeMethod* method)
{
	{
		// public Ship Instance { get; private set; }
		Ship_t03BCF5F62D0A7F03FEE2AA479AB36F8EF5B5295E * L_0 = ___value0;
		__this->set_U3CInstanceU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_UnaryNegation_m362EA356F4CADEDB39F965A0DBDED6EA890925F7_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___a0;
		float L_3 = L_2.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = ___a0;
		float L_5 = L_4.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6;
		memset((&L_6), 0, sizeof(L_6));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_6), ((-L_1)), ((-L_3)), ((-L_5)), /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_001e;
	}

IL_001e:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7 = V_0;
		return L_7;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void StateMachine_set_CurrentState_m1B411CA93F64340414238167A0DF7B8DBD57B8C8_inline (StateMachine_t8E24C2687A18E15B27A6A8FEED2DFE22D910119F * __this, RuntimeObject* ___value0, const RuntimeMethod* method)
{
	{
		// public IState CurrentState { get; private set; }
		RuntimeObject* L_0 = ___value0;
		__this->set_U3CCurrentStateU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void ToolTip_set_Instance_mD51AE6A1572D2CFFED1C930D088CB363C31B30D2_inline (ToolTip_t044257731DC5931927D3AB0671C17515BD8E251E * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ToolTip_t044257731DC5931927D3AB0671C17515BD8E251E_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static ToolTip Instance { get; private set; }
		ToolTip_t044257731DC5931927D3AB0671C17515BD8E251E * L_0 = ___value0;
		((ToolTip_t044257731DC5931927D3AB0671C17515BD8E251E_StaticFields*)il2cpp_codegen_static_fields_for(ToolTip_t044257731DC5931927D3AB0671C17515BD8E251E_il2cpp_TypeInfo_var))->set_U3CInstanceU3Ek__BackingField_7(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void WorldMap_set_Instance_mDDE558CAD26A7D4680B28F1C7FA3AB7F3A47A7B6_inline (WorldMap_t01A07DDC9EA386C88D5814CF18E0F37CAF05270C * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WorldMap_t01A07DDC9EA386C88D5814CF18E0F37CAF05270C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static WorldMap Instance { get; private set; }
		WorldMap_t01A07DDC9EA386C88D5814CF18E0F37CAF05270C * L_0 = ___value0;
		((WorldMap_t01A07DDC9EA386C88D5814CF18E0F37CAF05270C_StaticFields*)il2cpp_codegen_static_fields_for(WorldMap_t01A07DDC9EA386C88D5814CF18E0F37CAF05270C_il2cpp_TypeInfo_var))->set_U3CInstanceU3Ek__BackingField_4(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929(/*hidden argument*/NULL);
	}

IL_000e:
	{
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_2 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)__this->get__items_1();
		int32_t L_3 = ___index0;
		RuntimeObject * L_4;
		L_4 = IL2CPP_ARRAY_UNSAFE_LOAD((ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)L_2, (int32_t)L_3);
		return (RuntimeObject *)L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return (int32_t)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_gshared_inline (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_current_3();
		return (RuntimeObject *)L_0;
	}
}
