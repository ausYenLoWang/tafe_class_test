﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void NewBehaviourScript::Start()
extern void NewBehaviourScript_Start_m783F84A617DADC4574B0BF1524481E6B96C65661 (void);
// 0x00000002 System.Void NewBehaviourScript::Update()
extern void NewBehaviourScript_Update_m411C4D5C2D993FD70092FDA0FE2AC4786F8AC001 (void);
// 0x00000003 System.Void NewBehaviourScript::.ctor()
extern void NewBehaviourScript__ctor_m437970EA37D66BDF32972F4CC0F65B95E5961FAA (void);
// 0x00000004 ActionMenu ActionMenu::get_Instance()
extern void ActionMenu_get_Instance_m0A358E0ED4326BDF16E867E22F1E5BF5B9AD2DC0 (void);
// 0x00000005 System.Void ActionMenu::set_Instance(ActionMenu)
extern void ActionMenu_set_Instance_m4D0D66C264341B224B923BEA333A7257A7B8E82F (void);
// 0x00000006 System.Void ActionMenu::Awake()
extern void ActionMenu_Awake_mF389F85308C8A469685278C9EB1A0BA881E714A2 (void);
// 0x00000007 System.Void ActionMenu::Start()
extern void ActionMenu_Start_mCC99B103AA695673F44496113AA07AA4F5A79AE8 (void);
// 0x00000008 System.Void ActionMenu::Update()
extern void ActionMenu_Update_mD53D9CBD0C575A7E145C71E92415F1E002222471 (void);
// 0x00000009 System.Void ActionMenu::UpdateUISelection()
extern void ActionMenu_UpdateUISelection_mA81DEC42B1B5D91EF6D7FDC89FC3C5BDBD2FC85F (void);
// 0x0000000A System.Void ActionMenu::Activate()
extern void ActionMenu_Activate_mD3CE0F110A26F537889E78930F7F78B0969FCC4D (void);
// 0x0000000B System.Int32 ActionMenu::Deactivate()
extern void ActionMenu_Deactivate_mFA0E81D3F5C0B0FC0267242D68B3B0BA21F36D85 (void);
// 0x0000000C System.Void ActionMenu::.ctor()
extern void ActionMenu__ctor_m94CC62F0105BC9ED7DE5851FDE69845E7291D8D5 (void);
// 0x0000000D Actions Actions::get_Instance()
extern void Actions_get_Instance_mFCF21C897955B911E7D3B36BE438793642556B1C (void);
// 0x0000000E System.Void Actions::set_Instance(Actions)
extern void Actions_set_Instance_m1F264080CD39D9EF3210325A20AEC5721E281845 (void);
// 0x0000000F System.Void Actions::Awake()
extern void Actions_Awake_m9F5BFB055871E0A1D7F18FDACB86E65C58524BED (void);
// 0x00000010 System.Void Actions::Start()
extern void Actions_Start_m7093D52055EE9FCF9145A5F06DC7D89609D29B0C (void);
// 0x00000011 System.Void Actions::Update()
extern void Actions_Update_mA16AB2E471F8E5369DF9018D706C24F192DD7924 (void);
// 0x00000012 System.Void Actions::Boost(System.Boolean)
extern void Actions_Boost_m5CB5C10BC32C2DE52E94274EAEF5F486716E49E4 (void);
// 0x00000013 System.Void Actions::Sonar(System.Boolean)
extern void Actions_Sonar_m9974B6F2FE0C34C6FADC1792DD56F5013D247130 (void);
// 0x00000014 System.Void Actions::Map(System.Boolean)
extern void Actions_Map_m76DFCD66B1AA4F860F1956778B07ADE092E771D9 (void);
// 0x00000015 System.Void Actions::.ctor()
extern void Actions__ctor_mA309EB283343FDDB8A1FE0D25D6EB563A75F8AFD (void);
// 0x00000016 System.String Fish::Inspect()
extern void Fish_Inspect_mE6F49E168DC57D78B1CA0B6EE9F12AC1CBEC011F (void);
// 0x00000017 System.Void Fish::Photograph(System.String)
extern void Fish_Photograph_mCA93E8601A6B867BF98917CDD6806BF7B4F9B127 (void);
// 0x00000018 System.Void Fish::.ctor()
extern void Fish__ctor_mEDE54A2E8EAB7C21444968366A1E9E12AC8D4A48 (void);
// 0x00000019 System.Void FlockAgent::Initialize(FlockManager,System.Single)
extern void FlockAgent_Initialize_m8D1C04B1942FF5534A44DAC4526F9A8EF16CA1A6 (void);
// 0x0000001A System.Void FlockAgent::Move()
extern void FlockAgent_Move_mE006DB28962C8459F5677FCE8F3B9DA4302F4C99 (void);
// 0x0000001B System.Void FlockAgent::FindNeighbors()
extern void FlockAgent_FindNeighbors_m084145F3ABC4617EA0A00724C318BEBD459B1BBE (void);
// 0x0000001C System.Void FlockAgent::CalculateSpeed()
extern void FlockAgent_CalculateSpeed_m0CA1C3F5ACDD0DA9A4A121596281AA5AEC6646B1 (void);
// 0x0000001D UnityEngine.Vector3 FlockAgent::Cohese()
extern void FlockAgent_Cohese_mCBA014DB1CABC0C892246D2B8C48DC8B8CCC7D48 (void);
// 0x0000001E UnityEngine.Vector3 FlockAgent::Separate()
extern void FlockAgent_Separate_mD2D03FDAC8EB6894C85FED42F19C5BD69AA1DBC2 (void);
// 0x0000001F UnityEngine.Vector3 FlockAgent::Align()
extern void FlockAgent_Align_m7890991F9B43B074A1420EB79A42EC79A7659EBB (void);
// 0x00000020 UnityEngine.Vector3 FlockAgent::Bounds()
extern void FlockAgent_Bounds_m4300D54C6A0AF7BF4FB2B650EB878EA72A996CD1 (void);
// 0x00000021 UnityEngine.Vector3 FlockAgent::ObstacleAvoidance()
extern void FlockAgent_ObstacleAvoidance_m630F88E9A64AE156AA32C14C11538797BCB8BDDD (void);
// 0x00000022 UnityEngine.Vector3 FlockAgent::AvoidObstacle()
extern void FlockAgent_AvoidObstacle_m9274B9DABEC71E0C1CFCABC33E63019C2BE14B6B (void);
// 0x00000023 System.Boolean FlockAgent::VectorInView(UnityEngine.Vector3)
extern void FlockAgent_VectorInView_m40DF4A6C365ADD655A97BB18413186B022867CE5 (void);
// 0x00000024 System.Void FlockAgent::.ctor()
extern void FlockAgent__ctor_m7901AE28864B436FF807D1A533D94F577A6FA977 (void);
// 0x00000025 FlockAgent[] FlockManager::get_SpawnedAgents()
extern void FlockManager_get_SpawnedAgents_m050E5346A4490CAAD7EB7B9F8A7E6399405B4C7A (void);
// 0x00000026 System.Void FlockManager::set_SpawnedAgents(FlockAgent[])
extern void FlockManager_set_SpawnedAgents_m86395D548FB2FC61178CBDF82A5C5629A8716BB6 (void);
// 0x00000027 System.Void FlockManager::Start()
extern void FlockManager_Start_m2CAA7577F414C6299641BA18F29B39E7DCB5EC37 (void);
// 0x00000028 System.Void FlockManager::Update()
extern void FlockManager_Update_m9A5DF45FD8F6018A6BF35881B36217BD505B9717 (void);
// 0x00000029 System.Void FlockManager::SpawnAgents()
extern void FlockManager_SpawnAgents_m075D462688CF50D13A0F934C0989C1CFCE54B3B3 (void);
// 0x0000002A System.Void FlockManager::OnDrawGizmos()
extern void FlockManager_OnDrawGizmos_m33A47EA1DF5486A1A47B8869073F955F986B5529 (void);
// 0x0000002B System.Void FlockManager::.ctor()
extern void FlockManager__ctor_m317ABCE65877D367E812F102E2D7D3A49FA28DFF (void);
// 0x0000002C GameManager GameManager::get_Instance()
extern void GameManager_get_Instance_m758C0D98BBDEF0C2DCA163A5D60B22DBB5B07232 (void);
// 0x0000002D System.Void GameManager::set_Instance(GameManager)
extern void GameManager_set_Instance_mAEF75ABE6135BDD61B9A3AED1606C0449787C5C7 (void);
// 0x0000002E IFauna GameManager::get_Target()
extern void GameManager_get_Target_m3ACB72AC913A603DF141C5D74A3F5C3B42D25B8D (void);
// 0x0000002F System.Void GameManager::set_Target(IFauna)
extern void GameManager_set_Target_mB22AB1128EBC08C3818777166428FF6DEEE341E6 (void);
// 0x00000030 System.Boolean GameManager::get_TargetFound()
extern void GameManager_get_TargetFound_mD646125D22C8183B642D2ECD48C64FE59D1D12AE (void);
// 0x00000031 System.Void GameManager::set_TargetFound(System.Boolean)
extern void GameManager_set_TargetFound_mA5DAB51223A80F7D092FF2967C48AC6EB5464CB2 (void);
// 0x00000032 System.Boolean GameManager::get_OrbitCameraTarget()
extern void GameManager_get_OrbitCameraTarget_m9B1FB59BEA2FEF5094B7EB93548C815087CA0E11 (void);
// 0x00000033 System.Void GameManager::set_OrbitCameraTarget(System.Boolean)
extern void GameManager_set_OrbitCameraTarget_m30E1AB1FE69E612022E9ECBB93579479690F219B (void);
// 0x00000034 System.Boolean GameManager::get_OrbitCameraTarget2()
extern void GameManager_get_OrbitCameraTarget2_m8967752F71CD50D7C56AB9F21BC4D5576A1DF23B (void);
// 0x00000035 System.Void GameManager::set_OrbitCameraTarget2(System.Boolean)
extern void GameManager_set_OrbitCameraTarget2_m82942D4D17D5A3922A44BD6398ED44370EC6F7D4 (void);
// 0x00000036 System.Boolean GameManager::get_PanToCameraTarget()
extern void GameManager_get_PanToCameraTarget_m650916A4853E8626442E0A99A7CFEE2C7C168A8C (void);
// 0x00000037 System.Void GameManager::set_PanToCameraTarget(System.Boolean)
extern void GameManager_set_PanToCameraTarget_mD06BE8F791C2C13FC8FC4DE7AA41E8D6C3FA773F (void);
// 0x00000038 UnityEngine.Transform GameManager::get_CameraTarget()
extern void GameManager_get_CameraTarget_mBBC82927B5A0D320D5B38CA1B94C04FDB1EBC4F8 (void);
// 0x00000039 System.Void GameManager::set_CameraTarget(UnityEngine.Transform)
extern void GameManager_set_CameraTarget_m40A31328DAA4F5876B52EDE84970FE6B7E686919 (void);
// 0x0000003A System.Void GameManager::Awake()
extern void GameManager_Awake_m22F42B2A82708B10F652CAD8F2E0A4767110FF30 (void);
// 0x0000003B System.Void GameManager::Start()
extern void GameManager_Start_m26461AEF27E44DB8FECCBC19D6C9E228B658BF8E (void);
// 0x0000003C System.Void GameManager::Update()
extern void GameManager_Update_mC9303BA7C3117BD861F49F8E36151CC52117E6C1 (void);
// 0x0000003D System.Void GameManager::SpawnZone(System.Collections.Generic.List`1<UnityEngine.Transform>)
extern void GameManager_SpawnZone_m095B9AA853D168B937A20973F44CE3D7D5B3B8CE (void);
// 0x0000003E System.Void GameManager::TogglePlayer(System.Boolean)
extern void GameManager_TogglePlayer_m6D6635AAAE3C34217AAF3AE377EC5B97AE4CAA47 (void);
// 0x0000003F System.Void GameManager::PositionCinematicCamera(UnityEngine.Transform)
extern void GameManager_PositionCinematicCamera_mA50236AF87D5EB57368C70AF4CFA8B1B243C8F16 (void);
// 0x00000040 System.Collections.IEnumerator GameManager::PlayCutScene(UnityEngine.Playables.PlayableAsset)
extern void GameManager_PlayCutScene_mEE157DD535C601E8C248C3286884E2622B9E5851 (void);
// 0x00000041 System.Void GameManager::.ctor()
extern void GameManager__ctor_mE8666F6D0CA9C31E16B719F79780DC4B0245B64D (void);
// 0x00000042 System.Void GameManager/<PlayCutScene>d__46::.ctor(System.Int32)
extern void U3CPlayCutSceneU3Ed__46__ctor_m4E4D899A2AEC184002EB6F6615F66D0C16DB3B84 (void);
// 0x00000043 System.Void GameManager/<PlayCutScene>d__46::System.IDisposable.Dispose()
extern void U3CPlayCutSceneU3Ed__46_System_IDisposable_Dispose_mB18D4E5DB6D1BD3E1C843F02FE1E24B1BDE7D74E (void);
// 0x00000044 System.Boolean GameManager/<PlayCutScene>d__46::MoveNext()
extern void U3CPlayCutSceneU3Ed__46_MoveNext_mB515DE3B1BDBD5CA0DE9351C37D45A10916D405D (void);
// 0x00000045 System.Object GameManager/<PlayCutScene>d__46::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPlayCutSceneU3Ed__46_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF10EC54DA7CA333966D062D4013D540731306F66 (void);
// 0x00000046 System.Void GameManager/<PlayCutScene>d__46::System.Collections.IEnumerator.Reset()
extern void U3CPlayCutSceneU3Ed__46_System_Collections_IEnumerator_Reset_m328A12CA0CF591B501C192E4A0942571129FBCC6 (void);
// 0x00000047 System.Object GameManager/<PlayCutScene>d__46::System.Collections.IEnumerator.get_Current()
extern void U3CPlayCutSceneU3Ed__46_System_Collections_IEnumerator_get_Current_mF952E38AE8940B317C0261CB53D49F19C7DC24EA (void);
// 0x00000048 System.Boolean Gate::get_Locked()
extern void Gate_get_Locked_mD0F7FDCDBBF9A7B92DB3A64C563342F48D302F4B (void);
// 0x00000049 System.Void Gate::set_Locked(System.Boolean)
extern void Gate_set_Locked_mC4655A379C367E8C658ED505B433698BC32E7765 (void);
// 0x0000004A System.Void Gate::Awake()
extern void Gate_Awake_m9E870AA2F62C390A3CA0F5AB8E75B744519FF1FC (void);
// 0x0000004B System.Void Gate::Start()
extern void Gate_Start_m27BE6AC1FF73604405C96D634D19AF13DD9A1234 (void);
// 0x0000004C System.Void Gate::Update()
extern void Gate_Update_mBA10874B7CBA45BC8F8BAFB54DD6DAE131DC3A85 (void);
// 0x0000004D System.String Gate::Inspect()
extern void Gate_Inspect_m60870B3FA73AADCC9875C320E9D12E3A13411E03 (void);
// 0x0000004E System.Boolean Gate::Unlock()
extern void Gate_Unlock_m4D20EBE5FC66340D63BB8D9157A0C09DC6CDC5F4 (void);
// 0x0000004F System.Void Gate::.ctor()
extern void Gate__ctor_mEACCC67CFE6FC196994848EE1207B13283FB518E (void);
// 0x00000050 Interaction Interaction::get_Instance()
extern void Interaction_get_Instance_m49A31B95D41F5321AA7036861F8330D6BB09A940 (void);
// 0x00000051 System.Void Interaction::set_Instance(Interaction)
extern void Interaction_set_Instance_m1C2D19F892D930D83F98A19E2D65EF47701A7C51 (void);
// 0x00000052 System.Void Interaction::Awake()
extern void Interaction_Awake_m07EB608CF485E6B7DE48D22C0FF5DF5558C9BE45 (void);
// 0x00000053 System.Void Interaction::Update()
extern void Interaction_Update_m19CE4CF0CFFA37FF15C650076DB2F1CF3F677899 (void);
// 0x00000054 System.Boolean Interaction::Addkey(Key/KeyType)
extern void Interaction_Addkey_mCC3293ED3A6334A742C2196DF288E220BF1073A5 (void);
// 0x00000055 System.Boolean Interaction::HasKey(Key/KeyType)
extern void Interaction_HasKey_m0E72EF86A1EF62EB1478BD83D9D20B904CBE76A1 (void);
// 0x00000056 System.Void Interaction::.ctor()
extern void Interaction__ctor_m70406E11A94FE15ADD18C51085F78B5463FACEB8 (void);
// 0x00000057 System.String IInteraction::Inspect()
// 0x00000058 System.Void IFauna::Photograph(System.String)
// 0x00000059 System.Void IKey::Pickup()
// 0x0000005A System.Boolean IGate::Unlock()
// 0x0000005B System.Void IShip::Leave()
// 0x0000005C InteractionMenu InteractionMenu::get_Instance()
extern void InteractionMenu_get_Instance_m60C03978FEDDC7D708EB1C5C30EBD081344385FF (void);
// 0x0000005D System.Void InteractionMenu::set_Instance(InteractionMenu)
extern void InteractionMenu_set_Instance_m64EBB469675FBC80A34E3E4457B021DE07FED4EA (void);
// 0x0000005E System.Void InteractionMenu::Awake()
extern void InteractionMenu_Awake_mDD193A70CB13CF5429EFF02481B45135665A2889 (void);
// 0x0000005F System.Void InteractionMenu::UpdateUISelection()
extern void InteractionMenu_UpdateUISelection_mCDE9D19A0556DB8B15CBC355E369A362C18D2A2D (void);
// 0x00000060 System.Void InteractionMenu::Start()
extern void InteractionMenu_Start_mCFC55383AE7C50EEEBA53097C75A503474091A25 (void);
// 0x00000061 System.Void InteractionMenu::Update()
extern void InteractionMenu_Update_m89548D247EF54D378478EBF006756F060DD3F368 (void);
// 0x00000062 System.Void InteractionMenu::Photograph(IFauna)
extern void InteractionMenu_Photograph_mDF8C7A662AA42E89A39027BBAB69C54692736571 (void);
// 0x00000063 System.Boolean InteractionMenu::GetInteractionAsType(InteractionType&)
// 0x00000064 System.Void InteractionMenu::Activate(IInteraction)
extern void InteractionMenu_Activate_m45B84D655B2FCBC3A0429DDEB4F1B8393A397160 (void);
// 0x00000065 System.Void InteractionMenu::Deactivate()
extern void InteractionMenu_Deactivate_m912B95CC8E501BE8859EB5BB477A9D0A5DCFE10A (void);
// 0x00000066 System.Void InteractionMenu::.ctor()
extern void InteractionMenu__ctor_m7D2580EE5A91D03AC0C8191AACF351421AD3C3EC (void);
// 0x00000067 System.Void Key::Start()
extern void Key_Start_m32134F86EACDABA7FFF8FB47E9DA60B2D7F71B4A (void);
// 0x00000068 System.Void Key::Pickup()
extern void Key_Pickup_m2A0EA27BF1A8B8D1EFA82F77FD5829B902E8F026 (void);
// 0x00000069 System.String Key::Inspect()
extern void Key_Inspect_m1443EDF14E5EFD3C262BBFFF0A08D28DF9191365 (void);
// 0x0000006A System.Void Key::.ctor()
extern void Key__ctor_mDED000F0CC8C37C5B2A6916661E52F7735A49725 (void);
// 0x0000006B System.Void MainMenu::Awake()
extern void MainMenu_Awake_mA8CB83E7D49CE72C6D0DE5D7D6313F8305A1A4FE (void);
// 0x0000006C System.Void MainMenu::Start()
extern void MainMenu_Start_m3B552CE289B1D7E5343961C8461C484EA61DB621 (void);
// 0x0000006D System.Collections.IEnumerator MainMenu::LoadGame()
extern void MainMenu_LoadGame_m75C2A4F48DA3B9EB7A2ADF6872C66FC286639600 (void);
// 0x0000006E System.Void MainMenu::StartGame()
extern void MainMenu_StartGame_m8EFE49FE95784A266A683D0CCAAE7C2274981049 (void);
// 0x0000006F System.Void MainMenu::Info()
extern void MainMenu_Info_m9B449123D7739667B9D5569898441C2A89400461 (void);
// 0x00000070 System.Void MainMenu::Gallery()
extern void MainMenu_Gallery_m3FC2EDFC3E9D08BBD78F49732962217470E974B2 (void);
// 0x00000071 System.Void MainMenu::SelectGalleryImage()
extern void MainMenu_SelectGalleryImage_mBE00416F61B0A0B8B5648DE827A479FFE9CC8B69 (void);
// 0x00000072 System.Void MainMenu::QuitToDesktop()
extern void MainMenu_QuitToDesktop_m4BA7361022A9F78B96A3EE6C4B117CECF2D53D31 (void);
// 0x00000073 System.Void MainMenu::.ctor()
extern void MainMenu__ctor_m4D77CEC8F91682A2D9492AE815F89B178BF9717D (void);
// 0x00000074 System.Void MainMenu/<LoadGame>d__7::.ctor(System.Int32)
extern void U3CLoadGameU3Ed__7__ctor_m57695A7D16F59DB4E5587AE9E06B571D1940260F (void);
// 0x00000075 System.Void MainMenu/<LoadGame>d__7::System.IDisposable.Dispose()
extern void U3CLoadGameU3Ed__7_System_IDisposable_Dispose_mCA4CA8352FE4514D8F778377A7D22B29D22AE734 (void);
// 0x00000076 System.Boolean MainMenu/<LoadGame>d__7::MoveNext()
extern void U3CLoadGameU3Ed__7_MoveNext_mE59F0C0AE491D8965AE5B3F02E8C32070F32015D (void);
// 0x00000077 System.Object MainMenu/<LoadGame>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadGameU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3186843C05568405BEBF7CDB5C0E01AFBB1ACF99 (void);
// 0x00000078 System.Void MainMenu/<LoadGame>d__7::System.Collections.IEnumerator.Reset()
extern void U3CLoadGameU3Ed__7_System_Collections_IEnumerator_Reset_mEAC358DD13F122444F8E3E2908F91C48BFE5DC9A (void);
// 0x00000079 System.Object MainMenu/<LoadGame>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CLoadGameU3Ed__7_System_Collections_IEnumerator_get_Current_m23B975A7A51A041008F94718C1C759C1670C098A (void);
// 0x0000007A MouseLook MouseLook::get_Instance()
extern void MouseLook_get_Instance_m4F889A9C207EAE72011AF1BA9DA70046C5404386 (void);
// 0x0000007B System.Void MouseLook::set_Instance(MouseLook)
extern void MouseLook_set_Instance_mA80F2A4EF6E41B80058108D5AFF17C03E4F6251D (void);
// 0x0000007C System.Boolean MouseLook::get_LookEnabled()
extern void MouseLook_get_LookEnabled_m872F548C1ADDCAE36BCFFC9743F4F6E240A703C5 (void);
// 0x0000007D System.Void MouseLook::set_LookEnabled(System.Boolean)
extern void MouseLook_set_LookEnabled_mE39640BB02C6774A01AE2C1E80613702480B2C99 (void);
// 0x0000007E System.Void MouseLook::set_CursorToggle(System.Boolean)
extern void MouseLook_set_CursorToggle_m2DBA5589D35ED1938A52174EC990627601603468 (void);
// 0x0000007F System.Void MouseLook::Awake()
extern void MouseLook_Awake_m10C7098FFAA05035E7FB675ECCC5EF1FD79B8B95 (void);
// 0x00000080 System.Void MouseLook::Update()
extern void MouseLook_Update_m3D49361C94E0433BB35499708EE783B4543D83D5 (void);
// 0x00000081 System.Void MouseLook::.ctor()
extern void MouseLook__ctor_mD12D8075DDEA2085341B59FF8BA9FB353613200B (void);
// 0x00000082 PlayerController PlayerController::get_Instance()
extern void PlayerController_get_Instance_m8C373614456176CE37B49E24BA9B7E019C5A0440 (void);
// 0x00000083 System.Void PlayerController::set_Instance(PlayerController)
extern void PlayerController_set_Instance_m1DBDE7741593D098536751F470647F587A7A0D0A (void);
// 0x00000084 System.Void PlayerController::Awake()
extern void PlayerController_Awake_m23059564DCFDD324EA9DB966473251896647CD23 (void);
// 0x00000085 System.Void PlayerController::Update()
extern void PlayerController_Update_mB31159CAD7DD2329859472554BC9154A83D8E794 (void);
// 0x00000086 System.Void PlayerController::MoveToPosition(UnityEngine.Vector3)
extern void PlayerController_MoveToPosition_m9E1B5DA225A7BE1C6E859C6294A44281E5480876 (void);
// 0x00000087 System.Void PlayerController::.ctor()
extern void PlayerController__ctor_mF30385729DAFDFCB895C4939F6051DCE6C0327FB (void);
// 0x00000088 FiniteStateMachine.StateMachine Ship::get_StateMachine()
extern void Ship_get_StateMachine_m9110695EE50E4C7260E64164574C5394CFF403C1 (void);
// 0x00000089 System.Void Ship::set_StateMachine(FiniteStateMachine.StateMachine)
extern void Ship_set_StateMachine_mEB49B2626B6B51769D40F7CC9FC3FA2D59961F13 (void);
// 0x0000008A UnityEngine.AI.NavMeshAgent Ship::get_Agent()
extern void Ship_get_Agent_m2733EC0E5F1D092B7FB03B5BA882B9FAD8C4CF96 (void);
// 0x0000008B System.Void Ship::set_Agent(UnityEngine.AI.NavMeshAgent)
extern void Ship_set_Agent_m99D21CDD3A46A0B76541A0FBC9B8DA8BA9858A8D (void);
// 0x0000008C System.Void Ship::Awake()
extern void Ship_Awake_m956DC332783F1DAA6F7F13588816260AD621C9A2 (void);
// 0x0000008D System.Void Ship::Start()
extern void Ship_Start_m809FEE5ACEBDF1EC3E7A57D31FF5D6D994E0F4BF (void);
// 0x0000008E System.Void Ship::Update()
extern void Ship_Update_mC04E9DB38367AC83C98CCF1C377D7C442FAE2A7E (void);
// 0x0000008F System.String Ship::Inspect()
extern void Ship_Inspect_mCB84D7CA931A5BFB47F385CFF0C8EFD4284BCFFF (void);
// 0x00000090 System.Void Ship::Leave()
extern void Ship_Leave_m126EADEB1988B84E8ED2F86F7E20A0373C5AF64B (void);
// 0x00000091 System.Void Ship::OnDrawGizmos()
extern void Ship_OnDrawGizmos_m9D86874A855361682803C23AB3ED843A9EAB0D49 (void);
// 0x00000092 System.Void Ship::.ctor()
extern void Ship__ctor_mA4FC970EB2F615E5B00A86D2B89DCBAC88C9CC1E (void);
// 0x00000093 Ship ShipState::get_Instance()
extern void ShipState_get_Instance_m11696AE1492C8B96116FF2687CE13EA7B45428A0 (void);
// 0x00000094 System.Void ShipState::set_Instance(Ship)
extern void ShipState_set_Instance_m37F9D75A65E59E0500535234B7F46301E108BFCF (void);
// 0x00000095 System.Void ShipState::.ctor(Ship)
extern void ShipState__ctor_mEA5C98A1E34AE5F3ADF5D3940A212D61561FBD48 (void);
// 0x00000096 System.Void ShipState::OnEnter()
extern void ShipState_OnEnter_mD09267B90F3E7D321C6F73DDD37D625DB1EC3B44 (void);
// 0x00000097 System.Void ShipState::OnExit()
extern void ShipState_OnExit_mD10B24E4176AF5474BEACBCB48E50368FDE8264D (void);
// 0x00000098 System.Void ShipState::OnUpdate()
extern void ShipState_OnUpdate_mB9D658F5434ABD6F3AA4770908C5F361B3627D03 (void);
// 0x00000099 System.Void ShipState::DrawGizmos()
extern void ShipState_DrawGizmos_m673B6C785E859D1C49481C9183CA27910A3DF0F7 (void);
// 0x0000009A System.Void ShipWait::.ctor(Ship)
extern void ShipWait__ctor_mC7C641CCA9F94A3C7D6BBF10F32B8F2B77496FA5 (void);
// 0x0000009B System.Void ShipWait::OnEnter()
extern void ShipWait_OnEnter_m9E6683753E750BF44EEB10B25083DF5E6D00BDF9 (void);
// 0x0000009C System.Void ShipWait::OnUpdate()
extern void ShipWait_OnUpdate_mC4EC33BF6D9C15D7FD6F5B23D178774D2960C781 (void);
// 0x0000009D System.Void ShipWait::OnExit()
extern void ShipWait_OnExit_m253088D6C12BD0C78612108AC96BFDB0EDD682E9 (void);
// 0x0000009E System.Void ShipMove::.ctor(Ship)
extern void ShipMove__ctor_mB007B621D62FBD63A71C4B9A679A3E2707A8C639 (void);
// 0x0000009F System.Void ShipMove::OnEnter()
extern void ShipMove_OnEnter_mAF5CB80592877AC4484E83F9027248012294BF6D (void);
// 0x000000A0 System.Void ShipMove::OnUpdate()
extern void ShipMove_OnUpdate_mCD2173519CDBE3734589F653C812EE130F678485 (void);
// 0x000000A1 System.Void ShipMove::DrawGizmos()
extern void ShipMove_DrawGizmos_m798CCF44823A7E5864E8948948B2ECBAE0E57807 (void);
// 0x000000A2 UnityEngine.Vector3 ShipMove::GetRandomPosInBounds()
extern void ShipMove_GetRandomPosInBounds_mF73CF65739E672B709F7304EE16A896E94E1F7E4 (void);
// 0x000000A3 System.Void SoloFish::Update()
extern void SoloFish_Update_mF84BAC95F9ECBA23F0C5E7B257508B31D1425450 (void);
// 0x000000A4 System.Void SoloFish::OnDrawGizmos()
extern void SoloFish_OnDrawGizmos_mFE0A5C49D7FA515541574BAD7297DD258BCFFD71 (void);
// 0x000000A5 UnityEngine.Vector3 SoloFish::GetRandomPosInBounds()
extern void SoloFish_GetRandomPosInBounds_m8622D90A0BB64C591B451299165E119524C6B3E1 (void);
// 0x000000A6 System.Void SoloFish::MoveStep()
extern void SoloFish_MoveStep_m75074289C1B548F4A42BFC3A0E377F3B58031E3E (void);
// 0x000000A7 UnityEngine.Vector3 SoloFish::ObstacleAvoidance()
extern void SoloFish_ObstacleAvoidance_m5D27E74C36F7F0C727F4AEA88E249EF022CD779C (void);
// 0x000000A8 UnityEngine.Vector3 SoloFish::Bounds()
extern void SoloFish_Bounds_m456DD94835D1E087A1A45D1258B1496EBB7778A7 (void);
// 0x000000A9 System.Void SoloFish::.ctor()
extern void SoloFish__ctor_m9EABDF66EC0910BB39AF54AB993A344F63775020 (void);
// 0x000000AA System.Void TempInteraction::Start()
extern void TempInteraction_Start_mEF47E0A950D8B4009BA62B8D29562A46B409BD87 (void);
// 0x000000AB System.String TempInteraction::Inspect()
extern void TempInteraction_Inspect_mD82B8EC83A055FA4946EF96D2C191736298520E3 (void);
// 0x000000AC System.Void TempInteraction::Leave()
extern void TempInteraction_Leave_mB34E7DA19EA52B8D6922633E0BA000ABABF47032 (void);
// 0x000000AD System.Void TempInteraction::.ctor()
extern void TempInteraction__ctor_m25A903DAB88BDA6319528D907AD373A55B31284D (void);
// 0x000000AE System.Void TestFIsh::.ctor()
extern void TestFIsh__ctor_m28D4EF296E3A469E92FC6EBCFF88E2399CE288D6 (void);
// 0x000000AF System.Void TestFIsh::Start()
extern void TestFIsh_Start_mB4AF08A9F80D5012BFDAFF6E861AD6439B2F1484 (void);
// 0x000000B0 System.Void TestFIsh::Update()
extern void TestFIsh_Update_mE8C86846C90A65D720C8DFAC85D3594A4A08BE17 (void);
// 0x000000B1 ToolTip ToolTip::get_Instance()
extern void ToolTip_get_Instance_m5CB773BB76F41361836DE8051081FD5CDF562969 (void);
// 0x000000B2 System.Void ToolTip::set_Instance(ToolTip)
extern void ToolTip_set_Instance_mD51AE6A1572D2CFFED1C930D088CB363C31B30D2 (void);
// 0x000000B3 System.Void ToolTip::Awake()
extern void ToolTip_Awake_mDE594ACEB5DD7B0AF6922A947B8112E08CD22AD4 (void);
// 0x000000B4 System.Void ToolTip::Start()
extern void ToolTip_Start_mEA60282A4DDCE048FBF93A97ADBE152A7FC6B172 (void);
// 0x000000B5 System.Void ToolTip::Update()
extern void ToolTip_Update_m17C942983DD1AB8DD7153F31DD8125CBD2CF4C00 (void);
// 0x000000B6 System.Void ToolTip::Toggle(System.String)
extern void ToolTip_Toggle_m7BB7DD0FB5AA7DD002A5173E3391AF8457627F6C (void);
// 0x000000B7 System.Void ToolTip::.ctor()
extern void ToolTip__ctor_m24E99AD27DDCED6355927FB404EF7F35B72153F5 (void);
// 0x000000B8 WorldMap WorldMap::get_Instance()
extern void WorldMap_get_Instance_m119402AD39383BC71435F9F36C3219A2281289CA (void);
// 0x000000B9 System.Void WorldMap::set_Instance(WorldMap)
extern void WorldMap_set_Instance_mDDE558CAD26A7D4680B28F1C7FA3AB7F3A47A7B6 (void);
// 0x000000BA System.Void WorldMap::Awake()
extern void WorldMap_Awake_mA1651490B068B066B7F5D421CC73376A7206A70A (void);
// 0x000000BB System.Void WorldMap::Start()
extern void WorldMap_Start_mF99A53DAB2AB491270FADC92F6DEFADB377C17F7 (void);
// 0x000000BC System.Void WorldMap::Update()
extern void WorldMap_Update_m0E632030EF67DA098ABAE289E6E2181884AAF743 (void);
// 0x000000BD System.Void WorldMap::UpdateLegendSelection()
extern void WorldMap_UpdateLegendSelection_m96C6070EF674FED8B832E12C9ED1403EFAD2B33A (void);
// 0x000000BE System.Boolean WorldMap::AddInteraction(UnityEngine.GameObject)
extern void WorldMap_AddInteraction_mF2CC83A625FCFB963745D016D72BC262AB4A94F4 (void);
// 0x000000BF System.Void WorldMap::Activate()
extern void WorldMap_Activate_m270FF00B4D2A645DB02BFEE99F12E78D75054808 (void);
// 0x000000C0 System.Void WorldMap::Deactivate()
extern void WorldMap_Deactivate_m45F3C427C11E7AA6FC6C11B1346F94BA221F8641 (void);
// 0x000000C1 System.Void WorldMap::.ctor()
extern void WorldMap__ctor_mA2D5FF343986CEAC6DC4300F4120F84801BC47E5 (void);
// 0x000000C2 System.Void FiniteStateMachine.IState::OnEnter()
// 0x000000C3 System.Void FiniteStateMachine.IState::OnUpdate()
// 0x000000C4 System.Void FiniteStateMachine.IState::OnExit()
// 0x000000C5 FiniteStateMachine.IState FiniteStateMachine.StateMachine::get_CurrentState()
extern void StateMachine_get_CurrentState_mCDB0B1FC6472E599268D5E92F31C0D890613B326 (void);
// 0x000000C6 System.Void FiniteStateMachine.StateMachine::set_CurrentState(FiniteStateMachine.IState)
extern void StateMachine_set_CurrentState_m1B411CA93F64340414238167A0DF7B8DBD57B8C8 (void);
// 0x000000C7 System.Void FiniteStateMachine.StateMachine::.ctor()
extern void StateMachine__ctor_m7EFA2F5F3CA4100D082EE15109939ACAA8B16221 (void);
// 0x000000C8 System.Void FiniteStateMachine.StateMachine::.ctor(FiniteStateMachine.IState)
extern void StateMachine__ctor_m4362A8F7E5321C0B2F5D8E85462C1643F863E457 (void);
// 0x000000C9 System.Void FiniteStateMachine.StateMachine::SetState(FiniteStateMachine.IState)
extern void StateMachine_SetState_m7BA252E835DBB9FBFF89545B6634AFD0DEB659A0 (void);
// 0x000000CA System.Void FiniteStateMachine.StateMachine::OnUpdate()
extern void StateMachine_OnUpdate_m5C824B8C704BF7B7E99B53B7BC0A17D05EFE3DA6 (void);
// 0x000000CB StateType FiniteStateMachine.StateMachine::GetCurrentStateAsType()
static Il2CppMethodPointer s_methodPointers[203] = 
{
	NewBehaviourScript_Start_m783F84A617DADC4574B0BF1524481E6B96C65661,
	NewBehaviourScript_Update_m411C4D5C2D993FD70092FDA0FE2AC4786F8AC001,
	NewBehaviourScript__ctor_m437970EA37D66BDF32972F4CC0F65B95E5961FAA,
	ActionMenu_get_Instance_m0A358E0ED4326BDF16E867E22F1E5BF5B9AD2DC0,
	ActionMenu_set_Instance_m4D0D66C264341B224B923BEA333A7257A7B8E82F,
	ActionMenu_Awake_mF389F85308C8A469685278C9EB1A0BA881E714A2,
	ActionMenu_Start_mCC99B103AA695673F44496113AA07AA4F5A79AE8,
	ActionMenu_Update_mD53D9CBD0C575A7E145C71E92415F1E002222471,
	ActionMenu_UpdateUISelection_mA81DEC42B1B5D91EF6D7FDC89FC3C5BDBD2FC85F,
	ActionMenu_Activate_mD3CE0F110A26F537889E78930F7F78B0969FCC4D,
	ActionMenu_Deactivate_mFA0E81D3F5C0B0FC0267242D68B3B0BA21F36D85,
	ActionMenu__ctor_m94CC62F0105BC9ED7DE5851FDE69845E7291D8D5,
	Actions_get_Instance_mFCF21C897955B911E7D3B36BE438793642556B1C,
	Actions_set_Instance_m1F264080CD39D9EF3210325A20AEC5721E281845,
	Actions_Awake_m9F5BFB055871E0A1D7F18FDACB86E65C58524BED,
	Actions_Start_m7093D52055EE9FCF9145A5F06DC7D89609D29B0C,
	Actions_Update_mA16AB2E471F8E5369DF9018D706C24F192DD7924,
	Actions_Boost_m5CB5C10BC32C2DE52E94274EAEF5F486716E49E4,
	Actions_Sonar_m9974B6F2FE0C34C6FADC1792DD56F5013D247130,
	Actions_Map_m76DFCD66B1AA4F860F1956778B07ADE092E771D9,
	Actions__ctor_mA309EB283343FDDB8A1FE0D25D6EB563A75F8AFD,
	Fish_Inspect_mE6F49E168DC57D78B1CA0B6EE9F12AC1CBEC011F,
	Fish_Photograph_mCA93E8601A6B867BF98917CDD6806BF7B4F9B127,
	Fish__ctor_mEDE54A2E8EAB7C21444968366A1E9E12AC8D4A48,
	FlockAgent_Initialize_m8D1C04B1942FF5534A44DAC4526F9A8EF16CA1A6,
	FlockAgent_Move_mE006DB28962C8459F5677FCE8F3B9DA4302F4C99,
	FlockAgent_FindNeighbors_m084145F3ABC4617EA0A00724C318BEBD459B1BBE,
	FlockAgent_CalculateSpeed_m0CA1C3F5ACDD0DA9A4A121596281AA5AEC6646B1,
	FlockAgent_Cohese_mCBA014DB1CABC0C892246D2B8C48DC8B8CCC7D48,
	FlockAgent_Separate_mD2D03FDAC8EB6894C85FED42F19C5BD69AA1DBC2,
	FlockAgent_Align_m7890991F9B43B074A1420EB79A42EC79A7659EBB,
	FlockAgent_Bounds_m4300D54C6A0AF7BF4FB2B650EB878EA72A996CD1,
	FlockAgent_ObstacleAvoidance_m630F88E9A64AE156AA32C14C11538797BCB8BDDD,
	FlockAgent_AvoidObstacle_m9274B9DABEC71E0C1CFCABC33E63019C2BE14B6B,
	FlockAgent_VectorInView_m40DF4A6C365ADD655A97BB18413186B022867CE5,
	FlockAgent__ctor_m7901AE28864B436FF807D1A533D94F577A6FA977,
	FlockManager_get_SpawnedAgents_m050E5346A4490CAAD7EB7B9F8A7E6399405B4C7A,
	FlockManager_set_SpawnedAgents_m86395D548FB2FC61178CBDF82A5C5629A8716BB6,
	FlockManager_Start_m2CAA7577F414C6299641BA18F29B39E7DCB5EC37,
	FlockManager_Update_m9A5DF45FD8F6018A6BF35881B36217BD505B9717,
	FlockManager_SpawnAgents_m075D462688CF50D13A0F934C0989C1CFCE54B3B3,
	FlockManager_OnDrawGizmos_m33A47EA1DF5486A1A47B8869073F955F986B5529,
	FlockManager__ctor_m317ABCE65877D367E812F102E2D7D3A49FA28DFF,
	GameManager_get_Instance_m758C0D98BBDEF0C2DCA163A5D60B22DBB5B07232,
	GameManager_set_Instance_mAEF75ABE6135BDD61B9A3AED1606C0449787C5C7,
	GameManager_get_Target_m3ACB72AC913A603DF141C5D74A3F5C3B42D25B8D,
	GameManager_set_Target_mB22AB1128EBC08C3818777166428FF6DEEE341E6,
	GameManager_get_TargetFound_mD646125D22C8183B642D2ECD48C64FE59D1D12AE,
	GameManager_set_TargetFound_mA5DAB51223A80F7D092FF2967C48AC6EB5464CB2,
	GameManager_get_OrbitCameraTarget_m9B1FB59BEA2FEF5094B7EB93548C815087CA0E11,
	GameManager_set_OrbitCameraTarget_m30E1AB1FE69E612022E9ECBB93579479690F219B,
	GameManager_get_OrbitCameraTarget2_m8967752F71CD50D7C56AB9F21BC4D5576A1DF23B,
	GameManager_set_OrbitCameraTarget2_m82942D4D17D5A3922A44BD6398ED44370EC6F7D4,
	GameManager_get_PanToCameraTarget_m650916A4853E8626442E0A99A7CFEE2C7C168A8C,
	GameManager_set_PanToCameraTarget_mD06BE8F791C2C13FC8FC4DE7AA41E8D6C3FA773F,
	GameManager_get_CameraTarget_mBBC82927B5A0D320D5B38CA1B94C04FDB1EBC4F8,
	GameManager_set_CameraTarget_m40A31328DAA4F5876B52EDE84970FE6B7E686919,
	GameManager_Awake_m22F42B2A82708B10F652CAD8F2E0A4767110FF30,
	GameManager_Start_m26461AEF27E44DB8FECCBC19D6C9E228B658BF8E,
	GameManager_Update_mC9303BA7C3117BD861F49F8E36151CC52117E6C1,
	GameManager_SpawnZone_m095B9AA853D168B937A20973F44CE3D7D5B3B8CE,
	GameManager_TogglePlayer_m6D6635AAAE3C34217AAF3AE377EC5B97AE4CAA47,
	GameManager_PositionCinematicCamera_mA50236AF87D5EB57368C70AF4CFA8B1B243C8F16,
	GameManager_PlayCutScene_mEE157DD535C601E8C248C3286884E2622B9E5851,
	GameManager__ctor_mE8666F6D0CA9C31E16B719F79780DC4B0245B64D,
	U3CPlayCutSceneU3Ed__46__ctor_m4E4D899A2AEC184002EB6F6615F66D0C16DB3B84,
	U3CPlayCutSceneU3Ed__46_System_IDisposable_Dispose_mB18D4E5DB6D1BD3E1C843F02FE1E24B1BDE7D74E,
	U3CPlayCutSceneU3Ed__46_MoveNext_mB515DE3B1BDBD5CA0DE9351C37D45A10916D405D,
	U3CPlayCutSceneU3Ed__46_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF10EC54DA7CA333966D062D4013D540731306F66,
	U3CPlayCutSceneU3Ed__46_System_Collections_IEnumerator_Reset_m328A12CA0CF591B501C192E4A0942571129FBCC6,
	U3CPlayCutSceneU3Ed__46_System_Collections_IEnumerator_get_Current_mF952E38AE8940B317C0261CB53D49F19C7DC24EA,
	Gate_get_Locked_mD0F7FDCDBBF9A7B92DB3A64C563342F48D302F4B,
	Gate_set_Locked_mC4655A379C367E8C658ED505B433698BC32E7765,
	Gate_Awake_m9E870AA2F62C390A3CA0F5AB8E75B744519FF1FC,
	Gate_Start_m27BE6AC1FF73604405C96D634D19AF13DD9A1234,
	Gate_Update_mBA10874B7CBA45BC8F8BAFB54DD6DAE131DC3A85,
	Gate_Inspect_m60870B3FA73AADCC9875C320E9D12E3A13411E03,
	Gate_Unlock_m4D20EBE5FC66340D63BB8D9157A0C09DC6CDC5F4,
	Gate__ctor_mEACCC67CFE6FC196994848EE1207B13283FB518E,
	Interaction_get_Instance_m49A31B95D41F5321AA7036861F8330D6BB09A940,
	Interaction_set_Instance_m1C2D19F892D930D83F98A19E2D65EF47701A7C51,
	Interaction_Awake_m07EB608CF485E6B7DE48D22C0FF5DF5558C9BE45,
	Interaction_Update_m19CE4CF0CFFA37FF15C650076DB2F1CF3F677899,
	Interaction_Addkey_mCC3293ED3A6334A742C2196DF288E220BF1073A5,
	Interaction_HasKey_m0E72EF86A1EF62EB1478BD83D9D20B904CBE76A1,
	Interaction__ctor_m70406E11A94FE15ADD18C51085F78B5463FACEB8,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	InteractionMenu_get_Instance_m60C03978FEDDC7D708EB1C5C30EBD081344385FF,
	InteractionMenu_set_Instance_m64EBB469675FBC80A34E3E4457B021DE07FED4EA,
	InteractionMenu_Awake_mDD193A70CB13CF5429EFF02481B45135665A2889,
	InteractionMenu_UpdateUISelection_mCDE9D19A0556DB8B15CBC355E369A362C18D2A2D,
	InteractionMenu_Start_mCFC55383AE7C50EEEBA53097C75A503474091A25,
	InteractionMenu_Update_m89548D247EF54D378478EBF006756F060DD3F368,
	InteractionMenu_Photograph_mDF8C7A662AA42E89A39027BBAB69C54692736571,
	NULL,
	InteractionMenu_Activate_m45B84D655B2FCBC3A0429DDEB4F1B8393A397160,
	InteractionMenu_Deactivate_m912B95CC8E501BE8859EB5BB477A9D0A5DCFE10A,
	InteractionMenu__ctor_m7D2580EE5A91D03AC0C8191AACF351421AD3C3EC,
	Key_Start_m32134F86EACDABA7FFF8FB47E9DA60B2D7F71B4A,
	Key_Pickup_m2A0EA27BF1A8B8D1EFA82F77FD5829B902E8F026,
	Key_Inspect_m1443EDF14E5EFD3C262BBFFF0A08D28DF9191365,
	Key__ctor_mDED000F0CC8C37C5B2A6916661E52F7735A49725,
	MainMenu_Awake_mA8CB83E7D49CE72C6D0DE5D7D6313F8305A1A4FE,
	MainMenu_Start_m3B552CE289B1D7E5343961C8461C484EA61DB621,
	MainMenu_LoadGame_m75C2A4F48DA3B9EB7A2ADF6872C66FC286639600,
	MainMenu_StartGame_m8EFE49FE95784A266A683D0CCAAE7C2274981049,
	MainMenu_Info_m9B449123D7739667B9D5569898441C2A89400461,
	MainMenu_Gallery_m3FC2EDFC3E9D08BBD78F49732962217470E974B2,
	MainMenu_SelectGalleryImage_mBE00416F61B0A0B8B5648DE827A479FFE9CC8B69,
	MainMenu_QuitToDesktop_m4BA7361022A9F78B96A3EE6C4B117CECF2D53D31,
	MainMenu__ctor_m4D77CEC8F91682A2D9492AE815F89B178BF9717D,
	U3CLoadGameU3Ed__7__ctor_m57695A7D16F59DB4E5587AE9E06B571D1940260F,
	U3CLoadGameU3Ed__7_System_IDisposable_Dispose_mCA4CA8352FE4514D8F778377A7D22B29D22AE734,
	U3CLoadGameU3Ed__7_MoveNext_mE59F0C0AE491D8965AE5B3F02E8C32070F32015D,
	U3CLoadGameU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3186843C05568405BEBF7CDB5C0E01AFBB1ACF99,
	U3CLoadGameU3Ed__7_System_Collections_IEnumerator_Reset_mEAC358DD13F122444F8E3E2908F91C48BFE5DC9A,
	U3CLoadGameU3Ed__7_System_Collections_IEnumerator_get_Current_m23B975A7A51A041008F94718C1C759C1670C098A,
	MouseLook_get_Instance_m4F889A9C207EAE72011AF1BA9DA70046C5404386,
	MouseLook_set_Instance_mA80F2A4EF6E41B80058108D5AFF17C03E4F6251D,
	MouseLook_get_LookEnabled_m872F548C1ADDCAE36BCFFC9743F4F6E240A703C5,
	MouseLook_set_LookEnabled_mE39640BB02C6774A01AE2C1E80613702480B2C99,
	MouseLook_set_CursorToggle_m2DBA5589D35ED1938A52174EC990627601603468,
	MouseLook_Awake_m10C7098FFAA05035E7FB675ECCC5EF1FD79B8B95,
	MouseLook_Update_m3D49361C94E0433BB35499708EE783B4543D83D5,
	MouseLook__ctor_mD12D8075DDEA2085341B59FF8BA9FB353613200B,
	PlayerController_get_Instance_m8C373614456176CE37B49E24BA9B7E019C5A0440,
	PlayerController_set_Instance_m1DBDE7741593D098536751F470647F587A7A0D0A,
	PlayerController_Awake_m23059564DCFDD324EA9DB966473251896647CD23,
	PlayerController_Update_mB31159CAD7DD2329859472554BC9154A83D8E794,
	PlayerController_MoveToPosition_m9E1B5DA225A7BE1C6E859C6294A44281E5480876,
	PlayerController__ctor_mF30385729DAFDFCB895C4939F6051DCE6C0327FB,
	Ship_get_StateMachine_m9110695EE50E4C7260E64164574C5394CFF403C1,
	Ship_set_StateMachine_mEB49B2626B6B51769D40F7CC9FC3FA2D59961F13,
	Ship_get_Agent_m2733EC0E5F1D092B7FB03B5BA882B9FAD8C4CF96,
	Ship_set_Agent_m99D21CDD3A46A0B76541A0FBC9B8DA8BA9858A8D,
	Ship_Awake_m956DC332783F1DAA6F7F13588816260AD621C9A2,
	Ship_Start_m809FEE5ACEBDF1EC3E7A57D31FF5D6D994E0F4BF,
	Ship_Update_mC04E9DB38367AC83C98CCF1C377D7C442FAE2A7E,
	Ship_Inspect_mCB84D7CA931A5BFB47F385CFF0C8EFD4284BCFFF,
	Ship_Leave_m126EADEB1988B84E8ED2F86F7E20A0373C5AF64B,
	Ship_OnDrawGizmos_m9D86874A855361682803C23AB3ED843A9EAB0D49,
	Ship__ctor_mA4FC970EB2F615E5B00A86D2B89DCBAC88C9CC1E,
	ShipState_get_Instance_m11696AE1492C8B96116FF2687CE13EA7B45428A0,
	ShipState_set_Instance_m37F9D75A65E59E0500535234B7F46301E108BFCF,
	ShipState__ctor_mEA5C98A1E34AE5F3ADF5D3940A212D61561FBD48,
	ShipState_OnEnter_mD09267B90F3E7D321C6F73DDD37D625DB1EC3B44,
	ShipState_OnExit_mD10B24E4176AF5474BEACBCB48E50368FDE8264D,
	ShipState_OnUpdate_mB9D658F5434ABD6F3AA4770908C5F361B3627D03,
	ShipState_DrawGizmos_m673B6C785E859D1C49481C9183CA27910A3DF0F7,
	ShipWait__ctor_mC7C641CCA9F94A3C7D6BBF10F32B8F2B77496FA5,
	ShipWait_OnEnter_m9E6683753E750BF44EEB10B25083DF5E6D00BDF9,
	ShipWait_OnUpdate_mC4EC33BF6D9C15D7FD6F5B23D178774D2960C781,
	ShipWait_OnExit_m253088D6C12BD0C78612108AC96BFDB0EDD682E9,
	ShipMove__ctor_mB007B621D62FBD63A71C4B9A679A3E2707A8C639,
	ShipMove_OnEnter_mAF5CB80592877AC4484E83F9027248012294BF6D,
	ShipMove_OnUpdate_mCD2173519CDBE3734589F653C812EE130F678485,
	ShipMove_DrawGizmos_m798CCF44823A7E5864E8948948B2ECBAE0E57807,
	ShipMove_GetRandomPosInBounds_mF73CF65739E672B709F7304EE16A896E94E1F7E4,
	SoloFish_Update_mF84BAC95F9ECBA23F0C5E7B257508B31D1425450,
	SoloFish_OnDrawGizmos_mFE0A5C49D7FA515541574BAD7297DD258BCFFD71,
	SoloFish_GetRandomPosInBounds_m8622D90A0BB64C591B451299165E119524C6B3E1,
	SoloFish_MoveStep_m75074289C1B548F4A42BFC3A0E377F3B58031E3E,
	SoloFish_ObstacleAvoidance_m5D27E74C36F7F0C727F4AEA88E249EF022CD779C,
	SoloFish_Bounds_m456DD94835D1E087A1A45D1258B1496EBB7778A7,
	SoloFish__ctor_m9EABDF66EC0910BB39AF54AB993A344F63775020,
	TempInteraction_Start_mEF47E0A950D8B4009BA62B8D29562A46B409BD87,
	TempInteraction_Inspect_mD82B8EC83A055FA4946EF96D2C191736298520E3,
	TempInteraction_Leave_mB34E7DA19EA52B8D6922633E0BA000ABABF47032,
	TempInteraction__ctor_m25A903DAB88BDA6319528D907AD373A55B31284D,
	TestFIsh__ctor_m28D4EF296E3A469E92FC6EBCFF88E2399CE288D6,
	TestFIsh_Start_mB4AF08A9F80D5012BFDAFF6E861AD6439B2F1484,
	TestFIsh_Update_mE8C86846C90A65D720C8DFAC85D3594A4A08BE17,
	ToolTip_get_Instance_m5CB773BB76F41361836DE8051081FD5CDF562969,
	ToolTip_set_Instance_mD51AE6A1572D2CFFED1C930D088CB363C31B30D2,
	ToolTip_Awake_mDE594ACEB5DD7B0AF6922A947B8112E08CD22AD4,
	ToolTip_Start_mEA60282A4DDCE048FBF93A97ADBE152A7FC6B172,
	ToolTip_Update_m17C942983DD1AB8DD7153F31DD8125CBD2CF4C00,
	ToolTip_Toggle_m7BB7DD0FB5AA7DD002A5173E3391AF8457627F6C,
	ToolTip__ctor_m24E99AD27DDCED6355927FB404EF7F35B72153F5,
	WorldMap_get_Instance_m119402AD39383BC71435F9F36C3219A2281289CA,
	WorldMap_set_Instance_mDDE558CAD26A7D4680B28F1C7FA3AB7F3A47A7B6,
	WorldMap_Awake_mA1651490B068B066B7F5D421CC73376A7206A70A,
	WorldMap_Start_mF99A53DAB2AB491270FADC92F6DEFADB377C17F7,
	WorldMap_Update_m0E632030EF67DA098ABAE289E6E2181884AAF743,
	WorldMap_UpdateLegendSelection_m96C6070EF674FED8B832E12C9ED1403EFAD2B33A,
	WorldMap_AddInteraction_mF2CC83A625FCFB963745D016D72BC262AB4A94F4,
	WorldMap_Activate_m270FF00B4D2A645DB02BFEE99F12E78D75054808,
	WorldMap_Deactivate_m45F3C427C11E7AA6FC6C11B1346F94BA221F8641,
	WorldMap__ctor_mA2D5FF343986CEAC6DC4300F4120F84801BC47E5,
	NULL,
	NULL,
	NULL,
	StateMachine_get_CurrentState_mCDB0B1FC6472E599268D5E92F31C0D890613B326,
	StateMachine_set_CurrentState_m1B411CA93F64340414238167A0DF7B8DBD57B8C8,
	StateMachine__ctor_m7EFA2F5F3CA4100D082EE15109939ACAA8B16221,
	StateMachine__ctor_m4362A8F7E5321C0B2F5D8E85462C1643F863E457,
	StateMachine_SetState_m7BA252E835DBB9FBFF89545B6634AFD0DEB659A0,
	StateMachine_OnUpdate_m5C824B8C704BF7B7E99B53B7BC0A17D05EFE3DA6,
	NULL,
};
static const int32_t s_InvokerIndices[203] = 
{
	1508,
	1508,
	1508,
	2521,
	2493,
	1508,
	1508,
	1508,
	1508,
	1508,
	1457,
	1508,
	2521,
	2493,
	1508,
	1508,
	1508,
	1301,
	1301,
	1301,
	1508,
	1470,
	1282,
	1508,
	844,
	1508,
	1508,
	1508,
	1506,
	1506,
	1506,
	1506,
	1506,
	1506,
	1178,
	1508,
	1470,
	1282,
	1508,
	1508,
	1508,
	1508,
	1508,
	2521,
	2493,
	2521,
	2493,
	1495,
	1301,
	1495,
	1301,
	1495,
	1301,
	1495,
	1301,
	1470,
	1282,
	1508,
	1508,
	1508,
	1282,
	1301,
	1282,
	1037,
	1508,
	1270,
	1508,
	1495,
	1470,
	1508,
	1470,
	1495,
	1301,
	1508,
	1508,
	1508,
	1470,
	1495,
	1508,
	2521,
	2493,
	1508,
	1508,
	1132,
	1132,
	1508,
	1470,
	1282,
	1508,
	1495,
	1508,
	2521,
	2493,
	1508,
	1508,
	1508,
	1508,
	1282,
	-1,
	1282,
	1508,
	1508,
	1508,
	1508,
	1470,
	1508,
	1508,
	1508,
	1470,
	1508,
	1508,
	1508,
	1508,
	1508,
	1508,
	1270,
	1508,
	1495,
	1470,
	1508,
	1470,
	2521,
	2493,
	1495,
	1301,
	1301,
	1508,
	1508,
	1508,
	2521,
	2493,
	1508,
	1508,
	1313,
	1508,
	1470,
	1282,
	1470,
	1282,
	1508,
	1508,
	1508,
	1470,
	1508,
	1508,
	1508,
	1470,
	1282,
	1282,
	1508,
	1508,
	1508,
	1508,
	1282,
	1508,
	1508,
	1508,
	1282,
	1508,
	1508,
	1508,
	1506,
	1508,
	1508,
	1506,
	1508,
	1506,
	1506,
	1508,
	1508,
	1470,
	1508,
	1508,
	1508,
	1508,
	1508,
	2521,
	2493,
	1508,
	1508,
	1508,
	1282,
	1508,
	2521,
	2493,
	1508,
	1508,
	1508,
	1508,
	1146,
	1508,
	1508,
	1508,
	1508,
	1508,
	1508,
	1470,
	1282,
	1508,
	1282,
	1282,
	1508,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[2] = 
{
	{ 0x06000063, { 0, 1 } },
	{ 0x060000CB, { 1, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[2] = 
{
	{ (Il2CppRGCTXDataType)2, 102 },
	{ (Il2CppRGCTXDataType)2, 182 },
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	203,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	2,
	s_rgctxIndices,
	2,
	s_rgctxValues,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
