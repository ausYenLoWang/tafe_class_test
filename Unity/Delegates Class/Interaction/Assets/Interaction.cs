using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interaction : MonoBehaviour
{
    public float distance = 2f; 
    
    public static Interaction Instance { get; private set; }

   private void Awake()
    {
        Instance = this;
    }

    void Update()
    {
        //if the interaction menu is not active
        if(InteractionMenu.Instance.gameObject.activeSelf == false) 
        {
            if (Input.GetButtonDown("Interact") == true)  //if the interaction input is pressed and no menus open
            {
                //if ray cast hits a collider
                if (Physics.SphereCast(transform.position, 1, transform.forward, out RaycastHit hit, distance) == true)
                {
                    //check if collider has interaction component
                    if (hit.collider.TryGetComponent(out IInteraction interaction) == true)
                    {                   //activate the interaction
                        InteractionMenu.Instance.Activate(interaction);
                                        //draw green debug line
                        Debug.DrawRay(transform.position, transform.forward * distance, Color.green, 0.1f);
                    }
                    else
                    {            //else draw yellow debug line
                        Debug.DrawRay(transform.position, transform.forward * distance, Color.yellow, 0.1f);
                    }
                }
                else
                {
                    //else draw redline
                    Debug.DrawRay(transform.position, transform.forward * distance, Color.red, 0.1f);
                }
            }
        }
        else
        {
            //if interact button is released
            if (Input.GetButtonUp("Interact") == true)
            {
                //deactivate interaction menu
                InteractionMenu.Instance.Deactivate();
            }
        }




    }
}

public interface IInteraction 
{
    void Inspect();
}

public interface IFauna : IInteraction
{
    void Photograph(string folderPath = null);
}

public interface IKey : IInteraction
{
    void Pickup();
}

public interface IKeyLock : IInteraction
{
    bool Unlock();
}

