﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonInteraction : MonoBehaviour, IInteraction
{

    private Animator anim;

    private void Awake()
    {
        anim = GetComponent<Animator>();
    }
    public void Activate()
    {
        anim.SetTrigger("pressButton");
    }

    
}
