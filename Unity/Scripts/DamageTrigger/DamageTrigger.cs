using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageTrigger : MonoBehaviour
{
    public float damage = 15f;

    private void Awake()
    {
        //DONT EVER USE FINDOBJECTOFTYPE !!!
        FindObjectOfType<Health>().damageDelegate += Foo;
    }

    private void Foo()
    {
        Debug.Log("Bar");
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            if (other.TryGetComponent(out Health health) == true)
            {
                if(health.OnDamage(damage) == true)
                {
                    Debug.Log(health.CurrentHealth);
                }
                else
                {
                    Debug.Log("Damage UnSuccessful");

                }
            }
        }
    }

}
