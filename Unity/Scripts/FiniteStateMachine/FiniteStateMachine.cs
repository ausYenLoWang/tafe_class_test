using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]  //will add a navmesh component to the object if its not already there

public class FiniteStateMachine : MonoBehaviour
{
    public IdleState idleState;
    public ChaseState chaseState;
    public float lookRadius = 10f;

    public Transform Target { get; private set; }

    private BehaviourState currentState;

    public NavMeshAgent Agent { get; private set; }

    public Transform CheckForPLayer()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, lookRadius);
        foreach (Collider c in colliders)
        {
            if (c.tag == "Player")
            {
                return c.transform;
            }
        }
        return null;
    }


    private void Awake()
    {
        //setting the table
        Agent = GetComponent<NavMeshAgent>();
        idleState = new IdleState(this);
        chaseState = new ChaseState(this);
    }
    private void Start()
    {
        //getting ready to eat
        SetState(idleState);
    }


    public void SetState(BehaviourState newState)
    {
        if(currentState != null) //this is checking to see if state is already running
        {
            currentState.OnStateExit(); //if a state is currently running exit the state
        }
        currentState = newState;  //set the current state to the new state
        currentState.OnStateEnter();  //Call the enter function
    }

    private void Update()
    {
        Target = CheckForPLayer();
        //eating the meal

        if(currentState != null)
        {
            currentState.OnStateUpdate();

        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;
        Gizmos.DrawWireSphere(transform.position, lookRadius);
    }


}

public abstract class BehaviourState

{

    protected FiniteStateMachine Instance { get; private set; }
    public BehaviourState(FiniteStateMachine instance) 
    {
        Instance = instance;
    }


    public virtual void OnStateEnter() { }
    public virtual void OnStateUpdate() { }
    public virtual void OnStateExit() { }
}

public class IdleState : BehaviourState
{
    //Constructer for TestStateA
    //Passes FSM instance to the base class
    public IdleState(FiniteStateMachine instance) : base(instance)
    {

    }

    public override void OnStateEnter()
    {
        Instance.Agent.isStopped = true;
    }
    public override void OnStateUpdate()
    {

        if(Instance.Target != null)
        {
            Instance.SetState(Instance.chaseState);
        }
       
    }

}

public class ChaseState : BehaviourState
{
    private float distance;


    //Constructer for TestStateB
    //Passes FSM instance to the base class
    public ChaseState(FiniteStateMachine instance) : base(instance)
    {
        
    }

    public override void OnStateEnter()
    {
        if(Instance.Target == null)
        {
            Instance.SetState(Instance.idleState);
        }
        Instance.Agent.isStopped = false;
    }
    public override void OnStateUpdate()
    {
        if (Instance.Target != null)
        {


            //set distance to distance AI to Target
            distance = Vector3.Distance(Instance.transform.position, Instance.Target.position);
            //Check to see if distance is less than agents stopping distance
            if (distance <= Instance.Agent.stoppingDistance)
            {
                if (Instance.Agent.isStopped == false)
                {
                    Instance.Agent.isStopped = true;
                }
            }
            else  //else if distance is greater than agent stopping distance
            {
                if (Instance.Agent.isStopped == true)  //if stopepd unstop agent
                {
                    Instance.Agent.isStopped = false;
                }
                else  //else if not stopped continue moving towards target
                {
                    Vector3 targetPos = Instance.Target.position;  // create a target position vector
                    targetPos.y = Instance.transform.position.y; //ignore the targets Y value
                    Instance.Agent.SetDestination(targetPos);  //Move towards the target position
                }
            }


        }
        else
        {
            Instance.SetState(Instance.idleState);
        }

    }

}

