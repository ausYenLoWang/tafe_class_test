using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interaction : MonoBehaviour
{
    public float distance = 2;

    //keyring variable

    public static Interaction Instance { get; private set; }

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            enabled = false;
        }

    }

    void Update()
    {
        if (InteractionMenu.Instance.gameObject.activeSelf == false)
        {
            //if action menu and map are not open then execute the following
            if (Input.GetButtonDown("Interaction") == true)
            {
                if (Physics.SphereCast(transform.position, 1, transform.forward, out RaycastHit hit, distance) == true)
                {
                    // check hit for interaction type
                    if (hit.collider.TryGetComponent(out IInteraction interaction) == true)
                    {
                        Debug.DrawRay(transform.position, transform.forward * distance, Color.green, 0.25f);
                        InteractionMenu.Instance.Activate(interaction);
                    }

                }

            }
        }
        else
        {
            if(Input.GetButtonUp("Interaction") == true)
            {
                InteractionMenu.Instance.Deactivate();
            }
        }
       
    }
}

public interface IInteraction
{
    string Inspect();
}

public interface IFauna : IInteraction
{
    void Photograph(string folderPath = null);
}

public interface IKey : IInteraction
{
    void Pickup();
}

public interface IGate : IInteraction
{
    bool Unlock();
}

public interface IShip : IInteraction
{
    void Leave();
}