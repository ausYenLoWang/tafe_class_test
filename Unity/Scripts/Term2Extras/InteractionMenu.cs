using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class InteractionMenu : MonoBehaviour
{
    public Image optionA;
    public Image optionB;
    public Text optionBText;

    private IInteraction currentInteraction;
    private int optionIndex = -1;
    private float timer = -1;

    public static InteractionMenu Instance { get; private set; }

    private void Awake()
    {
        //singleton initialization
        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            enabled = false;
        }
        //photo gallery folder construction
        if (Directory.Exists(Application.dataPath + "/Resources/PhotoGallery") == false)
        {
            Directory.CreateDirectory(Application.dataPath + "/Resources/PhotoGallery");
        }
    }

   void UpdateUISelection()
    {
        switch (optionIndex)
        {
            case 0:
                optionA.color = Color.white;
                optionB.color = Color.grey;
                break;
            case 1:
                optionA.color = Color.grey;
                optionB.color = Color.white;
                break;
        }
    }


    // Start is called before the first frame update
    void Start()
    {
        Deactivate();
    }

    // Update is called once per frame
    void Update()
    {
       if(gameObject.activeSelf == true && timer == -1)
        {
            int axis = Mathf.FloorToInt(Input.GetAxis("Menu Selection"));
            if(axis != 0)
            {
                timer = 0;
                optionIndex += axis;
                if(optionIndex < 0)
                {
                    optionIndex = 1;
                }
                else if(optionIndex > 1)
                {
                    optionIndex = 0;
                }
                UpdateUISelection();
            }
        } 
       if(timer > - 1)
        {
            timer += Time.deltaTime;
            if(timer > 0.25f)
            {
                timer = -1f;
            }
        }
    }

    void Photograph(IFauna fauna)
    {
        string gallery = Application.dataPath + "/Resources/PhotoGallery";
        DirectoryInfo dir = new DirectoryInfo(gallery);
        FileInfo[] files = dir.GetFiles("*.png");
        int count = 0;
        foreach (FileInfo info in files)
        {
            if(info.Name.Contains("UWW_") == true)
            {
                count++;
            }
        }

        if(count < 30)
        {
            fauna.Photograph(gallery);
        }
        else
        {
            fauna.Photograph();
        }

    }

    bool GetInteractionAsType<InteractionType>(out InteractionType target) where InteractionType : IInteraction
    {
        if(currentInteraction is InteractionType interaction)
        {
            target = interaction;
            return true;
        }
        target = default;
        return false;
    }

    public void Activate(IInteraction interaction)
    {
        currentInteraction = interaction;
        if (GetInteractionAsType(out IFauna fauna) == true)
        {
            optionBText.text = "Photograph";
            optionBText.color = Color.black;

        }
        else if (GetInteractionAsType(out IKey key) == true)
        {
            optionBText.text = "Pick Up";
            optionBText.color = Color.black;
        }
        else if (GetInteractionAsType(out IGate gate) == true)
        {
            optionBText.text = "Unlock";
            optionBText.color = Color.black;
        }
        else if (GetInteractionAsType(out IShip ship) == true)
        {
            optionBText.text = "Leave";
            optionBText.color = Color.black;
        }
        optionIndex = 0;
        UpdateUISelection();
        gameObject.SetActive(true);
    }

    public void Deactivate()
    {
        if(optionIndex == 0)
        {
            //pass current interactiont to tooltip
        }
        else if(optionIndex == 1)
        {
            if (GetInteractionAsType(out IFauna fauna) == true)
            {
                Photograph(fauna);
            }
            else if (GetInteractionAsType(out IKey key) == true)
            {
                key.Pickup();
            }
            else if(GetInteractionAsType(out IGate gate) == true)
            {
                gate.Unlock();
            }
            else if(GetInteractionAsType(out IShip ship) == true)
            {
                ship.Leave();
            }
        }
        gameObject.SetActive(false);
        optionIndex = -1;
        currentInteraction = null;
    }
}
